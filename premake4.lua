
--
-- TODO This premake is hardcoded for Mac OSX.
-- Shouldn't take much work converting it to Win/Linux.
-- The painful part would be fixing the external dependencies...
--

--
-- Debug switches worth enabling:
--
-- _SECURE_SCL    : for Visual Studio, to enable/disable STL bounds checking.
-- _GLIBCXX_DEBUG : for GCC only! Enables several debugging facilities.
-- _LIBCPP_DEBUG  : for Clang (libc++). See: http://stackoverflow.com/a/21847033/1198654
--                  It does not work with std::array<> however! (doesn't seem to work on std::deque either!)
--                  _LIBCPP_DEBUG2 Seems to have no effect, but leaving it there won't harm...
--

-- Specify compiler (for gmake):
premake.gcc.cc  = "clang";
premake.gcc.cxx = "clang++";

--------------------------------------------------------
-- Common configurations for all projects:
--------------------------------------------------------
solution("space-sim");
	configurations("Debug", "Release");
	location("build");
	targetdir("build");
	defines({
		"APP_NAME_TITLE='\"Space Ship Game\"'",

		-- Engine debug:
		"GL_DO_ERROR_CHECKING=1",
		"GL_ERRORS_ARE_FATAL=0",
		"ENGINE_DEBUG_BUILD=1",
		"ENGINE_BUILD_UNIT_TESTS=1",
		"_VECTORMATH_DEBUG=1",

		-- Lua:
		"LUA_DEBUG=1",
		"LUA_USE_APICHECK=1",
		"LUA_COMPAT_ALL=1",

		-- Other debug switches:
		"_GLIBCXX_DEBUG",
		"_LIBCPP_DEBUG=0",
		"_LIBCPP_DEBUG2=0",
		"_SECURE_SCL"
	});

--[[
--------------------------------------------------------
-- Lua Static Library:
--------------------------------------------------------
project("LuaLib");
	language("C");
	kind("StaticLib");
	location("build");
	configuration("macosx", "linux", "gmake"); --Debug/Release
		buildoptions({
			"-Wall",
			"-Wextra",
			"-Winit-self",
			"-Wuninitialized",
			"-Wswitch-default",
			"-Wpointer-arith",
			"-Wwrite-strings",
			"-g" -- g is for debug!
		});
	includedirs("../space-sim/source/ThirdParty/lualib/include");
	files({
		"source/ThirdParty/lualib/include/*.h",
		"source/ThirdParty/lualib/src/*.c"
	});
]]

--------------------------------------------------------
-- Engine Static Library:
--------------------------------------------------------
project("EngineLib");
	language("C++");
	kind("StaticLib");
	location("build");
	configuration("macosx", "linux", "gmake"); --Debug/Release
		linkoptions({ "-all_load" }); -- Need this to load all the static RTTS stuff!
		buildoptions({
			"-Wall",
			"-Wextra",
			"-Weffc++",
			"-Winit-self",
			"-Wuninitialized",
			"-Wswitch-default",
			"-Wpointer-arith",
			"-Wwrite-strings",
			"-std=c++11",
			"-stdlib=libc++",
			"-g" -- g is for debug!
		});
	includedirs("../space-sim/source/");
	files({
		"source/Engine/Application/*.hpp",
		"source/Engine/Application/*.inl",
		"source/Engine/Application/*.cpp",

		"source/Engine/Core/*.hpp",
		"source/Engine/Core/*.inl",
		"source/Engine/Core/*.cpp",

		"source/Engine/Core/Math/*.hpp",
		"source/Engine/Core/Math/*.inl",
		"source/Engine/Core/Math/*.cpp",

		"source/Engine/Core/Geometry/*.hpp",
		"source/Engine/Core/Geometry/*.inl",
		"source/Engine/Core/Geometry/*.cpp",

		"source/Engine/EntityComponent/*.hpp",
		"source/Engine/EntityComponent/*.inl",
		"source/Engine/EntityComponent/*.cpp",

		"source/Engine/Input/*.hpp",
		"source/Engine/Input/*.inl",
		"source/Engine/Input/*.cpp",

		"source/Engine/Networking/*.hpp",
		"source/Engine/Networking/*.inl",
		"source/Engine/Networking/*.cpp",

		"source/Engine/Rendering/*.hpp",
		"source/Engine/Rendering/*.inl",
		"source/Engine/Rendering/*.cpp",

		"source/Engine/Resources/*.hpp",
		"source/Engine/Resources/*.inl",
		"source/Engine/Resources/*.cpp",

		"source/Engine/Scripting/*.hpp",
		"source/Engine/Scripting/*.inl",
		"source/Engine/Scripting/*.cpp"
	});

--------------------------------------------------------
-- Game Executable:
--------------------------------------------------------

project("Game");
	language("C++");
	kind("ConsoleApp");
	location("build");
	configuration("macosx", "linux", "gmake"); --Debug/Release
		linkoptions({ "-all_load" }); -- Need this to load all the static RTTS stuff!
		buildoptions({
			"-Wall",
			"-Wextra",
			"-Weffc++",
			"-Winit-self",
			"-Wuninitialized",
			"-Wswitch-default",
			"-Wpointer-arith",
			"-Wwrite-strings",
			"-std=c++11",
			"-stdlib=libc++",
			"-g" -- g is for debug!
		});
	includedirs({
		"../space-sim/source/",
		-- bullet:
		"../space-sim/source/ThirdParty/bullet3/src/",
		"../space-sim/source/ThirdParty/bullet3/src/Bullet3Collision/",
		"../space-sim/source/ThirdParty/bullet3/src/Bullet3Common/",
		"../space-sim/source/ThirdParty/bullet3/src/Bullet3Dynamics/",
		"../space-sim/source/ThirdParty/bullet3/src/Bullet3Geometry/",
		"../space-sim/source/ThirdParty/bullet3/src/LinearMath/"
	});
	files({
		"source/Game/EntityComponent/*.hpp",
		"source/Game/EntityComponent/*.inl",
		"source/Game/EntityComponent/*.cpp",

		"source/Game/*.hpp",
		"source/Game/*.inl",
		"source/Game/*.cpp"
	});
	links({
		"EngineLib",
		"LuaLib",
		"glew",
		"glfw3",
		"assimp",
		"z", -- libz (z) is required by ASSIMP

		-- Mac OS specific stuff must be handled separately!
		"OpenGL.framework",
		"Cocoa.framework",
		"IOKit.framework",
		"CoreFoundation.framework",
		"CoreServices.framework",
		"QuartzCore.framework",

		-- Bullet Physics (not sure if all these are needed...):
		"Bullet3Collision",
		"Bullet3Common",
		"Bullet3Dynamics",
		"Bullet3Geometry",
		"Bullet2FileLoader",
		"BulletLinearMath",
		"BulletCollision",
		"BulletDynamics",
		"BulletLinearMath",
		"BulletSoftBody"
	});

