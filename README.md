
# SCS - Space Combat Sim
----

## About:

This game is a hobby project where I attempted to create a Space-Sim-like game, inspired by the old-time classics like
[*Freelancer*](http://en.wikipedia.org/wiki/Freelancer_%28video_game%29),
[*Wing Commander*](http://en.wikipedia.org/wiki/Wing_Commander_%28video_game%29) and
[*TIE Fighter*](http://www.gog.com/game/star_wars_tie_fighter_special_edition) series.

## Technical details:

The main point of focus on this project was in the "game engine"-side. I've implemented
an OpenGL 3+ renderer from scratch for this game. Some other interesting things in the
engine include:

- Lua configuration scripts (not used very much by the game at the moment).
- Support for the XBOX 360 Controller.
- Basic portable networking support.
- XML Serialization.
- A somewhat hackish macro-based Runtime Type System (RTTI).
- Real-time video capture of the game to uncompressed AVI video.
- Basic Resource/Assets Management system.
- Split-screen gameplay for two local players.
- Runs on Mac OSX (the only platform officially supported at the moment!).

The game was only tested and built on Mac OSX. I never got around writing a build script for Windows
nor fixing a few in-code platform specific things. Porting the code to Windows should be very easy,
though fixing the external dependencies might be a bit of a pain.

## Directory Structure:

    +-space-sim/
     |
     +-resources/   => Game assets. Most of them are not currently used by the prototype game.
     |
     +-screens/     => A few in-game screenshots for the eye candy.
     |
     +-source/      => Main source code path for the whole project.
      |
      +-Engine/     => The game engine, well divided into a couple modules.
      |
      +-Game/       => Hackish code for a prototype of the game. Mostly throw-away and testing code.
      |
      +-ThirdParty/ => Third party libraries and dependencies, such as Lua, Bullet-Physics, GLFW, etc...

## License:

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

## In-game screenshots:

![Rogue Base](https://bytebucket.org/glampert/space-sim/raw/4250ed440a923b3914cc0c67b8a4907f2e209c0a/screens/scs1.png "Rogue Base")

![Asteroid Field](https://bytebucket.org/glampert/space-sim/raw/4250ed440a923b3914cc0c67b8a4907f2e209c0a/screens/scs3.png "Phobos Asteroid Field")

![Jupiter Orbit](https://bytebucket.org/glampert/space-sim/raw/4250ed440a923b3914cc0c67b8a4907f2e209c0a/screens/scs10.png "Jupiter's Orbit")

