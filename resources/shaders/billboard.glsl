
// ================================================================================================
// -*- GLSL -*-
// File: billboard.glsl
// Author: Guilherme R. Lampert
// Created on: 02/09/14
// Brief: 3D billboard rendering.
// ================================================================================================

// ======================================================
// Vertex Shader:
// ======================================================
@vertex_shader

// Vertex inputs, in model space:
layout(location = 0) in vec3 a_position;
layout(location = 1) in vec4 a_color;
layout(location = 2) in vec2 a_texCoords;

// Vertex shader outputs:
layout(location = 0) out vec4 v_color;
layout(location = 1) out vec2 v_texCoords;

// Uniform variables:
uniform mat4 u_MVPMatrix;

void main()
{
	gl_Position = u_MVPMatrix * vec4(a_position, 1.0);
	v_texCoords = a_texCoords;
	v_color     = a_color;
}

@vertex_end

// ======================================================
// Fragment Shader:
// ======================================================
@fragment_shader

// Inputs from previous stage:
layout(location = 0) in vec4 v_color;
layout(location = 1) in vec2 v_texCoords;

// Uniform variables:
uniform sampler2D s_diffuseTexture; // tmu:0

// Final color written to the framebuffer:
out vec4 fragColor;

void main()
{
	fragColor = texture(s_diffuseTexture, v_texCoords) * v_color;
}

@fragment_end
