
// ================================================================================================
// -*- GLSL -*-
// File: particle.glsl
// Author: Guilherme R. Lampert
// Created on: 02/09/14
// Brief: 3D particle rendering with point sprites.
// ================================================================================================

// ======================================================
// Vertex Shader:
// ======================================================
@vertex_shader

// Vertex inputs, in model space:
layout(location = 0) in vec4 a_position_size; // Point size in w coord
layout(location = 1) in vec4 a_color;         // RGBA color modulated with texture

// Vertex shader outputs:
layout(location = 0) out vec4 v_color;

// Uniform variables:
uniform mat4 u_viewProjMatrix;
uniform mat4 u_modelMatrix;
uniform vec3 u_cameraPos;

// Constants (tweakable):
const float minPointScale = 0.1;
const float maxPointScale = 0.7;
const float maxDistance   = 100.0;

void main()
{
	// Transform the point to its current location in the world
	// so that the next calculations can work properly.
	vec4 pointWorldPosition = u_modelMatrix * vec4(a_position_size.xyz, 1.0);

	// Calculate point scale based on distance from the viewer
	// to compensate for the fact that gl_PointSize is the point
	// size in rasterized points / pixels.
	float cameraDist = distance(pointWorldPosition.xyz, u_cameraPos);
	float pointScale = 1.0 - (cameraDist / maxDistance);
	pointScale = max(pointScale, minPointScale);
	pointScale = min(pointScale, maxPointScale);

	// Set GL globals and forward the color:
	gl_Position  = u_viewProjMatrix  * pointWorldPosition;
	gl_PointSize = a_position_size.w * pointScale; // w is the point/particle size
	v_color      = a_color;
}

@vertex_end

// ======================================================
// Fragment Shader:
// ======================================================
@fragment_shader

// Inputs from previous stage:
layout(location = 0) in vec4 v_color;

// Uniform variables:
uniform sampler2D s_diffuseTexture; // tmu:0

// Final color written to the framebuffer:
out vec4 fragColor;

void main()
{
	fragColor = texture(s_diffuseTexture, gl_PointCoord) * v_color;
}

@fragment_end
