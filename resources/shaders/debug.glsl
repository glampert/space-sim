
// ================================================================================================
// -*- GLSL -*-
// File: debug.glsl
// Author: Guilherme R. Lampert
// Created on: 02/09/14
// Brief: Debug drawing shader. Used with GL_LINES and GL_POINTS.
// ================================================================================================

// ======================================================
// Vertex Shader:
// ======================================================
@vertex_shader

// Vertex inputs, in model space:
layout(location = 0) in vec3 a_position;
layout(location = 1) in vec4 a_colorPointSize; // xyz=color, w=point_size

// Vertex shader outputs:
layout(location = 0) out vec4 v_color;

// Uniform variables:
uniform mat4 u_MVPMatrix;

void main()
{
	gl_Position  = u_MVPMatrix * vec4(a_position, 1.0);
	gl_PointSize = a_colorPointSize.w;              // w stores the point size, if drawing points
	v_color      = vec4(a_colorPointSize.xyz, 1.0); // Always opaque (alpha = 1)
}

@vertex_end

// ======================================================
// Fragment Shader:
// ======================================================
@fragment_shader

// Inputs from previous stage:
layout(location = 0) in vec4 v_color;

// Final color written to the framebuffer:
out vec4 fragColor;

void main()
{
	fragColor = v_color;
}

@fragment_end
