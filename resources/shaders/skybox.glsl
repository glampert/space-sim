
// ================================================================================================
// -*- GLSL -*-
// File: skybox.glsl
// Author: Guilherme R. Lampert
// Created on: 01/09/14
// Brief: Skybox rendering shader program.
// ================================================================================================

// ======================================================
// Vertex Shader:
// ======================================================
@vertex_shader

// Vertex inputs, in model space:
layout(location = 0) in vec3 a_position;

// Vertex shader outputs:
layout(location = 0) out vec3 v_texCoords;

// Uniform variables:
uniform mat4 u_MVPMatrix;

void main()
{
	vec4 transformedPosition = u_MVPMatrix * vec4(a_position, 1.0);
	gl_Position = transformedPosition.xyww;
	v_texCoords = a_position;
}

@vertex_end

// ======================================================
// Fragment Shader:
// ======================================================
@fragment_shader

// Inputs from previous stage:
layout(location = 0) in vec3 v_texCoords;

// Uniform variables:
uniform samplerCube s_cubeMap; // tmu:0

// Final color written to the framebuffer:
out vec4 fragColor;

void main()
{
	fragColor = texture(s_cubeMap, v_texCoords);
}

@fragment_end
