
// ================================================================================================
// -*- GLSL -*-
// File: particle_player_view.glsl
// Author: Guilherme R. Lampert
// Created on: 02/09/14
// Brief: 3D particle rendering with point sprites.
// ================================================================================================

// ======================================================
// Vertex Shader:
// ======================================================
@vertex_shader

// Vertex inputs, in model space:
layout(location = 0) in vec4 a_position_size; // Point size in w coord
layout(location = 1) in vec4 a_color;         // RGBA color modulated with texture

// Vertex shader outputs:
layout(location = 0) out vec4 v_color;

// Uniform variables:
uniform mat4 u_MVPMatrix;

void main()
{
	// Set GL globals and forward the color:
	gl_Position  = u_MVPMatrix * vec4(a_position_size.xyz, 1.0);
	gl_PointSize = a_position_size.w; // w is the point/particle size
	v_color      = a_color;
}

@vertex_end

// ======================================================
// Fragment Shader:
// ======================================================
@fragment_shader

// Inputs from previous stage:
layout(location = 0) in vec4 v_color;

// Uniform variables:
uniform sampler2D s_diffuseTexture; // tmu:0

// Final color written to the framebuffer:
out vec4 fragColor;

void main()
{
	fragColor = texture(s_diffuseTexture, gl_PointCoord) * v_color;
}

@fragment_end
