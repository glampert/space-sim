
// ================================================================================================
// -*- GLSL -*-
// File: sprite2d.glsl
// Author: Guilherme R. Lampert
// Created on: 02/09/14
// Brief: 2D sprite rendering.
// ================================================================================================

// ======================================================
// Vertex Shader:
// ======================================================
@vertex_shader

// Vertex inputs, in model space:
layout(location = 0) in vec4 a_positionTexCoords; // 2d pos + uvs packed in the same vec4
layout(location = 1) in vec4 a_color;             // RGBA vertex color

// Vertex shader outputs:
layout(location = 0) out vec4 v_color;
layout(location = 1) out vec2 v_texCoords;

// Uniform variables:
uniform mat4 u_MVPMatrix;

void main()
{
	gl_Position = u_MVPMatrix * vec4(a_positionTexCoords.xy, 0.0, 1.0); // xy is the 2D vertex position.
	v_texCoords = a_positionTexCoords.zw; // zw are the vertex texture coordinates, packed into the vec4.
	v_color     = a_color;
}

@vertex_end

// ======================================================
// Fragment Shader:
// ======================================================
@fragment_shader

// Inputs from previous stage:
layout(location = 0) in vec4 v_color;
layout(location = 1) in vec2 v_texCoords;

// Uniform variables:
uniform sampler2D s_diffuseTexture; // tmu:0

// Final color written to the framebuffer:
out vec4 fragColor;

void main()
{
	fragColor = texture(s_diffuseTexture, v_texCoords) * v_color;
}

@fragment_end
