
--------------------------------------------------------
-- In-game flight camera configuration parameters:
--------------------------------------------------------
cameraConfig = {
	boosterSpeedScale = 4.0,
	forwardSpeed      = 0.2,
	maxForwardSpeed   = 0.15,
	movementSpeed     = 15.0,
	turnSpeed         = 0.08,
	rollSpeed         = 0.4,
	maxPitchRate      = 0.8,
	maxYawRate        = 0.8,
	movementDamping   = 0.4,
	lookDamping       = 0.4,
	rollResetRate     = 0.1,
	worldPosition     = { 0, 0,  0 }, -- xyz
	lookAtPos         = { 0, 0, -1 }, -- xyz
	farClip           = 10000.0
};

Debug.logComment("Camera configuration script executed successfully!");

