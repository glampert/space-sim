
--------------------------------------------------------
-- Renderer configurations:
--------------------------------------------------------
rendererConfig = {
	msaa         = 0,
	stencilBits  = 8,
	depthBits    = 24,
	windowWidth  = 1024,
	windowHeight = 768,
	vsync        = true,
	fullscreen   = false,

	modelImporterSilent       = false,
	modelImporterListTextures = true,
	heightMapsAsNormalMaps    = true,

	maxBillboardsPerBillboardRenderer = 1024,
	maxParticlesPerParticleRenderer   = 2048,
	maxSpriteTrisPerSpriteRenderer    = 2048,
	maxDebugVertexBufferSize          = 4096,

	splitScreenGame    = false,
	debugParticles     = false,
	debugRenderPhysics = false,
	drawFPSCounter     = true,
	drawHUD            = true
};

--------------------------------------------------------
-- ResourceManager configurations:
--------------------------------------------------------
resourcesConfig = {
	bypassResourcePacks = true
};

Debug.logComment("Global Engine configuration script executed successfully!");

