
#ifndef __lua_hpp_inc__
#define __lua_hpp_inc__

// lua.hpp
// Lua header files for C++
// <<extern "C">> not supplied automatically because Lua also compiles as C++

extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

#endif // __lua_hpp_inc__
