/*
** $Id: lualib.h,v 1.43 2011/12/08 12:11:37 roberto Exp $
** Lua standard libraries
** See Copyright Notice in lua.h
*/

#ifndef lualib_h
#define lualib_h

#include "lua.h"
#include "lauxlib.h"
#include <assert.h>

LUAMOD_API int luaopen_base(lua_State *L);

#define LUA_COLIBNAME	"Coroutine"
LUAMOD_API int luaopen_coroutine(lua_State *L);

#define LUA_TABLIBNAME	"Table"
LUAMOD_API int luaopen_table(lua_State *L);

#define LUA_IOLIBNAME	"IO"
LUAMOD_API int luaopen_io(lua_State *L);

#define LUA_SYSLIBNAME	"System"
LUAMOD_API int lua_system_set_extensions(const luaL_Reg *extra_functions, int num_extra_funcs);
LUAMOD_API int luaopen_system(lua_State *L);

#define LUA_STRLIBNAME	"String"
LUAMOD_API int luaopen_string(lua_State *L);

#define LUA_BITLIBNAME	"Bit32"
LUAMOD_API int luaopen_bit32(lua_State *L);

#define LUA_MATHLIBNAME	"Math"
LUAMOD_API int lua_math_set_extensions(const luaL_Reg *extra_functions, int num_extra_funcs);
LUAMOD_API int luaopen_math(lua_State *L);

#define LUA_DBLIBNAME	"Debug"
LUAMOD_API int lua_debug_set_extensions(const luaL_Reg *extra_functions, int num_extra_funcs);
LUAMOD_API int luaopen_debug(lua_State *L);

#define LUA_LOADLIBNAME	"Package"
LUAMOD_API int luaopen_package(lua_State *L);

/* open all previous libraries: */
LUALIB_API void luaL_openlibs(lua_State *L);

/* Use std assert(): */
#if !defined(lua_assert)
	#define lua_assert assert
#endif // lua_assert

#endif
