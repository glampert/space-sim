
// ================================================================================================
// -*- C++ -*-
// File: Sockets.cpp
// Author: Guilherme R. Lampert
// Created on: 28/10/14
// Brief: Basic socket interfaces.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine core:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"

// Networking:
#include "Engine/Networking/NetCommon.hpp"
#include "Engine/Networking/Sockets.hpp"

// This will automatically add the network
// library for a Windows build on Visual Studio.
#ifdef _MSC_VER
#pragma comment(lib, "Ws2_32.lib")
#endif

namespace Engine
{

// ======================================================
// Socket exception type:
// ======================================================

SocketError::SocketError(const std::string & message)
#ifdef _WIN32
    : std::runtime_error(message + std::string(" | sys error: ") + std::to_string(WSAGetLastError()))
#else  // !_WIN32
    : std::runtime_error(message + std::string(" | sys error: ") + std::strerror(errno))
#endif // _WIN32
{
    // Socket error message will be formated as:
    //   user message | sys error: return from strerror(errno) or WSAGetLastError()
    //
    // List of WSA error codes:
    //   http://msdn.microsoft.com/en-us/library/windows/desktop/ms740668(v=vs.85).aspx
}

// ======================================================
// Static network initialization:
// ======================================================

int Socket::numAliveSockets = 0;

void Socket::NetInit()
{
    if (numAliveSockets == 0)
    {
        common->PrintF("-------- Socket::NetInit() ---------");

#ifdef _WIN32
        WSADATA info;
        if (WSAStartup(MAKEWORD(2, 0), &info) != 0)
        {
            throw SocketError("Couldn't start WSA!");
        }
#endif // _WIN32
    }

    ++numAliveSockets;
}

void Socket::NetShutdown()
{
    --numAliveSockets;

    // Finalize the network when the last connection closes.
    if (numAliveSockets == 0)
    {
        common->PrintF("------ Socket::NetShutdown() -------");

#ifdef _WIN32
        WSACleanup();
#endif // _WIN32
    }
}

// ======================================================
// Socket:
// ======================================================

Socket::Socket()
    : socketId(InvalidNetSocketId)
{
    NetInit();

    // `protocol = 0` lets the system choose the best one available.
    socketId = socket(AF_INET, SOCK_STREAM, /* protocol = */ 0);
    if (socketId == InvalidNetSocketId)
    {
        throw SocketError("Failed to create socket with 'socket(AF_INET, SOCK_STREAM, 0)'!");
    }
}

Socket::Socket(const NetSocketId sid)
    : socketId(InvalidNetSocketId)
{
    NetInit();
    socketId = sid;
}

Socket::Socket(Socket && other) noexcept
{
    socketId = other.socketId;
    other.socketId = InvalidNetSocketId;
}

Socket & Socket::operator=(Socket && other)
{
    Close();

    socketId = other.socketId;
    other.socketId = InvalidNetSocketId;

    return *this;
}

Socket::~Socket()
{
    Close();
}

void Socket::Close()
{
    if (socketId == InvalidNetSocketId)
    {
        return;
    }

#ifdef _WIN32
    int result = closesocket(socketId);
#else  // !_WIN32
    int result = close(socketId);
#endif // _WIN32

    // Currently, I'm not bothering in handling the case
    // where a connection fails to close for whatever reasons.
    // But we should at least log the error to let someone
    // know about this problem...
    if (result == 0)
    {
        common->PrintF("Closed socket connection...");
    }
    else
    {
        common->WarningF("Failed to close a socket connection!");
    }

    socketId = InvalidNetSocketId;
    NetShutdown();
}

size_t Socket::ReceiveBytes(void * buffer, const size_t numBytes)
{
    assert(socketId != InvalidNetSocketId);
    assert(buffer != nullptr);
    assert(numBytes != 0);

    const long result = recv(socketId, buffer, numBytes, 0);
    if (result == NetSocketErrorCode)
    {
        throw SocketError("Failed to 'recv()' bytes! NetSocketError generated!");
    }

    return result;
}

size_t Socket::SendBytes(const void * buffer, const size_t numBytes)
{
    assert(socketId != InvalidNetSocketId);
    assert(buffer != nullptr);
    assert(numBytes != 0);

    const long result = send(socketId, buffer, numBytes, 0);
    if (result == NetSocketErrorCode)
    {
        throw SocketError("Failed to 'send()' bytes! NetSocketError generated!");
    }

    return result;
}

// ======================================================
// ClientSocket:
// ======================================================

ClientSocket::ClientSocket(const std::string & hostNameOrIpAddress, const HostShort & portNumber)
{
    // Should have been initialized by the superclass.
    assert(socketId != InvalidNetSocketId);

    // Must have an IP address or URL.
    assert(!hostNameOrIpAddress.empty());

    // Note: `gethostbyname()` is deprecated in WinSock2 in favor
    // of `getaddrinfo()`. So this is a possible future improvement.
    hostent * hostEntity = gethostbyname(hostNameOrIpAddress.c_str());
    if (hostEntity == nullptr)
    {
        Close();
        throw SocketError("Failed to get hostent for " + hostNameOrIpAddress);
    }

    sockaddr_in addr{};
    addr.sin_family = AF_INET;
    addr.sin_port = portNumber.ToNetShort().GetRawValue();
    addr.sin_addr = *reinterpret_cast<in_addr *>(hostEntity->h_addr);

    if (connect(socketId, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) != 0)
    {
        Close();
        throw SocketError("Failed to connect with host " + hostNameOrIpAddress +
                          " at port #" + std::to_string(portNumber.GetRawValue()));
    }
}

// ======================================================
// ServerSocket:
// ======================================================

ServerSocket::ServerSocket(const HostShort & portNumber, const int pendingQueueSize, const Type type)
{
    // Should have been initialized by the superclass.
    assert(socketId != InvalidNetSocketId);

    sockaddr_in addr{};
    addr.sin_family = PF_INET;
    addr.sin_port = portNumber.ToNetShort().GetRawValue();

    if (type == Type::NonBlocking)
    {
#ifdef _WIN32
        u_long arg = 1;
        ioctlsocket(socketId, FIONBIO, &arg);
#else  // !_WIN32
        fcntl(socketId, F_SETFL, O_NONBLOCK);
#endif // _WIN32
    }

    if (bind(socketId, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) != 0)
    {
        Close();
        throw SocketError("Failed to 'bind()' socket to port #" +
                          std::to_string(portNumber.GetRawValue()));
    }

    if (listen(socketId, pendingQueueSize) != 0)
    {
        Close();
        throw SocketError("Failed to 'listen()' at port #" +
                          std::to_string(portNumber.GetRawValue()));
    }
}

ServerSocket::SocketConnectionPtr ServerSocket::AcceptConnection()
{
    assert(socketId != InvalidNetSocketId);

    errno = 0;
    const NetSocketId newSocket = accept(socketId, nullptr, nullptr);
    if (newSocket == InvalidNetSocketId)
    {
// Non-blocking call and no request pending.
#ifdef _WIN32
        if (WSAGetLastError() == WSAEWOULDBLOCK)
        {
            return nullptr;
        }
#else  // !_WIN32
        if (errno == EAGAIN)
        {
            return nullptr;
        }
#endif // _WIN32

        throw SocketError("'accept()' failed with an invalid socketId!");
    }

    return SocketConnectionPtr(new Socket(newSocket));
}

} // namespace Engine {}
