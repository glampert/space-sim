
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: NetCommon.hpp
// Author: Guilherme R. Lampert
// Created on: 28/10/14
// Brief: Networking main.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#ifdef _WIN32
#include <WinSock2.h>
#else // !_WIN32
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <errno.h>
#endif // _WIN32

namespace Engine
{

// ======================================================
// Miscellaneous / typedefs:
// ======================================================

#ifdef _WIN32
typedef SOCKET NetSocketId;
constexpr NetSocketId InvalidNetSocketId = INVALID_SOCKET;
constexpr NetSocketId NetSocketErrorCode = SOCKET_ERROR;
#else  // !_WIN32
typedef int NetSocketId;
constexpr NetSocketId InvalidNetSocketId = -1;
constexpr NetSocketId NetSocketErrorCode = -1;
#endif // _WIN32

class NetShort;
class NetLong;
class HostShort;
class HostLong;

// ======================================================
// NetShort:
// ======================================================

// Network byte order short int.
class NetShort
{
  public:

    // Construct with the value 0.
    NetShort();

    // Can be constructed from converting a HostShort.
    explicit NetShort(const HostShort & hostShort);

    // Explicit conversions:
    HostShort ToHostShort() const;
    void FromHostShort(const HostShort & hostShort);

    // Raw data accessors (no byte-order conversion applied):
    u_short GetRawValue() const;
    void SetRawValue(u_short val);

  private:

    u_short value;
};

// ======================================================
// NetLong:
// ======================================================

// Network byte order long int
class NetLong
{
  public:

    // Construct with the value 0.
    NetLong();

    // Can be constructed from converting a HostLong.
    explicit NetLong(const HostLong & hostLong);

    // Explicit conversions:
    HostLong ToHostLong() const;
    void FromHostLong(const HostLong & hostLong);

    // Raw data accessors (no byte-order conversion applied):
    u_long GetRawValue() const;
    void SetRawValue(u_long val);

  private:

    u_long value;
};

// ======================================================
// HostShort:
// ======================================================

// Host (machine) byte order short int
class HostShort
{
  public:

    // Construct with the value 0.
    HostShort();

    // Can be explicitly constructed from an integer.
    // (Host byte order assumed).
    explicit HostShort(u_short val);

    // Can be constructed from converting a NetShort.
    explicit HostShort(const NetShort & netShort);

    // Explicit conversions:
    NetShort ToNetShort() const;
    void FromNetShort(const NetShort & netShort);

    // Raw data accessors (no byte-order conversion applied):
    u_short GetRawValue() const;
    void SetRawValue(u_short val);

  private:

    u_short value;
};

// ======================================================
// HostLong:
// ======================================================

// Host (machine) byte order long int
class HostLong
{
  public:

    // Construct with the value 0.
    HostLong();

    // Can be explicitly constructed from an integer.
    // (Host byte order assumed).
    explicit HostLong(u_long val);

    // Can be constructed from converting a NetLong.
    explicit HostLong(const NetLong & netLong);

    // Explicit conversions:
    NetLong ToNetLong() const;
    void FromNetLong(const NetLong & netLong);

    // Raw data accessors (no byte-order conversion applied):
    u_long GetRawValue() const;
    void SetRawValue(u_long val);

  private:

    u_long value;
};

// Include the inline definitions in the same namespace:
#include "Engine/Networking/NetCommon.inl"

} // namespace Engine {}
