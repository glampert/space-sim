
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Sockets.hpp
// Author: Guilherme R. Lampert
// Created on: 28/10/14
// Brief: Basic socket interfaces.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <stdexcept>
#include <string>

namespace Engine
{

// ======================================================
// Socket exception type:
// ======================================================

class SocketError final
    : public std::runtime_error
{
  public:
    SocketError(const std::string & message);
};

// ======================================================
// Socket base class:
// ======================================================

class Socket
{
  public:

    explicit Socket(NetSocketId sid);
    virtual ~Socket();

    // Copy is disallowed.
    Socket(const Socket & other) = delete;
    Socket & operator=(const Socket & other) = delete;

    // Is movable.
    Socket(Socket && other) noexcept;
    Socket & operator=(Socket && other);

    // Read and write bytes to the socket stream.
    // Returns the number of bytes sent/received, which might be less that numBytes.
    // Might throw SocketError if an unrecoverable error occurs.
    size_t ReceiveBytes(void * buffer, size_t numBytes);
    size_t SendBytes(const void * buffer, size_t numBytes);

    // Manually close a connection.
    // Note: This is done automatically by the destructor.
    void Close();

  protected:

    Socket();

    // Underlaying socket handle, specific to the platform.
    NetSocketId socketId;

  private:

    // Shared states management.
    // Network is initialized when the fist socket is created
    // and terminated when the last connection is closed.
    static void NetInit();
    static void NetShutdown();
    static int numAliveSockets;
};

// ======================================================
// ClientSocket:
// ======================================================

class ClientSocket
    : public Socket
{
  public:

    // Full socket setup. Might throw SocketError.
    ClientSocket(const std::string & hostNameOrIpAddress, const HostShort & portNumber);
};

// ======================================================
// ServerSocket:
// ======================================================

class ServerSocket
    : public Socket
{
  public:

    using SocketConnectionPtr = std::unique_ptr<Socket>;

    // This controls the behavior of AcceptConnection().
    // If blocking, AcceptConnection() will stall until a connection
    // arrives. Else, it returns immediately with a null socket pointer
    // if not pending connections are available.
    enum class Type
    {
        Blocking,
        NonBlocking
    };

    // Full socket setup. Might throw SocketError.
    ServerSocket(const HostShort & portNumber, int pendingQueueSize, Type type);

    // Returns null if this is a non-blocking socket and there are no pending
    // connections. Block and wait for a connection if this is a blocking socket.
    SocketConnectionPtr AcceptConnection();
};

} // namespace Engine {}
