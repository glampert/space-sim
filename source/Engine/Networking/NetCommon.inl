
// ================================================================================================
// -*- C++ -*-
// File: NetCommon.inl
// Author: Guilherme R. Lampert
// Created on: 28/10/14
// Brief: Inline definitions for the network methods and types.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// ======================================================
// NetShort:
// ======================================================

inline NetShort::NetShort()
    : value(0)
{
}

inline NetShort::NetShort(const HostShort & hostShort)
{
    FromHostShort(hostShort);
}

inline HostShort NetShort::ToHostShort() const
{
    HostShort hostShort;
    hostShort.SetRawValue(ntohs(value));
    return hostShort;
}

inline void NetShort::FromHostShort(const HostShort & hostShort)
{
    value = htons(hostShort.GetRawValue());
}

inline u_short NetShort::GetRawValue() const
{
    return value;
}

inline void NetShort::SetRawValue(const u_short val)
{
    value = val;
}

// ======================================================
// NetLong:
// ======================================================

inline NetLong::NetLong()
    : value(0)
{
}

inline NetLong::NetLong(const HostLong & hostLong)
{
    FromHostLong(hostLong);
}

inline HostLong NetLong::ToHostLong() const
{
    HostLong hostLong;
    hostLong.SetRawValue(ntohl(value));
    return hostLong;
}

inline void NetLong::FromHostLong(const HostLong & hostLong)
{
    value = htonl(hostLong.GetRawValue());
}

inline u_long NetLong::GetRawValue() const
{
    return value;
}

inline void NetLong::SetRawValue(const u_long val)
{
    value = val;
}

// ======================================================
// HostShort:
// ======================================================

inline HostShort::HostShort()
    : value(0)
{
}

inline HostShort::HostShort(const u_short val)
    : value(val)
{
}

inline HostShort::HostShort(const NetShort & netShort)
{
    FromNetShort(netShort);
}

inline NetShort HostShort::ToNetShort() const
{
    NetShort netShort;
    netShort.SetRawValue(htons(value));
    return netShort;
}

inline void HostShort::FromNetShort(const NetShort & netShort)
{
    value = ntohs(netShort.GetRawValue());
}

inline u_short HostShort::GetRawValue() const
{
    return value;
}

inline void HostShort::SetRawValue(const u_short val)
{
    value = val;
}

// ======================================================
// HostLong:
// ======================================================

inline HostLong::HostLong()
    : value(0)
{
}

inline HostLong::HostLong(const u_long val)
    : value(val)
{
}

inline HostLong::HostLong(const NetLong & netLong)
{
    FromNetLong(netLong);
}

inline NetLong HostLong::ToNetLong() const
{
    NetLong netLong;
    netLong.SetRawValue(htonl(value));
    return netLong;
}

inline void HostLong::FromNetLong(const NetLong & netLong)
{
    value = ntohl(netLong.GetRawValue());
}

inline u_long HostLong::GetRawValue() const
{
    return value;
}

inline void HostLong::SetRawValue(const u_long val)
{
    value = val;
}
