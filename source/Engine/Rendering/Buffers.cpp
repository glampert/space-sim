
// ================================================================================================
// -*- C++ -*-
// File: Buffers.cpp
// Author: Guilherme R. Lampert
// Created on: 01/09/14
// Brief: Thin object wrappers to OpenGL buffers, such as Vertex and Index Buffers.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"

// Renderer:
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/OpenGL.hpp"

namespace Engine
{

// ======================================================
// BufferStorage:
// ======================================================

unsigned int BufferStorageToGL(const BufferStorage storage)
{
    switch (storage)
    {
    case BufferStorage::StaticDraw:
        return GL_STATIC_DRAW;
    case BufferStorage::DynamicDraw:
        return GL_DYNAMIC_DRAW;
    case BufferStorage::StreamDraw:
        return GL_STREAM_DRAW;
    default:
        common->FatalErrorF("Invalid BufferStorage!");
    } // switch (storage)
}

std::string BufferStorageToString(const BufferStorage storage)
{
    switch (storage)
    {
    case BufferStorage::StaticDraw:
        return "StaticDraw";
    case BufferStorage::DynamicDraw:
        return "DynamicDraw";
    case BufferStorage::StreamDraw:
        return "StreamDraw";
    default:
        common->FatalErrorF("Invalid BufferStorage!");
    } // switch (storage)
}

// ======================================================
// VertexBuffer:
// ======================================================

VertexBuffer::VertexBuffer()
    : vao(0)
    , vbo(0)
    , vertexCount(0)
{
}

VertexBuffer::~VertexBuffer()
{
    FreeVAO();
    FreeVBO();
}

void VertexBuffer::BindVAO() const
{
    if (renderMgr->GetState().currentVAO != vao)
    {
        renderMgr->GetState().currentVAO = vao;
        glBindVertexArray(vao);
    }
}

void VertexBuffer::BindVBO() const
{
    if (renderMgr->GetState().currentVBO != vbo)
    {
        renderMgr->GetState().currentVBO = vbo;
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
    }
}

void VertexBuffer::UnbindVAO() const
{
    renderMgr->GetState().currentVAO = 0;
    glBindVertexArray(0);
}

void VertexBuffer::UnbindVBO() const
{
    renderMgr->GetState().currentVBO = 0;
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool VertexBuffer::IsVAOCurrent() const
{
    return (vao != 0) && (vao == renderMgr->GetState().currentVAO);
}

bool VertexBuffer::IsVBOCurrent() const
{
    return (vbo != 0) && (vbo == renderMgr->GetState().currentVBO);
}

bool VertexBuffer::AllocateVAO()
{
    if (vao == 0)
    {
        glGenVertexArrays(1, &vao);
        if (vao == 0)
        {
            CheckGLErrors();
            common->ErrorF("Failed to generate non-zero GL VAO id!");
            return false;
        }
    }

    BindVAO();
    return true;
}

void VertexBuffer::FreeVAO()
{
    if (vao != 0)
    {
        if (vao == renderMgr->GetState().currentVAO)
        {
            UnbindVAO();
        }
        glDeleteVertexArrays(1, &vao);
        vao = 0;
    }
}

bool VertexBuffer::AllocateVBO(const BufferStorage storage, const unsigned int numVerts, const size_t sizeOfAVertex, const void * initData)
{
    // 'initData' may be null.
    assert(numVerts != 0 && sizeOfAVertex != 0);

    // Gen buffer handle once:
    if (vbo == 0)
    {
        glGenBuffers(1, &vbo);
        if (vbo == 0)
        {
            CheckGLErrors();
            common->ErrorF("Failed to generate non-zero GL VBO id!");
            return false;
        }
    }

    BindVBO();
    glBufferData(GL_ARRAY_BUFFER, (numVerts * sizeOfAVertex), initData, BufferStorageToGL(storage));
    CheckGLErrors();

    vertexCount = numVerts;
    return true;
}

bool VertexBuffer::AllocateVBO()
{
    // Gen buffer handle once:
    if (vbo == 0)
    {
        glGenBuffers(1, &vbo);
        if (vbo == 0)
        {
            CheckGLErrors();
            common->ErrorF("Failed to generate non-zero GL VBO id!");
            return false;
        }
    }

    BindVBO();
    return true;
}

void VertexBuffer::FreeVBO()
{
    if (vbo != 0)
    {
        if (vbo == renderMgr->GetState().currentVBO)
        {
            UnbindVBO();
        }
        glDeleteBuffers(1, &vbo);
        vbo = 0;
    }
    vertexCount = 0;
}

void * VertexBuffer::MapVBO(const unsigned int mapFlags)
{
    assert(IsVBOCurrent()); // Must be already bound!
    return glMapBuffer(GL_ARRAY_BUFFER, mapFlags);
}

void * VertexBuffer::MapVBORange(const size_t offsetInBytes, const size_t sizeInBytes, const unsigned int mapFlags)
{
    assert(IsVBOCurrent()); // Must be already bound!
    return glMapBufferRange(GL_ARRAY_BUFFER, offsetInBytes, sizeInBytes, mapFlags);
}

void VertexBuffer::UnmapVBO()
{
    assert(IsVBOCurrent()); // Must be already bound!
    if (!glUnmapBuffer(GL_ARRAY_BUFFER))
    {
        common->ErrorF("glUnmapBuffer(GL_ARRAY_BUFFER) failed for GL VBO %d!", vbo);
    }
}

// ======================================================
// IndexBuffer:
// ======================================================

IndexBuffer::IndexBuffer()
    : ibo(0)
    , indexCount(0)
{
}

IndexBuffer::~IndexBuffer()
{
    FreeIBO();
}

void IndexBuffer::BindIBO() const
{
    if (renderMgr->GetState().currentIBO != ibo)
    {
        renderMgr->GetState().currentIBO = ibo;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    }
}

void IndexBuffer::UnbindIBO() const
{
    renderMgr->GetState().currentIBO = 0;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool IndexBuffer::IsIBOCurrent() const
{
    return (ibo != 0) && (ibo == renderMgr->GetState().currentIBO);
}

bool IndexBuffer::AllocateIBO(const BufferStorage storage, const unsigned int numIndexes, const size_t sizeOfAnIndex, const void * initData)
{
    // 'initData' may be null.
    assert(numIndexes != 0 && sizeOfAnIndex != 0);

    // Gen buffer handle once:
    if (ibo == 0)
    {
        glGenBuffers(1, &ibo);
        if (ibo == 0)
        {
            CheckGLErrors();
            common->ErrorF("Failed to generate non-zero GL IBO id!");
            return false;
        }
    }

    BindIBO();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, (numIndexes * sizeOfAnIndex), initData, BufferStorageToGL(storage));
    CheckGLErrors();

    indexCount = numIndexes;
    return true;
}

void IndexBuffer::FreeIBO()
{
    if (ibo != 0)
    {
        if (ibo == renderMgr->GetState().currentIBO)
        {
            UnbindIBO();
        }
        glDeleteBuffers(1, &ibo);
        ibo = 0;
    }
    indexCount = 0;
}

void * IndexBuffer::MapIBO(const unsigned int mapFlags)
{
    assert(IsIBOCurrent()); // Must be already bound!
    return glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, mapFlags);
}

void * IndexBuffer::MapIBORange(const size_t offsetInBytes, const size_t sizeInBytes, const unsigned int mapFlags)
{
    assert(IsIBOCurrent()); // Must be already bound!
    return glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, offsetInBytes, sizeInBytes, mapFlags);
}

void IndexBuffer::UnmapIBO()
{
    assert(IsIBOCurrent()); // Must be already bound!
    if (!glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER))
    {
        common->ErrorF("glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER) failed for GL IBO %d!", ibo);
    }
}

} // namespace Engine {}
