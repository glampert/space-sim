
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Camera.hpp
// Author: Guilherme R. Lampert
// Created on: 27/08/14
// Brief: Minimal camera interface.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Dependencies:
#include "Engine/Core/Geometry/Frustum.hpp"

namespace Engine
{

// ======================================================
// Camera interface:
// ======================================================

class Camera
{
  public:

    // World position, lookAt and up:
    virtual Vector3 GetWorldPosition() const = 0;
    virtual Vector3 GetLookAt()        const = 0;
    virtual Vector3 GetUpVector()      const = 0;

    // FoV, viewport and clipping planes:
    virtual float GetFOVRadians() const = 0; // Vertical Field of View (FOV-y) radians
    virtual float GetAspect()     const = 0; // viewport.width / viewport.height
    virtual Vec4u GetViewport()   const = 0; // Visible rectangle
    virtual Vec2f GetClipping()   const = 0; // [x=zNear, y=zFar]

    // Get the camera matrices:
    virtual Matrix4 GetViewMatrix() const = 0;
    virtual Matrix4 GetProjectionMatrix() const = 0;

    virtual const Frustum & GetFrustum() const = 0;
    virtual ~Camera() = default;
};

} // namespace Engine {}
