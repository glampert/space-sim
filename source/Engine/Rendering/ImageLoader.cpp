
// ================================================================================================
// -*- C++ -*-
// File: ImageLoader.cpp
// Author: Guilherme R. Lampert
// Created on: 11/08/14
// Brief: Loader for image file formats, such as TGA, JPEG, PNG, etc.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"

// Renderer:
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/ImageLoader.hpp"

// STB Image library:
// (NO_HDR to disable the High Dynamic Range (hdr) image loader).
#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_HDR
#include "ThirdParty/stb/stb_image.h"

namespace Engine
{

// ======================================================
// ImageData:
// ======================================================

ImageData::ImageData()
    : pixels(nullptr)
    , sizeInBytes(0)
    , dimensions(0, 0)
    , pixelFormat(PixelFormat::Invalid)
    , colorComponents(0)
{
}

ImageData::~ImageData()
{
    // Use stbi_image_free() because the data was allocated
    // by the library, so don't assume free() or delete[].
    if (pixels != nullptr)
    {
        stbi_image_free(pixels);
        pixels = nullptr;
    }
}

bool ImageData::IsValid() const
{
    return ((pixels != nullptr) && (sizeInBytes != 0) &&
            ((dimensions.width + dimensions.height) > 0) &&
            (pixelFormat != PixelFormat::Invalid) && (colorComponents > 0));
}

// ======================================================
// ImageLoader singleton:
// ======================================================

bool ImageLoader::LoadImageFromFile(const std::string & filename, ImageData & img)
{
    assert(!filename.empty());
    assert((img.pixels == nullptr) && "Creating new image would cause a memory leak!");

    int w, h, comp;
    stbi_uc * imageData = stbi_load(filename.c_str(), &w, &h, &comp, /* req_comp = */ 0);

    return ImageDataSetup(w, h, comp, imageData, filename, img, "stbi_load()");
}

bool ImageLoader::LoadImageFromData(const DataBlob blob, const std::string & filename, ImageData & img)
{
    assert(blob.IsValid());
    assert((img.pixels == nullptr) && "Creating new image would cause a memory leak!");

    int w, h, comp;
    stbi_uc * imageData = stbi_load_from_memory(reinterpret_cast<const stbi_uc *>(blob.data),
                                                static_cast<int>(blob.sizeInBytes), &w, &h,
                                                &comp, /* req_comp = */ 0);

    return ImageDataSetup(w, h, comp, imageData, filename, img, "stbi_load_from_memory()");
}

bool ImageLoader::ImageDataSetup(int w, int h, int comp, uint8_t * pixels,
                                 const std::string & filename, ImageData & img,
                                 const char * func)
{
    if (pixels == nullptr)
    {
        common->WarningF("%s failed with error: %s", func, stbi_failure_reason());
        return false;
    }

    switch (comp)
    {
    case 3: // RGB
        img.pixelFormat = Texture::PixelFormat::RGB_U8;
        break;

    case 4: // RGBA
        img.pixelFormat = Texture::PixelFormat::RGBA_U8;
        break;

    default: // Unsupported
        stbi_image_free(pixels);
        common->WarningF("\"%s\" image format (comp=%d) not supported!", filename.c_str(), comp);
        return false;
    } // switch (comp)

    // Non power-of-2 dimensions check:
    if (((w & (w - 1)) != 0) || ((h & (h - 1)) != 0))
    {
        // NOTE: This should be improved in the future.
        // We should define a configuration parameter that enables
        // downscaling a non POT image.
        //
        common->WarningF("\"%s\" image does not have power-of-2 dimensions!", filename.c_str());
    }

    // Set up image and return:
    img.dimensions.width = w;
    img.dimensions.height = h;
    img.colorComponents = static_cast<short>(comp);
    img.sizeInBytes = (w * h * comp);
    img.pixels = pixels;

    // All good.
    common->PrintF("\"%s\" image loaded successfully. w=%d, h=%d, comp=%d", filename.c_str(), w, h, comp);
    return true;
}

} // namespace Engine {}
