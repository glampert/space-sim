
// ================================================================================================
// -*- C++ -*-
// File: Model3D.cpp
// Author: Guilherme R. Lampert
// Created on: 08/08/14
// Brief: 3D model/mesh/object.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/Model3D.hpp"
#include "Engine/Rendering/DebugRenderer.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/OpenGL.hpp"

// Model3D importer:
#include "Engine/Rendering/Model3DImporter.hpp"

namespace Engine
{

// ======================================================
// Model3D:
// ======================================================

bool Model3D::InitFromFile(std::string filename, std::string resourceId)
{
    // Import:
    if (!Model3DImporter::ImportModel3DFromFile(filename, *this))
    {
        common->WarningF("Failed to import Model3D from data!");
        return false;
    }

    // Remember file that originated this model.
    modelFileName = std::move(resourceId);

    // Finish buffer setup:
    if (!InitBuffers())
    {
        common->WarningF("Failed to properly setup GL buffers! Model3D is incomplete!");
        return false;
    }

    return true;
}

bool Model3D::InitFromData(const DataBlob blob, std::string filename)
{
    // Import:
    if (!Model3DImporter::ImportModel3DFromData(blob, filename, *this))
    {
        common->WarningF("Failed to import Model3D from data!");
        return false;
    }

    // Remember file that originated this model.
    modelFileName = std::move(filename);

    // Finish buffer setup:
    if (!InitBuffers())
    {
        common->WarningF("Failed to properly setup GL buffers! Model3D is incomplete!");
        return false;
    }

    return true;
}

void Model3D::FreeAllData()
{
    subMeshes.clear();
    modelFileName.clear();
    vertexBuffer.FreeAllBuffers();
}

bool Model3D::InitBuffers()
{
    if (subMeshes.empty())
    {
        common->WarningF("Model3D \"%s\" has no sub-meshes!", modelFileName.c_str());
        return false;
    }

    // Count max vertexes/indexes to allocate exact memory:
    unsigned int totalVerts = 0;
    unsigned int totalIndexes = 0;
    const size_t numMeshes = subMeshes.size();
    for (size_t m = 0; m < numMeshes; ++m)
    {
        // Increment global counts:
        const SubMesh & mesh = subMeshes[m];
        totalVerts += mesh.vertexes.size();
        totalIndexes += mesh.indexes.size();
    }

    // Validate:
    if ((totalVerts == 0) || (totalIndexes == 0))
    {
        common->WarningF("Model3D \"%s\" has no vertexes or indexes!", modelFileName.c_str());
        return false;
    }

    ClearGLErrors();

    // Allocate and bind the GL buffers:
    if (!vertexBuffer.AllocateVAO())
    {
        return false;
    }
    if (!vertexBuffer.AllocateVBO(vertexBuffer.GetStorage(), totalVerts, sizeof(DrawVertex), nullptr))
    {
        return false;
    }
    if (!vertexBuffer.AllocateIBO(vertexBuffer.GetStorage(), totalIndexes, sizeof(IndexType), nullptr))
    {
        return false;
    }

    // Map buffers and download the data:
    void * pVB = vertexBuffer.MapVBO(GL_WRITE_ONLY);
    void * pIB = vertexBuffer.MapIBO(GL_WRITE_ONLY);

    bool success;
    if ((pVB != nullptr) && (pIB != nullptr))
    {
        CopyDataToBuffers(pVB, pIB);
        success = true;
    }
    else
    {
        common->ErrorF("Failed to map GL buffers! Unable to download Model3D data to GL...");
        success = false;
    }

    // Unmap memory:
    vertexBuffer.UnmapIBO();
    vertexBuffer.UnmapVBO();

    // Set "vertex format":
    glEnableVertexAttribArray(DrawVertex::PositionAttrIndex);
    glVertexAttribPointer(DrawVertex::PositionAttrIndex, 3, GL_FLOAT, GL_FALSE,
                          sizeof(DrawVertex), reinterpret_cast<const GLvoid *>(DrawVertex::PositionOffset));

    glEnableVertexAttribArray(DrawVertex::NormalAttrIndex);
    glVertexAttribPointer(DrawVertex::NormalAttrIndex, 3, GL_FLOAT, GL_FALSE,
                          sizeof(DrawVertex), reinterpret_cast<const GLvoid *>(DrawVertex::NormalOffset));

    glEnableVertexAttribArray(DrawVertex::TangentAttrIndex);
    glVertexAttribPointer(DrawVertex::TangentAttrIndex, 3, GL_FLOAT, GL_FALSE,
                          sizeof(DrawVertex), reinterpret_cast<const GLvoid *>(DrawVertex::TangentOffset));

    glEnableVertexAttribArray(DrawVertex::BiTangentAttrIndex);
    glVertexAttribPointer(DrawVertex::BiTangentAttrIndex, 3, GL_FLOAT, GL_FALSE,
                          sizeof(DrawVertex), reinterpret_cast<const GLvoid *>(DrawVertex::BiTangentOffset));

    glEnableVertexAttribArray(DrawVertex::TexCoordsAttrIndex);
    glVertexAttribPointer(DrawVertex::TexCoordsAttrIndex, 2, GL_FLOAT, GL_FALSE,
                          sizeof(DrawVertex), reinterpret_cast<const GLvoid *>(DrawVertex::TexCoordsOffset));

    // Cleanup:
    vertexBuffer.UnbindAllBuffers();
    CheckGLErrors();

    if (success)
    {
        common->PrintF("GL buffers created for Model3D \"%s\"...", modelFileName.c_str());
    }
    return success;
}

void Model3D::CopyDataToBuffers(void * __restrict pVB, void * __restrict pIB) const
{
    assert(pVB != nullptr);
    assert(pIB != nullptr);

    DrawVertex * __restrict verts = reinterpret_cast<DrawVertex *>(pVB);
    IndexType * __restrict indexes = reinterpret_cast<IndexType *>(pIB);

    const size_t numMeshes = subMeshes.size();
    for (size_t m = 0; m < numMeshes; ++m)
    {
        const SubMesh & mesh = subMeshes[m];

        const size_t totalVerts = mesh.vertexes.size();
        const size_t totalIndexes = mesh.indexes.size();

        assert(totalVerts != 0);
        assert(totalIndexes != 0);

        // Fast memcpy mesh vertexes:
        std::memcpy(verts, &mesh.vertexes[0], totalVerts * sizeof(DrawVertex));
        verts += totalVerts;

        // Copy indexes adding start offset (baseVertex) so that
        // we are able to render without glDrawElementsBaseVertex().
        for (size_t i = 0; i < totalIndexes; ++i)
        {
            *indexes++ = (mesh.indexes[i] + mesh.firstVertexInVB);
        }
    }
}

bool Model3D::IsDefault() const
{
    return false; // TODO
}

size_t Model3D::GetEstimateMemoryUsage() const
{
    // There is the GPU memory from the GL buffers plus
    // a CPU shadow copy of the data inside each SubMesh, therefore * 2.
    const size_t vertIndexMemory = (GetVertexBufferSizeBytes() + GetIndexBufferSizeBytes()) * 2;
    return (vertIndexMemory + (GetSubMeshCount() * sizeof(SubMesh)));
}

std::string Model3D::GetResourceId() const
{
    return modelFileName;
}

std::string Model3D::GetResourceTypeString() const
{
    return "Model3D";
}

void Model3D::PrintSelf() const
{
    common->PrintF("------- Model3D -------");
    common->PrintF("Model/mesh file.: %s", modelFileName.c_str());
    common->PrintF("Memory usage....: %s", FormatMemoryUnit(GetEstimateMemoryUsage(), true).c_str());
    common->PrintF("Vertex count....: %u", GetVertexCount());
    common->PrintF("Index count.....: %u", GetIndexCount());
    common->PrintF("Sub-Mesh count..: %u", GetSubMeshCount());
    common->PrintF("VB memory.......: %s", FormatMemoryUnit(GetVertexBufferSizeBytes(), true).c_str());
    common->PrintF("IB memory.......: %s", FormatMemoryUnit(GetIndexBufferSizeBytes(), true).c_str());
    common->PrintF("Sub-Mesh memory.: %s", FormatMemoryUnit(GetSubMeshCount() * sizeof(SubMesh), true).c_str());
    common->PrintF("Storage.........: %s", BufferStorageToString(vertexBuffer.GetStorage()).c_str());
    common->PrintF("GL VAO id.......: %u", vertexBuffer.GetVAOHandle());
    common->PrintF("GL VBO id.......: %u", vertexBuffer.GetVBOHandle());
    common->PrintF("GL IBO id.......: %u", vertexBuffer.GetIBOHandle());
    common->PrintF("Is default?.....: %s", (IsDefault() ? "yes" : "no"));
}

void Model3D::ComputeBounds()
{
    aabb.Clear();
    const size_t numMeshes = subMeshes.size();
    for (size_t m = 0; m < numMeshes; ++m)
    {
        aabb.AddBounds(subMeshes[m].ComputeBounds());
    }
}

void Model3D::DrawWithMaterial(const ConstMaterialPtr & material, const Matrix4 * mvp, const Matrix4 * mv) const
{
    // If no material provided, use renderer default.
    const ConstMaterialPtr mtr((material != nullptr) ? material : renderMgr->defaultMaterial);

    // Set the material:
    if (mvp != nullptr && mv != nullptr)
    {
        mtr->SetMVPTransform(*mvp, *mv);
    }
    mtr->Apply();

    // We can render whole index buffer from the start with one call,
    // since a single material will be applied to the entire model.
    vertexBuffer.BindForDrawing();
    glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(vertexBuffer.GetIndexCount()), GetGLTypeForIndexType(), nullptr);
    vertexBuffer.UnbindDrawing();
}

void Model3D::DrawWithMVP(const Matrix4 & mvp, const Matrix4 & mv) const
{
    vertexBuffer.BindForDrawing();

    // Render every mesh:
    const size_t numMeshes = subMeshes.size();
    for (size_t m = 0; m < numMeshes; ++m)
    {
        const SubMesh & mesh = subMeshes[m];

        // Set the material:
        if (mesh.material != nullptr)
        {
            mesh.material->SetMVPTransform(mvp, mv);
            mesh.material->Apply();
        }
        else
        {
            renderMgr->defaultMaterial->SetMVPTransform(mvp, mv);
            renderMgr->defaultMaterial->Apply();
        }

        // Draw:
        glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mesh.indexes.size()),
                       GetGLTypeForIndexType(), reinterpret_cast<const GLvoid *>(mesh.firstIndexInIB * sizeof(IndexType)));
    }

    vertexBuffer.UnbindDrawing();
}

void Model3D::DrawModelAABBs(DebugRenderer & debugRenderer, const Matrix4 * transform,
                             const Color4f color, const uint32_t durationMs, const bool depthEnabled) const
{
    const size_t numMeshes = subMeshes.size();

    if (transform != nullptr) // Draw AABB transformed:
    {
        for (size_t m = 0; m < numMeshes; ++m)
        {
            debugRenderer.AddAABB(subMeshes[m].aabb.Transformed(*transform), color, durationMs, depthEnabled);
        }
        // Also draw the parent box if this is not a single mesh model:
        if (numMeshes > 1)
        {
            debugRenderer.AddAABB(aabb.Transformed(*transform), color, durationMs, depthEnabled);
        }
    }
    else // Draw using raw world-space points:
    {
        for (size_t m = 0; m < numMeshes; ++m)
        {
            debugRenderer.AddAABB(subMeshes[m].aabb, color, durationMs, depthEnabled);
        }
        // Also draw the parent box if this is not a single mesh model:
        if (numMeshes > 1)
        {
            debugRenderer.AddAABB(aabb, color, durationMs, depthEnabled);
        }
    }
}

void Model3D::AllocateSubMeshes(const size_t num)
{
    if (num == 0)
    {
        subMeshes.clear();
    }
    else
    {
        subMeshes.resize(num);
    }
}

unsigned int Model3D::GetSubMeshCount() const
{
    return static_cast<unsigned int>(subMeshes.size());
}

Model3D::SubMesh & Model3D::GetSubMeshAt(const size_t index)
{
    assert(index < subMeshes.size());
    return subMeshes[index];
}

const Model3D::SubMesh & Model3D::GetSubMeshAt(const size_t index) const
{
    assert(index < subMeshes.size());
    return subMeshes[index];
}

unsigned int Model3D::GetVertexCount() const
{
    return vertexBuffer.GetVertexCount();
}

unsigned int Model3D::GetIndexCount() const
{
    return vertexBuffer.GetIndexCount();
}

size_t Model3D::GetVertexBufferSizeBytes() const
{
    return vertexBuffer.GetVertexCount() * sizeof(DrawVertex);
}

size_t Model3D::GetIndexBufferSizeBytes() const
{
    return vertexBuffer.GetIndexCount() * sizeof(IndexType);
}

BufferStorage Model3D::GetBufferStorage() const
{
    return vertexBuffer.GetStorage();
}

void Model3D::SetBufferStorage(const BufferStorage storage)
{
    vertexBuffer.SetStorage(storage);
}

constexpr unsigned int Model3D::GetGLTypeForIndexType()
{
    // If IndexType is uint16_t, then change to GL_UNSIGNED_SHORT!
    static_assert(sizeof(IndexType) == 4, "Update this if Model3D::IndexType changed!");
    return GL_UNSIGNED_INT;
}

// ======================================================
// Model3D factory function for ResourceManager:
// ======================================================

static ResourcePtr Model3D_FactoryFunction(const bool forceDefault)
{
    // TODO: A default model!
    (void)forceDefault;
    return renderMgr->NewModel3D();
}

// Register the callback:
REGISTER_RESOURCE_FACTORY_FOR_TYPE(Resource::Type::Model3D, Model3D_FactoryFunction);

} // namespace Engine {}
