
// ================================================================================================
// -*- C++ -*-
// File: RenderManager.cpp
// Author: Guilherme R. Lampert
// Created on: 07/08/14
// Brief: RenderManager singleton.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

// Renderer components:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Font.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/Model3D.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/ParticleRenderer.hpp"
#include "Engine/Rendering/DebugRenderer.hpp"
#include "Engine/Rendering/Camera.hpp"
#include "Engine/Rendering/OpenGL.hpp"

//
// Enable/disable this definition to
// turn AVI screen capture on and off.
// Will significantly drop the frame rate!
// Also have to call renderMgr->EnableAviScreenCapture() once.
//
//#define HAS_AVI_SCREEN_CAPTURE 1

// AVI video writing API and helpers:
#ifdef HAS_AVI_SCREEN_CAPTURE

#include <queue>
#include <chrono>
#include <thread>
#include <mutex>

struct AviWriter;
extern AviWriter * avi_writer_new(FILE * file, int width, int height, float frameRate);
extern void avi_writer_append_frame(AviWriter * self, const uint8_t * pixels, int width, int height, int rowStride, int channels);
extern void avi_writer_close(AviWriter * self);

#endif // HAS_AVI_SCREEN_CAPTURE

namespace Engine
{

// ======================================================
// AviCaptureHelper:
// ======================================================

#ifdef HAS_AVI_SCREEN_CAPTURE

struct AviCaptureHelper
{
    static constexpr int frameRate    = 15;
    static constexpr int targetWidth  = 1024;
    static constexpr int targetHeight = 768;

    volatile bool terminateWriter;
    unsigned int  frameNum;
    AviWriter *   writer;
    uint8_t   *   framebuffer; // Temp buffer for an uncompressed fame of the game.

    // Producer consumer queue to communicate with the writer thread:
    std::mutex mtx;
    std::queue<uint8_t *> frames;

    // Background thread that does the file IO:
    std::thread writerThread;

    AviCaptureHelper(int fbWidth, int fbHeight)
        : terminateWriter(false)
        , frameNum(0)
        , writer(nullptr)
        , framebuffer(new uint8_t[fbWidth * fbHeight * 4])
        , writerThread(std::bind(&AviCaptureHelper::FrameWriterThread, this))
    {
        FILE * file = std::fopen("avi_demo.avi", "wb");
        if (file == nullptr)
        {
            common->FatalErrorF("Failed to create AVI demo file!");
        }

        // Allocate a buffer big enough to hold an entire frame plus some extra space
        // for housekeeping AVI data. This should reduce the number of flushes while writing the file.
        std::setvbuf(file, nullptr, _IOFBF, (targetWidth * targetHeight * 4) + 1024);

        // Takes ownership of the file pointer.
        writer = avi_writer_new(file, targetWidth, targetHeight, frameRate);
    }

    ~AviCaptureHelper()
    {
        // This will also close the FILE and delete the writer.
        terminateWriter = true;
        writerThread.join();
        avi_writer_close(writer);
        delete[] framebuffer;
    }

    void CapruteFrame()
    {
        // Capture one out of each two frames (always the odd one).
        if (frameNum++ & 1)
        {
            uint8_t * img = new uint8_t[targetWidth * targetHeight * 4];
            renderMgr->TakeScreenshot(targetWidth, targetHeight, img, framebuffer, true);

            std::lock_guard<std::mutex> lock(mtx);
            frames.push(img);
        }
    }

    void FrameWriterThread()
    {
        // We are able to run fairly well using the writer thread.
        // If that per-frame memory allocation could be eliminated,
        // this would probably run even more smoothly.
        while (!terminateWriter)
        {
            if (!frames.empty())
            {
                uint8_t * img;
                {
                    std::lock_guard<std::mutex> lock(mtx);
                    img = frames.front();
                    frames.pop();
                }

                avi_writer_append_frame(writer, img, targetWidth, targetHeight, (targetWidth * 4), 4);
                delete[] img;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
};

static AviCaptureHelper * aviCap;
static bool enableAviCapture;

#endif // HAS_AVI_SCREEN_CAPTURE

// ======================================================
// RenderManager singleton:
// ======================================================

// Color used to wipe the screen at the beginning of a frame:
const Color4f RenderManager::clearScrColor(0.0f, 0.0f, 0.0f, 1.0f);

RenderManager::RenderManager()
    : windowObj(nullptr) // Not owned by RenderManager
    , framebufferWidth(0)
    , framebufferHeight(0)
    , windowWidth(0)
    , windowHeight(0)
    , originalFramebufferSize(0, 0)
    , camera(nullptr)
    , projectionMatrix(Matrix4::identity())
    , viewMatrix(Matrix4::identity())
    , vpMatrix(Matrix4::identity())
    , lightPosWorldSpace(0.0, 0.0, 0.0)
{
}

void RenderManager::Init()
{
    common->PrintF("------ RenderManager::Init() -------");

    // Common should have set it already!
    assert(windowObj != nullptr);

    // Refresh renderer screen/framebuffer sizes:
    int winWidth = 0, winHeight = 0;
    int fbWidth = 0, fbHeight = 0;
    glfwGetWindowSize(reinterpret_cast<GLFWwindow *>(windowObj), &winWidth, &winHeight);
    glfwGetFramebufferSize(reinterpret_cast<GLFWwindow *>(windowObj), &fbWidth, &fbHeight);
    assert((winWidth + winHeight) > 0);
    assert((fbWidth + fbHeight) > 0);

    // Set full-window viewport:
    glViewport(0, 0, fbWidth, fbHeight);

    // Clear the screen ASAP:
    glClearColor(clearScrColor.r, clearScrColor.g, clearScrColor.b, clearScrColor.a);
    glClear(GL_COLOR_BUFFER_BIT);
    glfwSwapBuffers(reinterpret_cast<GLFWwindow *>(windowObj));
    glFinish();

    // Save & print framebuffer dimensions:
    framebufferWidth = fbWidth;
    framebufferHeight = fbHeight;
    common->PrintF("Framebuffer dimensions: %dx%d", framebufferWidth, framebufferHeight);

    // Save & print window dimensions:
    windowWidth = winWidth;
    windowHeight = winHeight;
    common->PrintF("Window dimensions: %dx%d", windowWidth, windowHeight);

    originalFramebufferSize.width = fbWidth;
    originalFramebufferSize.height = fbHeight;

    // Store the Shading Language version for later:
    ParseGLSLVersion();

    // Set default GL states:
    SetDefaultGLStates();

    // Clear GL error flags:
    while (glGetError() != GL_NO_ERROR)
    {
    }

    // Create the built-in resources:
    InitDefaultTextures();
    InitDefaultMaterialShader();

#ifdef HAS_AVI_SCREEN_CAPTURE
    aviCap = new AviCaptureHelper(framebufferWidth, framebufferHeight);
#endif // HAS_AVI_SCREEN_CAPTURE
}

void RenderManager::Shutdown()
{
    common->PrintF("----- RenderManager::Shutdown() ----");

#ifdef HAS_AVI_SCREEN_CAPTURE
    delete aviCap;
    aviCap = nullptr;
#endif // HAS_AVI_SCREEN_CAPTURE

    DebugRenderer::ShutdownSharedData();
    ParticleRenderer::ShutdownSharedData();

    // Release built-in resources:
    whiteTexture = nullptr;
    blackTexture = nullptr;
    grayTexture = nullptr;
    defaultNormalTexture = nullptr;
    defaultDiffuseTexture = nullptr;
    defaultSpecularTexture = nullptr;
    defaultMaterial = nullptr;
    defaultShaderProg = nullptr;

    // Reset/clear everything else:
    windowObj = nullptr;
    framebufferWidth = 0;
    framebufferHeight = 0;
    windowWidth = 0;
    windowHeight = 0;

    glState.Reset();
    glslVersionTag.clear();
}

void RenderManager::SetDefaultGLStates()
{
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDisable(GL_SCISSOR_TEST);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_BLEND);

    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);

    // OpenGL's default is GL_CCW
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);

    glDrawBuffer(GL_BACK);
    glReadBuffer(GL_BACK);

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glState.Reset();
}

void RenderManager::ParseGLSLVersion()
{
    GLint major = 0, minor = 0;

    // Parse GLSL version from string:
    const char * glslVersionStr = reinterpret_cast<const char *>(glGetString(GL_SHADING_LANGUAGE_VERSION));
    if (glslVersionStr == nullptr)
    {
        common->ErrorF("glGetString(GL_SHADING_LANGUAGE_VERSION) returned null!");
        // Default: 120
        major = 1, minor = 20;
    }

    // Try to get the GLSL version: <MAJOR.MINOR>
    if (std::sscanf(glslVersionStr, "%d.%d", &major, &minor) != 2)
    {
        common->ErrorF("Failed to parse GLSL version numbers!");
        // Default: 120
        major = 1, minor = 20;
    }

    const int v = (major * 100) + minor;
    glslVersionTag = Format("#version %d core\n", v);
    common->PrintF("GLSL #version: %d (core)", v);
}

void RenderManager::ClearScreen()
{
// Extra error checking.
// Clear errors at the start of each frame
// and check errors at the end.
#if GL_DO_ERROR_CHECKING
    ClearGLErrors();
#endif // GL_DO_ERROR_CHECKING

    // Clear screen:
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderManager::SwapBuffers()
{
    // Every PushMatrix must be paired with a PopMatrix before the end of a frame.
    assert(matrixStack.empty() && "Garbage left in the matrix stack!");

#ifdef HAS_AVI_SCREEN_CAPTURE
    if (enableAviCapture)
    {
        aviCap->CapruteFrame();
    }
#endif // HAS_AVI_SCREEN_CAPTURE

    glfwSwapBuffers(reinterpret_cast<GLFWwindow *>(windowObj));

// Check rendering errors everything frame.
// This will slow the app quite a bit, so should only
// happen in debug builds.
#if GL_DO_ERROR_CHECKING
    CheckGLErrors();
#endif // GL_DO_ERROR_CHECKING
}

//
//TODO some state caching for the following GL states is in order!
//

void RenderManager::EnableDepthWrite()
{
    // Enabled by default once the RenderManager starts.
    glDepthMask(GL_TRUE);
}

void RenderManager::DisableDepthWrite()
{
    glDepthMask(GL_FALSE);
}

void RenderManager::EnableDepthTest()
{
    glEnable(GL_DEPTH_TEST);
}

void RenderManager::DisableDepthTest()
{
    glDisable(GL_DEPTH_TEST);
}

void RenderManager::EnablePolygonCull()
{
    glEnable(GL_CULL_FACE);
}

void RenderManager::DisablePolygonCull()
{
    glDisable(GL_CULL_FACE);
}

void RenderManager::EnableSpriteBlendMode()
{
    // glBlendFunc() is begin called by SetDefaultGLStates()
    // and never changed. If it is changed at some point,
    // then this method should reset it.
    glEnable(GL_BLEND);
}

void RenderManager::DisableColorBlend()
{
    glDisable(GL_BLEND);
}

void RenderManager::TakeScreenshot(const unsigned int targetWidth, const unsigned int targetHeight,
                                   uint8_t * scaledImage, uint8_t * framebuffer, const bool needFlipping)
{
    assert(targetWidth != 0);
    assert(targetHeight != 0);
    assert(framebuffer != nullptr);

    glReadBuffer(GL_BACK);
    glReadPixels(0, 0, framebufferWidth, framebufferHeight, GL_RGBA, GL_UNSIGNED_BYTE, framebuffer);

    // Attempt to scaled the framebuffer to target dimensions.
    // In this case, the provided buffer must not be null and must be big enough.
    if ((targetWidth != framebufferWidth) || (targetHeight != framebufferHeight))
    {
        assert(scaledImage != nullptr);

        // Resample the image using a fast box-filter:
        {
            uint8_t * __restrict src = framebuffer;
            uint8_t * __restrict dest = scaledImage;

            const unsigned int idx1 = (framebufferWidth * 4);
            const unsigned int idx2 = (framebufferWidth + 1) * 4;

            for (unsigned int y = 0; y < targetHeight; ++y)
            {
                for (unsigned int x = 0; x < targetWidth; ++x)
                {
                    for (int k = 0; k < 4; ++k)
                    {
                        *dest++ = static_cast<uint8_t>((
                                      static_cast<int>(src[0]) +
                                      static_cast<int>(src[4]) +
                                      static_cast<int>(src[idx1]) +
                                      static_cast<int>(src[idx2]) + 2) >>
                                      2);
                        src++;
                    }
                    src += 4;
                }
                src += 4 * framebufferWidth;
            }
        }

        // Flip vertically in-place, if needed.
        if (needFlipping)
        {
            // Format is always assumed RGBA 8.
            uint32_t * __restrict dest = reinterpret_cast<uint32_t *>(scaledImage);

            for (unsigned int y = 0; y < (targetHeight / 2); ++y)
            {
                for (unsigned int x = 0; x < targetWidth; ++x)
                {
                    std::swap(dest[x + y * targetWidth], dest[x + ((targetHeight - 1) - y) * targetWidth]);
                }
            }
        }
    }
}

bool RenderManager::EnableAviScreenCapture(const bool enable)
{
#ifdef HAS_AVI_SCREEN_CAPTURE
    bool oldState = enableAviCapture;
    enableAviCapture = enable;
    return oldState;
#else  // !HAS_AVI_SCREEN_CAPTURE
    (void)enable;
    return false;
#endif // HAS_AVI_SCREEN_CAPTURE
}

void RenderManager::SetSplitScreen(const SplitScreen mode)
{
    switch (mode)
    {
    case SplitScreen::TopView:
    {
        framebufferWidth = originalFramebufferSize.width;
        framebufferHeight = originalFramebufferSize.height / 2;
        glViewport(0, framebufferHeight, framebufferWidth, framebufferHeight);
        break;
    }
    case SplitScreen::BottonView:
    {
        framebufferWidth = originalFramebufferSize.width;
        framebufferHeight = originalFramebufferSize.height / 2;
        glViewport(0, 0, framebufferWidth, framebufferHeight);
        break;
    }
    case SplitScreen::FullView:
    {
        framebufferWidth = originalFramebufferSize.width;
        framebufferHeight = originalFramebufferSize.height;
        glViewport(0, 0, framebufferWidth, framebufferHeight);
        break;
    }
    default:
        common->FatalErrorF("Invalid SplitScreen flag!");
    } // switch (mode)
}

void RenderManager::GetDefaultGLTexFilter(unsigned int * minFilter, unsigned int * magFilter, unsigned int * anisotropyAmount) const
{
    // These values will have to come from a configuration script at some point!!!
    if (minFilter != nullptr)
    {
        //TODO temp
        (*minFilter) = GL_LINEAR_MIPMAP_LINEAR;
    }
    if (magFilter != nullptr)
    {
        //TODO temp
        (*magFilter) = GL_LINEAR;
    }
    if (anisotropyAmount != nullptr)
    {
        //TODO temp
        (*anisotropyAmount) = 0;
    }
}

Vector3 RenderManager::GetLightDirEyeSpace() const
{
    assert(camera != nullptr);
    Vector4 result(camera->GetViewMatrix() * lightPosWorldSpace);
    return normalize(Vector3(result[0], result[1], result[2]));
}

void RenderManager::PushMatrix(const Matrix4 & m)
{
    // First matrix for this frame?
    if (matrixStack.empty())
    {
        matrixStack.push(m);
        return;
    }

    // Save the current matrix:
    const Matrix4 topMatrix = matrixStack.top();
    matrixStack.push(topMatrix);

    // There are two copies of the current
    // matrix now, one at the top and the one right
    // bellow it. Multiply top by the new matrix
    // making the current one a combination of top + new.
    matrixStack.top() *= m;
}

const Matrix4 & RenderManager::GetTopMatrix() const
{
    assert(!matrixStack.empty() && "Matrix stack is empty!");
    return matrixStack.top();
}

void RenderManager::PopMatrix()
{
    assert(!matrixStack.empty() && "Matrix stack already empty!");
    matrixStack.pop();
}

// ======================================================
// Resource allocators:
// ======================================================

//
// Why the ugly make_shared_enabler hack?
// std::make_shared() is preferable over operator new
// because it can allocate the object and the reference
// counter in the same memory block, saving one allocation
// and keeping data together. Win-win.
//
// However, the renderer resources have private constructors,
// which would disable the use of std::make_shared().
// By creating this local hidden type we give the resource
// a "temporary" public constructor, which works fine.
// This was adapted from this SO thread:
// http://stackoverflow.com/questions/8147027/how-do-i-call-stdmake-shared-on-a-class-with-only-protected-or-private-const
//

ShaderProgramPtr RenderManager::NewShaderProgram()
{
    struct make_shared_enabler : public ShaderProgram
    {
    };
    return std::make_shared<make_shared_enabler>();
}

TexturePtr RenderManager::NewTexture()
{
    struct make_shared_enabler : public Texture
    {
    };
    return std::make_shared<make_shared_enabler>();
}

FontPtr RenderManager::NewFont()
{
    struct make_shared_enabler : public Font
    {
    };
    return std::make_shared<make_shared_enabler>();
}

MaterialPtr RenderManager::NewMaterial()
{
    struct make_shared_enabler : public Material
    {
    };
    return std::make_shared<make_shared_enabler>();
}

Model3DPtr RenderManager::NewModel3D()
{
    struct make_shared_enabler : public Model3D
    {
    };
    return std::make_shared<make_shared_enabler>();
}

// ======================================================
// Global RenderManager instance:
// ======================================================

static RenderManager theRenderManager;
RenderManager * renderMgr = &theRenderManager;

// ======================================================
// OpenGL error checking:
// ======================================================

#if GL_DO_ERROR_CHECKING

static const char * GLErrorToString(const GLenum errCode)
{
    switch (errCode)
    {
    case GL_NO_ERROR:
        return "GL_NO_ERROR";
    case GL_INVALID_ENUM:
        return "GL_INVALID_ENUM";
    case GL_INVALID_VALUE:
        return "GL_INVALID_VALUE";
    case GL_INVALID_OPERATION:
        return "GL_INVALID_OPERATION";
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        return "GL_INVALID_FRAMEBUFFER_OPERATION";
    case GL_OUT_OF_MEMORY:
        return "GL_OUT_OF_MEMORY";
    default:
        return "Unknown OpenGL error";
    } // switch (errCode)
}

void ClearGLErrors()
{
    while (glGetError() != GL_NO_ERROR)
    {
        // Spin...
    }
}

void HelperCheckGLErrors(const char * func, const char * file, const int line)
{
    GLenum errCode;
    int errCount = 0;

    while ((errCode = glGetError()) != GL_NO_ERROR)
    {
        common->WarningF("%d > OpenGL reported error: '%s' (0x%X) at file %s(%d), %s()",
                         errCount, GLErrorToString(errCode), errCode, file, line, func);
        ++errCount;
    }

#if GL_ERRORS_ARE_FATAL
    if (errCount > 0)
    {
        common->FatalErrorF("%d fatal OpenGL error(s) encountered!", errCount);
    }
#endif // GL_ERRORS_ARE_FATAL
}

#endif // GL_DO_ERROR_CHECKING

} // namespace Engine {}
