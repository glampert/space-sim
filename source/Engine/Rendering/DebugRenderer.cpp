
// ================================================================================================
// -*- C++ -*-
// File: DebugRenderer.cpp
// Author: Guilherme R. Lampert
// Created on: 18/09/14
// Brief: Debug rendering tools.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/Geometry/Frustum.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Font.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/SpriteBatchRenderer.hpp"
#include "Engine/Rendering/Basic2DTextRenderer.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/DebugRenderer.hpp"
#include "Engine/Rendering/OpenGL.hpp"

namespace Engine
{
namespace /* unnamed */
{

// ======================================================
// Private types:
// ======================================================

// Make it packed since we declare some pretty big arrays of these types.
#pragma pack(push, 1)

struct DebugText2D
{
    std::string str;     // Text string
    Vec2f pos;           // Staring position
    uint32_t durationMs; // Lifetime milliseconds
    Color4b color4b;     // Byte-packed RGBA color of text. Background is transparent
    // Default init:
    DebugText2D()
        : pos(0.0f)
        , durationMs(0)
        , color4b(uint8_t(0))
    {
    }
};

struct DebugLabel3D
{
    std::string str;     // Text string
    Vec3f pos;           // Staring position
    uint32_t durationMs; // Lifetime milliseconds
    Color4b color4b;     // Byte-packed RGBA color of text. Background is transparent
    // Default init:
    DebugLabel3D()
        : pos(0.0f)
        , durationMs(0)
        , color4b(uint8_t(0))
    {
    }
};

struct DebugPoint3D
{
    Vec3f pos;            // Point in space
    Color3f color3f;      // Float RGB color
    float size;           // Point size
    uint32_t durationMs;  // Lifetime milliseconds
    int16_t depthEnabled; // true|false
    // Default init:
    DebugPoint3D()
        : pos(0.0f)
        , color3f(0.0f)
        , size(0.0f)
        , durationMs(0)
        , depthEnabled(0)
    {
    }
};

struct DebugLine3D
{
    Vec3f from;           // Line start
    Vec3f to;             // Line end
    Color3f color3f;      // Float RGB color
    uint32_t durationMs;  // Lifetime milliseconds
    int16_t depthEnabled; // true|false
    // Default init:
    DebugLine3D()
        : from(0.0f)
        , to(0.0f)
        , color3f(0.0f)
        , durationMs(0)
        , depthEnabled(0)
    {
    }
};

// The only actual drawing type.
struct DebugVertex
{
    Vec3f position;     // Vertex position.
    Vec4f rgbPointSize; // xyz=RGB float vertex color; w=point size
    // Init with params:
    DebugVertex(const Vec3f & p, const Vec3f & c, const float pointSize)
        : position(p), rgbPointSize(c, pointSize) {}
};

#pragma pack(pop)

// ======================================================
// Private data:
// ======================================================

// Misc:
static bool sharedDataInitialized;
static Basic2DTextRenderer * localTextRenderer;
static unsigned int renderMode;      // GL render mode: LINES or POINTS
static float debugArrowStep = 60.0f; // Step size of arrow cone line rotation in degrees

// VBOs:
static VertexBuffer vertexBuffer;                    // GPU Vertex Buffer
static std::vector<DebugVertex> sysVertexBuf;        // System-side Vertex Buffer
static unsigned int maxDebugVertexBufferSize = 1024; // Max size of the VBOs. Initialized from script config

// Debug draw shader:
static ConstShaderProgramPtr debugShader;
static ShaderUniformHandle u_MVPMatrix = InvalidShaderUniformHandle;

// Max elements of each type at a given time.
// These are hard constraints. If not enough, change and recompile.
static constexpr int Max2DStrings = 256;
static constexpr int Max3DLabels  = 256;
static constexpr int Max3DPoints  = 8192;
static constexpr int Max3DLines   = 32768;

// Array of 2D debug strings:
static uint32_t numDebugStrings2D;
static uint32_t debugStrings2DTime;
static DebugText2D debugStrings2D[Max2DStrings];

// Array of 3D debug text labels:
static uint32_t numDebugLabels3D;
static uint32_t debugLabels3DTime;
static DebugLabel3D debugLabels3D[Max3DLabels];

// Array of 3D debug points:
static uint32_t numDebugPoints3D;
static uint32_t debugPoints3DTime;
static DebugPoint3D debugPoints3D[Max3DPoints];

// Array of 3D debug lines:
static uint32_t numDebugLines3D;
static uint32_t debugLines3DTime;
static DebugLine3D debugLines3D[Max3DLines];

// ======================================================
// ToColor4f() color storage conversion:
// ======================================================

static Color4f ToColor4f(const Color4b & color)
{
    return Color4f((color.r * (1.0f / 255.0f)),
                   (color.g * (1.0f / 255.0f)),
                   (color.b * (1.0f / 255.0f)),
                   (color.a * (1.0f / 255.0f)));
}

// ======================================================
// ToColor4b() color storage conversion:
// ======================================================

static Color4b ToColor4b(const Color4f & color)
{
    return Color4b(static_cast<uint8_t>(color.r * 255.0f),
                   static_cast<uint8_t>(color.g * 255.0f),
                   static_cast<uint8_t>(color.b * 255.0f),
                   static_cast<uint8_t>(color.a * 255.0f));
}

// ======================================================
// InitShaderProgram():
// ======================================================

static void InitShaderProgram()
{
    debugShader = checked_pointer_cast<const ShaderProgram>(
            resourceMgr->FindOrLoadResource("debug", Resource::Type::ShaderProgram));
    assert(debugShader != nullptr);

    u_MVPMatrix = debugShader->FindShaderUniform("u_MVPMatrix");
    if (u_MVPMatrix == InvalidShaderUniformHandle)
    {
        common->ErrorF("Failed to initialize DebugRenderer shader uniform variables!");
    }

    // We need this to be enabled since the shader might use gl_PointSize.
    glEnable(GL_PROGRAM_POINT_SIZE);
}

// ======================================================
// InitVertexBuffers():
// ======================================================

static void InitVertexBuffers()
{
    assert(maxDebugVertexBufferSize != 0);

    // Reserved vector memory:
    sysVertexBuf.reserve(maxDebugVertexBufferSize);

    // Allocate empty VBO. We re-set the data every time it has to be updated.
    vertexBuffer.AllocateVAO();
    vertexBuffer.AllocateVBO();

    // Set "vertex format":
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(DebugVertex), reinterpret_cast<const GLvoid *>(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(DebugVertex), reinterpret_cast<const GLvoid *>(sizeof(Vec3f)));

    vertexBuffer.UnbindVAO();
    vertexBuffer.UnbindVBO();
    CheckGLErrors();
}

// ======================================================
// InitSharedData():
// ======================================================

static void InitSharedData()
{
    assert(sharedDataInitialized == false);

    // Fetch configuration parameters:
    scriptMgr->GetConfigValue("rendererConfig.maxDebugVertexBufferSize", maxDebugVertexBufferSize);
    if (maxDebugVertexBufferSize == 0)
    {
        common->FatalErrorF("\'rendererConfig.maxDebugVertexBufferSize\' must not be zero!");
    }

    InitShaderProgram();
    InitVertexBuffers();

    sharedDataInitialized = true;
    common->PrintF("DebugRenderer shared data initialized. maxDebugVertexBufferSize = %u (%s)",
                   maxDebugVertexBufferSize, FormatMemoryUnit(maxDebugVertexBufferSize * sizeof(DebugVertex)).c_str());
}

// ======================================================
// Clear2DDebugText():
// ======================================================

static void Clear2DDebugText(const uint32_t timeMillisec)
{
    debugStrings2DTime = timeMillisec;
    if (timeMillisec == 0)
    {
        numDebugStrings2D = 0;
        return;
    }

    uint32_t num = 0;
    DebugText2D * text2d = debugStrings2D;
    // Concatenate text strings that still need to be draw on future frames:
    for (uint32_t i = 0; i < numDebugStrings2D; ++i, ++text2d)
    {
        if (text2d->durationMs > timeMillisec)
        {
            if (num != i)
            {
                debugStrings2D[num] = *text2d;
            }
            ++num;
        }
    }
    numDebugStrings2D = num;
}

// ======================================================
// Clear3DDebugLabels():
// ======================================================

static void Clear3DDebugLabels(const uint32_t timeMillisec)
{
    debugLabels3DTime = timeMillisec;
    if (timeMillisec == 0)
    {
        numDebugLabels3D = 0;
        return;
    }

    uint32_t num = 0;
    DebugLabel3D * label3d = debugLabels3D;
    // Concatenate labels that still need to be draw on future frames:
    for (uint32_t i = 0; i < numDebugLabels3D; ++i, ++label3d)
    {
        if (label3d->durationMs > timeMillisec)
        {
            if (num != i)
            {
                debugLabels3D[num] = *label3d;
            }
            ++num;
        }
    }
    numDebugLabels3D = num;
}

// ======================================================
// Clear3DDebugPoints():
// ======================================================

static void Clear3DDebugPoints(const uint32_t timeMillisec)
{
    debugPoints3DTime = timeMillisec;
    if (timeMillisec == 0)
    {
        numDebugPoints3D = 0;
        return;
    }

    uint32_t num = 0;
    DebugPoint3D * point3d = debugPoints3D;
    // Concatenate points that still need to be draw on future frames:
    for (uint32_t i = 0; i < numDebugPoints3D; ++i, ++point3d)
    {
        if (point3d->durationMs > timeMillisec)
        {
            if (num != i)
            {
                debugPoints3D[num] = *point3d;
            }
            ++num;
        }
    }
    numDebugPoints3D = num;
}

// ======================================================
// Clear3DDebugLines():
// ======================================================

static void Clear3DDebugLines(const uint32_t timeMillisec)
{
    debugLines3DTime = timeMillisec;
    if (timeMillisec == 0)
    {
        numDebugLines3D = 0;
        return;
    }

    uint32_t num = 0;
    DebugLine3D * line3d = debugLines3D;
    // Concatenate lines that still need to be draw on future frames:
    for (uint32_t i = 0; i < numDebugLines3D; ++i, ++line3d)
    {
        if (line3d->durationMs > timeMillisec)
        {
            if (num != i)
            {
                debugLines3D[num] = *line3d;
            }
            ++num;
        }
    }
    numDebugLines3D = num;
}

// ======================================================
// FlushDebugVertexes():
// ======================================================

static void FlushDebugVertexes()
{
    if (!sysVertexBuf.empty())
    {
        assert(sysVertexBuf.size() <= maxDebugVertexBufferSize);
        vertexBuffer.AllocateVBO(BufferStorage::StreamDraw, sysVertexBuf.size(), sizeof(DebugVertex), sysVertexBuf.data());
        glDrawArrays(renderMode, 0, sysVertexBuf.size());
        sysVertexBuf.clear();
    }
}

// ======================================================
// PushDebugVertex():
// ======================================================

static void PushDebugVertex(const Vec3f & position, const Vec3f & color, const float pointSize)
{
    if (sysVertexBuf.size() == maxDebugVertexBufferSize)
    {
        FlushDebugVertexes();
    }
    sysVertexBuf.emplace_back(position, color, pointSize);
}

// ======================================================
// Render2DDebugText():
// ======================================================

static void Render2DDebugText()
{
    if ((numDebugStrings2D == 0) || (localTextRenderer == nullptr))
    {
        return;
    }

    // 2D text will be left aligned.
    for (uint32_t i = 0; i < numDebugStrings2D; ++i)
    {
        const DebugText2D & text2d = debugStrings2D[i];
        localTextRenderer->DrawTextML(text2d.pos, ToColor4f(text2d.color4b), TextAlign::Left, text2d.str.c_str());
    }
}

// ======================================================
// Render3DDebugLabels():
// ======================================================

static void Render3DDebugLabels(const Matrix4 & view, const Matrix4 & projection)
{
    if ((numDebugLabels3D == 0) || (localTextRenderer == nullptr))
    {
        return;
    }

    // Center aligned is probably the most reasonable
    // option for most text drawing applications.
    constexpr TextAlign textAlign = TextAlign::Center;

    const Frustum frustum(view, projection);
    const Vec4u rect(renderMgr->GetFramebufferRect());

    for (uint32_t i = 0; i < numDebugLabels3D; ++i)
    {
        const DebugLabel3D & label3d = debugLabels3D[i];

        // Draw if visible:
        if (frustum.TestPoint(label3d.pos))
        {
            // Project from 3D to screen:
            Vector3 windowCoordinates;
            Project(label3d.pos.x, label3d.pos.y, label3d.pos.z, view, projection, rect, windowCoordinates);

            // Add to draw queue:
            localTextRenderer->DrawTextML(Vec2f(windowCoordinates[0], windowCoordinates[1]),
                                          ToColor4f(label3d.color4b), textAlign, label3d.str.c_str());
        }
    }
}

// ======================================================
// Render3DDebugPoints():
// ======================================================

static void Render3DDebugPoints()
{
    if (numDebugPoints3D == 0)
    {
        return;
    }

    renderMode = GL_POINTS;

    // First pass, points with depth test on:
    int numDepthlessPoints = 0;
    renderMgr->EnableDepthTest();
    for (uint32_t i = 0; i < numDebugPoints3D; ++i)
    {
        const DebugPoint3D & point3d = debugPoints3D[i];
        if (point3d.depthEnabled)
        {
            PushDebugVertex(point3d.pos, point3d.color3f, point3d.size);
        }
        numDepthlessPoints += !point3d.depthEnabled;
    }
    FlushDebugVertexes();

    // Second pass draws points with depth disabled:
    if (numDepthlessPoints > 0)
    {
        renderMgr->DisableDepthTest();
        for (uint32_t i = 0; i < numDebugPoints3D; ++i)
        {
            const DebugPoint3D & point3d = debugPoints3D[i];
            if (!point3d.depthEnabled)
            {
                PushDebugVertex(point3d.pos, point3d.color3f, point3d.size);
            }
        }
        FlushDebugVertexes();
    }
}

// ======================================================
// Render3DDebugLines():
// ======================================================

static void Render3DDebugLines()
{
    if (numDebugLines3D == 0)
    {
        return;
    }

    renderMode = GL_LINES;

    // First pass, lines with depth test on:
    int numDepthlessLines = 0;
    renderMgr->EnableDepthTest();
    for (uint32_t i = 0; i < numDebugLines3D; ++i)
    {
        const DebugLine3D & line3d = debugLines3D[i];
        if (line3d.depthEnabled)
        {
            PushDebugVertex(line3d.from, line3d.color3f, 0.0f);
            PushDebugVertex(line3d.to, line3d.color3f, 0.0f);
        }
        numDepthlessLines += !line3d.depthEnabled;
    }
    FlushDebugVertexes();

    // Second pass draws lines with depth disabled:
    if (numDepthlessLines > 0)
    {
        renderMgr->DisableDepthTest();
        for (uint32_t i = 0; i < numDebugLines3D; ++i)
        {
            const DebugLine3D & line3d = debugLines3D[i];
            if (!line3d.depthEnabled)
            {
                PushDebugVertex(line3d.from, line3d.color3f, 0.0f);
                PushDebugVertex(line3d.to, line3d.color3f, 0.0f);
            }
        }
        FlushDebugVertexes();
    }
}

} // namespace unnamed {}

// ======================================================
// DebugRenderer:
// ======================================================

DebugRenderer::DebugRenderer(Basic2DTextRenderer * textRenderer)
{
    // First instance ever created will setup the local shared data.
    if (!sharedDataInitialized)
    {
        InitSharedData();
    }

    SetTextRenderer(textRenderer);
}

void DebugRenderer::AddPoint(const Vec3f point, const Color4f color, const float size, const uint32_t durationMs, const bool depthEnabled)
{
    if (numDebugPoints3D < Max3DPoints)
    {
        DebugPoint3D & point3d = debugPoints3D[numDebugPoints3D++];
        point3d.pos = point;
        point3d.color3f = Color3f(color.data);
        point3d.size = size;
        point3d.durationMs = debugPoints3DTime + durationMs;
        point3d.depthEnabled = int16_t(depthEnabled);
    }
    else
    {
        // Avoid printing the message forever
        static int timesNotified = 0;
        if (timesNotified++ < 10)
        {
            common->WarningF("\'Max3DPoints\' (%d) limit reached! Dropping further debug point drawing requests...", Max3DPoints);
        }
    }
}

void DebugRenderer::AddLine(const Vec3f from, const Vec3f to, const Color4f color, const uint32_t durationMs, const bool depthEnabled)
{
    if (numDebugLines3D < Max3DLines)
    {
        DebugLine3D & line3d = debugLines3D[numDebugLines3D++];
        line3d.from = from;
        line3d.to = to;
        line3d.color3f = Color3f(color.data);
        line3d.durationMs = debugLines3DTime + durationMs;
        line3d.depthEnabled = int16_t(depthEnabled);
    }
    else
    {
        // Avoid printing the message forever
        static int timesNotified = 0;
        if (timesNotified++ < 10)
        {
            common->WarningF("\'Max3DLines\' (%d) limit reached! Dropping further debug line drawing requests...", Max3DLines);
        }
    }
}

void DebugRenderer::Add2DTextF(const Vec2f pos, const Color4f color, const uint32_t durationMs, const char * format, ...)
{
    assert(format != nullptr);

    if (numDebugStrings2D < Max2DStrings)
    {
        va_list vaList;
        char tempStr[MaxTempStringLen];

        va_start(vaList, format);
        const int count = std::vsnprintf(tempStr, ArrayLength(tempStr), format, vaList);
        va_end(vaList);

        if (count > 0)
        {
            // Ensure null terminated:
            if (count >= MaxTempStringLen)
            {
                tempStr[MaxTempStringLen - 1] = '\0'; // Truncate the string.
            }

            DebugText2D & text2d = debugStrings2D[numDebugStrings2D++];
            text2d.str = tempStr;
            text2d.pos = pos;
            text2d.durationMs = debugStrings2DTime + durationMs;
            text2d.color4b = ToColor4b(color);
        }
    }
    else
    {
        // Avoid printing the message forever
        static int timesNotified = 0;
        if (timesNotified++ < 10)
        {
            common->WarningF("\'Max2DStrings\' (%d) limit reached! Dropping further debug text drawing requests...", Max2DStrings);
        }
    }
}

void DebugRenderer::Add3DLabel(std::string str, const Vec3f pos, const Color4f color, const uint32_t durationMs)
{
    if ((numDebugLabels3D < Max3DLabels) && !str.empty())
    {
        DebugLabel3D & label3d = debugLabels3D[numDebugLabels3D++];
        label3d.str = std::move(str);
        label3d.pos = pos;
        label3d.durationMs = debugLabels3DTime + durationMs;
        label3d.color4b = ToColor4b(color);
    }
    else
    {
        // Avoid printing the message forever
        static int timesNotified = 0;
        if (timesNotified++ < 10)
        {
            common->WarningF("\'Max3DLabels\' (%d) limit reached! Dropping further 3D debug text label drawing requests...", Max3DLabels);
        }
    }
}

void DebugRenderer::AddArrow(const Vec3f from, const Vec3f to, const Color4f color, const float size, const uint32_t durationMs, const bool depthEnabled)
{
    static float arrowCos[45];
    static float arrowSin[45];
    static float arrowStep;
    float a, s;

    // Calculate sine and cosine when step size changes:
    if (arrowStep != debugArrowStep)
    {
        int i;
        arrowStep = debugArrowStep;
        for (i = 0, a = 0; a < 360.0f; a += arrowStep, ++i)
        {
            arrowCos[i] = std::cos(a * MathConst::DegToRad);
            arrowSin[i] = std::sin(a * MathConst::DegToRad);
        }
        arrowCos[i] = arrowCos[0];
        arrowSin[i] = arrowSin[0];
    }

    // Body line:
    AddLine(from, to, color, durationMs, depthEnabled);

    // Aux vectors:
    Vec3f right, up, v1, v2;
    const Vec3f forward = normalize(to - from);
    NormalVectors(forward, right, up);

    // Arrow head as a cone (size is used here):
    for (int i = 0, a = 0; a < 360.0f; a += arrowStep, i++)
    {
        s = 0.5f * size * arrowCos[i];
        v1 = to - size * forward;
        v1 = v1 + s * right;
        s = 0.5f * size * arrowSin[i];
        v1 = v1 + s * up;

        s = 0.5f * size * arrowCos[i + 1];
        v2 = to - size * forward;
        v2 = v2 + s * right;
        s = 0.5f * size * arrowSin[i + 1];
        v2 = v2 + s * up;

        AddLine(v1, to, color, durationMs, depthEnabled);
        AddLine(v1, v2, color, durationMs, depthEnabled);
    }
}

void DebugRenderer::AddCross(const Vec3f point, const float length, const uint32_t durationMs, const bool depthEnabled)
{
    // Shortcut variables:
    const float px = point.x;
    const float py = point.y;
    const float pz = point.z;

    // X - length to X + length:
    AddLine(Vec3f(px - length, py, pz), Vec3f(px + length, py, pz), Color4f(1.0f, 0.0f, 0.0f, 1.0f), durationMs, depthEnabled); // red

    // Y - length to Y + length:
    AddLine(Vec3f(px, py - length, pz), Vec3f(px, py + length, pz), Color4f(0.0f, 1.0f, 0.0f, 1.0f), durationMs, depthEnabled); // green

    // Z - length to Z + length:
    AddLine(Vec3f(px, py, pz - length), Vec3f(px, py, pz + length), Color4f(0.0f, 0.0f, 1.0f, 1.0f), durationMs, depthEnabled); // blue
}

void DebugRenderer::AddAxisTriad(const Matrix4 & transform, const float size, const float length, const uint32_t durationMs, const bool depthEnabled)
{
    // If 'length' is positive, the arrows will point to the positive side of each axis.
    Vector4 v1;
    const Vector4 v2(transform * Point3(0.0f, 0.0f, 0.0f));
    const Vec3f origin(v2[0], v2[1], v2[2]);

    const Point3 xEnd(length, 0.0f, 0.0f);
    v1 = (transform * xEnd);
    AddArrow(origin, Vec3f(v1[0], v1[1], v1[2]), Color4f(1.0f, 0.0f, 0.0f, 1.0f), size, durationMs, depthEnabled); // red

    const Point3 yEnd(0.0f, length, 0.0f);
    v1 = (transform * yEnd);
    AddArrow(origin, Vec3f(v1[0], v1[1], v1[2]), Color4f(0.0f, 1.0f, 0.0f, 1.0f), size, durationMs, depthEnabled); // green

    const Point3 zEnd(0.0f, 0.0f, length);
    v1 = (transform * zEnd);
    AddArrow(origin, Vec3f(v1[0], v1[1], v1[2]), Color4f(0.0f, 0.0f, 1.0f, 1.0f), size, durationMs, depthEnabled); // blue
}

void DebugRenderer::AddCircle(const Vec3f center, const Vec3f planeNormal, const Color4f color, const float radius,
                              const float numSteps, const uint32_t durationMs, const bool depthEnabled)
{
    float a;
    Vec3f left, up, point, lastPoint;

    OrthogonalBasis(planeNormal, left, up);

    up = (up * radius);
    left = (left * radius);
    lastPoint = (center + up);

    for (int i = 1; i <= numSteps; ++i)
    {
        a = MathConst::TwoPI * i / numSteps;
        point = center + std::sin(a) * left + std::cos(a) * up;
        AddLine(lastPoint, point, color, durationMs, depthEnabled);
        lastPoint = point;
    }
}

void DebugRenderer::AddPlane(const Vec3f center, const Vec3f planeNormal, const Color4f planeColor, const Color4f normalVecColor,
                             const float planeScale, const float normalVecScale, const uint32_t durationMs, const bool depthEnabled)
{
    Vec3f tangent, bitangent;
    OrthogonalBasis(planeNormal, tangent, bitangent);

    const Vec3f v1(center - (tangent * planeScale) - (bitangent * planeScale));
    const Vec3f v2(center + (tangent * planeScale) - (bitangent * planeScale));
    const Vec3f v3(center + (tangent * planeScale) + (bitangent * planeScale));
    const Vec3f v4(center - (tangent * planeScale) + (bitangent * planeScale));

    // Draw wireframe plane quadrilateral:
    AddLine(v1, v2, planeColor, durationMs, depthEnabled);
    AddLine(v2, v3, planeColor, durationMs, depthEnabled);
    AddLine(v3, v4, planeColor, durationMs, depthEnabled);
    AddLine(v4, v1, planeColor, durationMs, depthEnabled);

    // Optionally And a line depicting the plane normal:
    if (normalVecScale != 0.0f)
    {
        const Vec3f vn(
        (center.x + planeNormal.x * normalVecScale),
        (center.y + planeNormal.y * normalVecScale),
        (center.z + planeNormal.z * normalVecScale));
        AddLine(center, vn, normalVecColor, durationMs, depthEnabled);
    }
}

void DebugRenderer::AddSphere(const Vec3f center, const Color4f color, const float radius, const uint32_t durationMs, const bool depthEnabled)
{
    float s, c;
    Vec3f p, lastp;

    Vec3f lastArray[360 / 15];
    lastArray[0] = center + Vec3f(0.0f, 0.0f, radius);
    for (size_t n = 1; n < ArrayLength(lastArray); ++n)
    {
        lastArray[n] = lastArray[0];
    }

    for (int i = 15; i <= 360; i += 15)
    {
        s = std::sin(i * MathConst::DegToRad);
        c = std::cos(i * MathConst::DegToRad);

        lastp.x = center.x;
        lastp.y = center.y + radius * s;
        lastp.z = center.z + radius * c;

        int n, j;
        for (n = 0, j = 15; j <= 360; j += 15, ++n)
        {
            p.x = center.x + std::sin(j * MathConst::DegToRad) * radius * s;
            p.y = center.y + std::cos(j * MathConst::DegToRad) * radius * s;
            p.z = lastp.z;

            AddLine(lastp, p, color, durationMs, depthEnabled);
            AddLine(lastp, lastArray[n], color, durationMs, depthEnabled);

            lastArray[n] = lastp;
            lastp = p;
        }
    }
}

void DebugRenderer::AddCube(const Vec3f center, const Color4f color, const float width, const float height,
                            const float depth, const uint32_t durationMs, const bool depthEnabled)
{
    // Shortcut variables:
    const float cx = center.x;
    const float cy = center.y;
    const float cz = center.x;
    const float w = width  / 2.0f;
    const float h = height / 2.0f;
    const float d = depth  / 2.0f;

    // Create all the 8 points:
    const Vec3f topLeftFront(cx - w, cy + h, cz + d);
    const Vec3f topLeftBack(cx - w, cy + h, cz - d);
    const Vec3f topRightBack(cx + w, cy + h, cz - d);
    const Vec3f topRightFront(cx + w, cy + h, cz + d);
    const Vec3f bottomLeftFront(cx - w, cy - h, cz + d);
    const Vec3f bottomLeftBack(cx - w, cy - h, cz - d);
    const Vec3f bottomRightBack(cx + w, cy - h, cz - d);
    const Vec3f bottomRightFront(cx + w, cy - h, cz + d);

    // Add lines:
    AddLine(topLeftFront, topRightFront, color, durationMs, depthEnabled);
    AddLine(topLeftBack, topRightBack, color, durationMs, depthEnabled);
    AddLine(topLeftFront, topLeftBack, color, durationMs, depthEnabled);
    AddLine(topRightFront, topRightBack, color, durationMs, depthEnabled);
    AddLine(bottomLeftFront, bottomRightFront, color, durationMs, depthEnabled);
    AddLine(bottomLeftBack, bottomRightBack, color, durationMs, depthEnabled);
    AddLine(bottomLeftFront, bottomLeftBack, color, durationMs, depthEnabled);
    AddLine(bottomRightFront, bottomRightBack, color, durationMs, depthEnabled);
    AddLine(topLeftFront, bottomLeftFront, color, durationMs, depthEnabled);
    AddLine(topLeftBack, bottomLeftBack, color, durationMs, depthEnabled);
    AddLine(topRightBack, bottomRightBack, color, durationMs, depthEnabled);
    AddLine(topRightFront, bottomRightFront, color, durationMs, depthEnabled);
}

void DebugRenderer::AddCone(const Vec3f apex, const Vec3f dir, const Color4f color, const float baseRadius,
                            const float apexRadius, const uint32_t durationMs, const bool depthEnabled)
{
    int i;
    Vec3f axis[3];
    Vec3f top, p1, p2, lastp1, lastp2, d;

    axis[2] = dir;
    axis[2] = normalize(axis[2]);
    NormalVectors(axis[2], axis[0], axis[1]);
    axis[1] = Vec3f(-axis[1].x, -axis[1].y, -axis[1].z);

    top = apex + dir;
    lastp2 = top + baseRadius * axis[1];

    if (apexRadius == 0.0f)
    {
        for (i = 20; i <= 360; i += 20)
        {
            d = std::sin(i * MathConst::DegToRad) * axis[0] + std::cos(i * MathConst::DegToRad) * axis[1];
            p2 = top + d * baseRadius;

            AddLine(lastp2, p2, color, durationMs, depthEnabled);
            AddLine(p2, apex, color, durationMs, depthEnabled);

            lastp2 = p2;
        }
    }
    else
    {
        lastp1 = apex + apexRadius * axis[1];
        for (i = 20; i <= 360; i += 20)
        {
            d = std::sin(i * MathConst::DegToRad) * axis[0] + std::cos(i * MathConst::DegToRad) * axis[1];
            p1 = apex + d * apexRadius;
            p2 = top + d * baseRadius;

            AddLine(lastp1, p1, color, durationMs, depthEnabled);
            AddLine(lastp2, p2, color, durationMs, depthEnabled);
            AddLine(p1, p2, color, durationMs, depthEnabled);

            lastp1 = p1;
            lastp2 = p2;
        }
    }
}

void DebugRenderer::AddAABB(const AABB & aabb, const Color4f color, const uint32_t durationMs, const bool depthEnabled)
{
    Vector3 points[8];
    aabb.ToPoints(points);

    for (int i = 0; i < 4; ++i)
    {
        AddLine(points[i], points[(i + 1) & 3], color, durationMs, depthEnabled);
        AddLine(points[4 + i], points[4 + ((i + 1) & 3)], color, durationMs, depthEnabled);
        AddLine(points[i], points[4 + i], color, durationMs, depthEnabled);
    }
}

void DebugRenderer::AddFrustum(const Frustum & frustum, const Color4f color, const uint32_t durationMs, const bool depthEnabled)
{
    const Vector4 planes[8] =
    {
        { -1, -1, -1, 1 }, { 1, -1, -1, 1 }, { 1, 1, -1, 1 }, { -1, 1, -1, 1 }, // Near
        { -1, -1,  1, 1 }, { 1, -1,  1, 1 }, { 1, 1,  1, 1 }, { -1, 1,  1, 1 }  // Far
    };

    Vector4 points[8];
    const Matrix4 inv = inverse(frustum.clipMatrix); // inverse() is expensive, be we don't draw that often...

    for (int i = 0; i < 8; ++i)
    {
        points[i] = (inv * planes[i]);
    }

    for (int i = 0; i < 8; ++i)
    {
        points[i][0] /= points[i][3];
        points[i][1] /= points[i][3];
        points[i][2] /= points[i][3];
        points[i][3] = 1.0f;
    }

    // Connect the dots:
    // 0-1, 1-2, 2-3, 3-0
    // 4-5, 5-6, 6-7, 7-4
    // 0-4, 1-5, 2-6, 3-7
    //
    AddLine(Vec3f(points[0]), Vec3f(points[1]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[1]), Vec3f(points[2]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[2]), Vec3f(points[3]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[3]), Vec3f(points[0]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[4]), Vec3f(points[5]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[5]), Vec3f(points[6]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[6]), Vec3f(points[7]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[7]), Vec3f(points[4]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[0]), Vec3f(points[4]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[1]), Vec3f(points[5]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[2]), Vec3f(points[6]), color, durationMs, depthEnabled);
    AddLine(Vec3f(points[3]), Vec3f(points[7]), color, durationMs, depthEnabled);
}

void DebugRenderer::AddVertexNormal(const Vec3f point, const Vec3f norm, const float size, const uint32_t durationMs, const bool depthEnabled)
{
    // Vertex normals are WHITE:
    const Vec3f vn((point.x + norm.x * size), (point.y + norm.y * size), (point.z + norm.z * size));
    AddLine(point, vn, Color4f(1.0f), durationMs, depthEnabled);
}

void DebugRenderer::AddTangentBasis(const Vec3f point, const Vec3f norm, const Vec3f tang, const Vec3f bitang,
                                    const float sizes, const uint32_t durationMs, const bool depthEnabled)
{
    // Vertex normals are WHITE:
    const Vec3f vn((point.x + norm.x * sizes), (point.y + norm.y * sizes), (point.z + norm.z * sizes));
    AddLine(point, vn, Color4f(1.0f), durationMs, depthEnabled);

    // Tangents are YELLOW:
    const Vec3f vt((point.x + tang.x * sizes), (point.y + tang.y * sizes), (point.z + tang.z * sizes));
    AddLine(point, vt, Color4f(1.0f, 1.0f, 0.0f, 1.0f), durationMs, depthEnabled);

    // Bi-tangents are MAGENTA:
    const Vec3f vb((point.x + bitang.x * sizes), (point.y + bitang.y * sizes), (point.z + bitang.z * sizes));
    AddLine(point, vb, Color4f(1.0f, 0.0f, 1.0f, 1.0f), durationMs, depthEnabled);
}

void DebugRenderer::SetTextRenderer(Basic2DTextRenderer * textRenderer)
{
    localTextRenderer = textRenderer;
}

void DebugRenderer::SetDebugArrowStep(const float stepDegrees)
{
    debugArrowStep = stepDegrees;
}

bool DebugRenderer::HasDebugDataToDraw() const
{
    return (numDebugStrings2D + numDebugLabels3D + numDebugPoints3D + numDebugLines3D) > 0;
}

void DebugRenderer::FlushDebugRenderData(const GameTime time, const Matrix4 & view, const Matrix4 & projection, const Matrix4 * mvp)
{
    if (!HasDebugDataToDraw())
    {
        return;
    }

    // Set render states:
    renderMgr->DisableColorBlend();
    renderMgr->DisablePolygonCull();
    renderMgr->EnableDepthWrite();

    // Set GPU Vertex Buffer for update and drawing:
    vertexBuffer.BindVAO();
    vertexBuffer.BindVBO();

    // Set shader and uniform variables:
    assert(debugShader != nullptr);
    debugShader->Bind();
    debugShader->SetShaderUniform(u_MVPMatrix, ((mvp != nullptr) ? *mvp : (projection * view)));

    // Issue render calls:
    Render3DDebugLines();
    Render3DDebugPoints();
    Render3DDebugLabels(view, projection);
    Render2DDebugText();

    // Restore/cleanup:
    vertexBuffer.UnbindVAO();
    vertexBuffer.UnbindVBO();

    renderMgr->EnablePolygonCull();
    renderMgr->EnableDepthTest(); // Some of the Render*() functions might have disabled it.

    // Clear expired objects:
    Clear2DDebugText(time.currentTime.milliseconds);
    Clear3DDebugLabels(time.currentTime.milliseconds);
    Clear3DDebugPoints(time.currentTime.milliseconds);
    Clear3DDebugLines(time.currentTime.milliseconds);
}

void DebugRenderer::ClearAllDebugData()
{
    Clear2DDebugText(0);
    Clear3DDebugLabels(0);
    Clear3DDebugPoints(0);
    Clear3DDebugLines(0);
    sysVertexBuf.clear();
}

void DebugRenderer::ShutdownSharedData()
{
    // Free GPU vertex buffer:
    vertexBuffer.FreeVAO();
    vertexBuffer.FreeVBO();

    // And system-side VB:
    sysVertexBuf.clear();
    sysVertexBuf.shrink_to_fit();

    // Reset local vars:
    localTextRenderer     = nullptr;
    debugShader           = nullptr;
    u_MVPMatrix           = InvalidShaderUniformHandle;
    numDebugStrings2D     = 0;
    debugStrings2DTime    = 0;
    numDebugLabels3D      = 0;
    debugLabels3DTime     = 0;
    numDebugPoints3D      = 0;
    debugPoints3DTime     = 0;
    numDebugLines3D       = 0;
    debugLines3DTime      = 0;
    sharedDataInitialized = false;
}

} // namespace Engine {}
