
// ================================================================================================
// -*- C++ -*-
// File: Basic2DTextRenderer.cpp
// Author: Guilherme R. Lampert
// Created on: 13/09/14
// Brief: Basic 2D text renderer helper. Can be inherited by other classes.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Font.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/SpriteBatchRenderer.hpp"
#include "Engine/Rendering/Basic2DTextRenderer.hpp"

namespace Engine
{

// ======================================================
// Basic2DTextRenderer:
// ======================================================

//
// Font drawing code adapted from:
//   "AngelCode Tool Box Library"
//   Copyright (c) 2007-2008 Andreas Jonsson
// Found here:
//   http://www.angelcode.com/dev/bmfonts/
//
Basic2DTextRenderer::Basic2DTextRenderer(const Font & fnt, SpriteBatchRenderer & batch, const SpriteLayer layer)
    : font(fnt)
    , spriteBatch(batch)
    , textLayer(layer)
{
}

void Basic2DTextRenderer::DrawText(const Vec2f pos, const Color4f color, const TextAlign align, const char * text)
{
    assert(text != nullptr);

    const int textLength = font.GetTextLength(text);
    float x = pos.x;
    float y = pos.y;

    if (align == TextAlign::Center)
    {
        x -= (GetTextWidth(text, textLength) / 2.0f);
    }
    else if (align == TextAlign::Right)
    {
        x -= GetTextWidth(text, textLength);
    }

    InternalWrite(x, y, color, text, textLength);
}

void Basic2DTextRenderer::DrawTextML(const Vec2f pos, const Color4f color, const TextAlign align, const char * text)
{
    assert(text != nullptr);

    const float fontHeight = font.GetHeight();
    const int textLength = font.GetTextLength(text);

    float x = pos.x;
    float y = pos.y;
    int linePos = 0;

    // Get first line:
    int len = font.FindTextChar(text, linePos, textLength, '\n');
    if (len == -1)
    {
        len = textLength;
    }

    while (linePos < textLength)
    {
        float newX = x;
        if (align == TextAlign::Center)
        {
            newX -= (GetTextWidth(&text[linePos], len) / 2.0f);
        }
        else if (align == TextAlign::Right)
        {
            newX -= GetTextWidth(&text[linePos], len);
        }

        InternalWrite(newX, y, color, &text[linePos], len);
        y -= fontHeight;

        // Get next line:
        linePos += len;
        const int ch = font.GetTextChar(text, linePos, &linePos);
        if (ch == '\n')
        {
            len = font.FindTextChar(text, linePos, textLength, '\n');
            if (len == -1)
            {
                len = (textLength - linePos);
            }
            else
            {
                len = (len - linePos);
            }
        }
    }
}

void Basic2DTextRenderer::DrawTextMaxWidth(const Vec2f pos, const float maxWidth, const Color4f color, const TextAlign align, const char * text)
{
    assert(text != nullptr);
    assert(maxWidth > 0.0f);

    const float fontHeight = font.GetHeight();
    const float spaceWidth = GetTextWidth(" ", 1);
    const int textLength = font.GetTextLength(text);

    float x = pos.x;
    float y = pos.y;
    float currWidth = 0.0f;
    float wordWidth = 0.0f;
    int lineS = 0;
    int lineE = 0;
    int wordS = 0;
    int wordE = 0;
    int wordCount = 0;
    bool softBreak = false;

    for (; lineS < textLength;)
    {
        // Determine the extent of the line:
        for (;;)
        {
            // Determine the number of characters in the word:
            while ((wordE < textLength) && (font.GetTextChar(text, wordE) != ' ') && (font.GetTextChar(text, wordE) != '\n'))
            {
                // Advance the cursor to the next character.
                font.GetTextChar(text, wordE, &wordE);
            }

            // Determine the width of the word:
            if (wordE > wordS)
            {
                wordCount++;
                wordWidth = GetTextWidth(&text[wordS], wordE - wordS);
            }
            else
            {
                wordWidth = 0.0f;
            }

            // Does the word fit on the line? The first word is always accepted.
            if ((wordCount == 1) || (currWidth + (wordCount > 1 ? spaceWidth : 0) + wordWidth <= maxWidth))
            {
                // Increase the line extent to the end of the word:
                lineE = wordE;
                currWidth += (wordCount > 1 ? spaceWidth : 0) + wordWidth;

                // Did we reach the end of the line?
                if ((wordE == textLength) || (font.GetTextChar(text, wordE) == '\n'))
                {
                    softBreak = false;
                    // Skip the newline character.
                    if (wordE < textLength)
                    {
                        // Advance the cursor to the next character:
                        font.GetTextChar(text, wordE, &wordE);
                    }
                    break;
                }

                // Skip the trailing space:
                if ((wordE < textLength) && (font.GetTextChar(text, wordE) == ' '))
                {
                    // Advance the cursor to the next character:
                    font.GetTextChar(text, wordE, &wordE);
                }

                // Move to next word.
                wordS = wordE;
            }
            else
            {
                softBreak = true;
                // Skip the trailing space:
                if ((wordE < textLength) && (font.GetTextChar(text, wordE) == ' '))
                {
                    // Advance the cursor to the next character:
                    font.GetTextChar(text, wordE, &wordE);
                }
                break;
            }
        }

        // Write the line
        if (align == TextAlign::Justify)
        {
            float spacing = 0.0f;
            if (softBreak)
            {
                if (wordCount > 2)
                {
                    spacing = (maxWidth - currWidth) / (wordCount - 2);
                }
                else
                {
                    spacing = (maxWidth - currWidth);
                }
            }

            InternalWrite(x, y, color, &text[lineS], lineE - lineS, spacing);
        }
        else
        {
            float newX = x;
            if (align == TextAlign::Right)
            {
                newX = x + maxWidth - currWidth;
            }
            else if (align == TextAlign::Center)
            {
                newX = x + 0.5f * (maxWidth - currWidth);
            }

            InternalWrite(newX, y, color, &text[lineS], lineE - lineS);
        }

        if (softBreak)
        {
            // Skip the trailing space:
            if ((lineE < textLength) && (font.GetTextChar(text, lineE) == ' '))
            {
                // Advance the cursor to the next character:
                font.GetTextChar(text, lineE, &lineE);
            }
            // We've already counted the first word on the next line.
            wordCount = 1;
            currWidth = wordWidth;
        }
        else
        {
            // Skip the line break:
            if ((lineE < textLength) && (font.GetTextChar(text, lineE) == '\n'))
            {
                // Advance the cursor to the next character:
                font.GetTextChar(text, lineE, &lineE);
            }
            wordCount = 0;
            currWidth = 0.0f;
        }

        // Move to the next line:
        lineS = lineE;
        wordS = wordE;
        y -= fontHeight;
    }
}

float Basic2DTextRenderer::GetTextWidth(const char * text, int textLength) const
{
    assert(text != nullptr);

    if (textLength <= 0)
    {
        textLength = font.GetTextLength(text);
    }

    float x = 0.0f;
    for (int n = 0; n < textLength;)
    {
        const int charId = font.GetTextChar(text, n, &n);
        const Font::GlyphInfo & glyph = font.GetGlyphInfo(charId);

        x += font.GetScale() * glyph.xAdvance;
        if (n < textLength)
        {
            x += font.AdjustForKerningPairs(charId, font.GetTextChar(text, n));
        }
    }

    return x;
}

Matrix4 Basic2DTextRenderer::GetFullscreenOrthoMatrix()
{
    const Vec2u fb = renderMgr->GetFramebufferDimensions();
    return Matrix4::orthographic(
        /* left   = */ 0.0f,
        /* right  = */ static_cast<float>(fb.width),
        /* bottom = */ 0.0f,
        /* top    = */ static_cast<float>(fb.height),
        /* zNear  = */ -1.0f,
        /* zFar   = */ 1.0f);
}

void Basic2DTextRenderer::InternalWrite(float x, float y, const Color4f color, const char * text, const int textLength, const float spacing)
{
    assert(text != nullptr);
    if (textLength == 0)
    {
        return;
    }

    // Indexes of a quadrilateral made of two triangles:
    const uint16_t indexes[] = { 0, 1, 2, 2, 3, 0 };

    // Glyph quadrilateral:
    SpriteVertex verts[4];

    // Shortcut variables:
    const float scale  = font.GetScale();
    const float scaleW = font.GetScaleW();
    const float scaleH = font.GetScaleH();
    y += font.GetTopOffset();

    for (int n = 0; n < textLength;)
    {
        const int charId = font.GetTextChar(text, n, &n);
        const Font::GlyphInfo & glyph = font.GetGlyphInfo(charId);

        // Map the center of the pixel to the corners
        // in order to get pixel perfect mapping.
        const float u0 = (static_cast<float>(glyph.x) + 0.5f) / scaleW;
        const float v0 = (static_cast<float>(glyph.y) + 0.5f) / scaleH;
        const float u1 = u0 + static_cast<float>(glyph.width) / scaleW;
        const float v1 = v0 + static_cast<float>(glyph.height) / scaleH;
        const float a = scale * static_cast<float>(glyph.xAdvance);
        const float w = scale * static_cast<float>(glyph.width);
        const float h = scale * static_cast<float>(glyph.height);
        const float ox = scale * static_cast<float>(glyph.xOffset);
        const float oy = scale * static_cast<float>(glyph.yOffset);

        // Glyph page texture:
        ConstTexturePtr pageTex = font.GetGlyphPageTexture(glyph.pageNum);

        // Set up vertexes:
        verts[0].positionTexCoords.Set(x + ox, y - oy, u0, v0);
        verts[1].positionTexCoords.Set(x + w + ox, y - oy, u1, v0);
        verts[2].positionTexCoords.Set(x + w + ox, y - h - oy, u1, v1);
        verts[3].positionTexCoords.Set(x + ox, y - h - oy, u0, v1);

        // And color:
        verts[0].color = color;
        verts[1].color = color;
        verts[2].color = color;
        verts[3].color = color;

        // Add to sprite batch:
        spriteBatch.DrawTriangles(verts, ArrayLength(verts), indexes, ArrayLength(indexes), pageTex, textLayer);

        // Adjust position for next char:
        x += a;
        if (charId == ' ')
        {
            x += spacing;
        }
        if (n < textLength)
        {
            x += font.AdjustForKerningPairs(charId, font.GetTextChar(text, n));
        }
    }
}

} // namespace Engine {}
