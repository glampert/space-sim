
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Model3DImporter.hpp
// Author: Guilherme R. Lampert
// Created on: 10/08/14
// Brief: Thin wrapper to the ASSIMP mesh importer library that operates on Model3D.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// ASSIMP Library:
#include "ThirdParty/assimp3/include/assimp/cimport.h"     // C importer interface.
#include "ThirdParty/assimp3/include/assimp/version.h"     // Library version, for printing
#include "ThirdParty/assimp3/include/assimp/postprocess.h" // Post processing flags
#include "ThirdParty/assimp3/include/assimp/scene.h"       // Importer output data structure

namespace Engine
{

// ======================================================
// Model3DImporter singleton:
// ======================================================

//
// Helper class used internally by Model3D.
// The importer relies on the ASSIMP library to load mesh data from file.
//
// This class is not directly instantiated.
// All of its methods are static and construction is private.
//
class Model3DImporter final
{
  public:

    // Import a model from a file on disk.
    static bool ImportModel3DFromFile(const std::string & filename, Model3D & mdl);

    // Import a model from a memory data blob. The blob is usually the unchanged contents of a file loaded into memory.
    static bool ImportModel3DFromData(const DataBlob blob, const std::string & filename, Model3D & mdl);

  private:

    static void Init();
    static void ASSIMPLogCallback(const char * message, char * /* user */);
    static bool ImportModelHelper(const aiScene & scene, const std::string & filename, Model3D & mdl);
    static void ProcessMesh(const aiMesh * source, Model3D::SubMesh & dest);
    static void ProcessMaterial(const aiMaterial * aiMat, Model3D::SubMesh & dest);
    static void ProcessTextures(const aiMaterial & aiMat, const aiTextureType target, MaterialPtr & newMaterial);
    static void ListAllTextures(const aiMaterial & aiMat);
    static void FixASSIMPMaterialName(aiString & name);

    // Do not print ASSIMP log to the Engine log.
    // Fetched from config param "rendererConfig.modelImporterSilent".
    static bool modelImporterSilent;

    // List all textures for the material before loading 'em?
    // Fetched from config param "rendererConfig.modelImporterListTextures"
    static bool modelImporterListTextures;

    // Optionally treat height-maps as normal maps.
    // "rendererConfig.heightMapsAsNormalMaps"
    static bool heightMapsAsNormalMaps;

    // We keep this flag to avoid initializing the ASSIMP library more than once.
    static bool isInitialized;

    // Log object provided to ASSIMP.
    static aiLogStream assimpLog;

    // Model3DImporter is not instantiated.
    // All of its methods and data are static.
    NON_INSTANTIABLE(Model3DImporter)
};

} // namespace Engine {}
