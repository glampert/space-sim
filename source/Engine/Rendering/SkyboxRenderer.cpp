
// ================================================================================================
// -*- C++ -*-
// File: SkyboxRenderer.cpp
// Author: Guilherme R. Lampert
// Created on: 01/09/14
// Brief: Manages the drawing of a skybox.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/SkyboxRenderer.hpp"
#include "Engine/Rendering/OpenGL.hpp"

namespace Engine
{

// ======================================================
// SkyboxRenderer:
// ======================================================

SkyboxRenderer::SkyboxRenderer()
    : u_MVPMatrix(InvalidShaderUniformHandle)
    , s_cubeMap(InvalidShaderUniformHandle)
{
    // Create VBO/VAO:
    InitVertexBuffer();

    // Find the shader:
    InitShaderProgram();
}

SkyboxRenderer::SkyboxRenderer(const ConstTexturePtr & cubeMapTexture)
    : cubeMap(cubeMapTexture)
    , u_MVPMatrix(InvalidShaderUniformHandle)
    , s_cubeMap(InvalidShaderUniformHandle)
{
    // Validate skybox texture:
    assert(cubeMap != nullptr);
    assert(cubeMap->GetType() == Texture::Type::CubeMap);

    // Create VBO/VAO:
    InitVertexBuffer();

    // Find the shader:
    InitShaderProgram();
}

SkyboxRenderer::SkyboxRenderer(const std::string & cubeMapName)
    : u_MVPMatrix(InvalidShaderUniformHandle)
    , s_cubeMap(InvalidShaderUniformHandle)
{
    // Find/load the cube-map:
    SetTexture(cubeMapName);

    // Create VBO/VAO:
    InitVertexBuffer();

    // Find the shader:
    InitShaderProgram();
}

void SkyboxRenderer::InitVertexBuffer()
{
    // All sides of a cube/box:
    const float boxVerts[] =
    {
        -10.0f, 10.0f, -10.0f,
        -10.0f, -10.0f, -10.0f,
        10.0f, -10.0f, -10.0f,
        10.0f, -10.0f, -10.0f,
        10.0f, 10.0f, -10.0f,
        -10.0f, 10.0f, -10.0f,

        -10.0f, -10.0f, 10.0f,
        -10.0f, -10.0f, -10.0f,
        -10.0f, 10.0f, -10.0f,
        -10.0f, 10.0f, -10.0f,
        -10.0f, 10.0f, 10.0f,
        -10.0f, -10.0f, 10.0f,

        10.0f, -10.0f, -10.0f,
        10.0f, -10.0f, 10.0f,
        10.0f, 10.0f, 10.0f,
        10.0f, 10.0f, 10.0f,
        10.0f, 10.0f, -10.0f,
        10.0f, -10.0f, -10.0f,

        -10.0f, -10.0f, 10.0f,
        -10.0f, 10.0f, 10.0f,
        10.0f, 10.0f, 10.0f,
        10.0f, 10.0f, 10.0f,
        10.0f, -10.0f, 10.0f,
        -10.0f, -10.0f, 10.0f,

        -10.0f, 10.0f, -10.0f,
        10.0f, 10.0f, -10.0f,
        10.0f, 10.0f, 10.0f,
        10.0f, 10.0f, 10.0f,
        -10.0f, 10.0f, 10.0f,
        -10.0f, 10.0f, -10.0f,

        -10.0f, -10.0f, -10.0f,
        -10.0f, -10.0f, 10.0f,
        10.0f, -10.0f, -10.0f,
        10.0f, -10.0f, -10.0f,
        -10.0f, -10.0f, 10.0f,
        10.0f, -10.0f, 10.0f
    };

    // Create and bind the buffers:
    vertexBuffer.AllocateVAO();
    vertexBuffer.AllocateVBO(BufferStorage::StaticDraw, NumBoxVerts, sizeof(float) * 3, boxVerts);

    // Set "vertex format":
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    // Cleanup:
    vertexBuffer.UnbindVAO();
    vertexBuffer.UnbindVBO();
    CheckGLErrors();
}

void SkyboxRenderer::InitShaderProgram()
{
    skyboxShader = checked_pointer_cast<const ShaderProgram>(resourceMgr->FindOrLoadResource("skybox", Resource::Type::ShaderProgram));
    assert(skyboxShader != nullptr);

    u_MVPMatrix = skyboxShader->FindShaderUniform("u_MVPMatrix");
    s_cubeMap = skyboxShader->FindShaderUniform("s_cubeMap");

    if ((u_MVPMatrix == InvalidShaderUniformHandle) || (s_cubeMap == InvalidShaderUniformHandle))
    {
        common->ErrorF("Failed to initialize SkyboxRenderer shader uniform variables!");
    }

    // Texture samplers only need to be set one. They do not change.
    skyboxShader->Bind();
    skyboxShader->SetShaderUniform(s_cubeMap, static_cast<int>(Material::TMU::CubeMap));
    skyboxShader->Unbind();
}

void SkyboxRenderer::DrawWithMVP(const Matrix4 & mvp) const
{
    if (cubeMap == nullptr)
    {
        return;
    }

    vertexBuffer.BindVAO();
    glDepthFunc(GL_LEQUAL);

    cubeMap->Bind();
    skyboxShader->Bind();
    skyboxShader->SetShaderUniform(u_MVPMatrix, mvp);

    glDrawArrays(GL_TRIANGLES, 0, NumBoxVerts);

    glDepthFunc(GL_LESS);
    vertexBuffer.UnbindVAO();

    // Reset the material, just in case...
    renderMgr->GetState().currentMaterial = 0;
}

void SkyboxRenderer::SetTexture(const ConstTexturePtr & cubeMapTexture)
{
    // Set new:
    cubeMap = cubeMapTexture;

    // Validate it:
    assert(cubeMap != nullptr);
    assert(cubeMap->GetType() == Texture::Type::CubeMap);
}

void SkyboxRenderer::SetTexture(const std::string & cubeMapName)
{
    assert(!cubeMapName.empty());
    cubeMap = checked_pointer_cast<const Texture>(
            resourceMgr->FindOrLoadResource(ResourceManager::CubeMapsDirectory + System::PathSepString + cubeMapName + ".cm"));
}

} // namespace Engine {}
