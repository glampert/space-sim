
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Material.hpp
// Author: Guilherme R. Lampert
// Created on: 08/08/14
// Brief: Rendering and shading properties for a surface/object.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Material:
// ======================================================

//
// Surface shading properties. AKA Material.
//
// NOTE: Material instances are created via RenderManager::NewMaterial().
//
class Material
    : public Resource
{
  public:

    // Texture Mapping Unit (TMU) constants:
    enum class TMU
    {
        CubeMap     = 0,
        DiffuseMap  = 0,
        NormalMap   = 1,
        SpecularMap = 2
    };

    // Init the material from a file on disk.
    bool InitFromFile(std::string filename, std::string resourceId);

    // Init the resource from a block of data. If the data originated from a file, the filename is also provided.
    bool InitFromData(const DataBlob blob, std::string filename);

    // Frees all underlaying resource data.
    void FreeAllData();

    // Test if the material is a default. This is usually the case if an initialization error occurs.
    bool IsDefault() const;

    // Estimate the amount of memory consumed by this resource.
    size_t GetEstimateMemoryUsage() const;

    // Resource name or id. Usually a filename.
    std::string GetResourceId() const;

    // Short string describing the resource type.
    std::string GetResourceTypeString() const;

    // Print a description of the resource to the developer log (Common).
    void PrintSelf() const;

    // Apply the material and its properties as current render state.
    void Apply() const;

    // Set all material parameters (except its name) to defaults.
    void SetDefaults();

    // Selects a new Shader Program for the material that best matches its parameters.
    void SetupShaderProgram();

    // Updates the values of all shader uniform for the current program. This is called automatically by Material::Apply().
    void CommitShaderUniforms() const;

    // Shader Program:
    ConstShaderProgramPtr GetShaderProgram() const;
    void SetShaderProgram(const ConstShaderProgramPtr & prog);

    // Diffuse Texture:
    ConstTexturePtr GetDiffuseTexture() const;
    void SetDiffuseTexture(const ConstTexturePtr & tex);

    // Normal-map Texture:
    ConstTexturePtr GetNormalTexture() const;
    void SetNormalTexture(const ConstTexturePtr & tex);

    // Specular-map Texture:
    ConstTexturePtr GetSpecularTexture() const;
    void SetSpecularTexture(const ConstTexturePtr & tex);

    // Ambient color/contribution:
    Color4f GetAmbientColor() const;
    void SetAmbientColor(const Color4f c);

    // Diffuse color:
    Color4f GetDiffuseColor() const;
    void SetDiffuseColor(const Color4f c);

    // Specular color:
    Color4f GetSpecularColor() const;
    void SetSpecularColor(const Color4f c);

    // Emissive color:
    Color4f GetEmissiveColor() const;
    void SetEmissiveColor(const Color4f c);

    // Shininess/Phong-power:
    float GetShininess() const;
    void SetShininess(const float s);

    // Get/set material name:
    const std::string & GetName() const;
    void SetName(std::string name);

    // Ge/set all Material flags:
    unsigned int GetAllFlags() const;
    void SetAllFlags(const unsigned int flags);

    // Set, clear and test individual flags:
    void SetFlag(const unsigned int flag);
    void ClearFlag(const unsigned int flag);
    bool TestFlag(const unsigned int flag) const;

    // Set the Model-View-Projection matrix. This parameter is mutable on every material.
    void SetMVPTransform(const Matrix4 & mvp, const Matrix4 & mv) const;

    // Set this material as a default. This is only used internally by the renderer.
    void MarkAsDefault();

    // Get a sequential/enumerated name for a material.
    static std::string GetEnumeratedMaterialName();

    // Special color constant: (0,0,0,0)
    static const Color4f NullColor;

  protected:

    // RenderManager is the only object that can create Materials.
    friend class RenderManager;

    // Default constructor. Creates a defaulted material.
    Material();

  private:

    // Refresh all shader uniform handles.
    void InitShaderUniformHandles();
    void ClearShaderUniformHandles();

    //
    // Convenient way to pack all handles together.
    // Variable names follow the same naming as the uniform
    // vars declared in the shader program.
    //
    struct ShaderUniformHandles
    {

        // Texture samplers: s_<samplerName>
        ShaderUniformHandle s_diffuseTexture;
        ShaderUniformHandle s_normalTexture;
        ShaderUniformHandle s_specularTexture;

        // Color uniforms (vec4): u_<varName>
        ShaderUniformHandle u_ambientColor;
        ShaderUniformHandle u_diffuseColor;
        ShaderUniformHandle u_specularColor;
        ShaderUniformHandle u_emissiveColor;

        // Miscellaneous parameters: u_<varName>
        ShaderUniformHandle u_shininess;
        ShaderUniformHandle u_MVPMatrix;
        ShaderUniformHandle u_MVMatrix;
        ShaderUniformHandle u_normalMatrix;
        ShaderUniformHandle u_lightDirEyeSpace;
    };

    // Shader program selected based on material properties.
    ConstShaderProgramPtr shaderProg;

    // Textures:
    ConstTexturePtr diffuseTexture;
    ConstTexturePtr normalTexture;
    ConstTexturePtr specularTexture;

    // Colors:
    Color4f ambientColor;
    Color4f diffuseColor;
    Color4f specularColor;
    Color4f emissiveColor;
    float shininess;

    // Miscellaneous flags:
    unsigned int materialFlags;

    // Unique id. Assigned on creation.
    unsigned int materialId;

    // Shader Uniform var handles:
    ShaderUniformHandles uniforms;

    // Model-View-Projection. Can be set on a const material.
    mutable Matrix4 mvpMatrix;
    mutable Matrix4 mvMatrix;

    // Name/id of this material.
    std::string materialName;

    // Boolean flags:
    mutable bool mvpMatrixDirty; // Only update the mvpMatrix if relly needed.
    bool isDefault;              // The renderer can mark some materials as default/built-in.

    // Static counters:
    static unsigned int nextMaterialNum; // Global counter for material names.
    static unsigned int nextMaterialId;  // Global counter for material ids.

    // No copy allowed. This is a shareable resource.
    DISABLE_COPY_AND_ASSIGN(Material)
};

// Strong reference counted pointer to Material.
using MaterialPtr = std::shared_ptr<Material>;
using ConstMaterialPtr = std::shared_ptr<const Material>;

// Include inline definitions in the same namespace:
#include "Engine/Rendering/Material.inl"

} // namespace Engine {}
