
// ================================================================================================
// -*- C++ -*-
// File: ShaderProgram.cpp
// Author: Guilherme R. Lampert
// Created on: 08/08/14
// Brief: Basic OpenGL Shader Program.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/OpenGL.hpp"

namespace Engine
{

// ======================================================
// ShaderProgram:
// ======================================================

ShaderProgram::ShaderProgram()
    : sourceFileSize(0)
    , programId(0)
    , isDefault(false)
{
}

ShaderProgram::~ShaderProgram()
{
    if (programId != 0)
    {
        if (programId == renderMgr->GetState().currentShaderProg)
        {
            Unbind();
        }
        glDeleteProgram(programId);
        programId = 0;
    }
}

bool ShaderProgram::InitFromFile(std::string filename, std::string resourceId)
{
    DataBlob fileContents = DataBlob::LoadFile(filename, /* isBinary = */ false);
    SCOPE_EXIT({ fileContents.FreeData(); });

    // Proceed if the file was loaded and had contents.
    // On error, LoadFile() will print more info to the Engine log.
    if (fileContents.IsValid())
    {
        return InitFromData(fileContents, std::move(resourceId));
    }

    common->ErrorF("Failed to initialized ShaderProgram from file!");
    return false;
}

bool ShaderProgram::InitFromData(const DataBlob blob, std::string filename)
{
    assert(blob.IsValid());

    // Best #version tag added to both shaders:
    const std::string glslVersionTag = renderMgr->GetGLSLVersionTag(); // Already ended with '\n'
    std::string vsSource(glslVersionTag);
    std::string fsSource(glslVersionTag);

    // Slip the input blob into VS and FS parts (must have both):
    if (!SplitGLSLSource(blob, vsSource, fsSource))
    {
        common->ErrorF("Can't create Shader Program from data source \"%s\"! Format is invalid.", filename.c_str());
        return false;
    }

    ClearGLErrors();

    // Allocate ids:
    if (programId == 0)
    {
        programId = glCreateProgram();
    }

    const GLuint vsId = glCreateShader(GL_VERTEX_SHADER);
    const GLuint fsId = glCreateShader(GL_FRAGMENT_SHADER);

    if ((programId == 0) || (vsId == 0) || (fsId == 0))
    {
        if (glIsProgram(programId))
        {
            glDeleteProgram(programId);
            programId = 0;
        }
        if (glIsShader(vsId))
        {
            glDeleteShader(vsId);
        }
        if (glIsShader(fsId))
        {
            glDeleteShader(fsId);
        }

        CheckGLErrors();
        common->ErrorF("\"%s\": Problems when creating Shader Program! Failed to allocate GL ids!", filename.c_str());
        return false;
    }

    // Compile & attach shaders:
    common->PrintF("\"%s\": Compiling shaders and linking GL program...", filename.c_str());

    const GLchar * vssrc = vsSource.c_str();
    glShaderSource(vsId, 1, &vssrc, nullptr);
    glCompileShader(vsId);
    glAttachShader(programId, vsId);

    const GLchar * fssrc = fsSource.c_str();
    glShaderSource(fsId, 1, &fssrc, nullptr);
    glCompileShader(fsId);
    glAttachShader(programId, fsId);

    // Link the Shader Program:
    glLinkProgram(programId);

    // Print errors/warnings for the just compiled shaders and program:
    PrintInfoLogs(programId, vsId, fsId);

    // After attached to a program the shader objects can be deleted.
    glDeleteShader(vsId);
    glDeleteShader(fsId);

    // Save source filename and size:
    sourceFileName = std::move(filename);
    sourceFileSize = blob.sizeInBytes;

    // Check/print errors:
    CheckGLErrors();

    common->PrintF("New Shader Program initialized from file \"%s\" (%s)",
                   sourceFileName.c_str(), FormatMemoryUnit(sourceFileSize).c_str());

    return true;
}

void ShaderProgram::FreeAllData()
{
    if (programId != 0)
    {
        if (programId == renderMgr->GetState().currentShaderProg)
        {
            Unbind();
        }
        glDeleteProgram(programId);
        programId = 0;
    }

    sourceFileSize = 0;
    sourceFileName.clear();
}

void ShaderProgram::PrintInfoLogs(const unsigned int programId, const unsigned int vsId, const unsigned int fsId)
{
    GLsizei charsWritten;
    char infoLogBuf[MaxTempStringLen];

    charsWritten = 0;
    ZeroArray(infoLogBuf);
    glGetProgramInfoLog(programId, static_cast<GLsizei>(ArrayLength(infoLogBuf) - 1), &charsWritten, infoLogBuf);
    if (charsWritten > 0)
    {
        common->WarningF("------ PROGRAM INFO LOG ------\n%s", infoLogBuf);
    }

    charsWritten = 0;
    ZeroArray(infoLogBuf);
    glGetShaderInfoLog(vsId, static_cast<GLsizei>(ArrayLength(infoLogBuf) - 1), &charsWritten, infoLogBuf);
    if (charsWritten > 0)
    {
        common->WarningF("--------- VS INFO LOG --------\n%s", infoLogBuf);
    }

    charsWritten = 0;
    ZeroArray(infoLogBuf);
    glGetShaderInfoLog(fsId, static_cast<GLsizei>(ArrayLength(infoLogBuf) - 1), &charsWritten, infoLogBuf);
    if (charsWritten > 0)
    {
        common->WarningF("--------- FS INFO LOG --------\n%s", infoLogBuf);
    }
}

bool ShaderProgram::SplitGLSLSource(const DataBlob blob, std::string & vsSource, std::string & fsSource)
{
    assert(blob.IsValid());
    const char * data = reinterpret_cast<const char *>(blob.data);

    // Everything following the '@vertex_shader' tag until
    // a '@vertex_end' is a Vertex Shader.
    {
        static const char vsBeginTag[] = "@vertex_shader";
        static const char vsEndTag[]   = "@vertex_end";

        const char * pVSBegin = std::strstr(data, vsBeginTag);
        const char * pVSEnd = std::strstr(data, vsEndTag);

        if (pVSBegin == nullptr)
        {
            common->ErrorF("Missing '%s'!", vsBeginTag);
            return false;
        }
        if (pVSEnd == nullptr)
        {
            common->ErrorF("Missing '%s'!", vsEndTag);
            return false;
        }

        pVSBegin += ArrayLength(vsBeginTag) - 1;
        vsSource.append(pVSBegin, pVSEnd - pVSBegin);
    }

    // Everything following the '@fragment_shader' tag until
    // a '@fragment_end' is a Fragment Shader.
    {
        static const char fsBeginTag[] = "@fragment_shader";
        static const char fsEndTag[]   = "@fragment_end";

        const char * pFSBegin = std::strstr(data, fsBeginTag);
        const char * pFSEnd = std::strstr(data, fsEndTag);

        if (pFSBegin == nullptr)
        {
            common->ErrorF("Missing '%s'!", fsBeginTag);
            return false;
        }
        if (pFSEnd == nullptr)
        {
            common->ErrorF("Missing '%s'!", fsEndTag);
            return false;
        }

        pFSBegin += ArrayLength(fsBeginTag) - 1;
        fsSource.append(pFSBegin, pFSEnd - pFSBegin);
    }

    return true;
}

bool ShaderProgram::IsDefault() const
{
    return isDefault;
}

size_t ShaderProgram::GetEstimateMemoryUsage() const
{
    // Estimate that GL is using at least as much memory
    // as it took to store the input source file that created this shader prog.
    return sourceFileSize;
}

std::string ShaderProgram::GetResourceId() const
{
    return sourceFileName;
}

std::string ShaderProgram::GetResourceTypeString() const
{
    return "ShaderProgram";
}

void ShaderProgram::PrintSelf() const
{
    common->PrintF("---- ShaderProgram ----");
    common->PrintF("Source file.....: %s", sourceFileName.c_str());
    common->PrintF("Memory usage....: %s", FormatMemoryUnit(sourceFileSize, true).c_str());
    common->PrintF("GL program id...: %u", programId);
    common->PrintF("Is default?.....: %s", (IsDefault() ? "yes" : "no"));
}

bool ShaderProgram::IsValid() const
{
    return programId != 0;
}

void ShaderProgram::Bind() const
{
    if (renderMgr->GetState().currentShaderProg != programId)
    {
        renderMgr->GetState().currentShaderProg = programId;
        glUseProgram(programId);
    }
}

void ShaderProgram::Unbind() const
{
    renderMgr->GetState().currentShaderProg = 0;
    glUseProgram(0);
}

bool ShaderProgram::IsCurrent() const
{
    return (programId != 0) && (programId == renderMgr->GetState().currentShaderProg);
}

ShaderUniformHandle ShaderProgram::FindShaderUniform(const char * varName) const
{
    assert(varName != nullptr);
    const GLint loc = glGetUniformLocation(programId, varName);
    if (loc < 0)
    {
        common->WarningF("Failed to get location of shader uniform '%s' for program %d (%s)",
                         varName, programId, sourceFileName.c_str());
    }
    return loc;
}

bool ShaderProgram::SetShaderUniform(const ShaderUniformHandle var, const int i) const
{
    assert(IsCurrent());
    if (var < 0)
    {
        common->WarningF("SetShaderUniform(int) => invalid uniform var handle!");
        return false;
    }
    glUniform1i(var, i);
    return true;
}

bool ShaderProgram::SetShaderUniform(const ShaderUniformHandle var, const float f) const
{
    assert(IsCurrent());
    if (var < 0)
    {
        common->WarningF("SetShaderUniform(float) => invalid uniform var handle!");
        return false;
    }
    glUniform1f(var, f);
    return true;
}

bool ShaderProgram::SetShaderUniform(const ShaderUniformHandle var, const Vec2f & v2) const
{
    assert(IsCurrent());
    if (var < 0)
    {
        common->WarningF("SetShaderUniform(Vec2f) => invalid uniform var handle!");
        return false;
    }
    glUniform2f(var, v2.x, v2.y);
    return true;
}

bool ShaderProgram::SetShaderUniform(const ShaderUniformHandle var, const Vec3f & v3) const
{
    assert(IsCurrent());
    if (var < 0)
    {
        common->WarningF("SetShaderUniform(Vec3f) => invalid uniform var handle!");
        return false;
    }
    glUniform3f(var, v3.x, v3.y, v3.z);
    return true;
}

bool ShaderProgram::SetShaderUniform(const ShaderUniformHandle var, const Vec4f & v4) const
{
    assert(IsCurrent());
    if (var < 0)
    {
        common->WarningF("SetShaderUniform(Vec4f) => invalid uniform var handle!");
        return false;
    }
    glUniform4f(var, v4.x, v4.y, v4.z, v4.w);
    return true;
}

bool ShaderProgram::SetShaderUniform(const ShaderUniformHandle var, const Vector3 & v3) const
{
    assert(IsCurrent());
    if (var < 0)
    {
        common->WarningF("SetShaderUniform(Vector3) => invalid uniform var handle!");
        return false;
    }
    glUniform3f(var, v3[0], v3[1], v3[2]);
    return true;
}

bool ShaderProgram::SetShaderUniform(const ShaderUniformHandle var, const Vector4 & v4) const
{
    assert(IsCurrent());
    if (var < 0)
    {
        common->WarningF("SetShaderUniform(Vector4) => invalid uniform var handle!");
        return false;
    }
    glUniform4f(var, v4[0], v4[1], v4[2], v4[3]);
    return true;
}

bool ShaderProgram::SetShaderUniform(const ShaderUniformHandle var, const Matrix4 & m4) const
{
    assert(IsCurrent());
    if (var < 0)
    {
        common->WarningF("SetShaderUniform(Matrix4) => invalid uniform var handle!");
        return false;
    }
    glUniformMatrix4fv(var, 1, GL_FALSE, ToFloatPtr(m4));
    return true;
}

bool ShaderProgram::SetShaderUniform(const ShaderUniformHandle var, const float * fv, const size_t numElements) const
{
    assert(IsCurrent());
    assert(fv != nullptr);
    assert(numElements != 0);

    if (var < 0)
    {
        common->WarningF("SetShaderUniform(float[]) => invalid uniform var handle!");
        return false;
    }

    switch (numElements)
    {
    case 1:
        glUniform1f(var, fv[0]);
        break;
    case 2:
        glUniform2f(var, fv[0], fv[1]);
        break;
    case 3:
        glUniform3f(var, fv[0], fv[1], fv[2]);
        break;
    case 4:
        glUniform4f(var, fv[0], fv[1], fv[2], fv[3]);
        break;
    default:
        common->WarningF("SetShaderUniform(float[]) => array length is invalid!");
        return false;
    } // switch (fv.length)

    return true;
}

// ======================================================
// ShaderProgram factory function for ResourceManager:
// ======================================================

static ResourcePtr ShaderProgram_FactoryFunction(const bool forceDefault)
{
    if (forceDefault)
    {
        return renderMgr->defaultShaderProg;
    }
    else
    {
        return renderMgr->NewShaderProgram();
    }
}

// Register the callback:
REGISTER_RESOURCE_FACTORY_FOR_TYPE(Resource::Type::ShaderProgram, ShaderProgram_FactoryFunction);

} // namespace Engine {}
