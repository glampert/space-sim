
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: SkyboxRenderer.hpp
// Author: Guilherme R. Lampert
// Created on: 01/09/14
// Brief: Manages the drawing of a skybox.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// SkyboxRenderer:
// ======================================================

class SkyboxRenderer
{
  public:

    // Create a skybox rendered with no texture.
    // You must SetTexture() once before drawing it.
    SkyboxRenderer();

    // Create the skybox with an existing cube-map texture.
    // Texture must be of type CubeMap.
    SkyboxRenderer(const ConstTexturePtr & cubeMapTexture);

    // Create the skybox and loads the cube-map from file. 'cubeMapName' should not include file extension.
    // Will look for the file inside the standard directory where cube-maps are stored. See 'ResourceManager::CubeMapsDirectory'.
    SkyboxRenderer(const std::string & cubeMapName);

    // Renders the skybox using the provided matrix. Skybox should always be the last object rendered!
    void DrawWithMVP(const Matrix4 & mvp) const;

    // Change the skybox cube-map texture. Same rules of the constructors.
    void SetTexture(const ConstTexturePtr & cubeMapTexture);
    void SetTexture(const std::string & cubeMapName);

  private:

    // Internal helpers:
    void InitVertexBuffer();
    void InitShaderProgram();

    static constexpr int NumBoxVerts = 36;

    // GL vertex buffer:
    VertexBuffer vertexBuffer;

    // The cube-map texture:
    ConstTexturePtr cubeMap;

    // Skybox rendering program:
    ConstShaderProgramPtr skyboxShader;

    // The uniform variables we require (same names in the shader code):
    ShaderUniformHandle u_MVPMatrix;
    ShaderUniformHandle s_cubeMap;

    DISABLE_COPY_AND_ASSIGN(SkyboxRenderer)
};

} // namespace Engine {}
