
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Buffers.hpp
// Author: Guilherme R. Lampert
// Created on: 01/09/14
// Brief: Thin object wrappers to OpenGL buffers, such as Vertex and Index Buffers.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// BufferStorage:
// ======================================================

enum class BufferStorage : short
{
    StaticDraw,  // Mesh/buffer never changes.
    DynamicDraw, // Changes frequently.
    StreamDraw   // Changes very frequently (update->use->discard).
};

// Enum conversion helpers:
unsigned int BufferStorageToGL(const BufferStorage storage);
std::string BufferStorageToString(const BufferStorage storage);

// ======================================================
// VertexBuffer:
// ======================================================

class VertexBuffer
{
  public:

    // Creates and empty VertexBuffer.
    VertexBuffer();

    // Destructor frees the underlaying GPU buffers.
    ~VertexBuffer();

    // Bind as current render state:
    void BindVAO() const;
    void BindVBO() const;

    // Clear bindings:
    void UnbindVAO() const;
    void UnbindVBO() const;

    // Test bindings:
    bool IsVAOCurrent() const;
    bool IsVBOCurrent() const;

    // Allocate and free the Vertex Array Object. Will make the VAO current.
    bool AllocateVAO();
    void FreeVAO();

    // Allocate and free the Vertex Buffer. 'initData' may be null. Will make the buffer current.
    bool AllocateVBO(const BufferStorage storage, const unsigned int numVerts, const size_t sizeOfAVertex, const void * initData);
    bool AllocateVBO(); // Just creates and binds the VBO id.
    void FreeVBO();

    // Map into client memory. 'mapFlags' are the standard GL buffer mapping flags. Must bind first!
    void * MapVBO(const unsigned int mapFlags);
    void * MapVBORange(const size_t offsetInBytes, const size_t sizeInBytes, const unsigned int mapFlags);
    void UnmapVBO();

    // Inline accessors:
    unsigned int GetVAOHandle()   const { return vao; }
    unsigned int GetVBOHandle()   const { return vbo; }
    unsigned int GetVertexCount() const { return vertexCount; }

  private:

    // GL Vertex Array Object and Vertex Buffer Object:
    unsigned int vao;
    unsigned int vbo;

    // Number of vertexes allocated:
    unsigned int vertexCount;

    DISABLE_COPY_AND_ASSIGN(VertexBuffer)
};

// ======================================================
// IndexBuffer:
// ======================================================

class IndexBuffer
{
  public:

    // Creates and empty IndexBuffer.
    IndexBuffer();

    // Destructor frees the underlaying GPU buffers.
    ~IndexBuffer();

    // Bind as current render state:
    void BindIBO() const;

    // Clear bindings:
    void UnbindIBO() const;

    // Test bindings:
    bool IsIBOCurrent() const;

    // Allocate and free the Index Buffer. 'initData' may be null. Will make the buffer current.
    bool AllocateIBO(const BufferStorage storage, const unsigned int numIndexes, const size_t sizeOfAnIndex, const void * initData);
    void FreeIBO();

    // Map into client memory. 'mapFlags' are the standard GL buffer mapping flags. Must bind first!
    void * MapIBO(const unsigned int mapFlags);
    void * MapIBORange(const size_t offsetInBytes, const size_t sizeInBytes, const unsigned int mapFlags);
    void UnmapIBO();

    // Inline accessors:
    unsigned int GetIBOHandle()  const { return ibo; }
    unsigned int GetIndexCount() const { return indexCount; }

  private:

    // GL Index Buffer Object:
    unsigned int ibo;

    // Number of indexes allocated:
    unsigned int indexCount;

    DISABLE_COPY_AND_ASSIGN(IndexBuffer)
};

// ======================================================
// IndexedVertexBuffer:
// ======================================================

//
// NOTE: This class should never be dynamically allocated
// because we are taking a shortcut here by not making
// VertexBuffer and IndexBuffer destructors virtual!
// This removes the need for vtables, making the code
// a lot more compact and faster, but makes this class
// unsuitable to heap allocations.
//
class IndexedVertexBuffer final
    : public VertexBuffer
    , public IndexBuffer
{
  public:

    // Default constructor:
    IndexedVertexBuffer(const BufferStorage bufStorage = BufferStorage::StaticDraw)
        : storage(bufStorage) { }

    // Get/set BufferStorage:
    BufferStorage GetStorage() const { return storage; }
    void SetStorage(const BufferStorage bufStorage) { storage = bufStorage; }

    // Aliases for binding/unbinding the VAO, with a more suggestive name.
    void BindForDrawing() const { BindVAO();   }
    void UnbindDrawing()  const { UnbindVAO(); }

    // Unbind VAO, VBO and IBO.
    void UnbindAllBuffers()
    {
        UnbindVAO();
        UnbindVBO();
        UnbindIBO();
    }

    // Free the VAO, VBO and IBO.
    void FreeAllBuffers()
    {
        FreeVAO();
        FreeVBO();
        FreeIBO();
    }

  private:

    // Disable head allocations.
    // This will enforce the rule stated in the header comment.
    void * operator new(size_t)    = delete;
    void operator delete(void *)   = delete;
    void * operator new[](size_t)  = delete;
    void operator delete[](void *) = delete;
    DISABLE_COPY_AND_ASSIGN(IndexedVertexBuffer)

    BufferStorage storage;
};

} // namespace Engine {}
