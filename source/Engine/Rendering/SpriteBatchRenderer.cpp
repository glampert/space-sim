
// ================================================================================================
// -*- C++ -*-
// File: SpriteBatchRenderer.cpp
// Author: Guilherme R. Lampert
// Created on: 12/09/14
// Brief: 2D sprite batch rendering.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/SpriteBatchRenderer.hpp"
#include "Engine/Rendering/OpenGL.hpp"

namespace Engine
{

// ======================================================
// SpriteBatchRenderer:
// ======================================================

SpriteBatchRenderer::SpriteBatchRenderer()
    : maxSpriteTrisPerSpriteRenderer(1024) // Default initial value
    , u_MVPMatrix(InvalidShaderUniformHandle)
    , s_diffuseTexture(InvalidShaderUniformHandle)
{
    // Fetch config parameters:
    scriptMgr->GetConfigValue("rendererConfig.maxSpriteTrisPerSpriteRenderer", maxSpriteTrisPerSpriteRenderer);
    if (maxSpriteTrisPerSpriteRenderer == 0)
    {
        common->FatalErrorF("\'rendererConfig.maxSpriteTrisPerSpriteRenderer\' must not be zero!");
    }

    // Find/load the proper shader:
    InitShaderProgram();

    // Reserve memory beforehand:
    batchedVerts.reserve(maxSpriteTrisPerSpriteRenderer * 3);

    // Allocate & bind GL buffers:
    vertexBuffer.AllocateVAO();
    vertexBuffer.AllocateVBO(BufferStorage::StreamDraw, (maxSpriteTrisPerSpriteRenderer * 3), sizeof(SpriteVertex), nullptr);

    // Set "vertex format":
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), reinterpret_cast<const GLvoid *>(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), reinterpret_cast<const GLvoid *>(sizeof(Vec4f)));

    // Cleanup:
    vertexBuffer.UnbindVAO();
    vertexBuffer.UnbindVBO();
    CheckGLErrors();

    common->PrintF("New SpriteBatchRenderer initialized. maxSpriteTrisPerSpriteRenderer = %u", maxSpriteTrisPerSpriteRenderer);
}

void SpriteBatchRenderer::DrawTriangles(const SpriteVertex * vertexes, const size_t numVertexes, const uint16_t * indexes,
                                        const size_t numIndexes, const ConstTexturePtr & texture, const SpriteLayer layer)
{
    assert(numVertexes != 0);
    assert(vertexes != nullptr);
    assert(texture != nullptr);
    assert(size_t(layer) < NumBatches);

    auto & triBatch = batches[size_t(layer)];

    int t;
    BatchedTriangle tri;
    tri.texture = texture;

    // Optionally indexed geometry:
    if ((indexes != nullptr) && (numIndexes != 0))
    {
        assert((numIndexes % 3) == 0);

        t = 0;
        for (size_t i = 0; i < numIndexes; ++i)
        {
            if (batchedVerts.size() == (maxSpriteTrisPerSpriteRenderer * 3))
            {
                common->WarningF("Max sprite vertexes per frame (%u) limit reached! Dropping further draw calls...",
                                 (maxSpriteTrisPerSpriteRenderer * 3));
                return;
            }

            assert(indexes[i] < numVertexes); // Indexes are relative to 'vertexes[]'
            batchedVerts.push_back(vertexes[indexes[i]]);
            tri.indexes[t++] = static_cast<uint16_t>(batchedVerts.size() - 1);
            if (t == 3)
            {
                triBatch.push_back(tri);
                t = 0;
            }
        }
    }
    else // Unindexed triangles:
    {
        assert((numVertexes % 3) == 0);

        t = 0;
        for (size_t v = 0; v < numVertexes; ++v)
        {
            if (batchedVerts.size() == (maxSpriteTrisPerSpriteRenderer * 3))
            {
                common->WarningF("Max sprite vertexes per frame (%u) limit reached! Dropping further draw calls...",
                                 (maxSpriteTrisPerSpriteRenderer * 3));
                return;
            }

            batchedVerts.push_back(vertexes[v]);
            tri.indexes[t++] = static_cast<uint16_t>(batchedVerts.size() - 1);
            if (t == 3)
            {
                triBatch.push_back(tri);
                t = 0;
            }
        }
    }
}

void SpriteBatchRenderer::DrawQuad(const float x, const float y, const float width, const float height, const Color4f color,
                                   const ConstTexturePtr & texture, const bool invertV, const Vec2f * uvs, const SpriteLayer layer)
{
    assert(texture != nullptr);
    assert(size_t(layer) < NumBatches);

    if (width <= 0.0f)
    {
        return;
    }
    if (height <= 0.0f)
    {
        return;
    }

    // Default UVs for a quad, if no user defined UVs are provided:
    const Vec2f defaultQuadUVs[] =
    {
        { 0.0f, 0.0f },
        { 1.0f, 0.0f },
        { 1.0f, 1.0f },
        { 0.0f, 1.0f }
    };

    // Initial indexes of a quadrilateral made of two triangles:
    const uint16_t quadIndexes[] = { 0, 1, 2, 2, 3, 0 };

    // Temp quad geometry:
    SpriteVertex quadVerts[4];

    // Supply default UV set if needed:
    if (uvs == nullptr)
    {
        uvs = defaultQuadUVs;
    }

    // Invert texture coords vertically?
    if (invertV)
    {
        // Store position and tex coords:
        quadVerts[0].positionTexCoords.Set(x, y, uvs[0].u, 1.0f - uvs[0].v);
        quadVerts[1].positionTexCoords.Set(x + width, y, uvs[1].u, 1.0f - uvs[1].v);
        quadVerts[2].positionTexCoords.Set(x + width, y + height, uvs[2].u, 1.0f - uvs[2].v);
        quadVerts[3].positionTexCoords.Set(x, y + height, uvs[3].u, 1.0f - uvs[3].v);
    }
    else // Copy as is:
    {
        // Store position and tex coords:
        quadVerts[0].positionTexCoords.Set(x, y, uvs[0].u, uvs[0].v);
        quadVerts[1].positionTexCoords.Set(x + width, y, uvs[1].u, uvs[1].v);
        quadVerts[2].positionTexCoords.Set(x + width, y + height, uvs[2].u, uvs[2].v);
        quadVerts[3].positionTexCoords.Set(x, y + height, uvs[3].u, uvs[3].v);
    }

    // Store vertex color:
    quadVerts[0].color = color;
    quadVerts[1].color = color;
    quadVerts[2].color = color;
    quadVerts[3].color = color;

    // Add to triangle batch:
    DrawTriangles(quadVerts, ArrayLength(quadVerts), quadIndexes, ArrayLength(quadIndexes), texture, layer);
}

void SpriteBatchRenderer::FlushBatch(const Matrix4 & mvp)
{
    if (batchedVerts.empty())
    {
        return;
    }

    // Sanity check:
    assert(drawCalls.empty());

    // Sort each layer by texture to reduce the number of draw calls:
    SortBatches();

    // Set render buffers.
    // Needed for update and drawing:
    vertexBuffer.BindVAO();
    vertexBuffer.BindVBO();

    // Update VBO and render all layers:
    UpdateDrawBuffers();
    DrawBatches(mvp);

    // Cleanup:
    vertexBuffer.UnbindVAO();
    vertexBuffer.UnbindVBO();

    // Clear the batches once we are done.
    drawCalls.clear();
    batchedVerts.clear();
    for (auto & batch : batches)
    {
        batch.clear();
    }
}

void SpriteBatchRenderer::SortBatches()
{
    // Gather-up the ones with the same texture inside each layer.
    for (auto & batch : batches)
    {
        std::sort(std::begin(batch), std::end(batch),
                  [](const BatchedTriangle & a, const BatchedTriangle & b) -> bool
                  {
				      return (a.texture < b.texture);
                  });
    }
}

void SpriteBatchRenderer::UpdateDrawBuffers()
{
    // Map for writing, allow the driver to discard the previous buffer:
    void * pVB = vertexBuffer.MapVBORange(0, (maxSpriteTrisPerSpriteRenderer * 3 * sizeof(SpriteVertex)), (GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT));
    if (pVB == nullptr)
    {
        CheckGLErrors();
        common->FatalErrorF("SpriteBatchRenderer: Failed to map GL VBO! Unable to continue.");
    }

    // Temps:
    SpriteVertex * __restrict vertexes = reinterpret_cast<SpriteVertex *>(pVB);
    unsigned int totalVerts = 0;
    DrawCallInfo drawCall;

    // Send vertexes to the GPU, for each batch/layer:
    for (const auto & batch : batches)
    {
        drawCall.texture = nullptr;
        drawCall.numTris = 0;

        for (const auto & tri : batch)
        {
            if (tri.texture.get() != drawCall.texture)
            {
                if (drawCall.texture != nullptr) // If not the first one
                {
                    drawCalls.push_back(drawCall);
                }
                drawCall.texture = tri.texture.get();
                drawCall.numTris = 0;
            }
            vertexes[totalVerts++] = batchedVerts[tri.indexes[0]];
            vertexes[totalVerts++] = batchedVerts[tri.indexes[1]];
            vertexes[totalVerts++] = batchedVerts[tri.indexes[2]];
            drawCall.numTris++;
        }

        assert(totalVerts <= vertexBuffer.GetVertexCount());

        if (drawCall.texture != nullptr)
        {
            drawCalls.push_back(drawCall);
        }
    }

    // Unmap:
    vertexBuffer.UnmapVBO();
}

void SpriteBatchRenderer::DrawBatches(const Matrix4 & mvp) const
{
    // Set shader program:
    spriteShader->Bind();
    spriteShader->SetShaderUniform(u_MVPMatrix, mvp);

    // Perform the draw calls, no more than one per texture.
    GLsizei first = 0;
    for (const auto & drawCall : drawCalls)
    {
        assert(drawCall.texture != nullptr);
        drawCall.texture->Bind();

        const GLsizei count = (drawCall.numTris * 3);
        glDrawArrays(GL_TRIANGLES, first, count);
        first += count;
    }

    // Reset the material, just in case...
    renderMgr->GetState().currentMaterial = 0;
}

void SpriteBatchRenderer::InitShaderProgram()
{
    spriteShader = checked_pointer_cast<const ShaderProgram>(resourceMgr->FindOrLoadResource("sprite2d", Resource::Type::ShaderProgram));
    assert(spriteShader != nullptr);

    u_MVPMatrix = spriteShader->FindShaderUniform("u_MVPMatrix");
    s_diffuseTexture = spriteShader->FindShaderUniform("s_diffuseTexture");

    if ((u_MVPMatrix == InvalidShaderUniformHandle) || (s_diffuseTexture == InvalidShaderUniformHandle))
    {
        common->ErrorF("Failed to initialize SpriteBatchRenderer shader uniform variables!");
    }

    // Texture samplers only need to be set one. They do not change.
    spriteShader->Bind();
    spriteShader->SetShaderUniform(s_diffuseTexture, static_cast<int>(Material::TMU::DiffuseMap));
    spriteShader->Unbind();
}

} // namespace Engine {}
