
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: ImageLoader.hpp
// Author: Guilherme R. Lampert
// Created on: 11/08/14
// Brief: Loader for image file formats, such as TGA, JPEG, PNG, etc.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// ImageData:
// ======================================================

// Basic information pertaining an image.
// Destructor will automatically free the image pixels.
struct ImageData
{
    using PixelFormat = Texture::PixelFormat;

    uint8_t *   pixels;          // Image data. Might need to be cast to the proper type depending on pixel format.
    size_t      sizeInBytes;     // Total size in bytes of image data (size of pixels[] array)
    Vec2u       dimensions;      // Width and height of the image
    PixelFormat pixelFormat;     // Format of the pixel data. E.g.: RGB/RGBA/etc
    short       colorComponents; // Number of image color components; E.g.: RGBA=4

    // Constructor sets everything to null/zero.
    ImageData();

    // Frees the image pixels.
    ~ImageData();

    // Test if ImageData properties are valid.
    bool IsValid() const;
};

// ======================================================
// ImageLoader singleton:
// ======================================================

//
// Helper class used internally by Texture to create
// GL textures from image files.
//
// This class is not directly instantiated.
// All of its methods are static and construction is private.
//
class ImageLoader final
{
  public:

    // Load an image from a file on disk.
    static bool LoadImageFromFile(const std::string & filename, ImageData & img);

    // Load an image from a memory data blob. The blob is usually the unchanged contents of a file loaded into memory.
    static bool LoadImageFromData(const DataBlob blob, const std::string & filename, ImageData & img);

  private:

    static bool ImageDataSetup(int w, int h, int comp, uint8_t * pixels,
                               const std::string & filename, ImageData & img,
                               const char * func);

    // ImageLoader is not instantiated.
    // All of its methods and data are static.
    NON_INSTANTIABLE(ImageLoader)
};

} // namespace Engine {}
