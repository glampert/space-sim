
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: ParticleRenderer.hpp
// Author: Guilherme R. Lampert
// Created on: 04/09/14
// Brief: Particle emitter and renderer.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <vector>
#include "Engine/Core/Math/Random.hpp"
#include "Engine/Core/Geometry/Plane.hpp"
#include "Engine/Core/Containers/DestructionPolicies.hpp"
#include "Engine/Core/Containers/IntrusiveSList.hpp"

namespace Engine
{

// ======================================================
// Constants / helper types:
// ======================================================

// Results of a particle collision with a bounding surface.
enum class ParticleCollisionEffect : short
{
    Bounce,  // Particle bounces off by the given coefficient of restitution.
    Stick,   // Particle stick into the collision surface and stays there until its lifetime expires.
    FadeOut, // Particle fades out on collision.
    Random   // Select randomly one of the above.
};

// Fade out effect for particles.
enum class ParticleFadeEffect : short
{
    Shrink,    // Particles shrinks and disappears.
    SmoothFade // Particle fades out smoothly.
};

// Plane that defines a constraint to a particle emitter.
struct ParticleCollisionPlane
{
    Plane plane;            // Actual bounding plane.
    float restitutionCoeff; // Coefficient of restitution (or how bouncy the plane is).
};

// A single particle in a particle emitter.
struct Particle
{
    Vector3  position;   // Current world position of this particle.
    Vector3  velocity;   // Current velocity of this particle.
    Vec2f    size;       // Size/scale of this particle.
    uint32_t fadeTimeMs; // Once the duration is elapsed, fade out for this long.
    uint32_t durationMs; // Time this particle will go inactive, in milliseconds.
    short    isFading;   // Boolean set when fading out due to expired lifetime.
    ParticleCollisionEffect collisionEffect;
};

// ======================================================
// ParticleRenderer:
// ======================================================

// Parametric particle emitter and renderer.
// Instances can be linked to an IntrusiveSList.
class ParticleRenderer
    : public SListNode<ParticleRenderer>
{
  public:

    // Create a default particle emitter.
    ParticleRenderer();

    // Update the particle emitter and particles alive.
    // Should be called every frame for particles in the view.
    void UpdateAllParticles(const GameTime time);

    // Effectively renders the batched particles.
    void DrawAllParticles(const GameTime time, const Matrix4 & modelMatrix,
                          const Matrix4 & viewProjMatrix, const Vector3 cameraPos);

    // Check if there are pending particles to be rendered.
    bool HasParticlesToDraw() const;

    // Reset the particle emitter to its initial state.
    void ResetEmitter();

    // Set particle parameters to defaults. This will also reset the emitter.
    void SetDefaults();

    // Allocate/free the array of particles:
    void AllocateParticles();
    void FreeParticles();
    bool AreParticlesAllocated() const;

    // Add/remove collision planes:
    void AddCollisionPlane(const ParticleCollisionPlane plane);
    void AddCollisionPlane(const Plane plane, const float restitutionCoeff);
    void RemoveAllCollisionPlanes();

    // Set seed used by the underlaying random number generator:
    void SetRandomSeed(const unsigned int seed);

    // Get/set min/max number of particles in the emitter:
    void SetMinMaxParticles(const uint32_t min, const uint32_t max);
    void GetMinMaxParticles(uint32_t & min, uint32_t & max) const;

    // Get/set particle fade times:
    void SetParticleFadeTimes(const uint32_t fadeMinMs, const uint32_t fadeMaxMs);
    void GetParticleFadeTimes(uint32_t & fadeMinMs, uint32_t & fadeMaxMs) const;

    // Get/set result of particle collision with bounding surfaces:
    void SetParticleCollisionEffect(const ParticleCollisionEffect pc) { particleCollisionEffect = pc; }
    ParticleCollisionEffect GetParticleCollisionEffect() const { return particleCollisionEffect; }

    // Get/set particle fade out effect:
    void SetParticleFadeEffect(const ParticleFadeEffect effect) { particleFadeEffect = effect; }
    ParticleFadeEffect GetParticleFadeEffect() const { return particleFadeEffect; }

    // Get/set base particle color that is modulated with the texture color:
    void SetParticleBaseColor(const Color4f color) { baseColor = color; }
    Color4f GetParticleBaseColor() const { return baseColor; }

    // Get/set max particle sizes:
    void SetParticleMinSize(const Vec2f size) { minParticleSize = size; }
    Vec2f GetParticleMinSize() const { return minParticleSize; }

    // Get/set min particle sizes:
    void SetParticleMaxSize(const Vec2f size) { maxParticleSize = size; }
    Vec2f GetParticleMaxSize() const { return maxParticleSize; }

    // Get/set particle lifetime milliseconds:
    void SetParticleLifeCycle(const uint32_t ms) { particleLifeCycleMs = ms; }
    uint32_t GetParticleLifeCycle() const { return particleLifeCycleMs; }

    // Get/set new particle release interval milliseconds (flow rate):
    void SetParticleReleaseInterval(const uint32_t ms) { particleReleaseIntervalMs = ms; }
    uint32_t GetParticleReleaseInterval() const { return particleReleaseIntervalMs; }

    // Get/set number of new particles to release at every release interval:
    void SetParticleReleaseAmount(const uint32_t amount) { particleReleaseAmount = amount; }
    uint32_t GetParticleReleaseAmount() const { return particleReleaseAmount; }

    // Get/set velocity vector of particles:
    void SetParticleVelocity(const Vector3 velocity) { particleVelocityVec = velocity; }
    Vector3 GetParticleVelocity() const { return particleVelocityVec; }

    // Get/set particle emitter origin (in world coordinates):
    void SetEmitterOrigin(const Vector3 origin) { emitterOrigin = origin; }
    Vector3 GetEmitterOrigin() const { return emitterOrigin; }

    // Get/set gravity force applied to particles:
    void SetEmitterGravity(const Vector3 gravity) { gravityVec = gravity; }
    Vector3 GetEmitterGravity() const { return gravityVec; }

    // Get/set simulated wind velocity for this emitter:
    void SetWindVelocity(const Vector3 wind) { windVelocityVec = wind; }
    Vector3 GetWindVelocity() const { return windVelocityVec; }

    // Get/set variable velocity. Zero is constant:
    void SetVelocityVar(const float vel) { velocityVar = vel; }
    float GetVelocityVar() const { return velocityVar; }

    // Get/set the sprite texture applied to each sprite.
    void SetParticleSpriteTexture(const ConstTexturePtr & tex)
    {
        assert(tex != nullptr);
        particleSprite = tex;
    }
    ConstTexturePtr GetParticleSpriteTexture() const { return particleSprite; }

    // Get/set the custom particle shader.
    void SetCustomShader(const ConstShaderProgramPtr & shader)
    {
        assert(shader != nullptr);
        customShader = shader;
    }
    ConstShaderProgramPtr GetCustomShader() const { return customShader; }

    // Enable/disable simulated air resistance:
    void SetAirResistance(const bool airResistance) { simulateAirResistance = airResistance; }
    bool HasAirResistance() const { return simulateAirResistance; }

    // Make particle emitter run once, emitting 'particleReleaseAmount' particles and then stopping.
    void SetOneTimeEmission(const bool oneTime) { oneTimeEmission = oneTime; }
    bool IsOneTimeEmission() const { return oneTimeEmission; }

    // If this is a one-time emitter, check if it has always emitted all particles.
    bool IsOneTimeEmitterFinished() const { return (oneTimeEmission && (particlesEmitted >= particleReleaseAmount)); }

  private:

    // Generates a random vector where X, Y, and Z components are between -1 and 1.
    Vector3 GetRandVector();

    // Generates a random collision effect for a particle.
    ParticleCollisionEffect GetRandCollisionEffect();

    // Random particle fade time between particleFadeMinMs and particleFadeMaxMs.
    uint32_t GetRandFadeTime();

    // Random size of a particle billboard, between minParticleSize and maxParticleSize.
    Vec2f GetRandSize();

    // Performs particle collision test against the user provided collision planes.
    void CheckUserPlanesCollision(Particle & particle, const Vector3 & oldPosition, const uint32_t currentTimeMs) const;

    // Misc helpers:
    void SortParticles(const Vector3 cameraPos);
    void UpdateDrawBuffers(const GameTime & time) const;
    void DrawParticleBatch(const Matrix4 & modelMatrix, const Matrix4 & viewProjMatrix, const Vector3 cameraPos) const;

    // Initialization of the shared/static data of the particle renderer.
    // Performed by the first ParticleRenderer instance that is created.
    static void InitSharedData();
    static void InitDefaultShaderProgram();
    static void InitVertexBuffer();

    // Static data cleanup. Performed by RenderManager on application shutdown.
    static void ShutdownSharedData();

    // Variables shared by all ParticleRenderer instances:
    static VertexBuffer vertexBuffer;
    static ConstShaderProgramPtr defaultParticleShader;
    static ShaderUniformHandle u_viewProjMatrix;
    static ShaderUniformHandle u_modelMatrix;
    static ShaderUniformHandle u_cameraPos;
    static ShaderUniformHandle s_diffuseTexture;
    static bool sharedDataInitialized;
    static bool debugParticles;
    static unsigned int maxParticlesPerParticleRenderer;

    // Allow RenderManager to access ShutdownSharedData().
    friend class RenderManager;

    // Particle velocity, gravity and wind vectors:
    Vector3 gravityVec;
    Vector3 particleVelocityVec;
    Vector3 windVelocityVec;

    // Emitter origin in world space:
    Vector3 emitterOrigin;

    // Velocity variation added to particles,
    // combined with a random direction vector. See GetRandVector().
    float velocityVar;

    // Minimum and maximum sizes of the particle quadrilaterals.
    // Sizes are randomized between these values.
    Vec2f minParticleSize;
    Vec2f maxParticleSize;

    // Base color that modulates the sprite texture:
    Color4f baseColor;

    // Min and max fade times in milliseconds.
    // Particle fade times vary randomly between these two values.
    uint32_t particleFadeMinMs;
    uint32_t particleFadeMaxMs;

    // Amount of time each particle lives, in milliseconds:
    uint32_t particleLifeCycleMs;

    // Particle release interval (flow rate) in milliseconds:
    uint32_t particleReleaseIntervalMs;

    // Number of new particles to release at every release interval:
    uint32_t particleReleaseAmount;

    // Number of particles emitted since creation or reset.
    uint32_t particlesEmitted;

    // Last time the emitter was updated:
    uint32_t lastUpdateMs;

    // Minimum number of particles that should exist at any given time.
    // If this number is greater than zero, new particles are spawned when
    // old ones expire to keep at least this many in activity.
    uint32_t minParticles;

    // Maximum number of active particles at any given time.
    uint32_t maxParticles;

    // Active particles are always kept in sequence,
    // from particles[0] to particles[numActiveParticles - 1].
    uint32_t numActiveParticles;

    // Array of particles. Size equal to 'maxParticles'.
    // Active particles are always at the front, from index 0 to numActiveParticles-1
    std::vector<Particle> particles;

    // User defined particle collision planes:
    std::vector<ParticleCollisionPlane> collisionPlanes;

    // Sprite texture applied to each particle (modulated by baseColor):
    ConstTexturePtr particleSprite;

    // Custom shader program. Initially points to 'defaultParticleShader'.
    ConstShaderProgramPtr customShader;

    // Private random generator:
    XORShiftRandom random;

    // One of the collision results defined above:
    ParticleCollisionEffect particleCollisionEffect;

    // Fade out effect for expired particles:
    ParticleFadeEffect particleFadeEffect;

    // If set, simulate some air resistance using 'windVelocityVec':
    bool simulateAirResistance;

    // Will release 'particleReleaseAmount' particles and stop. False by default.
    bool oneTimeEmission;

    // No direct copy.
    DISABLE_COPY_AND_ASSIGN(ParticleRenderer)
};

} // namespace Engine {}
