
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: SpriteBatchRenderer.hpp
// Author: Guilherme R. Lampert
// Created on: 12/09/14
// Brief: 2D sprite batch rendering.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <vector>

namespace Engine
{

// ======================================================
// SpriteLayer:
// ======================================================

enum class SpriteLayer
{
    // Drawn first (at the bottom)
    Layer0,
    Layer1,
    Layer2,
    Layer3,
    Layer4,
    Layer5,
    // Drawn last (on top)

    // Layer count; Used internally.
    Count,

    // Topmost layer:
    Topmost = Layer5
};

// ======================================================
// SpriteVertex:
// ======================================================

struct SpriteVertex
{
    Vec4f positionTexCoords; // [x,y] = 2d position; [z,w] = tex coords.
    Vec4f color;             // RGBA float color.
};

// ======================================================
// SpriteBatchRenderer:
// ======================================================

class SpriteBatchRenderer
{
  public:

    // Sets everything to defaults.
    SpriteBatchRenderer();

    // Add a set of triangles to the batch. Indexes may be null for unindexed geometry.
    // Texture must not be null. 'layer' will set draw order. Lower values draw first.
    void DrawTriangles(const SpriteVertex * vertexes, const size_t numVertexes, const uint16_t * indexes,
                       const size_t numIndexes, const ConstTexturePtr & texture, const SpriteLayer = SpriteLayer::Layer0);

    // Adds a single 2D quadrilateral to the sprite batch. Lower layers draw first. Texture must not be null.
    void DrawQuad(const float x, const float y, const float width, const float height, const Color4f color,
                  const ConstTexturePtr & texture, const bool invertV = false, const Vec2f * uvs = nullptr,
                  const SpriteLayer = SpriteLayer::Layer0);

    // Effectively renders the batched sprites.
    // Color blending should be ON and depth testing and culling OFF.
    void FlushBatch(const Matrix4 & mvp);

    // Test if the batch has anything to draw with FlushBatch().
    bool IsEmpty() const { return batchedVerts.empty(); }

  private:

    // Internal helpers:
    void SortBatches();
    void UpdateDrawBuffers();
    void DrawBatches(const Matrix4 & mvp) const;
    void InitShaderProgram();

    // Packed to save as much space as possible.
    #pragma pack(push, 1)

    struct BatchedTriangle
    {
        ConstTexturePtr texture; // Texture applied to this surface
        uint16_t indexes[3];     // 3 vertexes that make up this triangle (batchedVerts)
    };

    struct DrawCallInfo
    {
        const Texture * texture; // No need for a ref-ptr. The texture is held by the batch.
        uint16_t numTris;        // Can we get away with a 16bit integer?
    };

    #pragma pack(pop)

    // One batch per layer.
    static constexpr int NumBatches = int(SpriteLayer::Count);

    // Max batched sprite triangles per frame. Batch counts are reset by FlushBatch().
    unsigned int maxSpriteTrisPerSpriteRenderer;

    // Dynamic vertex buffer for the sprite geometry.
    VertexBuffer vertexBuffer;

    // All vertexes for all batches.
    std::vector<SpriteVertex> batchedVerts;

    // Aux draw call state we use to group calls with same texture. Usually small.
    std::vector<DrawCallInfo> drawCalls;

    // Batch sorted by layer, each batch internally sorted by texture.
    std::vector<BatchedTriangle> batches[NumBatches];

    // 2D sprite rendering program.
    ConstShaderProgramPtr spriteShader;

    // The uniform variables we require (same names in the shader code).
    ShaderUniformHandle u_MVPMatrix;
    ShaderUniformHandle s_diffuseTexture;

    // No direct copy.
    DISABLE_COPY_AND_ASSIGN(SpriteBatchRenderer)
};

} // namespace Engine {}
