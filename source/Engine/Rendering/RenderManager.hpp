
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: RenderManager.hpp
// Author: Guilherme R. Lampert
// Created on: 06/08/14
// Brief: RenderManager singleton.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Containers/FixedArray.hpp"
#include <stack>

namespace Engine
{

// ======================================================
// Forward declarations:
// ======================================================

//
// Types/Resources:
//
class Font;
class Texture;
class Material;
class Model3D;
class ShaderProgram;
class Camera;

//
// Ref-counted pointers:
//
using TexturePtr            = std::shared_ptr<Texture>;
using ConstTexturePtr       = std::shared_ptr<const Texture>;
using FontPtr               = std::shared_ptr<Font>;
using ConstFontPtr          = std::shared_ptr<const Font>;
using MaterialPtr           = std::shared_ptr<Material>;
using ConstMaterialPtr      = std::shared_ptr<const Material>;
using Model3DPtr            = std::shared_ptr<Model3D>;
using ConstModel3DPtr       = std::shared_ptr<const Model3D>;
using ShaderProgramPtr      = std::shared_ptr<ShaderProgram>;
using ConstShaderProgramPtr = std::shared_ptr<const ShaderProgram>;

enum class SplitScreen
{
    TopView,
    BottonView,
    FullView
};

// ======================================================
// GLState:
// ======================================================

//
// Cached OpenGL states.
//
class GLState final
{
  public:

    // Currently bound ShaderProgram id:
    unsigned int currentShaderProg;

    // Currently applied material:
    unsigned int currentMaterial;

    // Current VAO and Vertex/Index Buffers:
    unsigned int currentVAO;
    unsigned int currentVBO;
    unsigned int currentIBO;

    // Max number of Texture Mapping Units we cache on GLState.
    // GL actually supports at least 16, but we don't need nearly that many.
    static constexpr int MaxTMUs = 10;

    // Currently bound Texture ids. One entry for each TMU:
    FixedArray<unsigned int, MaxTMUs> currentTexture;

    // Default constructor. Calls Reset().
    GLState();

    // Reset all cached GL states to defaults.
    void Reset();

  private:

    DISABLE_COPY_AND_ASSIGN(GLState)
};

// ======================================================
// RenderManager singleton:
// ======================================================

class RenderManager final
    : public SingleInstanceType<RenderManager>
{
  public:

    // Not directly used.
    // 'renderMgr' global is provided instead.
    RenderManager();

    // Initialization and shutdown. Called internally by the Engine.
    void Init();
    void Shutdown();

    // ---- Resource allocation: ----

    // Creates a new ShaderProgram object.
    ShaderProgramPtr NewShaderProgram();

    // Creates a new Texture object.
    TexturePtr NewTexture();

    // Creates a new Font object.
    FontPtr NewFont();

    // Creates a new Material object.
    MaterialPtr NewMaterial();

    // Creates a new Model3D object.
    Model3DPtr NewModel3D();

    // ---- Render states / screen management: ----

    // Matrix stack used by the hierarchical scene rendering.
    void PushMatrix(const Matrix4 & m);
    const Matrix4 & GetTopMatrix() const;
    void PopMatrix();

    // Render matrices: View matrix
    void SetViewMatrix(const Matrix4 & m);
    const Matrix4 & GetViewMatrix() const;

    // Render matrices: Projection matrix
    void SetProjectionMatrix(const Matrix4 & m);
    const Matrix4 & GetProjectionMatrix() const;

    // Projection * view.
    void SetViewProjectionMatrix(const Matrix4 & view, const Matrix4 & projection);
    const Matrix4 & GetViewProjectionMatrix() const;

    // Model-view-protection (model is GetTopMatrix() from matrixStack).
    Matrix4 GetCurrentMVPMatrix() const;

    // Get/set camera reference. Matrices are not affected.
    const Camera & GetCameraRef() const;
    void SetCameraRef(const Camera & cam);

    // Manage the world's light source.
    void SetLightPosition(const Vector3 & pos);
    Vector3 GetLightPosition() const;
    Vector3 GetLightDirEyeSpace() const;

    // Set the platform specific window handle.
    void SetWindowObject(void * windowObj);

    // Clear screen and swap-buffer/present the scene:
    void ClearScreen();
    void SwapBuffers();

    // Enable/disable writes to the depth buffer:
    void EnableDepthWrite();
    void DisableDepthWrite();

    // Enable/disable depth testing:
    void EnableDepthTest();
    void DisableDepthTest();

    // Enable/disable polygon face culling.
    void EnablePolygonCull();
    void DisablePolygonCull();

    // Blend mode used by billboards and 2D sprites:
    void EnableSpriteBlendMode();
    void DisableColorBlend();

    // Takes a screenshot of the current framebuffer.
    // If target dimensions are not the same as the current framebuffer, the screenshot
    // will be scaled to fit the requested size. In such case, 'scaledImage' must not be null
    // and its size must match the target dimensions. If the target dimensions match the
    // framebuffer size, 'scaledImage' is not touched and may be null. Either way, 'framebuffer'
    // receives the unaltered contents of the current color framebuffer. Image pixel format will always be RGBA 8bits.
    // 'needFlipping' only applied to 'scaledImage'. If set, will flip the output vertically.
    void TakeScreenshot(unsigned int targetWidth, unsigned int targetHeight,
                        uint8_t * scaledImage, uint8_t * framebuffer, bool needFlipping);

    // Turn AVI video screen capture on or off.
    bool EnableAviScreenCapture(bool enable);

    // Sets the way we are going to draw the screen for a split screen game.
    void SetSplitScreen(const SplitScreen mode);

    // ---- Misc queries: ----

    // Best #version tag for GLSL shaders.
    std::string GetGLSLVersionTag() const;

    // Max levels of anisotropy supported by the texturing hardware.
    unsigned int GetMaxTextureAniso() const;

    // Window & framebuffer dimensions in pixels:
    Vec2u GetWindowDimensions() const;
    Vec2u GetFramebufferDimensions() const;

    // Same as the above, but returning a Vec4u with x = y = 0.
    Vec4u GetWindowRect() const;
    Vec4u GetFramebufferRect() const;

    // Current render states:
    GLState & GetState();
    const GLState & GetState() const;

    // Default OpenGL texturing filter constants. This function returns OpenGL compatible values, NOT Texture enum constants!
    void GetDefaultGLTexFilter(unsigned int * minFilter, unsigned int * magFilter, unsigned int * anisotropyAmount) const;

    // ---- Built-in resources (public): ----

    // Default textures:
    TexturePtr whiteTexture;
    TexturePtr blackTexture;
    TexturePtr grayTexture;
    TexturePtr defaultNormalTexture;
    TexturePtr defaultDiffuseTexture;
    TexturePtr defaultSpecularTexture;

    // Default Material and ShaderProgram:
    MaterialPtr defaultMaterial;
    ShaderProgramPtr defaultShaderProg;

    // Color used to clear the screen at the start of every frame.
    static const Color4f clearScrColor;

  private:

    // Internal helpers:
    void SetDefaultGLStates();
    void ParseGLSLVersion();
    void InitDefaultTextures();
    void InitDefaultMaterialShader();

    // Handle to the GLFWv3 application window.
    // As a void* so we don't have to make the GLFW API public.
    // Set by Common via SetWindowObject().
    void * windowObj; // GLFWwindow*

    // Actual size in pixels of the framebuffer:
    unsigned int framebufferWidth;
    unsigned int framebufferHeight;

    // Size of the window, from config vars.
    // Might actually differ from the framebuffer size.
    unsigned int windowWidth;
    unsigned int windowHeight;

    // Original size, in case we change the size with a split-screen game.
    Vec2u originalFramebufferSize;

    // OpenGL state caching:
    GLState glState;

    // Camera reference for the current view.
    const Camera * camera;

    // Matrix stack used by the hierarchical scene rendering.
    std::stack<Matrix4> matrixStack;

    // Frame state matrices:
    Matrix4 projectionMatrix, viewMatrix, vpMatrix;

    // Position of the single light source in the world.
    Vector3 lightPosWorldSpace;

    // Proper #version tag to be added to shaders:
    std::string glslVersionTag;

    DISABLE_COPY_AND_ASSIGN(RenderManager)
};

// Globally unique instance:
extern RenderManager * renderMgr;

// ======================================================
// OpenGL error checking:
// ======================================================

#if GL_DO_ERROR_CHECKING
    void ClearGLErrors();
    void HelperCheckGLErrors(const char * func, const char * file, const int line);
    #define CheckGLErrors() HelperCheckGLErrors(__FUNCTION__, __FILE__, __LINE__)
#else // !GL_DO_ERROR_CHECKING
    #define ClearGLErrors() /* No-op */
    #define CheckGLErrors() /* No-op */
#endif // GL_DO_ERROR_CHECKING

// Include inline definitions in the same namespace:
#include "Engine/Rendering/RenderManager.inl"

} // namespace Engine {}
