
// ================================================================================================
// -*- C++ -*-
// File: DefaultMaterialShader.cpp
// Author: Guilherme R. Lampert
// Created on: 15/08/14
// Brief: Initialization of the built-in Material and Shader Program.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/RenderManager.hpp"

namespace Engine
{

// ======================================================
// Default built-in Shader Program. No File IO needed:
// ======================================================

//
// SIMPLE PHONG LIT WITH A SINGLE DIRECTIONAL LIGHT SOURCE:
//
#define GLSL_LITERAL(src) #src
static const char builtInProgram[] = GLSL_LITERAL(

    @vertex_shader

    // Vertex inputs, in model space:
    layout(location = 0) in vec3 a_position;
    layout(location = 1) in vec3 a_normal;
    layout(location = 2) in vec3 a_tangent;
    layout(location = 3) in vec3 a_bitangent;
    layout(location = 4) in vec2 a_texCoords;

    // Vertex shader outputs:
    layout(location = 0) out vec2 v_texCoords;
    layout(location = 1) out vec3 v_normal;
    layout(location = 2) out vec4 v_eye;

    // Uniform variables:
    uniform mat4 u_MVPMatrix;
    uniform mat4 u_MVMatrix;
    uniform mat4 u_normalMatrix;

    void main()
    {
        mat3 normalMatrix = mat3(u_normalMatrix);

        v_texCoords = a_texCoords;
        v_normal    = normalize(normalMatrix * a_normal);
        v_eye       = -(u_MVMatrix * vec4(a_position, 1.0));
        gl_Position = u_MVPMatrix  * vec4(a_position, 1.0);
    }

    @vertex_end

    @fragment_shader

    // Inputs from previous stage:
    layout(location = 0) in vec2 v_texCoords;
    layout(location = 1) in vec3 v_normal;
    layout(location = 2) in vec4 v_eye;

    // Uniform variables:
    uniform sampler2D s_diffuseTexture; // tmu:0
    uniform vec4 u_diffuseColor;
    uniform vec4 u_specularColor;
    uniform vec4 u_ambientColor;
    uniform float u_shininess;
    uniform vec3 u_lightDirEyeSpace;

    // Final color written to the framebuffer:
    out vec4 fragColor;

    void main()
    {
        vec4 spec = vec4(0.0);
        vec3 n = normalize(v_normal);
        vec3 e = normalize(vec3(v_eye));
        float intensity = max(dot(n, u_lightDirEyeSpace), 0.0);

        // If the vertex is lit compute the specular color
        if (intensity > 0.0)
        {
            vec3 h = normalize(u_lightDirEyeSpace + e);
            float intSpec = max(dot(h, n), 0.0);
            spec = u_specularColor * pow(intSpec, u_shininess);
        }

        fragColor  = max(intensity * u_diffuseColor + spec, u_ambientColor); // Light contribution
        fragColor *= texture(s_diffuseTexture, v_texCoords);                 // Modulates the texture color
    }

    @fragment_end
);
#undef GLSL_LITERAL

//
// OLD "DEFAULT" SHADER. TEXTURE COLOR ONLY:
//
/*
#define GLSL_LITERAL(src) #src
static const char builtInProgram[] = GLSL_LITERAL(

	@vertex_shader

		// Vertex inputs, in model space:
		layout(location = 0) in vec3 a_position;
		layout(location = 1) in vec3 a_normal;
		layout(location = 2) in vec3 a_tangent;
		layout(location = 3) in vec3 a_bitangent;
		layout(location = 4) in vec2 a_texCoords;

		// Vertex shader outputs:
		layout(location = 0) out vec3 v_normal;
		layout(location = 1) out vec2 v_texCoords;

		// Uniform variables:
		uniform mat4 u_MVPMatrix;

		void main()
		{
			v_normal    = a_normal;
			v_texCoords = a_texCoords;
			gl_Position = u_MVPMatrix * vec4(a_position, 1.0);
		}

	@vertex_end

	@fragment_shader

		// Inputs from previous stage:
		layout(location = 0) in vec3 v_normal;
		layout(location = 1) in vec2 v_texCoords;

		// Uniform variables:
		uniform sampler2D s_diffuseTexture; // tmu:0
		uniform vec4 u_diffuseColor;

		// Final color written to the framebuffer:
		out vec4 fragColor;

		void main()
		{
			fragColor = texture(s_diffuseTexture, v_texCoords) * u_diffuseColor;
		}

	@fragment_end
);
#undef GLSL_LITERAL
*/

// ======================================================
// RenderManager::InitDefaultMaterialShader():
// ======================================================

void RenderManager::InitDefaultMaterialShader()
{
    common->PrintF("Initializing built-in Material and ShaderProgram...");

    // _default_program
    defaultShaderProg = NewShaderProgram();
    defaultShaderProg->InitFromData(DataBlob(reinterpret_cast<const uint8_t *>(builtInProgram),
                                    ArrayLength(builtInProgram)),
                                    ResourceManager::DefaultShaderProgramName);

    defaultShaderProg->MarkAsDefault();
    resourceMgr->RegisterNewResource(defaultShaderProg);

    // _default_mtr_0
    defaultMaterial = NewMaterial(); // A new material already has the defaults set.
    defaultMaterial->SetName(Material::GetEnumeratedMaterialName());
    defaultMaterial->MarkAsDefault(); // Force it to use defaultShaderProg
    defaultMaterial->SetupShaderProgram();
    resourceMgr->RegisterNewResource(defaultMaterial);

    common->PrintF("Done!");
}

} // namespace Engine {}
