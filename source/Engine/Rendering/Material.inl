
// ================================================================================================
// -*- C++ -*-
// File: Material.inl
// Author: Guilherme R. Lampert
// Created on: 12/08/14
// Brief: Inline definitions for Material class.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

inline ConstShaderProgramPtr Material::GetShaderProgram() const
{
    return shaderProg;
}

inline void Material::SetShaderProgram(const ConstShaderProgramPtr & prog)
{
    assert(prog != nullptr); // Must not be null!

    // Set new shader and refresh uniform var handles:
    shaderProg = prog;
    InitShaderUniformHandles();
}

inline ConstTexturePtr Material::GetDiffuseTexture() const
{
    return diffuseTexture;
}

inline void Material::SetDiffuseTexture(const ConstTexturePtr & tex)
{
    diffuseTexture = tex;
}

inline ConstTexturePtr Material::GetNormalTexture() const
{
    return normalTexture;
}

inline void Material::SetNormalTexture(const ConstTexturePtr & tex)
{
    normalTexture = tex;
}

inline ConstTexturePtr Material::GetSpecularTexture() const
{
    return specularTexture;
}

inline void Material::SetSpecularTexture(const ConstTexturePtr & tex)
{
    specularTexture = tex;
}

inline Color4f Material::GetAmbientColor() const
{
    return ambientColor;
}

inline void Material::SetAmbientColor(const Color4f c)
{
    ambientColor = c;
}

inline Color4f Material::GetDiffuseColor() const
{
    return diffuseColor;
}

inline void Material::SetDiffuseColor(const Color4f c)
{
    diffuseColor = c;
}

inline Color4f Material::GetSpecularColor() const
{
    return specularColor;
}

inline void Material::SetSpecularColor(const Color4f c)
{
    specularColor = c;
}

inline Color4f Material::GetEmissiveColor() const
{
    return emissiveColor;
}

inline void Material::SetEmissiveColor(const Color4f c)
{
    emissiveColor = c;
}

inline float Material::GetShininess() const
{
    return shininess;
}

inline void Material::SetShininess(const float s)
{
    shininess = s;
}

inline const std::string & Material::GetName() const
{
    return materialName;
}

inline void Material::SetName(std::string name)
{
    assert(!name.empty()); // Should always have a name!
    materialName = std::move(name);
}

inline unsigned int Material::GetAllFlags() const
{
    return materialFlags;
}

inline void Material::SetAllFlags(const unsigned int flags)
{
    materialFlags = flags;
}

inline void Material::SetFlag(const unsigned int flag)
{
    materialFlags |= flag;
}

inline void Material::ClearFlag(const unsigned int flag)
{
    materialFlags &= ~(flag);
}

inline bool Material::TestFlag(const unsigned int flag) const
{
    return materialFlags & flag;
}

inline void Material::SetMVPTransform(const Matrix4 & mvp, const Matrix4 & mv) const
{
    mvpMatrix = mvp;
    mvMatrix  = mv;
    mvpMatrixDirty = true;
}

inline void Material::MarkAsDefault()
{
    isDefault = true;
}
