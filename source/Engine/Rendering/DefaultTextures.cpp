
// ================================================================================================
// -*- C++ -*-
// File: DefaultTextures.cpp
// Author: Guilherme R. Lampert
// Created on: 14/08/14
// Brief: Built-in texture generation functions.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/RenderManager.hpp"

namespace Engine
{

// ======================================================
// Texture generator functions:
// ======================================================

namespace /* unnamed */
{

//
// Creates color filled textures.
// Color is provided via template parameters.
// Texture size is always 2x2 pixels.
//
template <int R, int G, int B, int A>
struct GenDefaultTexture
{
    static bool Func(std::string name, Texture * texture)
    {
        assert(texture != nullptr);

        constexpr int pixelCount = (2 * 2);
        Color4b data[pixelCount];

        for (int i = 0; i < pixelCount; ++i)
        {
            data[i].r = R;
            data[i].g = G;
            data[i].b = B;
            data[i].a = A;
        }

        const Texture::SetupParams params =
        {
            /* baseDimensions   = */ Vec2u(2, 2),
            /* type             = */ Texture::Type::Texture2D,
            /* addressing       = */ Texture::Addressing::Default,
            /* filter           = */ Texture::Filter::Default,
            /* pixelFormat      = */ Texture::PixelFormat::RGBA_U8,
            /* anisotropyAmount = */ 0,
            /* numMipMapLevels  = */ 2,
            /* baseMipMapLevel  = */ 0,
            /* maxMipMapLevel   = */ 1,
            /* genMipMaps       = */ true
        };
        return texture->InitFromDataEx(std::move(name), params, reinterpret_cast<uint8_t *>(data), sizeof(data));
    }
};

//
// Instantiations of GenDefaultTexture:
//
using FlatNormalTexture = GenDefaultTexture<128, 128, 255, 255>; // _flat_normal (null/flat normal-map)
using WhiteTexture      = GenDefaultTexture<255, 255, 255, 255>; // _white (opaque white)
using GrayTexture       = GenDefaultTexture<128, 128, 128, 255>; // _gray  (the default specular map)
using BlackTexture      = GenDefaultTexture<0,   0,   0,   0  >; // _black (is transparent!)

//
// Checkerboard pattern texture.
// Used as the default standard diffuse texture (_checkerboard).
//
static bool CheckerPatternTexture(std::string name, Texture * texture)
{
    assert(texture != nullptr);

    constexpr int numSquares  = 8;                      // Number of checker squares
    constexpr int imgSize     = 64;                     // Square size of the texture
    constexpr int checkerSize = (imgSize / numSquares); // Size of one checker square, in pixels.

    // Pink/white:
    const Color4b pattern[] =
    {
        { 255, 170, 190, 255 },
        { 255, 255, 255, 255 }
    };

    int startY = 0;
    int lastColor = 0;
    int color, rowX;
    Color4b data[imgSize * imgSize];

    while (startY < imgSize)
    {
        for (int y = startY; y < (startY + checkerSize); ++y)
        {
            color = lastColor, rowX = 0;
            for (int x = 0; x < imgSize; ++x)
            {
                if (rowX == checkerSize)
                {
                    // Invert color every time we complete a checker square:
                    color = !color;
                    rowX = 0;
                }
                data[x + y * imgSize] = pattern[color];
                ++rowX;
            }
        }
        startY += checkerSize;
        lastColor = !lastColor;
    }

    const Texture::SetupParams params =
    {
        /* baseDimensions   = */ Vec2u(imgSize, imgSize),
        /* type             = */ Texture::Type::Texture2D,
        /* addressing       = */ Texture::Addressing::Default,
        /* filter           = */ Texture::Filter::Default,
        /* pixelFormat      = */ Texture::PixelFormat::RGBA_U8,
        /* anisotropyAmount = */ 0,
        /* numMipMapLevels  = */ 7,
        /* baseMipMapLevel  = */ 0,
        /* maxMipMapLevel   = */ 6,
        /* genMipMaps       = */ true
    };
    return texture->InitFromDataEx(std::move(name), params, reinterpret_cast<uint8_t *>(data), sizeof(data));
}

} // namespace unnamed {}

// ======================================================
// RenderManager::InitDefaultTextures():
// ======================================================

void RenderManager::InitDefaultTextures()
{
    common->PrintF("Initializing built-in textures...");

    // _white
    whiteTexture = NewTexture();
    whiteTexture->InitFromFunction(ResourceManager::WhiteTextureName, &WhiteTexture::Func);
    whiteTexture->MarkAsDefault();
    resourceMgr->RegisterNewResource(whiteTexture);

    // _black
    blackTexture = NewTexture();
    blackTexture->InitFromFunction(ResourceManager::BlackTextureName, &BlackTexture::Func);
    blackTexture->MarkAsDefault();
    resourceMgr->RegisterNewResource(blackTexture);

    // _gray
    grayTexture = NewTexture();
    grayTexture->InitFromFunction(ResourceManager::GrayTextureName, &GrayTexture::Func);
    grayTexture->MarkAsDefault();
    resourceMgr->RegisterNewResource(grayTexture);

    // _flat_normal
    defaultNormalTexture = NewTexture();
    defaultNormalTexture->InitFromFunction(ResourceManager::FlatNormalTextureName, &FlatNormalTexture::Func);
    defaultNormalTexture->MarkAsDefault();
    resourceMgr->RegisterNewResource(defaultNormalTexture);

    // FIXME: Changed to the white texture to be able to render
    // models that have color only using the same shader.
    // we should provide a different shader for those without texture
    // and keep the checkerboard as default, since it makes easy to spot missing textures.
    defaultDiffuseTexture = whiteTexture;
    auto fnptr = &CheckerPatternTexture;
    (void)fnptr; // Just to quiet a compiler warning while the function call is commented out...
    /*
	// _checkerboard
	defaultDiffuseTexture = NewTexture();
	defaultDiffuseTexture->InitFromFunction(ResourceManager::DefaultDiffuseTextureName, &CheckerPatternTexture);
	defaultDiffuseTexture->MarkAsDefault();
	resourceMgr->RegisterNewResource(defaultDiffuseTexture);
	*/

    // Just an alias.
    defaultSpecularTexture = grayTexture;

    common->PrintF("Done!");
}

} // namespace Engine {}
