
// ================================================================================================
// -*- C++ -*-
// File: Font.cpp
// Author: Guilherme R. Lampert
// Created on: 06/09/14
// Brief: Font resource for text rendering. Can be loaded from a .FNT file created with BMFont.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"

// Renderer:
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Font.hpp"
#include "Engine/Rendering/RenderManager.hpp"

namespace Engine
{

// ======================================================
// Private FNTLoader helper class:
// ======================================================

//
// Loader/parser for FNT text files.
//
// Code adapted from:
//   "AngelCode Tool Box Library"
//   Copyright (c) 2007-2008 Andreas Jonsson
// Found here:
//   http://www.angelcode.com/dev/bmfonts/
//
class FNTLoader
{
  public:

    // Sets up the loader. This is a throw away object to be used once, the discarded.
    FNTLoader(const DataBlob blob, const std::string & filename, Font & outputFont);

    // Does the loading. Font will be ready once this returns.
    void Load();

    // Did the last call to Load() succeeded?
    bool WasSuccessful() { return loadSuccessful; }

  private:

    void LoadPage(const int index, std::string pageFile);
    void AddKerningPair(const int first, const int second, const int amount);
    void SetCommonInfo(const int fontHeight, const int base, const int scaleW,
                       const int scaleH, const int pages, const bool isPacked);

    // Read a line of text from the data blob (fntData).
    bool ReadLine(char * __restrict lineBuf, int maxCharsToRead, size_t & firstChar) const;

    // Miscellaneous text parsing helpers:
    static int SkipWhiteSpace(const std::string & str, const int start);
    static int FindEndOfToken(const std::string & str, const int start);
    void ParseInfo(const std::string & str, const int start);
    void ParseCommon(const std::string & str, const int start);
    void ParseChar(const std::string & str, const int start);
    void ParseKerning(const std::string & str, const int start);
    void ParsePage(const std::string & str, const int start);

    Font & fnt; // Output font
    const DataBlob fntData;
    const std::string & fntFilename;
    int fntOutlineThickness;
    bool loadSuccessful;
};

// ======================================================

FNTLoader::FNTLoader(const DataBlob blob, const std::string & filename, Font & outputFont)
    : fnt(outputFont)
    , fntData(blob)
    , fntFilename(filename)
    , fntOutlineThickness(0)
    , loadSuccessful(false)
{
}

void FNTLoader::Load()
{
    // Binary variation not yet supported.
    if ((fntData.data[0] == 'B') &&
        (fntData.data[1] == 'M') &&
        (fntData.data[2] == 'F'))
    {
        loadSuccessful = false;
        common->ErrorF("Binary FNT file format not yet supported!");
        return;
    }

    // Aux parsing data:
    std::string line, token;
    size_t firstChar = 0;
    char lineBuf[MaxTempStringLen];

    while (ReadLine(lineBuf, ArrayLength(lineBuf), firstChar))
    {
        line = lineBuf;
        const int pos1 = SkipWhiteSpace(line, 0);
        const int pos2 = FindEndOfToken(line, pos1);
        token = line.substr(pos1, pos2 - pos1);

        // Handle rest of line:
        if (token == "info")
        {
            ParseInfo(line, pos2);
        }
        else if (token == "common")
        {
            ParseCommon(line, pos2);
        }
        else if (token == "page")
        {
            ParsePage(line, pos2);
        }
        else if (token == "char")
        {
            ParseChar(line, pos2);
        }
        else if (token == "kerning")
        {
            ParseKerning(line, pos2);
        }
    }

    // If we get here, all went well.
    loadSuccessful = true;
    common->PrintF("Successfully parsed text FNT file!");
}

void FNTLoader::SetCommonInfo(const int fontHeight, const int base, const int scaleW,
                              const int scaleH, const int pages, const bool isPacked)
{
    fnt.fontHeight = fontHeight;
    fnt.base = base;
    fnt.scaleW = scaleW;
    fnt.scaleH = scaleH;
    fnt.glyphPages.resize(pages);

    if (isPacked && fntOutlineThickness)
    {
        fnt.hasOutline = true;
    }
}

void FNTLoader::AddKerningPair(const int first, const int second, const int amount)
{
    if ((first >= 0) && (first < 256))
    {
        auto it = fnt.glyphMap.find(first);
        if (it != std::end(fnt.glyphMap))
        {
            it->second.kerningPairs.push_back(second);
            it->second.kerningPairs.push_back(amount);
        }
    }
}

void FNTLoader::LoadPage(const int index, std::string pageFile)
{
    if (pageFile.empty() || (index < 0) || (size_t(index) > fnt.glyphPages.size()))
    {
        common->WarningF("Invalid texture filename or page index for font \"%s\"! index: %d, pageFile: '%s'",
                         fntFilename.c_str(), index, pageFile.c_str());
        return;
    }

    // Prepend default dir if needed:
    if (!StringStartsWith(pageFile, ResourceManager::FontsDirectory))
    {
        // "fonts" + "/" + pageFile
        pageFile = ResourceManager::FontsDirectory + System::PathSepString + pageFile;
    }

    fnt.glyphPages[index] = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource(pageFile));
}

bool FNTLoader::ReadLine(char * __restrict lineBuf, int maxCharsToRead, size_t & firstChar) const
{
    char * __restrict pointer = lineBuf;

    while (--maxCharsToRead)
    {
        if (firstChar >= fntData.sizeInBytes)
        {
            if (pointer == lineBuf) // At the end
            {
                return false;
            }
            break;
        }

        const char ch = static_cast<char>(fntData.data[firstChar++]);
        if ((*pointer++ = ch) == '\n')
        {
            break;
        }
    }

    *pointer = '\0';
    return true;
}

int FNTLoader::SkipWhiteSpace(const std::string & str, const int start)
{
    const unsigned int len = str.length();
    unsigned int n = start;

    while (n < len)
    {
        const char ch = str[n];
        if ((ch != ' ') && (ch != '\t') && (ch != '\r') && (ch != '\n'))
        {
            break;
        }
        ++n;
    }

    return static_cast<int>(n);
}

int FNTLoader::FindEndOfToken(const std::string & str, const int start)
{
    const unsigned int len = str.length();
    unsigned int n = start;

    // Read quoted string:
    if (str[n] == '"')
    {
        n++;
        while (n < len)
        {
            if (str[n] == '"')
            {
                ++n;
                break;
            }
            ++n;
        }
    }
    else
    {
        while (n < len)
        {
            const char ch = str[n];
            if ((ch == ' ') || (ch == '\t') || (ch == '\r') || (ch == '\n') || (ch == '='))
            {
                break;
            }
            ++n;
        }
    }

    return static_cast<int>(n);
}

void FNTLoader::ParseInfo(const std::string & str, const int start)
{
    assert(StringStartsWith(str, "info"));

    int pos1, pos2 = start;
    std::string token, value;

    // Read in all attributes:
    for (;;)
    {
        pos1 = SkipWhiteSpace(str, pos2);
        pos2 = FindEndOfToken(str, pos1);
        token = str.substr(pos1, pos2 - pos1);

        pos1 = SkipWhiteSpace(str, pos2);
        if ((size_t(pos1) == str.length()) || (str[pos1] != '='))
        {
            break;
        }

        pos1 = SkipWhiteSpace(str, pos1 + 1);
        pos2 = FindEndOfToken(str, pos1);
        value = str.substr(pos1, pos2 - pos1);

        if (token == "face")
        {
            fnt.faceName = value.substr(1, value.length() - 2); // Remove the quotes
        }
        else if (token == "size")
        {
            fnt.fontSize = ParseInt(value);
        }
        else if (token == "bold")
        {
            fnt.isBold = ((value == "1" || value == "true" || value == "yes") ? true : false);
        }
        else if (token == "italic")
        {
            fnt.isItalic = ((value == "1" || value == "true" || value == "yes") ? true : false);
        }
        else if (token == "outline")
        {
            fntOutlineThickness = ParseInt(value);
        }
        // NOTE: There are other attributes not currently being handled
        // for the "info" line. Add more cases here to handle those!

        if (size_t(pos1) == str.length())
        {
            break;
        }
    }
}

void FNTLoader::ParseCommon(const std::string & str, const int start)
{
    assert(StringStartsWith(str, "common"));

    int pos1, pos2 = start;
    std::string token, value;

    int fontHeight = 0;
    int base   = 0;
    int scaleW = 0;
    int scaleH = 0;
    int pages  = 0;
    int packed = 0;

    // Read in all attributes:
    for (;;)
    {
        pos1 = SkipWhiteSpace(str, pos2);
        pos2 = FindEndOfToken(str, pos1);
        token = str.substr(pos1, pos2 - pos1);

        pos1 = SkipWhiteSpace(str, pos2);
        if ((size_t(pos1) == str.length()) || (str[pos1] != '='))
        {
            break;
        }

        pos1 = SkipWhiteSpace(str, pos1 + 1);
        pos2 = FindEndOfToken(str, pos1);
        value = str.substr(pos1, pos2 - pos1);

        if (token == "lineHeight")
        {
            fontHeight = ParseInt(value);
        }
        else if (token == "base")
        {
            base = ParseInt(value);
        }
        else if (token == "scaleW")
        {
            scaleW = ParseInt(value);
        }
        else if (token == "scaleH")
        {
            scaleH = ParseInt(value);
        }
        else if (token == "pages")
        {
            pages = ParseInt(value);
        }
        else if (token == "packed")
        {
            packed = ParseInt(value);
        }
        // Currently, all known attributes for "common" are being handled.

        if (size_t(pos1) == str.length())
        {
            break;
        }
    }

    SetCommonInfo(fontHeight, base, scaleW, scaleH, pages, (packed ? true : false));
}

void FNTLoader::ParseChar(const std::string & str, const int start)
{
    assert(StringStartsWith(str, "char"));

    int pos1, pos2 = start;
    std::string token, value;

    int id       = 0;
    int x        = 0;
    int y        = 0;
    int width    = 0;
    int height   = 0;
    int xoffset  = 0;
    int yoffset  = 0;
    int xadvance = 0;
    int page     = 0;
    int chnl     = 0;

    // Read in all attributes:
    for (;;)
    {
        pos1 = SkipWhiteSpace(str, pos2);
        pos2 = FindEndOfToken(str, pos1);
        token = str.substr(pos1, pos2 - pos1);

        pos1 = SkipWhiteSpace(str, pos2);
        if ((size_t(pos1) == str.length()) || (str[pos1] != '='))
        {
            break;
        }

        pos1 = SkipWhiteSpace(str, pos1 + 1);
        pos2 = FindEndOfToken(str, pos1);
        value = str.substr(pos1, pos2 - pos1);

        if (token == "id")
        {
            id = ParseInt(value);
        }
        else if (token == "x")
        {
            x = ParseInt(value);
        }
        else if (token == "y")
        {
            y = ParseInt(value);
        }
        else if (token == "width")
        {
            width = ParseInt(value);
        }
        else if (token == "height")
        {
            height = ParseInt(value);
        }
        else if (token == "xoffset")
        {
            xoffset = ParseInt(value);
        }
        else if (token == "yoffset")
        {
            yoffset = ParseInt(value);
        }
        else if (token == "xadvance")
        {
            xadvance = ParseInt(value);
        }
        else if (token == "page")
        {
            page = ParseInt(value);
        }
        else if (token == "chnl")
        {
            chnl = ParseInt(value);
        }
        // Currently, all known attributes for "char" are being handled.

        if (size_t(pos1) == str.length())
        {
            break;
        }
    }

    fnt.glyphMap.emplace(std::make_pair(id, Font::GlyphInfo(x, y, width, height, xoffset, yoffset, xadvance, page, chnl)));
}

void FNTLoader::ParseKerning(const std::string & str, const int start)
{
    assert(StringStartsWith(str, "kerning"));

    int pos1, pos2 = start;
    std::string token, value;

    int first  = 0;
    int second = 0;
    int amount = 0;

    // Read in all attributes:
    for (;;)
    {
        pos1 = SkipWhiteSpace(str, pos2);
        pos2 = FindEndOfToken(str, pos1);
        token = str.substr(pos1, pos2 - pos1);

        pos1 = SkipWhiteSpace(str, pos2);
        if ((size_t(pos1) == str.length()) || (str[pos1] != '='))
        {
            break;
        }

        pos1 = SkipWhiteSpace(str, pos1 + 1);
        pos2 = FindEndOfToken(str, pos1);
        value = str.substr(pos1, pos2 - pos1);

        if (token == "first")
        {
            first = ParseInt(value);
        }
        else if (token == "second")
        {
            second = ParseInt(value);
        }
        else if (token == "amount")
        {
            amount = ParseInt(value);
        }
        // Currently, all known attributes for "page" are being handled.

        if (size_t(pos1) == str.length())
        {
            break;
        }
    }

    AddKerningPair(first, second, amount);
}

void FNTLoader::ParsePage(const std::string & str, const int start)
{
    assert(StringStartsWith(str, "page"));

    int pos1, pos2 = start;
    std::string token, value;

    int id = 0;
    std::string file;

    // Read in all attributes:
    for (;;)
    {
        pos1 = SkipWhiteSpace(str, pos2);
        pos2 = FindEndOfToken(str, pos1);
        token = str.substr(pos1, pos2 - pos1);

        pos1 = SkipWhiteSpace(str, pos2);
        if ((size_t(pos1) == str.length()) || (str[pos1] != '='))
        {
            break;
        }

        pos1 = SkipWhiteSpace(str, pos1 + 1);
        pos2 = FindEndOfToken(str, pos1);
        value = str.substr(pos1, pos2 - pos1);

        if (token == "id")
        {
            id = ParseInt(value);
        }
        else if (token == "file")
        {
            file = value.substr(1, value.length() - 2); // Remove the quotes
        }
        // Currently, all known attributes for "page" are being handled.

        if (size_t(pos1) == str.length())
        {
            break;
        }
    }

    // Actually find/load the image:
    LoadPage(id, file);
}

// ======================================================
// Font::GlyphInfo:
// ======================================================

Font::GlyphInfo::GlyphInfo() noexcept
    : x(0)
    , y(0)
    , width(0)
    , height(0)
    , xOffset(0)
    , yOffset(0)
    , xAdvance(0)
    , pageNum(0)
    , channel(0)
{
}

Font::GlyphInfo::GlyphInfo(const int x, const int y, const int w, const int h, const int xOffs,
                           const int yOffs, const int xAdv, const int pageNum, const int channel) noexcept
    : x(int16_t(x))
    , y(int16_t(y))
    , width(int16_t(w))
    , height(int16_t(h))
    , xOffset(int16_t(xOffs))
    , yOffset(int16_t(yOffs))
    , xAdvance(int16_t(xAdv))
    , pageNum(int16_t(pageNum))
    , channel(int16_t(channel))
{
}

bool Font::GlyphInfo::operator==(const Font::GlyphInfo & other) const noexcept
{
    // Same object?
    if (this == &other)
    {
        return true;
    }

    // Do a full comparison:
    if (this->x != other.x)
    {
        return false;
    }
    if (this->y != other.y)
    {
        return false;
    }
    if (this->width != other.width)
    {
        return false;
    }
    if (this->height != other.height)
    {
        return false;
    }
    if (this->xOffset != other.xOffset)
    {
        return false;
    }
    if (this->yOffset != other.yOffset)
    {
        return false;
    }
    if (this->xAdvance != other.xAdvance)
    {
        return false;
    }
    if (this->channel != other.channel)
    {
        return false;
    }
    if (this->pageNum != other.pageNum)
    {
        return false;
    }
    if (this->kerningPairs != other.kerningPairs)
    {
        return false;
    }

    // Same attributes.
    return true;
}

bool Font::GlyphInfo::operator!=(const Font::GlyphInfo & other) const noexcept
{
    return !(*this == other);
}

// Dummy value returned by Font::GetGlyphInfo().
const Font::GlyphInfo Font::NullGlyphInfo;

// ======================================================
// Font:
// ======================================================

Font::Font()
    : fontHeight(0)
    , fontSize(0)
    , base(0)
    , scaleW(0)
    , scaleH(0)
    , scale(1.0f)
    , encoding(Encoding::ASCII)
    , hasOutline(false)
    , isBold(false)
    , isItalic(false)
{
}

bool Font::InitFromFile(std::string filename, std::string resourceId)
{
    DataBlob fileContents = DataBlob::LoadFile(filename, /* isBinary = */ false);
    SCOPE_EXIT({ fileContents.FreeData(); });

    // Proceed if the file was loaded and had contents.
    // On error, LoadFile() will print more info to the Engine log.
    if (fileContents.IsValid())
    {
        return InitFromData(fileContents, std::move(resourceId));
    }

    common->ErrorF("Failed to initialized Font from file!");
    return false;
}

bool Font::InitFromData(const DataBlob blob, std::string filename)
{
    assert(blob.IsValid());
    assert(!filename.empty());

    // Load from FNT data:
    FNTLoader fntLoader(blob, filename, *this);
    fntLoader.Load();

    // Remember original file:
    fontFilename = std::move(filename);

    // Currently, no errors are generate by FNTLoader.
    return fntLoader.WasSuccessful();
}

void Font::FreeAllData()
{
    fontHeight = 0;
    fontSize   = 0;
    base       = 0;
    scaleW     = 0;
    scaleH     = 0;
    scale      = 1.0f;
    encoding   = Encoding::ASCII;
    hasOutline = false;
    isBold     = false;
    isItalic   = false;

    glyphMap.clear();
    glyphPages.clear();
    faceName.clear();
    fontFilename.clear();
}

bool Font::IsDefault() const
{
    return false; // TODO
}

size_t Font::GetEstimateMemoryUsage() const
{
    // Glyph texture page memory:
    size_t texMem = 0;
    for (const auto & page : glyphPages)
    {
        texMem += page->GetEstimateMemoryUsage();
    }

    // Each glyph is stored as a pair of <int, GlyphInfo>
    const size_t glyphMem = glyphMap.size() * (sizeof(int) + sizeof(GlyphInfo));

    // Add everything up for a more accurate estimate:
    return sizeof(Font) + glyphMem + texMem + faceName.size() + fontFilename.size();
}

std::string Font::GetResourceId() const
{
    return fontFilename;
}

std::string Font::GetResourceTypeString() const
{
    return "Font";
}

void Font::PrintSelf() const
{
    const char * encodingStr[] = { "ASCII", "UTF-8", "UTF-16" };
    const char * boolStr[] = { "no", "yes" };

    common->PrintF("-------- Font ---------");
    common->PrintF("Face name.......: %s", faceName.c_str());
    common->PrintF("Font filename...: %s", fontFilename.c_str());
    common->PrintF("Memory usage....: %s", FormatMemoryUnit(GetEstimateMemoryUsage(), true).c_str());
    common->PrintF("Font height.....: %d", fontHeight);
    common->PrintF("Font size.......: %d", fontSize);
    common->PrintF("Base............: %d", base);
    common->PrintF("Scale W.........: %d", scaleW);
    common->PrintF("Scale H.........: %d", scaleH);
    common->PrintF("Float scale.....: %.3f", scale);
    common->PrintF("Encoding........: %s", encodingStr[int(encoding)]);
    common->PrintF("Has outline?....: %s", boolStr[int(HasOutline())]);
    common->PrintF("Is bold?........: %s", boolStr[int(IsBold())]);
    common->PrintF("Is italic?......: %s", boolStr[int(IsItalic())]);
    common->PrintF("Is default?.....: %s", boolStr[int(IsDefault())]);

    common->PrintF("----- Font Pages ------");
    for (const auto & page : glyphPages)
    {
        common->PrintF("\"%s\" : %ux%u", page->GetResourceId().c_str(), page->GetWidth(), page->GetHeight());
    }

    common->PrintF("----- Font Glyphs -----");
    for (const auto & glyphPair : glyphMap)
    {
        const GlyphInfo & glyph = glyphPair.second;
        common->PrintF("id=%-4d x=%-4d y=%-4d w=%-4d h=%-4d xOffs=%-4d yOffs=%-4d xAdvance=%-4d pageNum=%-4d chnl=%-4d kerningPairs=%zu",
                       glyphPair.first, glyph.x, glyph.y, glyph.width, glyph.height, glyph.xOffset,
                       glyph.yOffset, glyph.xAdvance, glyph.pageNum, glyph.channel, glyph.kerningPairs.size());
    }

    common->PrintF("-----------------------");
}

bool Font::HasGlyph(const int glyphId) const
{
    return glyphMap.find(glyphId) != std::end(glyphMap);
}

const Font::GlyphInfo & Font::GetGlyphInfo(const int glyphId) const
{
    const auto it = glyphMap.find(glyphId);
    if (it != std::end(glyphMap))
    {
        return it->second;
    }
    return NullGlyphInfo;
}

int Font::GetTextLength(const char * text) const
{
    assert(text != nullptr);

    if (encoding == Encoding::UTF16)
    {
        int textLen = 0;
        for (;;)
        {
            unsigned int len;
            const int r = DecodeUTF16(&text[textLen], &len);
            if (r > 0)
            {
                textLen += len;
            }
            else if (r < 0)
            {
                ++textLen;
            }
            else
            {
                return textLen;
            }
        }
    }

    // Both UTF8 and standard ASCII strings can use strlen().
    return static_cast<int>(strlen(text));
}

int Font::GetTextChar(const char * text, const int pos, int * nextPos) const
{
    assert(text != nullptr);

    int ch;
    unsigned int len;
    switch (encoding)
    {
    case Encoding::UTF8:
        ch = DecodeUTF8(&text[pos], &len);
        if (ch == -1)
        {
            len = 1;
        }
        break;

    case Encoding::UTF16:
        ch = DecodeUTF16(&text[pos], &len);
        if (ch == -1)
        {
            len = 2;
        }
        break;

    default: // ASCII
        ch = static_cast<unsigned char>(text[pos]);
        len = 1;
        break;
    } // switch (encoding)

    if (nextPos != nullptr)
    {
        *nextPos = (pos + len);
    }

    return ch;
}

int Font::FindTextChar(const char * text, const int start, const int length, const int ch) const
{
    assert(text != nullptr);

    int pos = start;
    int nextPos, currChar = -1;

    while (pos < length)
    {
        currChar = GetTextChar(text, pos, &nextPos);
        if (currChar == ch)
        {
            return pos;
        }
        pos = nextPos;
    }

    return -1;
}

float Font::AdjustForKerningPairs(const int first, const int second) const
{
    const auto it = glyphMap.find(first);
    if (it == std::end(glyphMap))
    {
        return 0.0f;
    }

    const GlyphInfo & glyph = it->second;
    const size_t numPairs = glyph.kerningPairs.size();

    for (size_t n = 0; n < numPairs; n += 2)
    {
        if (glyph.kerningPairs[n] == second)
        {
            return glyph.kerningPairs[n + 1] * scale;
        }
    }

    return 0.0f;
}

// ======================================================
// Font factory function for ResourceManager:
// ======================================================

static ResourcePtr Font_FactoryFunction(const bool forceDefault)
{
    if (forceDefault)
    {
        // FIXME: How do we make a default?
        // using defaultDiffuseTexture? Maybe the built-in fonts?
        return renderMgr->NewFont();
    }
    else
    {
        return renderMgr->NewFont();
    }
}

// Register the callback:
REGISTER_RESOURCE_FACTORY_FOR_TYPE(Resource::Type::Font, Font_FactoryFunction);

} // namespace Engine {}
