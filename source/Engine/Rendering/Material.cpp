
// ================================================================================================
// -*- C++ -*-
// File: Material.cpp
// Author: Guilherme R. Lampert
// Created on: 08/08/14
// Brief: Rendering and shading properties for a surface/object.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/RenderManager.hpp"

namespace Engine
{

// ======================================================
// Static properties of Material:
// ======================================================

const Color4f Material::NullColor(0.0f, 0.0f, 0.0f, 0.0f);
unsigned int Material::nextMaterialNum(0);
unsigned int Material::nextMaterialId(0);

// FIXME: We don't have to keep nextMaterialId or the integer id inside Material.
// The object address can safely be used instead, since each material is unique.
// Replace materialId with HashPointer(this).

// ======================================================
// Material:
// ======================================================

Material::Material()
    : materialId(++nextMaterialId) // Starts at 1
    , isDefault(false)
{
    // Init with defaults:
    ClearShaderUniformHandles();
    SetDefaults();
}

void Material::SetDefaults()
{
    diffuseTexture = renderMgr->defaultDiffuseTexture;
    normalTexture = renderMgr->defaultNormalTexture;
    specularTexture = renderMgr->defaultSpecularTexture;
    ambientColor = Color4f(0.3f, 0.3f, 0.3f, 1.0f);  // dark gray
    diffuseColor = Color4f(1.0f, 1.0f, 1.0f, 1.0f);  // white
    specularColor = Color4f(0.5f, 0.5f, 0.5f, 1.0f); // gray
    emissiveColor = Color4f(0.0f, 0.0f, 0.0f, 1.0f); // black
    shininess = 50.0f;
    materialFlags = 0;
    mvpMatrix = Matrix4Identity;
    mvMatrix = Matrix4Identity;
    mvpMatrixDirty = true;
}

void Material::SetupShaderProgram()
{
    if (isDefault)
    {
        SetShaderProgram(renderMgr->defaultShaderProg);
        // SetShaderProgram() calls InitShaderUniformHandles().
    }
    else
    {
        //TODO temp code.
        // select based on attributes of material?
        // or should we use a one-size-fits-all shader?
        SetShaderProgram(renderMgr->defaultShaderProg);
    }
}

void Material::InitShaderUniformHandles()
{
    assert(shaderProg != nullptr);

    // Textures:
    uniforms.s_diffuseTexture   = shaderProg->FindShaderUniform("s_diffuseTexture");
    uniforms.s_normalTexture    = shaderProg->FindShaderUniform("s_normalTexture");
    uniforms.s_specularTexture  = shaderProg->FindShaderUniform("s_specularTexture");

    // Colors/misc:
    uniforms.u_ambientColor     = shaderProg->FindShaderUniform("u_ambientColor");
    uniforms.u_diffuseColor     = shaderProg->FindShaderUniform("u_diffuseColor");
    uniforms.u_specularColor    = shaderProg->FindShaderUniform("u_specularColor");
    uniforms.u_emissiveColor    = shaderProg->FindShaderUniform("u_emissiveColor");
    uniforms.u_shininess        = shaderProg->FindShaderUniform("u_shininess");
    uniforms.u_MVPMatrix        = shaderProg->FindShaderUniform("u_MVPMatrix");
    uniforms.u_MVMatrix         = shaderProg->FindShaderUniform("u_MVMatrix");
    uniforms.u_normalMatrix     = shaderProg->FindShaderUniform("u_normalMatrix");
    uniforms.u_lightDirEyeSpace = shaderProg->FindShaderUniform("u_lightDirEyeSpace");

    // Texture samplers only need to be set one. They do not change.
    shaderProg->Bind();
    shaderProg->SetShaderUniform(uniforms.s_diffuseTexture, static_cast<int>(TMU::DiffuseMap));
    shaderProg->SetShaderUniform(uniforms.s_normalTexture, static_cast<int>(TMU::NormalMap));
    shaderProg->SetShaderUniform(uniforms.s_specularTexture, static_cast<int>(TMU::SpecularMap));
    shaderProg->Unbind();
}

void Material::ClearShaderUniformHandles()
{
    uniforms.s_diffuseTexture   = InvalidShaderUniformHandle;
    uniforms.s_normalTexture    = InvalidShaderUniformHandle;
    uniforms.s_specularTexture  = InvalidShaderUniformHandle;
    uniforms.u_ambientColor     = InvalidShaderUniformHandle;
    uniforms.u_diffuseColor     = InvalidShaderUniformHandle;
    uniforms.u_specularColor    = InvalidShaderUniformHandle;
    uniforms.u_emissiveColor    = InvalidShaderUniformHandle;
    uniforms.u_shininess        = InvalidShaderUniformHandle;
    uniforms.u_MVPMatrix        = InvalidShaderUniformHandle;
    uniforms.u_MVMatrix         = InvalidShaderUniformHandle;
    uniforms.u_normalMatrix     = InvalidShaderUniformHandle;
    uniforms.u_lightDirEyeSpace = InvalidShaderUniformHandle;
}

void Material::CommitShaderUniforms() const
{
    assert(shaderProg != nullptr);
    assert(shaderProg->IsCurrent());
    // Texture units are set once by SetupShaderProgram() and never change.

    if (uniforms.u_ambientColor != InvalidShaderUniformHandle)
    {
        shaderProg->SetShaderUniform(uniforms.u_ambientColor, ambientColor);
    }
    if (uniforms.u_diffuseColor != InvalidShaderUniformHandle)
    {
        shaderProg->SetShaderUniform(uniforms.u_diffuseColor, diffuseColor);
    }
    if (uniforms.u_specularColor != InvalidShaderUniformHandle)
    {
        shaderProg->SetShaderUniform(uniforms.u_specularColor, specularColor);
    }
    if (uniforms.u_emissiveColor != InvalidShaderUniformHandle)
    {
        shaderProg->SetShaderUniform(uniforms.u_emissiveColor, emissiveColor);
    }
    if (uniforms.u_shininess != InvalidShaderUniformHandle)
    {
        shaderProg->SetShaderUniform(uniforms.u_shininess, shininess);
    }
    if (uniforms.u_lightDirEyeSpace != InvalidShaderUniformHandle)
    {
        shaderProg->SetShaderUniform(uniforms.u_lightDirEyeSpace, renderMgr->GetLightDirEyeSpace());
    }
}

bool Material::InitFromFile(std::string filename, std::string resourceId)
{
    //TODO
    (void)filename;
    (void)resourceId;
    return false;
}

bool Material::InitFromData(const DataBlob blob, std::string filename)
{
    //TODO
    (void)blob;
    (void)filename;
    return false;
}

void Material::FreeAllData()
{
    // Set all pointers to null.
    // (Smart pointers; Will auto cleanup).
    shaderProg      = nullptr;
    diffuseTexture  = nullptr;
    normalTexture   = nullptr;
    specularTexture = nullptr;

    // Reset colors/params:
    ambientColor   = NullColor;
    diffuseColor   = NullColor;
    specularColor  = NullColor;
    emissiveColor  = NullColor;
    shininess      = 0.0f;
    materialFlags  = 0;
    mvpMatrix      = Matrix4Identity;
    mvMatrix       = Matrix4Identity;
    mvpMatrixDirty = true;

    // Invalidate all handles:
    ClearShaderUniformHandles();

    // Clear the name as well:
    materialName.clear();
}

bool Material::IsDefault() const
{
    return isDefault;
}

size_t Material::GetEstimateMemoryUsage() const
{
    return sizeof(Material);
}

std::string Material::GetResourceId() const
{
    return materialName;
}

std::string Material::GetResourceTypeString() const
{
    return "Material";
}

void Material::PrintSelf() const
{
    common->PrintF("------- Material ------");
    common->PrintF("Name............: %s", materialName.c_str());
    common->PrintF("Shader prog.....: %s", ((shaderProg != nullptr) ? shaderProg->GetResourceId().c_str() : "<NULL>"));
    common->PrintF("Diffuse tex.....: %s", ((diffuseTexture != nullptr) ? diffuseTexture->GetResourceId().c_str() : "<NULL>"));
    common->PrintF("Normal tex......: %s", ((normalTexture != nullptr) ? normalTexture->GetResourceId().c_str() : "<NULL>"));
    common->PrintF("Specular tex....: %s", ((specularTexture != nullptr) ? specularTexture->GetResourceId().c_str() : "<NULL>"));
    common->PrintF("Ambient color...: %s", ToString(ambientColor).c_str());
    common->PrintF("Diffuse color...: %s", ToString(diffuseColor).c_str());
    common->PrintF("Specular color..: %s", ToString(specularColor).c_str());
    common->PrintF("Emissive color..: %s", ToString(emissiveColor).c_str());
    common->PrintF("Shininess.......: %.2f", shininess);
    common->PrintF("Material flags..: 0x%x", materialFlags);
    common->PrintF("Material id.....: %u", materialId);
    common->PrintF("Is default?.....: %s", (IsDefault() ? "yes" : "no"));
}

void Material::Apply() const
{
    assert(shaderProg != nullptr);

    // Always need to update if dirty flag is set:
    if (mvpMatrixDirty)
    {
        shaderProg->Bind();
        if (uniforms.u_MVPMatrix != InvalidShaderUniformHandle)
        {
            shaderProg->SetShaderUniform(uniforms.u_MVPMatrix, mvpMatrix);
        }
        if (uniforms.u_MVMatrix != InvalidShaderUniformHandle)
        {
            shaderProg->SetShaderUniform(uniforms.u_MVMatrix, mvMatrix);
        }
        if (uniforms.u_normalMatrix != InvalidShaderUniformHandle)
        {
            const Matrix4 normalMatrix = transpose(inverse(mvMatrix));
            shaderProg->SetShaderUniform(uniforms.u_normalMatrix, normalMatrix);
        }
        if (uniforms.u_lightDirEyeSpace != InvalidShaderUniformHandle)
        {
            shaderProg->SetShaderUniform(uniforms.u_lightDirEyeSpace, renderMgr->GetLightDirEyeSpace());
        }
        mvpMatrixDirty = false;
    }

    GLState & glState = renderMgr->GetState();

    // Only go thru the trouble of resetting further material
    // properties if the current material have changed.
    if (materialId == glState.currentMaterial)
    {
        return;
    }

    if ((materialId != glState.currentMaterial) || !shaderProg->IsCurrent())
    {
        shaderProg->Bind();
        CommitShaderUniforms();
    }

    // Diffuse map:
    if (diffuseTexture != nullptr)
    {
        diffuseTexture->Bind(static_cast<int>(TMU::DiffuseMap));
    }
    else
    {
        renderMgr->defaultDiffuseTexture->Bind(static_cast<int>(TMU::DiffuseMap));
    }

    // Normal map:
    if (normalTexture != nullptr)
    {
        normalTexture->Bind(static_cast<int>(TMU::NormalMap));
    }
    else
    {
        renderMgr->defaultNormalTexture->Bind(static_cast<int>(TMU::NormalMap));
    }

    // Specular map:
    if (specularTexture != nullptr)
    {
        specularTexture->Bind(static_cast<int>(TMU::SpecularMap));
    }
    else
    {
        renderMgr->defaultSpecularTexture->Bind(static_cast<int>(TMU::SpecularMap));
    }

    // Set a current:
    glState.currentMaterial = materialId;
}

std::string Material::GetEnumeratedMaterialName()
{
    return Format("%s%u", ResourceManager::DefaultMaterialNamePrefix.c_str(), nextMaterialNum++);
}

// ======================================================
// Material factory function for ResourceManager:
// ======================================================

static ResourcePtr Material_FactoryFunction(const bool forceDefault)
{
    if (forceDefault)
    {
        return renderMgr->defaultMaterial;
    }
    else
    {
        return renderMgr->NewMaterial();
    }
}

// Register the callback:
REGISTER_RESOURCE_FACTORY_FOR_TYPE(Resource::Type::Material, Material_FactoryFunction);

} // namespace Engine {}
