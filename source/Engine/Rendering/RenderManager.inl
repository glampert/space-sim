
// ================================================================================================
// -*- C++ -*-
// File: RenderManager.inl
// Author: Guilherme R. Lampert
// Created on: 09/08/14
// Brief: Inline methods of RenderManager and helpers.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// ======================================================
// GLState:
// ======================================================

inline GLState::GLState()
{
    Reset();
}

inline void GLState::Reset()
{
    currentShaderProg = 0;
    currentMaterial   = 0;

    currentVAO = 0;
    currentVBO = 0;
    currentIBO = 0;

    currentTexture.fill(0);
}

// ======================================================
// RenderManager singleton:
// ======================================================

inline std::string RenderManager::GetGLSLVersionTag() const
{
    return glslVersionTag;
}

inline unsigned int RenderManager::GetMaxTextureAniso() const
{
    return 0; // TODO
}

inline Vec2u RenderManager::GetWindowDimensions() const
{
    return Vec2u(windowWidth, windowHeight);
}

inline Vec2u RenderManager::GetFramebufferDimensions() const
{
    return Vec2u(framebufferWidth, framebufferHeight);
}

inline Vec4u RenderManager::GetWindowRect() const
{
    return Vec4u(0, 0, windowWidth, windowHeight);
}

inline Vec4u RenderManager::GetFramebufferRect() const
{
    return Vec4u(0, 0, framebufferWidth, framebufferHeight);
}

inline GLState & RenderManager::GetState()
{
    return glState;
}

inline const GLState & RenderManager::GetState() const
{
    return glState;
}

inline void RenderManager::SetWindowObject(void * windowObj)
{
    assert(windowObj != nullptr);
    this->windowObj = windowObj;
}

inline void RenderManager::SetViewMatrix(const Matrix4 & m)
{
    viewMatrix = m;
    vpMatrix = projectionMatrix * viewMatrix;
}

inline const Matrix4 & RenderManager::GetViewMatrix() const
{
    return viewMatrix;
}

inline void RenderManager::SetProjectionMatrix(const Matrix4 & m)
{
    projectionMatrix = m;
    vpMatrix = projectionMatrix * viewMatrix;
}

inline const Matrix4 & RenderManager::GetProjectionMatrix() const
{
    return projectionMatrix;
}

inline void RenderManager::SetViewProjectionMatrix(const Matrix4 & view, const Matrix4 & projection)
{
    projectionMatrix = projection;
    viewMatrix = view;
    vpMatrix = projectionMatrix * viewMatrix;
}

inline const Matrix4 & RenderManager::GetViewProjectionMatrix() const
{
    return vpMatrix;
}

inline Matrix4 RenderManager::GetCurrentMVPMatrix() const
{
    return vpMatrix * GetTopMatrix();
}

inline const Camera & RenderManager::GetCameraRef() const
{
    assert(camera != nullptr);
    return *camera;
}

inline void RenderManager::SetCameraRef(const Camera & cam)
{
    camera = &cam;
}

inline void RenderManager::SetLightPosition(const Vector3 & pos)
{
    lightPosWorldSpace = pos;
}

inline Vector3 RenderManager::GetLightPosition() const
{
    return lightPosWorldSpace;
}
