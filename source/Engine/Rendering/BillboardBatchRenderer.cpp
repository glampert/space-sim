
// ================================================================================================
// -*- C++ -*-
// File: BillboardBatchRenderer.cpp
// Author: Guilherme R. Lampert
// Created on: 02/09/14
// Brief: 3D billboard rendering.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/BillboardBatchRenderer.hpp"
#include "Engine/Rendering/OpenGL.hpp"

namespace Engine
{

// ======================================================
// Local helpers:
// ======================================================

namespace /* unnamed */
{

// Billboard vertex format, used internally:
struct BBVertex
{
    Vec3f position;
    Vec4f color;
    Vec2f uvs;
};
static_assert(sizeof(BBVertex) == (sizeof(Vec3f) + sizeof(Vec4f) + sizeof(Vec2f)), "Wrong size! Fix this!");

// Convert from Color4b to Color4f:
static Color4f ToColor4f(const Color4b & color)
{
    return Color4f((color.r * (1.0f / 255.0f)),
                   (color.g * (1.0f / 255.0f)),
                   (color.b * (1.0f / 255.0f)),
                   (color.a * (1.0f / 255.0f)));
}

// Convert from Color4f to Color4b:
static Color4b ToColor4b(const Color4f & color)
{
    return Color4b(static_cast<uint8_t>(color.r * 255.0f),
                   static_cast<uint8_t>(color.g * 255.0f),
                   static_cast<uint8_t>(color.b * 255.0f),
                   static_cast<uint8_t>(color.a * 255.0f));
}

} // namespace unnamed {}

// ======================================================
// MakeBillboardFacingCameraPlane():
// ======================================================

Billboard MakeBillboardFacingCameraPlane(const Vector3 up, const Vector3 right,
                                         const Vector3 origin, const Vec2f size,
                                         const Color4f color)
{
    const Vector3 halfWidth = (size.width * 0.5f) * right; // X
    const Vector3 halfHeigh = (size.height * 0.5f) * up;   // Y

    Billboard result;
    result.origin = origin;

    // Create the points:
    result.points[0].Set(origin + halfWidth + halfHeigh);
    result.points[1].Set(origin - halfWidth + halfHeigh);
    result.points[2].Set(origin - halfWidth - halfHeigh);
    result.points[3].Set(origin + halfWidth - halfHeigh);

    // Set standard texture coords:
    result.texCoords[0].Set(0.0f, 0.0f);
    result.texCoords[1].Set(1.0f, 0.0f);
    result.texCoords[2].Set(1.0f, 1.0f);
    result.texCoords[3].Set(0.0f, 1.0f);

    // Set vertex color:
    const Color4b c = ToColor4b(color);
    result.colors[0] = c;
    result.colors[1] = c;
    result.colors[2] = c;
    result.colors[3] = c;

    return result;
}

// ======================================================
// MakeBillboardFacingCameraPosition():
// ======================================================

Billboard MakeBillboardFacingCameraPosition(const Vector3 up, const Vector3 cameraPos,
                                            const Vector3 origin, const Vec2f size,
                                            const Color4f color)
{
    const Vector3 Z = normalize(cameraPos - origin);
    const Vector3 X = normalize(cross(up, Z));
    const Vector3 Y = cross(Z, X);

    const Vector3 halfWidth = (size.width * 0.5f) * X;
    const Vector3 halfHeigh = (size.height * 0.5f) * Y;

    Billboard result;
    result.origin = origin;

    // Create the points:
    result.points[0].Set(origin + halfWidth + halfHeigh);
    result.points[1].Set(origin - halfWidth + halfHeigh);
    result.points[2].Set(origin - halfWidth - halfHeigh);
    result.points[3].Set(origin + halfWidth - halfHeigh);

    // Set standard texture coords:
    result.texCoords[0].Set(0.0f, 0.0f);
    result.texCoords[1].Set(1.0f, 0.0f);
    result.texCoords[2].Set(1.0f, 1.0f);
    result.texCoords[3].Set(0.0f, 1.0f);

    // Set vertex color:
    const Color4b c = ToColor4b(color);
    result.colors[0] = c;
    result.colors[1] = c;
    result.colors[2] = c;
    result.colors[3] = c;

    return result;
}

// ======================================================
// BillboardBatchRenderer:
// ======================================================

BillboardBatchRenderer::BillboardBatchRenderer()
    : maxBillboardsPerBillboardRenderer(512) // Default initial value
    , vertexBuffer(BufferStorage::StreamDraw)
    , u_MVPMatrix(InvalidShaderUniformHandle)
    , s_diffuseTexture(InvalidShaderUniformHandle)
{
    assert(vertexBuffer.GetStorage() == BufferStorage::StreamDraw);

    // Fetch config parameters:
    scriptMgr->GetConfigValue("rendererConfig.maxBillboardsPerBillboardRenderer", maxBillboardsPerBillboardRenderer);
    if (maxBillboardsPerBillboardRenderer == 0)
    {
        common->FatalErrorF("\'rendererConfig.maxBillboardsPerBillboardRenderer\' must not be zero!");
    }

    // Find/load the proper shader:
    InitShaderProgram();

    // Reserve memory beforehand:
    batchedBillboards.reserve(maxBillboardsPerBillboardRenderer);

    // Allocate & bind GL buffers: (4 verts and 6 indexes per billboard).
    vertexBuffer.AllocateVAO();
    vertexBuffer.AllocateVBO(vertexBuffer.GetStorage(), (maxBillboardsPerBillboardRenderer * 4), sizeof(BBVertex), nullptr);
    vertexBuffer.AllocateIBO(vertexBuffer.GetStorage(), (maxBillboardsPerBillboardRenderer * 6), sizeof(uint16_t), nullptr);

    // Set "vertex format":
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(BBVertex), reinterpret_cast<const GLvoid *>(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(BBVertex), reinterpret_cast<const GLvoid *>(sizeof(Vec3f)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(BBVertex), reinterpret_cast<const GLvoid *>(sizeof(Vec3f) + sizeof(Vec4f)));

    vertexBuffer.UnbindAllBuffers();
    CheckGLErrors();

    common->PrintF("New BillboardBatchRenderer initialized. maxBillboardsPerBillboardRenderer = %u", maxBillboardsPerBillboardRenderer);
}

void BillboardBatchRenderer::DrawBillboards(const Billboard * billboards, const size_t count, const ConstTexturePtr & texture)
{
    assert(texture != nullptr);
    assert(billboards != nullptr);
    assert(count != 0);

    for (size_t b = 0; b < count; ++b)
    {
        if (batchedBillboards.size() == maxBillboardsPerBillboardRenderer)
        {
            common->WarningF("Max billboards per frame (%u) limit reached! Dropping further draw calls...",
                             maxBillboardsPerBillboardRenderer);
            return;
        }

        batchedBillboards.emplace_back(texture, billboards[b]);
    }
}

void BillboardBatchRenderer::FlushBatch(const Matrix4 & mvp, const Vector3 cameraPos)
{
    if (batchedBillboards.empty())
    {
        return;
    }

    // Sort according to distance from the camera:
    SortBatch(cameraPos);

    // Set render buffers.
    // Needed for update and drawing:
    vertexBuffer.BindVAO();
    vertexBuffer.BindVBO();
    vertexBuffer.BindIBO();

    // Update VBO/IBO and render:
    UpdateDrawBuffers();
    DrawBatch(mvp);

    // Unbind the buffers:
    vertexBuffer.UnbindAllBuffers();

    // We are done with the batch. Clear it.
    // (Hopefully, the vector will maintain its capacity).
    batchedBillboards.clear();
}

void BillboardBatchRenderer::SortBatch(const Vector3 cameraPos)
{
    //
    // Depth-sort the billboard sprites.
    // This is important because most, if not all, billboards
    // are blended / have transparency areas. Blending gets messed up
    // if we don't depth sort before drawing.
    //
    std::sort(std::begin(batchedBillboards), std::end(batchedBillboards),
              [cameraPos](const BatchElement & a, const BatchElement & b) -> bool
              {
			      const float aDistSqr = lengthSqr(a.geometry.origin - cameraPos);
			      const float bDistSqr = lengthSqr(b.geometry.origin - cameraPos);
			      return aDistSqr > bDistSqr;
              });
}

void BillboardBatchRenderer::UpdateDrawBuffers()
{
    //
    // Also works, but more likely to synchronize:
    //   void * pVB = vertexBuffer.MapVBO(GL_WRITE_ONLY);
    //   void * pIB = vertexBuffer.MapIBO(GL_WRITE_ONLY);
    //
    // Hint the GL that the old data is irrelevant and can be discarded.
    // This should help to avoid a synchronization with the driver.
    //
    void * pVB = vertexBuffer.MapVBORange(0, (batchedBillboards.size() * 4 * sizeof(BBVertex)), (GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT));
    void * pIB = vertexBuffer.MapIBORange(0, (batchedBillboards.size() * 6 * sizeof(uint16_t)), (GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT));
    if ((pVB == nullptr) || (pIB == nullptr))
    {
        CheckGLErrors();
        common->FatalErrorF("BillboardBatchRenderer: Failed to map GL buffers! Unable to continue.");
    }

    // Vertex/Index Buffer pointers:
    BBVertex * __restrict vertexes = reinterpret_cast<BBVertex *>(pVB);
    uint16_t * __restrict indexes  = reinterpret_cast<uint16_t *>(pIB);

    // Temps:
    const uint16_t baseIndexes[6] = { 0, 1, 2, 2, 3, 0 };
    uint32_t vertexesSoFar = 0;
    uint32_t indexesSoFar  = 0;

    // Copy billboards to the GL buffers:
    for (auto & batchElement : batchedBillboards)
    {
        // Save staring index for this billboard:
        batchElement.firstIndexInIB = indexesSoFar;

        // Copy the 6 indexes (two triangles):
        for (size_t i = 0; i < 6; ++i)
        {
            indexes[i] = baseIndexes[i] + vertexesSoFar;
        }
        indexesSoFar += 6;
        indexes += 6;

        // Expand the four edges of the billboard quadrilateral:
        for (size_t v = 0; v < 4; ++v)
        {
            vertexes[v].position = batchElement.geometry.points[v];
            vertexes[v].uvs = batchElement.geometry.texCoords[v];
            vertexes[v].color = ToColor4f(batchElement.geometry.colors[v]);
        }
        vertexesSoFar += 4;
        vertexes += 4;
    }

    vertexBuffer.UnmapIBO();
    vertexBuffer.UnmapVBO();
}

void BillboardBatchRenderer::DrawBatch(const Matrix4 & mvp) const
{
    // Set shader program:
    billboardShader->Bind();
    billboardShader->SetShaderUniform(u_MVPMatrix, mvp);

    //
    // Billboard sprites are already sorted from distance
    // to the camera, so they shouldn't write to the depth buffer.
    // Billboards also must be rendered as the last thing in a scene
    // to make sure this works properly and translucent billboards
    // are correctly blended with the background and other objects.
    //
    for (const auto & batchElement : batchedBillboards)
    {
        batchElement.texture->Bind();
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT,
                       reinterpret_cast<const GLvoid *>(batchElement.firstIndexInIB * sizeof(uint16_t)));
    }

    // Reset the material, just in case...
    renderMgr->GetState().currentMaterial = 0;
}

void BillboardBatchRenderer::InitShaderProgram()
{
    billboardShader = checked_pointer_cast<const ShaderProgram>(resourceMgr->FindOrLoadResource("billboard", Resource::Type::ShaderProgram));
    assert(billboardShader != nullptr);

    u_MVPMatrix = billboardShader->FindShaderUniform("u_MVPMatrix");
    s_diffuseTexture = billboardShader->FindShaderUniform("s_diffuseTexture");

    if ((u_MVPMatrix == InvalidShaderUniformHandle) || (s_diffuseTexture == InvalidShaderUniformHandle))
    {
        common->ErrorF("Failed to initialize BillboardBatchRenderer shader uniform variables!");
    }

    // Texture samplers only need to be set one. They do not change.
    billboardShader->Bind();
    billboardShader->SetShaderUniform(s_diffuseTexture, static_cast<int>(Material::TMU::DiffuseMap));
    billboardShader->Unbind();
}

} // namespace Engine {}
