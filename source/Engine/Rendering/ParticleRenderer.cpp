
// ================================================================================================
// -*- C++ -*-
// File: ParticleRenderer.cpp
// Author: Guilherme R. Lampert
// Created on: 04/09/14
// Brief: Particle emitter and renderer.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/ParticleRenderer.hpp"
#include "Engine/Rendering/OpenGL.hpp"

namespace Engine
{

// ======================================================
// Rendering vertex format used by particles:
// ======================================================

struct ParticleVertex
{
    Vec4f positionPtSize; // xyz=position, w=gl_PointSize
    Vec4f color;          // RGBA color
};

// ======================================================
// Shared states of ParticleRenderer:
// ======================================================

VertexBuffer ParticleRenderer::vertexBuffer;
ConstShaderProgramPtr ParticleRenderer::defaultParticleShader;
ShaderUniformHandle ParticleRenderer::u_viewProjMatrix = InvalidShaderUniformHandle;
ShaderUniformHandle ParticleRenderer::u_modelMatrix = InvalidShaderUniformHandle;
ShaderUniformHandle ParticleRenderer::u_cameraPos = InvalidShaderUniformHandle;
ShaderUniformHandle ParticleRenderer::s_diffuseTexture = InvalidShaderUniformHandle;
bool ParticleRenderer::sharedDataInitialized = false;
bool ParticleRenderer::debugParticles = false;
unsigned int ParticleRenderer::maxParticlesPerParticleRenderer = 1024;

// ======================================================
// ParticleRenderer:
// ======================================================

ParticleRenderer::ParticleRenderer()
{
    // Initialize shared states if this is the
    // first instance ever created.
    if (!sharedDataInitialized)
    {
        InitSharedData();
    }

    // Construct a default emitter.
    SetDefaults();
}

void ParticleRenderer::UpdateAllParticles(const GameTime time)
{
    if (particles.empty())
    {
        AllocateParticles();
    }
    assert(numActiveParticles <= particles.size());

    // Shortcut variables:
    const double deltaTimeSec = time.deltaTime.seconds;
    const uint32_t currentTimeMs = static_cast<uint32_t>(time.currentTime.milliseconds);

    // First move the ones that are no longer active to the end
    // and mark those that will start to fade away:
    uint32_t num = 0;
    Particle * particle = particles.data();
    for (uint32_t i = 0; i < numActiveParticles; ++i, ++particle)
    {
        if ((particle->durationMs + particle->fadeTimeMs) > currentTimeMs)
        {
            if (currentTimeMs >= particle->durationMs)
            {
                particle->isFading = 1;
            }
            if (num != i)
            {
                assert(num < particles.size());
                particles[num] = *particle;
            }
            ++num;
        }
    }

    // Update active count:
    numActiveParticles = num;
    assert(numActiveParticles <= particles.size());

    // Update the ones still active:
    for (uint32_t i = 0; i < numActiveParticles; ++i)
    {
        particle = &particles[i];

        // Update velocity with respect to gravity (constant acceleration):
        particle->velocity += gravityVec * deltaTimeSec;

        // Update velocity with respect to wind
        // (acceleration based on difference of vectors):
        if (simulateAirResistance)
        {
            particle->velocity += (windVelocityVec - particle->velocity) * deltaTimeSec;
        }

        // Finally, update position with respect to velocity:
        const Vector3 oldPosition = particle->position;
        particle->position += particle->velocity * deltaTimeSec;

        // Check the particle against each plane that was set up:
        CheckUserPlanesCollision(*particle, oldPosition, currentTimeMs);
    }

    // Return early if this is a one-time emitter and it has already burned its particle budget.
    if (IsOneTimeEmitterFinished())
    {
        return;
    }

    // Emit new particles in accordance to the flow rate.
    // Also emit new ones if we have less particles active than the minimum required.
    if (((currentTimeMs - lastUpdateMs) > particleReleaseIntervalMs) || (numActiveParticles < minParticles))
    {
        lastUpdateMs = currentTimeMs;

        // Emit new particles at specified flow rate...
        for (uint32_t i = 0; i < particleReleaseAmount; ++i)
        {
            // Do we have any free particles to put back to work?
            if (numActiveParticles < maxParticles)
            {
                particle = &particles[numActiveParticles];
                particle->position = emitterOrigin;
                particle->velocity = particleVelocityVec;
                particle->size = GetRandSize();
                particle->fadeTimeMs = GetRandFadeTime();
                particle->durationMs = currentTimeMs + particleLifeCycleMs;
                particle->collisionEffect = (particleCollisionEffect == ParticleCollisionEffect::Random) ? GetRandCollisionEffect() : particleCollisionEffect;
                particle->isFading = 0;

                if (velocityVar != 0.0f)
                {
                    particle->velocity += GetRandVector() * velocityVar;
                }

                ++numActiveParticles;
                ++particlesEmitted;
            }
            else
            {
                // No more free particles, can't spawn new ones this frame.
                break;
            }
        }
    }
}

void ParticleRenderer::DrawAllParticles(const GameTime time, const Matrix4 & modelMatrix, const Matrix4 & viewProjMatrix, const Vector3 cameraPos)
{
    if ((numActiveParticles == 0) || particles.empty())
    {
        return;
    }

    // Invariants:
    assert(particleSprite != nullptr);
    assert(customShader != nullptr);
    assert(numActiveParticles < maxParticlesPerParticleRenderer);

    // Sort according to distance from the camera:
    SortParticles(cameraPos);

    // Set render buffers.
    // Needed for update and drawing:
    vertexBuffer.BindVAO();
    vertexBuffer.BindVBO();

    // Update VBO and draw:
    UpdateDrawBuffers(time);
    DrawParticleBatch(modelMatrix, viewProjMatrix, cameraPos);

    // Done, cleanup:
    vertexBuffer.UnbindVAO();
    vertexBuffer.UnbindVBO();
}

void ParticleRenderer::SortParticles(const Vector3 cameraPos)
{
    // Depth sort the particles.
    // This ensures proper blending. Also, they are drawn with depth write disabled.
    std::sort(std::begin(particles), std::begin(particles) + numActiveParticles,
              [cameraPos](const Particle & a, const Particle & b) -> bool
              {
			      const float aDistSqr = lengthSqr(a.position - cameraPos);
			      const float bDistSqr = lengthSqr(b.position - cameraPos);
			      return aDistSqr > bDistSqr;
              });
}

void ParticleRenderer::UpdateDrawBuffers(const GameTime & time) const
{
    void * pVB = vertexBuffer.MapVBORange(0, (numActiveParticles * sizeof(ParticleVertex)), (GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT));
    if (pVB == nullptr)
    {
        CheckGLErrors();
        common->FatalErrorF("ParticleRenderer: Failed to map GL VBO! Unable to continue.");
    }

    // Particle Vertex Buffer pointer:
    ParticleVertex * __restrict vertexes = reinterpret_cast<ParticleVertex *>(pVB);

    // If rendering in debug mode, make all alive particles green and the fading ones red.
    const Color4f aliveColor  = (!debugParticles ? baseColor : Color4f(0.0f, 1.0f, 0.0f, 1.0f));
    const Color4f fadingColor = (!debugParticles ? baseColor : Color4f(1.0f, 0.0f, 0.0f, 1.0f));

    // Copy particles to the GL VBO:
    for (uint32_t i = 0; i < numActiveParticles; ++i)
    {
        const Particle & particle = particles[i];

        // World position and point sprite size:
        const float size = (particle.size.width + particle.size.height) / 2.0f;
        vertexes[i].positionPtSize = Vec4f(particle.position, size);

        // Color:
        if (!particle.isFading)
        {
            vertexes[i].color = aliveColor;
        }
        else
        {
            float fadeFactor;
            const uint32_t currentTimeMs = static_cast<uint32_t>(time.currentTime.milliseconds);

            if (currentTimeMs <= (particle.durationMs + particle.fadeTimeMs))
            {
                const uint32_t millisecondsLeft = (particle.durationMs + particle.fadeTimeMs) - currentTimeMs;
                fadeFactor = ValueNormalize(static_cast<float>(millisecondsLeft), 0.0f, static_cast<float>(particle.fadeTimeMs));
            }
            else
            {
                // Completely expired. Render transparent.
                // This particle will be removed on the next update.
                fadeFactor = 0.0f;
            }

            vertexes[i].color = Color4f(fadingColor.r, fadingColor.g, fadingColor.b, fadingColor.a * fadeFactor);
        }
    }

    vertexBuffer.UnmapVBO();
}

void ParticleRenderer::DrawParticleBatch(const Matrix4 & modelMatrix, const Matrix4 & viewProjMatrix, const Vector3 cameraPos) const
{
    // Set texture and shader program:
    if (!debugParticles)
    {
        particleSprite->Bind();
    }
    else
    {
        // Will render with baseColor only.
        renderMgr->whiteTexture->Bind();
    }

    if (customShader == defaultParticleShader)
    {
        defaultParticleShader->Bind();
        defaultParticleShader->SetShaderUniform(u_viewProjMatrix, viewProjMatrix);
        defaultParticleShader->SetShaderUniform(u_modelMatrix, modelMatrix);
        defaultParticleShader->SetShaderUniform(u_cameraPos, cameraPos);
    }
    else
    {
        customShader->Bind();
        const ShaderUniformHandle mvpMatrixUnif = customShader->FindShaderUniform("u_MVPMatrix");
        customShader->SetShaderUniform(mvpMatrixUnif, viewProjMatrix * modelMatrix);
    }

    // Draw all the active ones:
    glDrawArrays(GL_POINTS, 0, numActiveParticles);

    // Reset the material, just in case...
    renderMgr->GetState().currentMaterial = 0;
}

bool ParticleRenderer::HasParticlesToDraw() const
{
    return (numActiveParticles != 0) && !particles.empty();
}

void ParticleRenderer::ResetEmitter()
{
    numActiveParticles = 0;
    particlesEmitted = 0;
    lastUpdateMs = 0;
}

void ParticleRenderer::SetDefaults()
{
    assert(sharedDataInitialized == true);

    // Trash current particles, if any:
    if (!particles.empty())
    {
        FreeParticles();
    }

    particleSprite = renderMgr->whiteTexture;
    customShader = defaultParticleShader;
    gravityVec = Vector3Zero;
    particleVelocityVec = Vector3Zero;
    windVelocityVec = Vector3Zero;
    emitterOrigin = Vector3Zero;
    velocityVar = 1.0f;
    minParticleSize = Vec2f(0.5f);
    maxParticleSize = Vec2f(1.0f);
    baseColor = Color4f(1.0f);
    particleFadeMinMs = 0;            // 0 sec
    particleFadeMaxMs = 500;          // 0.5 sec
    particleLifeCycleMs = 1000;       // 1 sec
    particleReleaseIntervalMs = 1000; // 1 sec
    particleReleaseAmount = 1;
    particlesEmitted = 0;
    lastUpdateMs = 0;
    minParticles = 1;
    maxParticles = 1;
    numActiveParticles = 0;
    particleCollisionEffect = ParticleCollisionEffect::Random;
    particleFadeEffect = ParticleFadeEffect::SmoothFade;
    simulateAirResistance = false;
    oneTimeEmission = false;
}

void ParticleRenderer::AllocateParticles()
{
    assert(maxParticles != 0);

    if (maxParticles > maxParticlesPerParticleRenderer)
    {
        common->WarningF("maxParticles (%u) > maxParticlesPerParticleRenderer (%u)! Clamping to maxParticlesPerParticleRenderer.",
                         maxParticles, maxParticlesPerParticleRenderer);

        maxParticles = maxParticlesPerParticleRenderer;
    }

    particles.resize(maxParticles);
}

void ParticleRenderer::FreeParticles()
{
    numActiveParticles = 0;

    particles.clear();
    particles.shrink_to_fit();
}

bool ParticleRenderer::AreParticlesAllocated() const
{
    return !particles.empty();
}

void ParticleRenderer::AddCollisionPlane(const ParticleCollisionPlane plane)
{
    collisionPlanes.push_back(plane);
}

void ParticleRenderer::AddCollisionPlane(const Plane plane, const float restitutionCoeff)
{
    collisionPlanes.push_back({ plane, restitutionCoeff });
}

void ParticleRenderer::RemoveAllCollisionPlanes()
{
    collisionPlanes.clear();
    collisionPlanes.shrink_to_fit();
}

void ParticleRenderer::SetRandomSeed(const unsigned int seed)
{
    random.Seed(seed);
}

void ParticleRenderer::SetMinMaxParticles(const uint32_t min, const uint32_t max)
{
    assert(max != 0);
    assert(min <= max);
    minParticles = min;
    maxParticles = max;
}

void ParticleRenderer::GetMinMaxParticles(uint32_t & min, uint32_t & max) const
{
    min = minParticles;
    max = maxParticles;
}

void ParticleRenderer::SetParticleFadeTimes(const uint32_t fadeMinMs, const uint32_t fadeMaxMs)
{
    assert(fadeMinMs <= fadeMaxMs);
    particleFadeMinMs = fadeMinMs;
    particleFadeMaxMs = fadeMaxMs;
}

void ParticleRenderer::GetParticleFadeTimes(uint32_t & fadeMinMs, uint32_t & fadeMaxMs) const
{
    fadeMinMs = particleFadeMinMs;
    fadeMaxMs = particleFadeMaxMs;
}

void ParticleRenderer::InitSharedData()
{
    assert(sharedDataInitialized == false);

    // Fetch configuration parameters:
    scriptMgr->GetConfigValue("rendererConfig.debugParticles", debugParticles);
    scriptMgr->GetConfigValue("rendererConfig.maxParticlesPerParticleRenderer", maxParticlesPerParticleRenderer);
    if (maxParticlesPerParticleRenderer == 0)
    {
        common->FatalErrorF("\'rendererConfig.maxParticlesPerParticleRenderer\' must not be zero!");
    }

    InitDefaultShaderProgram();
    InitVertexBuffer();

    sharedDataInitialized = true;
    common->PrintF("ParticleRenderer shared data initialized. maxParticlesPerParticleRenderer = %u",
                   maxParticlesPerParticleRenderer);
}

void ParticleRenderer::InitDefaultShaderProgram()
{
    defaultParticleShader = checked_pointer_cast<const ShaderProgram>(
            resourceMgr->FindOrLoadResource("particle", Resource::Type::ShaderProgram));
    assert(defaultParticleShader != nullptr);

    u_viewProjMatrix = defaultParticleShader->FindShaderUniform("u_viewProjMatrix");
    u_modelMatrix    = defaultParticleShader->FindShaderUniform("u_modelMatrix");
    u_cameraPos      = defaultParticleShader->FindShaderUniform("u_cameraPos");
    s_diffuseTexture = defaultParticleShader->FindShaderUniform("s_diffuseTexture");

    if ((u_viewProjMatrix == InvalidShaderUniformHandle) ||
        (u_modelMatrix    == InvalidShaderUniformHandle) ||
        (u_cameraPos      == InvalidShaderUniformHandle) ||
        (s_diffuseTexture == InvalidShaderUniformHandle))
    {
        common->ErrorF("Failed to initialize ParticleRenderer shader uniform variables!");
    }

    // Texture samplers only need to be set one. They do not change.
    defaultParticleShader->Bind();
    defaultParticleShader->SetShaderUniform(s_diffuseTexture, static_cast<int>(Material::TMU::DiffuseMap));
    defaultParticleShader->Unbind();

    // We need this to be enabled since the shader will use gl_PointSize.
    glEnable(GL_PROGRAM_POINT_SIZE);
    CheckGLErrors();
}

void ParticleRenderer::InitVertexBuffer()
{
    assert(maxParticlesPerParticleRenderer != 0);

    // Allocate & bind GL VBO.
    // One vertex per particle, since the draw as GL_POINTS.
    vertexBuffer.AllocateVAO();
    vertexBuffer.AllocateVBO(BufferStorage::StreamDraw, maxParticlesPerParticleRenderer, sizeof(ParticleVertex), nullptr);

    // Set "vertex format":
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), reinterpret_cast<const GLvoid *>(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), reinterpret_cast<const GLvoid *>(sizeof(Vec4f)));

    vertexBuffer.UnbindVAO();
    vertexBuffer.UnbindVBO();
    CheckGLErrors();
}

void ParticleRenderer::ShutdownSharedData()
{
    vertexBuffer.FreeVAO();
    vertexBuffer.FreeVBO();

    defaultParticleShader = nullptr;
    u_viewProjMatrix = InvalidShaderUniformHandle;
    u_modelMatrix = InvalidShaderUniformHandle;
    u_cameraPos = InvalidShaderUniformHandle;
    s_diffuseTexture = InvalidShaderUniformHandle;
    sharedDataInitialized = false;
    debugParticles = false;
}

Vector3 ParticleRenderer::GetRandVector()
{
    Vector3 v;

    // Pick a random Z between -1 and 1:
    v[2] = random.NextSymmetric();

    // Get radius of this circle:
    const float radius = std::sqrt(1.0f - (v[2] * v[2]));

    // Pick a random point on a circle:
    const float t = random.NextFloat(-MathConst::PI, MathConst::PI);

    // Compute matching X and Y for Z:
    v[0] = std::cos(t) * radius;
    v[1] = std::sin(t) * radius;

    return v;
}

ParticleCollisionEffect ParticleRenderer::GetRandCollisionEffect()
{
    const int fx = random.NextInteger(0, static_cast<int>(ParticleCollisionEffect::Random));
    assert(fx < static_cast<int>(ParticleCollisionEffect::Random));
    return static_cast<ParticleCollisionEffect>(fx);
}

uint32_t ParticleRenderer::GetRandFadeTime()
{
    return random.NextInteger(particleFadeMinMs, particleFadeMaxMs);
}

Vec2f ParticleRenderer::GetRandSize()
{
    Vec2f size;
    size.width  = random.NextFloat(minParticleSize.width,  maxParticleSize.width);
    size.height = random.NextFloat(minParticleSize.height, maxParticleSize.height);
    return size;
}

void ParticleRenderer::CheckUserPlanesCollision(Particle & particle, const Vector3 & oldPosition, const uint32_t currentTimeMs) const
{
    // If the particle is still, it has already collided, do nothing.
    if (FloatIsZero(lengthSqr(particle.velocity)))
    {
        return;
    }

    for (const auto & collisionPlane : collisionPlanes)
    {
        const Plane::Side side = collisionPlane.plane.WhichSide(particle.position);
        if ((side == Plane::SideBack) || (side == Plane::SideOn))
        {
            switch (particle.collisionEffect)
            {
            case ParticleCollisionEffect::Bounce:
            {
                // The new velocity vector of a particle that is bouncing
                // off a plane is computed as follows:
                //
                // Vn = (N.V) * N
                // Vt = V - Vn
                // Vp = Vt - Kr * Vn
                //
                // Where:
                // .  = Dot product operation
                // N  = The normal of the plane from which we bounced
                // V  = Velocity vector prior to bounce
                // Vn = Normal force
                // Kr = The coefficient of restitution (1 = Full bounce; 0 = Particle sticks)
                // Vp = New velocity vector after bounce
                //
                particle.position = oldPosition;
                const float Kr = collisionPlane.restitutionCoeff;
                const Vector3 Vn = dot(collisionPlane.plane.normal, particle.velocity) * collisionPlane.plane.normal;
                const Vector3 Vt = particle.velocity - Vn;
                particle.velocity = Vt - (Kr * Vn);
                break;
            }
            case ParticleCollisionEffect::Stick:
            {
                particle.position = oldPosition;
                particle.velocity = Vector3(0.0f, 0.0f, 0.0f);
                break;
            }
            case ParticleCollisionEffect::FadeOut:
            {
                particle.position = oldPosition;
                particle.velocity = Vector3(0.0f, 0.0f, 0.0f);
                if (!particle.isFading)
                {
                    particle.durationMs = currentTimeMs;
                    particle.isFading = 1;
                }
                break;
            }
            default:
                common->FatalErrorF("Invalid ParticleCollisionEffect!");
            } // switch (particle.collisionEffect)
        }
    }
}

} // namespace Engine {}
