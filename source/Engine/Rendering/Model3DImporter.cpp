
// ================================================================================================
// -*- C++ -*-
// File: Model3DImporter.cpp
// Author: Guilherme R. Lampert
// Created on: 10/08/14
// Brief: Thin wrapper to the ASSIMP mesh importer library that operates on Model3D.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

// Renderer:
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/Model3D.hpp"
#include "Engine/Rendering/Model3DImporter.hpp"
#include "Engine/Rendering/RenderManager.hpp"

namespace Engine
{
namespace /* unnamed */
{

// ======================================================
// importFlags:
// ======================================================

//
// Default ASSIMP post processing flags used.
//
static constexpr unsigned int importFlags =
    (aiProcess_Triangulate              |
     aiProcess_JoinIdenticalVertices    |
     aiProcess_RemoveRedundantMaterials |
     aiProcess_OptimizeMeshes           |
     aiProcess_OptimizeGraph            |
     aiProcess_ImproveCacheLocality     |
     aiProcess_GenSmoothNormals         |
     aiProcess_CalcTangentSpace);

// ======================================================
// texInfo[]:
// ======================================================

//
// ASSIMP texture types from:
// - http://assimp.sourceforge.net/lib_html/material_8h.html#a7dd415ff703a2cc53d1c22ddbbd7dde0
//
static const struct {
    aiTextureType aiType;
    const char * description;
} texInfo[] = {
    { aiTextureType_DIFFUSE,      "diffuse map"      },
    { aiTextureType_NORMALS,      "normal map"       },
    { aiTextureType_SPECULAR,     "specular map"     },
    { aiTextureType_OPACITY,      "alpha map"        },
    { aiTextureType_HEIGHT,       "height map"       },
    { aiTextureType_EMISSIVE,     "emissive map"     },
    { aiTextureType_SHININESS,    "gloss map"        },
    { aiTextureType_AMBIENT,      "ambient map"      },
    { aiTextureType_LIGHTMAP,     "light map"        },
    { aiTextureType_DISPLACEMENT, "displacement map" },
    { aiTextureType_REFLECTION,   "reflection map"   },
    { aiTextureType_NONE,         "none/invalid"     },
    { aiTextureType_UNKNOWN,      "unknown/invalid"  }
};

} // namespace unnamed {}

// ======================================================
// Model3DImporter:
// ======================================================

// Static properties of Model3DImporter:
bool Model3DImporter::modelImporterSilent;
bool Model3DImporter::modelImporterListTextures;
bool Model3DImporter::heightMapsAsNormalMaps;
bool Model3DImporter::isInitialized;
aiLogStream Model3DImporter::assimpLog;

bool Model3DImporter::ImportModel3DFromFile(const std::string & filename, Model3D & mdl)
{
    assert(!filename.empty());

    if (!isInitialized)
    {
        Init();
    }

    common->PrintF("Beginning to import Model3D from file \"%s\"...", filename.c_str());

    // Import from file:
    bool result = false;
    const aiScene * scene = aiImportFile(filename.c_str(), importFlags);

    if (scene != nullptr)
    {
        // Do the actual conversion from ASSIMP to our format:
        result = ImportModelHelper(*scene, filename, mdl);

        // Cleanup:
        aiReleaseImport(scene);
    }
    else
    {
        common->WarningF("aiImportFile() failed with error: %s", aiGetErrorString());
    }

    return result;
}

bool Model3DImporter::ImportModel3DFromData(const DataBlob blob, const std::string & filename, Model3D & mdl)
{
    assert(blob.IsValid());

    if (!isInitialized)
    {
        Init();
    }

    common->PrintF("Beginning to import Model3D from DataBlob \"%s\"...", filename.c_str());

    // Import from memory:
    bool result = false;
    const aiScene * scene = aiImportFileFromMemory(reinterpret_cast<const char *>(blob.data),
                                                   static_cast<unsigned int>(blob.sizeInBytes),
                                                   importFlags, filename.c_str());

    if (scene != nullptr)
    {
        // Do the actual conversion from ASSIMP to our format:
        result = ImportModelHelper(*scene, filename, mdl);

        // Cleanup:
        aiReleaseImport(scene);
    }
    else
    {
        common->WarningF("aiImportFileFromMemory() failed with error: %s", aiGetErrorString());
    }

    return result;
}

bool Model3DImporter::ImportModelHelper(const aiScene & scene, const std::string & filename, Model3D & mdl)
{
    // Each aiScene will output 1 Model3D, with 1..N sub-meshes.
    if (scene.mNumMeshes == 0)
    {
        common->WarningF("\"%s\": scene file had no meshes in it!", filename.c_str());
        return false;
    }

    // Pre-allocate sub-meshes:
    mdl.AllocateSubMeshes(scene.mNumMeshes);
    const unsigned int numSubMeshes = mdl.GetSubMeshCount();
    assert(numSubMeshes == scene.mNumMeshes);

    // Sub-mesh vertex array ranges are relative to the Mesh range in the array.
    unsigned int meshVertexCount = 0, meshIndexCount = 0;
    unsigned int meshBaseVertex = 0, meshBaseIndex = 0;

    for (unsigned int m = 0; m < numSubMeshes; ++m)
    {
        Model3D::SubMesh & dest = mdl.GetSubMeshAt(m);
        const aiMesh * source = scene.mMeshes[m];

        meshVertexCount = source->mNumVertices;
        meshIndexCount = source->mNumFaces * 3; // Always triangles (aiProcess_Triangulate)

        // Pre-allocate system-side data:
        dest.vertexes.resize(meshVertexCount);
        dest.indexes.resize(meshIndexCount);

        // Save base indexes:
        dest.firstVertexInVB = meshBaseVertex;
        dest.firstIndexInIB = meshBaseIndex;

        // Assemble vertex/index array for each sub-mesh in the scene:
        ProcessMesh(source, dest);

        // Get material for surface:
        ProcessMaterial(scene.mMaterials[source->mMaterialIndex], dest);

        // Next surface:
        meshBaseVertex += meshVertexCount;
        meshBaseIndex += meshIndexCount;
    }

    // Compute model bounds and we are done.
    mdl.ComputeBounds();

    common->PrintF("Finished importing Model3D from file \"%s\"", filename.c_str());
    return true;
}

void Model3DImporter::ProcessMesh(const aiMesh * source, Model3D::SubMesh & dest)
{
    assert(source != nullptr);
    assert(dest.vertexes.size() == source->mNumVertices);

    const aiVector3D aiVector3DZero(0.0f, 0.0f, 0.0f);

    for (unsigned int v = 0; v < source->mNumVertices; ++v)
    {
        Model3D::DrawVertex & dv = dest.vertexes[v];

        // Must have vertexes, obviously.
        assert(source->mVertices != nullptr);
        const aiVector3D & P = source->mVertices[v];
        dv.position.x = P.x;
        dv.position.y = P.y;
        dv.position.z = P.z;

        const aiVector3D & T = (source->mTangents != nullptr) ? source->mTangents[v] : aiVector3DZero;
        dv.tangent.x = T.x;
        dv.tangent.y = T.y;
        dv.tangent.z = T.z;

        const aiVector3D & B = (source->mBitangents != nullptr) ? source->mBitangents[v] : aiVector3DZero;
        dv.bitangent.x = B.x;
        dv.bitangent.y = B.y;
        dv.bitangent.z = B.z;

        const aiVector3D & N = (source->mNormals != nullptr) ? source->mNormals[v] : aiVector3DZero;
        dv.normal.x = N.x;
        dv.normal.y = N.y;
        dv.normal.z = N.z;

        const aiVector3D & TX = (source->mTextureCoords[0] != nullptr) ? source->mTextureCoords[0][v] : aiVector3DZero;
        dv.texCoords.x = TX.x;
        dv.texCoords.y = TX.y;
    }

    // Copy indexes:
    // NOTE: We only operate on triangle meshes.
    assert(dest.indexes.size() == (source->mNumFaces * 3));
    for (unsigned int t = 0; t < source->mNumFaces; ++t)
    {
        const aiFace & face = source->mFaces[t];
        assert(face.mNumIndices == 3);

        dest.indexes[(t * 3) + 0] = static_cast<Model3D::IndexType>(face.mIndices[0]);
        dest.indexes[(t * 3) + 1] = static_cast<Model3D::IndexType>(face.mIndices[1]);
        dest.indexes[(t * 3) + 2] = static_cast<Model3D::IndexType>(face.mIndices[2]);
    }
}

void Model3DImporter::ProcessMaterial(const aiMaterial * aiMat, Model3D::SubMesh & dest)
{
#define ToColor4f(c) Color4f((c).r, (c).g, (c).b, (c).a)

    assert(aiMat != nullptr);

    aiString aiMatName;
    aiColor4D aiMatColor;

    // Setup material name:
    aiMatName.data[0] = '\0';
    const bool defaultName = ((aiMat->Get(AI_MATKEY_NAME, aiMatName) != AI_SUCCESS) ||
                              (aiMatName.data[0] == '\0') ||
                              (std::strcmp(aiMatName.data, AI_DEFAULT_MATERIAL_NAME) == 0));

    // Not a new unnamed material.
    // Try to lookup, in case the same was already created:
    if (!defaultName)
    {
        FixASSIMPMaterialName(aiMatName);
        ConstResourcePtr existingMaterial = resourceMgr->FindLoadedResource(aiMatName.data);
        if (existingMaterial != nullptr)
        {
            common->PrintF("Found material \"%s\" for mesh.", existingMaterial->GetResourceId().c_str());
            dest.material = checked_pointer_cast<const Material>(existingMaterial);
            return;
        }
    }

    // Create new default:
    MaterialPtr newMaterial = renderMgr->NewMaterial();
    dest.material = newMaterial;

    if (defaultName) // Use a default sequential name:
    {
        newMaterial->SetName(Material::GetEnumeratedMaterialName());
    }
    else // Use the name provided by the mesh creator:
    {
        newMaterial->SetName(aiMatName.data);
    }

    //
    // Import new ASSIMP material:
    //

    // Diffuse color:
    if (aiMat->Get(AI_MATKEY_COLOR_DIFFUSE, aiMatColor) == AI_SUCCESS)
    {
        newMaterial->SetDiffuseColor(ToColor4f(aiMatColor));
    }

    // Specular color:
    if (aiMat->Get(AI_MATKEY_COLOR_SPECULAR, aiMatColor) == AI_SUCCESS)
    {
        newMaterial->SetSpecularColor(ToColor4f(aiMatColor));
    }

    // Emissive color:
    if (aiMat->Get(AI_MATKEY_COLOR_EMISSIVE, aiMatColor) == AI_SUCCESS)
    {
        newMaterial->SetEmissiveColor(ToColor4f(aiMatColor));
    }

    // Ambient color:
    if (aiMat->Get(AI_MATKEY_COLOR_AMBIENT, aiMatColor) == AI_SUCCESS)
    {
        newMaterial->SetAmbientColor(ToColor4f(aiMatColor));
    }

    // Shininess (Phong power):
    float shininess;
    if (aiMat->Get(AI_MATKEY_SHININESS, shininess) == AI_SUCCESS)
    {
        newMaterial->SetShininess(shininess);
    }

    // Debug dump all textures referenced by the material:
    if (modelImporterListTextures)
    {
        ListAllTextures(*aiMat);
    }

    // Diffuse textures:
    ProcessTextures(*aiMat, aiTextureType_DIFFUSE, newMaterial);

    // Normal maps:
    ProcessTextures(*aiMat, aiTextureType_NORMALS, newMaterial);

    // Specular maps:
    ProcessTextures(*aiMat, aiTextureType_SPECULAR, newMaterial);

    // Height maps (possibly a bump map/normal map):
    ProcessTextures(*aiMat, aiTextureType_HEIGHT, newMaterial);

    // Done. Register material with ResourceManager and optionally dump it to log:
    newMaterial->SetupShaderProgram();
    if (!modelImporterSilent)
    {
        common->PrintF("New material \"%s\" created!", newMaterial->GetName().c_str());
        newMaterial->PrintSelf();
    }
    resourceMgr->RegisterNewResource(newMaterial);

#undef ToColor4f
}

void Model3DImporter::ProcessTextures(const aiMaterial & aiMat, const aiTextureType target, MaterialPtr & newMaterial)
{
    const char * description = nullptr;
    for (size_t texType = 0; texType < ArrayLength(texInfo); ++texType)
    {
        if (texInfo[texType].aiType == target)
        {
            description = texInfo[texType].description;
            break;
        }
    }
    assert((description != nullptr) && "target is not one of the aiTextureType_* enum!");

    aiString aiPath;
    std::string textureFilePath;
    const unsigned int textureCount = aiMat.GetTextureCount(target);

    // Process every texture of this type for the given material:
    for (unsigned int nTex = 0; nTex < textureCount; ++nTex)
    {
        aiPath.Clear();
        aiPath.data[0] = '\0';
        if (aiMat.GetTexture(target, nTex, &aiPath, nullptr, nullptr, nullptr, nullptr, nullptr) != AI_SUCCESS)
        {
            common->WarningF("Failed to get %s texture %u!", description, nTex);
            continue;
        }

        // Got texture, add to material:
        common->PrintF("Found %s (index:%u): \"%s\"", description, nTex, aiPath.data);

        // ASSIMP sometimes prefixes the path with a "\" or a "./" or a "/",
        // we don't need that, so remove 'em first:
        const char * pPath = aiPath.data;
        while ((*pPath == '.') || (*pPath == '\\') || (*pPath == '/'))
        {
            if (*pPath == '\0')
            {
                common->WarningF("Texture path had no filename or valid path in it!");
                break;
            }
            ++pPath;
        }
        if (*pPath == '\0')
        {
            // This path had nothing in it or just a bogus "/", etc...
            continue;
        }

        // Since we are dealing with files generated by a variety of tools,
        // some might save texture paths with different path separators,
        // scan and replace non-conforming ones, just in case:
        textureFilePath = system->FixFilePath(pPath);

        // Find or load new texture:
        ConstResourcePtr resource = resourceMgr->FindOrLoadResource(textureFilePath);
        if (resource != nullptr)
        {
            ConstTexturePtr texture = checked_pointer_cast<const Texture>(resource);
            switch (target)
            {
            case aiTextureType_DIFFUSE:
                newMaterial->SetDiffuseTexture(texture);
                break;

            case aiTextureType_NORMALS:
                newMaterial->SetNormalTexture(texture);
                break;

            case aiTextureType_SPECULAR:
                newMaterial->SetSpecularTexture(texture);
                break;

            case aiTextureType_HEIGHT:
                // Optionally treat height-maps as normal maps:
                if (heightMapsAsNormalMaps && (target == aiTextureType_HEIGHT))
                {
                    newMaterial->SetNormalTexture(texture);
                }
                break;

            default:
                common->FatalErrorF("Texture target not supported by the renderer!");
            } // switch (target)
        }
    }
}

void Model3DImporter::ListAllTextures(const aiMaterial & aiMat)
{
    aiString str;
    str.Clear();
    str.data[0] = '\0';
    aiMat.Get(AI_MATKEY_NAME, str);

    common->PrintF("----- Texture listing for ASSIMP material \"%s\": -----", str.data);

    for (size_t texType = 0; texType < ArrayLength(texInfo); ++texType)
    {
        // List every texture of this type for the given material:
        const unsigned int textureCount = aiMat.GetTextureCount(texInfo[texType].aiType);
        for (unsigned int nTex = 0; nTex < textureCount; ++nTex)
        {
            str.Clear();
            str.data[0] = '\0';
            if (aiMat.GetTexture(texInfo[texType].aiType, nTex, &str,
                                 nullptr, nullptr, nullptr, nullptr, nullptr) == AI_SUCCESS)
            {
                common->PrintF("[%u]> %s: \"%s\"", nTex, texInfo[texType].description, str.data);
            }
        }
    }

    common->PrintF("----- End texture listing -----");
}

void Model3DImporter::FixASSIMPMaterialName(aiString & name)
{
    char * c = name.data;
    while (*c != '\0')
    {
        // Make name lowercase:
        if (std::isupper(*c))
        {
            *c = static_cast<char>(std::tolower(*c));
        }

        // Remove punctuation chars:
        if (std::ispunct(*c))
        {
            *c = '_'; // Replace by an underscore
        }

        // Replace white-spaces by an underscore:
        if (std::isspace(*c))
        {
            *c = '_';
        }

        // Next char:
        c++;
    }

    // Names starting with an underscore are reserved for
    // internal Engine use. Replace any such occurrence:
    if (name.data[0] == '_')
    {
        // Replace the '_' with an 'm', for "material".
        name.data[0] = 'm';
    }
}

void Model3DImporter::Init()
{
    if (!isInitialized)
    {
        // Cache config vars:
        scriptMgr->GetConfigValue("rendererConfig.modelImporterSilent", modelImporterSilent);
        scriptMgr->GetConfigValue("rendererConfig.modelImporterListTextures", modelImporterListTextures);
        scriptMgr->GetConfigValue("rendererConfig.heightMapsAsNormalMaps", heightMapsAsNormalMaps);

        // Register log callback if not silent:
        if (!modelImporterSilent)
        {
            assimpLog.user = nullptr;
            assimpLog.callback = &Model3DImporter::ASSIMPLogCallback;
            aiAttachLogStream(&assimpLog);
        }

        // Done!
        isInitialized = true;
        common->PrintF("ASSIMP library initialized! Lib version: %d.%d.%d",
                       aiGetVersionMajor(), aiGetVersionMinor(), aiGetVersionRevision());
    }
}

void Model3DImporter::ASSIMPLogCallback(const char * message, char * /* user */)
{
    if (!modelImporterSilent && (message != nullptr) && (message[0] != '\0'))
    {
        // Remove default '\n' added by ASSIMP.
        // Common::PrintF() will break the line by default.
        char tempStr[MaxTempStringLen];
        CopyCString(tempStr, ArrayLength(tempStr), message);
        const size_t n = std::strlen(tempStr);
        if (tempStr[n - 1] == '\n')
        {
            tempStr[n - 1] = '\0';
        }

        common->PrintF("[ASSIMP]: %s", tempStr);
    }
}

} // namespace Engine {}
