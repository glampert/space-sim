
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: ShaderProgram.hpp
// Author: Guilherme R. Lampert
// Created on: 08/08/14
// Brief: Basic OpenGL Shader Program.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// ShaderUniformHandle:
// ======================================================

// Opaque handle to Shader Uniform vars.
using ShaderUniformHandle = int;

// Not a valid ShaderUniformHandle.
constexpr ShaderUniformHandle InvalidShaderUniformHandle = -1;

// ======================================================
// ShaderProgram:
// ======================================================

//
// Groups all shader pipeline stages.
// Supports a Vertex + Fragment shader setup.
//
// NOTE: ShaderProgram instances are created via RenderManager::NewShaderProgram().
//
class ShaderProgram
    : public Resource
{
  public:

    // Init the Shader Program from a GLSL shader source file.
    bool InitFromFile(std::string filename, std::string resourceId);

    // Init the Shader Program with the contents of a GLSL shader source file.
    bool InitFromData(const DataBlob blob, std::string filename);

    // Frees all the underlaying data, making the object an invalid Shader Program.
    void FreeAllData();

    // Test if the Shader Program is a default. This is usually the case if an initialization error occurs.
    bool IsDefault() const;

    // Estimate the amount of memory consumed by this Shader Program.
    size_t GetEstimateMemoryUsage() const;

    // Shader source file name.
    std::string GetResourceId() const;

    // Short string describing the resource type.
    std::string GetResourceTypeString() const;

    // Print a description of the Shader Program to the developer log.
    void PrintSelf() const;

    // Test if the program has a valid GL id.
    bool IsValid() const;

    // Bind as current GL state:
    void Bind() const;
    void Unbind() const;
    bool IsCurrent() const;

    // Tries to find a Shader Uniform variable by its name. Name must match shader var declaration.
    // Returns InvalidShaderUniformHandle if var cannot be found.
    ShaderUniformHandle FindShaderUniform(const char * varName) const;

    // Set the value of a Shader Uniform. Must Bind() program first!
    bool SetShaderUniform(const ShaderUniformHandle var, const int i) const;
    bool SetShaderUniform(const ShaderUniformHandle var, const float f) const;
    bool SetShaderUniform(const ShaderUniformHandle var, const Vec2f & v2) const;
    bool SetShaderUniform(const ShaderUniformHandle var, const Vec3f & v3) const;
    bool SetShaderUniform(const ShaderUniformHandle var, const Vec4f & v4) const;
    bool SetShaderUniform(const ShaderUniformHandle var, const Vector3 & v3) const;
    bool SetShaderUniform(const ShaderUniformHandle var, const Vector4 & v4) const;
    bool SetShaderUniform(const ShaderUniformHandle var, const Matrix4 & m4) const;
    bool SetShaderUniform(const ShaderUniformHandle var, const float * fv, const size_t numElements) const;

    // Set this program as a default. This is only used internally by the renderer.
    void MarkAsDefault() { isDefault = true; }

    // Frees all associated data.
    ~ShaderProgram();

  protected:

    // RenderManager is the only object that can create ShaderPrograms.
    friend class RenderManager;

    // Default constructor. Creates an empty ShaderProgram.
    ShaderProgram();

  private:

    // Internal helpers:
    static void PrintInfoLogs(const unsigned int programId, const unsigned int vsId, const unsigned int fsId);
    static bool SplitGLSLSource(const DataBlob blob, std::string & vsSource, std::string & fsSource);

    // Name and size in bytes of the source file that created this program.
    std::string sourceFileName;
    size_t sourceFileSize;

    // GL program id; 0 for invalid/null program.
    unsigned int programId;

    // The renderer can mark some programs as default/built-in.
    bool isDefault;

    // No copy allowed. This is a shareable resource.
    DISABLE_COPY_AND_ASSIGN(ShaderProgram)
};

// Strong reference counted pointer to ShaderProgram.
using ShaderProgramPtr = std::shared_ptr<ShaderProgram>;
using ConstShaderProgramPtr = std::shared_ptr<const ShaderProgram>;

} // namespace Engine {}
