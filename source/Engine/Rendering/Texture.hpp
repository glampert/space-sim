
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Texture.hpp
// Author: Guilherme R. Lampert
// Created on: 08/08/14
// Brief: Basic OpenGL Texture.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Texture:
// ======================================================

//
// Texture base type.
// There is a one-to-one relation of this type with an OpenGL texture.
//
// NOTE: Texture instances are created via RenderManager::NewTexture().
//
class Texture
    : public Resource
{
  public:

    // The maximum number of mip-map levels for a single physical texture.
    // This is more than enough for current-generation graphics cards.
    // A 65536x65536 (2^16) image has a maximum of 16 levels.
    static constexpr int MaxMipMapLevels = 16;

    // Texture types/usages:
    enum class Type : short
    {
        Texture2D,     // 2D texture (WxH).
        CubeMap,       // 6-sided cube-map for skyboxes and environment-maps.
        Undefined = -1 // Dummy value. Used internally.
    };

    // Texture addressing (or wrapping) mode:
    enum class Addressing : short
    {
        ClampToEdge,    // Clamp (or stretch) texture to fill surface using the color of the edge pixel of the texture.
        Repeat,         // Repeat texture to fill surface.
        MirroredRepeat, // Repeat texture to fill surface, but mirrored.
        Default         // Default addressing mode. Same as 'Repeat'.
    };

    // Texture filtering modes:
    enum class Filter : short
    {
        Nearest,     // Nearest neighbor (or Manhattan Distance) filtering. Worst quality, best performance.
        Linear,      // Linear, mip-mapped filtering. Reasonable quality, good performance.
        Anisotropic, // Anisotropic filtering. Best quality, most expensive. Paired with an anisotropy amount.
        Default      // Let the renderer decide.
    };

    // Internal texture image format:
    enum class PixelFormat : short
    {
        RGB_U8,      // RGB  8:8:8   color
        RGBA_U8,     // RGBA 8:8:8:8 color
        BGR_U8,      // BGR  8:8:8   color
        BGRA_U8,     // BGRA 8:8:8:8 color
        Invalid = -1 // Sentinel constant. Internal use.
    };

    // All parameters needed for the creation of a texture in a convenient aggregate.
    struct SetupParams
    {
        Vec2u        baseDimensions;
        Type         type;
        Addressing   addressing;
        Filter       filter;
        PixelFormat  pixelFormat;
        unsigned int anisotropyAmount; // From 0 to RenderManager::GetMaxTextureAniso()
        unsigned int numMipMapLevels;  // From 1 to Texture::MaxMipMapLevels
        unsigned int baseMipMapLevel;  // From 0 to numMipMapLevels-1
        unsigned int maxMipMapLevel;   // From 0 to numMipMapLevels-1
        bool         genMipMaps;       // Only generate mip-maps if set
    };

    // Default SetupParams:
    static SetupParams GetDefaultSetupParams();

    // Conversion from the Texture enum constants to GL constants:
    static unsigned int TypeToGLTarget(const Type type);
    static unsigned int AddressingToGL(const Addressing addr);
    static void FilterToGL(const Filter filter, const bool withMipMaps, unsigned int & minFilter, unsigned int & magFilter);
    static void PixelFormatToGL(const PixelFormat pf, unsigned int & internalFormat, unsigned int & format, unsigned int & type);

    // Conversion from Texture enum constants to string:
    static std::string TypeToString(const Type type);
    static std::string AddressingToString(const Addressing addr);
    static std::string FilterToString(const Filter filter);
    static std::string PixelFormatToString(const PixelFormat pf);

    // Init the texture from a image file on disk.
    bool InitFromFile(std::string filename, std::string resourceId);

    // Init the texture from a block of data. If the data originated from a file, the filename should also be provided.
    bool InitFromData(const DataBlob blob, std::string filename);

    // Extended version of InitFromData that takes setup parameters and a raw image data buffer.
    bool InitFromDataEx(std::string name, const SetupParams & setupParams, const uint8_t * imageData, const size_t imageSize);

    // Initialize the texture by firing a callback function that will create the texture.
    bool InitFromFunction(std::string name, bool (*initTexture)(std::string, class Texture *));

    // Frees all underlaying texture data.
    void FreeAllData();

    // Test if the texture is a default. This is usually the case if an initialization error occurs.
    bool IsDefault() const;

    // Estimate the amount of memory consumed by this texture. GPU+CPU memory.
    size_t GetEstimateMemoryUsage() const;

    // Texture file name.
    std::string GetResourceId() const;

    // Short string describing the resource type.
    std::string GetResourceTypeString() const;

    // Print a description of the Texture to the developer log.
    void PrintSelf() const;

    // Test if the texture has a valid GL id.
    bool IsValid() const;

    // Test if texture has a mip-map set.
    bool IsMipMapped() const;

    // Test if the base dimensions are powers of two.
    bool IsPowerOfTwo() const;

    // Bind as current GL state:
    void Bind(const unsigned int texUnit = 0) const;
    void Unbind(const unsigned int texUnit = 0) const;
    bool IsCurrent(const unsigned int texUnit = 0) const;

    // Inline accessors:
    Vec2u       GetBaseDimensions() const { return params.baseDimensions; }
    PixelFormat GetPixelFormat()    const { return params.pixelFormat;    }
    Addressing  GetAddressing()     const { return params.addressing;     }
    Filter      GetFilter()         const { return params.filter;         }
    Type        GetType()           const { return params.type;           }

    unsigned int GetAnisotropyAmount() const { return params.anisotropyAmount; }
    unsigned int GetNumMipMapLevels()  const { return params.numMipMapLevels;  }
    unsigned int GetBaseMipMapLevel()  const { return params.baseMipMapLevel;  }
    unsigned int GetMaxMipMapLevel()   const { return params.maxMipMapLevel;   }

    // Shorthand for level 0 dimensions:
    unsigned int GetWidth()  const { return params.baseDimensions.width;  }
    unsigned int GetHeight() const { return params.baseDimensions.height; }

    // Get a ref to the whole set of texture parameters.
    const SetupParams & GetSetupParams() const { return params; }

    // Set this texture as a default. This is only used internally by the renderer.
    void MarkAsDefault() { isDefault = true; }

    // Frees all associated data.
    ~Texture();

  protected:

    // RenderManager is the only object that can create Textures.
    friend class RenderManager;

    // Default constructor. Creates an empty Texture.
    Texture();

  private:

    // Internal helpers:
    bool CreateGLTexId();
    static void RefreshGLTexParams(const SetupParams & setupParams);

    // Cube-map setup helpers:
    void SetupCubeMapSide(const int side, const struct ImageData & img) const;
    void FinishCubeMapSetup(std::string resourceId, const size_t imageSize, const PixelFormat pf, const Vec2u dimensions);

    // Size in bytes of the backing image data.
    size_t textureImageSize;

    // OpenGL texture handle. 0 for invalid/null program.
    unsigned int textureId;

    // Cached Texture target for OpenGL:
    unsigned int textureTarget;

    // Texture sampling parameters set at creation.
    SetupParams params;

    // The renderer can mark some textures as default/built-in.
    bool isDefault;

    // Name of the image file/data that created this object.
    std::string textureImageName;

    // No copy allowed. This is a shareable resource.
    DISABLE_COPY_AND_ASSIGN(Texture)
};

// Strong reference counted pointer to Texture.
using TexturePtr = std::shared_ptr<Texture>;
using ConstTexturePtr = std::shared_ptr<const Texture>;

} // namespace Engine {}
