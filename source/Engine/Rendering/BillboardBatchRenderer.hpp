
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: BillboardBatchRenderer.hpp
// Author: Guilherme R. Lampert
// Created on: 02/09/14
// Brief: 3D billboard rendering.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <vector>

namespace Engine
{

// ======================================================
// Billboard setup helpers:
// ======================================================

// Billboard geometry data:
struct Billboard
{
    Vector3 origin;
    Vec3f   points[4];
    Vec2f   texCoords[4];
    Color4b colors[4];
};

// Quadrilateral faces the camera plane.
Billboard MakeBillboardFacingCameraPlane(const Vector3 up, const Vector3 right,
                                         const Vector3 origin, const Vec2f size,
                                         const Color4f color);

// Quadrilateral faces the camera world position.
Billboard MakeBillboardFacingCameraPosition(const Vector3 up, const Vector3 cameraPos,
                                            const Vector3 origin, const Vec2f size,
                                            const Color4f color);

// ======================================================
// BillboardBatchRenderer:
// ======================================================

class BillboardBatchRenderer
{
  public:

    // Sets everything to defaults.
    BillboardBatchRenderer();

    // Add some billboards to the current batch. They are only effectively drawn by FlushBatch().
    void DrawBillboards(const Billboard * billboards, const size_t count, const ConstTexturePtr & texture);

    // Effectively renders the batched billboards. Color blending should be ON and depth write OFF.
    void FlushBatch(const Matrix4 & mvp, const Vector3 cameraPos);

    // Test if the batch has anything to draw with FlushBatch().
    bool IsEmpty() const { return batchedBillboards.empty(); }

  private:

    // Internal helpers:
    void SortBatch(const Vector3 cameraPos);
    void UpdateDrawBuffers();
    void DrawBatch(const Matrix4 & mvp) const;
    void InitShaderProgram();

    // Batched billboard instance.
    struct BatchElement
    {
        ConstTexturePtr texture;
        uint32_t firstIndexInIB;
        Billboard geometry;

        BatchElement(ConstTexturePtr tex, const Billboard & bb)
            : texture(tex), firstIndexInIB(0), geometry(bb)
        { }
    };

    // Max batched billboards per frame. Batch counts are reset by FlushBatch().
    unsigned int maxBillboardsPerBillboardRenderer;

    // Dynamic vertex buffer for the billboard geometry.
    IndexedVertexBuffer vertexBuffer;

    // Batch of Billboard instances. Sorted by depth.
    std::vector<BatchElement> batchedBillboards;

    // Billboard rendering program.
    ConstShaderProgramPtr billboardShader;

    // The uniform variables we require (same names in the shader code).
    ShaderUniformHandle u_MVPMatrix;
    ShaderUniformHandle s_diffuseTexture;

    // No direct copy.
    DISABLE_COPY_AND_ASSIGN(BillboardBatchRenderer)
};

} // namespace Engine {}
