
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: DebugRenderer.hpp
// Author: Guilherme R. Lampert
// Created on: 18/09/14
// Brief: Debug rendering tools.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Geometry/AABB.hpp"
#include "Engine/Core/Geometry/Frustum.hpp"

namespace Engine
{

// ======================================================
// DebugRenderer:
// ======================================================

//
// Visual debug drawing tools.
//
// When adding debug information to the debug renderer queue,
// an object with 'durationMs' less equal to zero will be rendered for a single frame, then discarded.
//
// DebugRenderer is an implicit singleton. Only one instance of it can exist at a time.
//
class DebugRenderer final
{
  public:

    // ---- Debug drawing tools (durations are in milliseconds): ----

    // Add a point in 3D space to the debug visualization queue.
    void AddPoint(const Vec3f point,
                  const Color4f color,
                  const float size,
                  const uint32_t durationMs = 0,
                  const bool depthEnabled = true);

    // Add a 3D line to the debug visualization queue.
    // Note that lines are expressed in world coordinates, and so are all
    // wireframe primitives which are built from lines.
    void AddLine(const Vec3f from,
                 const Vec3f to,
                 const Color4f color,
                 const uint32_t durationMs = 0,
                 const bool depthEnabled = true);

    // Add a 2D text string as an overlay to the current view, using a built-in font.
    void Add2DTextF(const Vec2f pos,
                    const Color4f color,
                    const uint32_t durationMs,
                    const char * format, ...) ATTRIBUTE_FORMAT_FUNC(printf, 5, 6);

    // Add a 3D text label starting at 'pos'. The label always faces the viewer.
    void Add3DLabel(std::string str,
                    const Vec3f pos,
                    const Color4f color,
                    const uint32_t durationMs = 0);

    // Add a 3D line with an arrow-like end to the debug visualization queue.
    // 'size' defines the arrow head size. 'from' and 'to' the length of the arrow base line.
    void AddArrow(const Vec3f from,
                  const Vec3f to,
                  const Color4f color,
                  const float size,
                  const uint32_t durationMs = 0,
                  const bool depthEnabled = true);

    // Add an axis-aligned cross (3 lines converging at a point) to the debug visualization queue.
    // 'length' defines the length of the crossing lines.
    void AddCross(const Vec3f point,
                  const float length,
                  const uint32_t durationMs = 0,
                  const bool depthEnabled = true);

    // Add a set of three coordinate axis depicting the position and orientation of the given transform.
    // 'size' defines the size of the arrow heads. 'length' defines the length of the arrow base line.
    void AddAxisTriad(const Matrix4 & transform,
                      const float size,
                      const float length,
                      const uint32_t durationMs = 0,
                      const bool depthEnabled = true);

    // Add a wireframe circle to the debug visualization queue.
    void AddCircle(const Vec3f center,
                   const Vec3f planeNormal,
                   const Color4f color,
                   const float radius,
                   const float numSteps,
                   const uint32_t durationMs = 0,
                   const bool depthEnabled = true);

    // Add a wireframe plane in 3D space to the debug visualization queue.
    // If 'normalVecScale' is not zero, a line depicting the plane normal is also draw.
    void AddPlane(const Vec3f center,
                  const Vec3f planeNormal,
                  const Color4f planeColor,
                  const Color4f normalVecColor,
                  const float planeScale,
                  const float normalVecScale,
                  const uint32_t durationMs = 0,
                  const bool depthEnabled = true);

    // Add a wireframe sphere to the debug visualization queue.
    void AddSphere(const Vec3f center,
                   const Color4f color,
                   const float radius,
                   const uint32_t durationMs = 0,
                   const bool depthEnabled = true);

    // Add a wireframe cube to the debug visualization queue.
    void AddCube(const Vec3f center,
                 const Color4f color,
                 const float width,
                 const float height,
                 const float depth,
                 const uint32_t durationMs = 0,
                 const bool depthEnabled = true);

    // Add a wireframe cone to the debug visualization queue.
    // The cone 'apex' is the side where all lines meet.
    void AddCone(const Vec3f apex,
                 const Vec3f dir,
                 const Color4f color,
                 const float baseRadius,
                 const float apexRadius,
                 const uint32_t durationMs = 0,
                 const bool depthEnabled = true);

    // Add an Axis Aligned Bounding Box (AABB) to the debug visualization queue.
    void AddAABB(const AABB & aabb,
                 const Color4f color,
                 const uint32_t durationMs = 0,
                 const bool depthEnabled = true);

    // Add a wireframe frustum pyramid to the debug visualization queue.
    void AddFrustum(const Frustum & frustum,
                    const Color4f color,
                    const uint32_t durationMs = 0,
                    const bool depthEnabled = true);

    // Add a vertex normal for debug visualization.
    // The normal vector 'norm' is assumed to be already normalized.
    // NOTE: The type used here is a Vec3f, since this is a common type used to store geometry data.
    void AddVertexNormal(const Vec3f point,
                         const Vec3f norm,
                         const float size,
                         const uint32_t durationMs = 0,
                         const bool depthEnabled = true);

    // Add a "tangent basis" at a given point in space. Color scheme used is: normal=WHITE, tangent=YELLOW, bi-tangent=MAGENTA.
    // The normal vector 'norm', tangent and bi-tangent are assumed to be already normalized.
    // NOTE: The type used here is a Vec3f, since this is a common type used to store geometry data.
    void AddTangentBasis(const Vec3f point,
                         const Vec3f norm,
                         const Vec3f tang,
                         const Vec3f bitang,
                         const float sizes,
                         const uint32_t durationMs = 0,
                         const bool depthEnabled = true);

    // ---- Debug queue management: ----

    // Set the text renderer that will be used to draw 3D text and projected 3D labels.
    void SetTextRenderer(class Basic2DTextRenderer * textRenderer);

    // Set step size of arrow cone line rotation in degrees (default is 60).
    void SetDebugArrowStep(const float stepDegrees);

    // Check if there are any pending debug draws to render this frame.
    bool HasDebugDataToDraw() const;

    // Flushes the queued debug render data. This should be called every frame. 'mvp' is optional. If not provided it is calculated.
    void FlushDebugRenderData(const GameTime time, const Matrix4 & view, const Matrix4 & projection, const Matrix4 * mvp);

    // Manually removes all queued debug render data.
    void ClearAllDebugData();

    // First instance created will initialize the shared data. 'textRenderer' is just a weak reference. Not copied.
    DebugRenderer(class Basic2DTextRenderer * textRenderer = nullptr);

  private:

    // Allow RenderManager to access ShutdownSharedData().
    friend class RenderManager;

    // Static data cleanup. Performed by RenderManager on application shutdown.
    static void ShutdownSharedData();

    DISABLE_COPY_AND_ASSIGN(DebugRenderer)
};

} // namespace Engine {}
