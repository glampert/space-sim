
// ================================================================================================
// -*- C++ -*-
// File: Texture.cpp
// Author: Guilherme R. Lampert
// Created on: 08/08/14
// Brief: Basic OpenGL Texture.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"

// Renderer:
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/OpenGL.hpp"

// Image loader:
#include "Engine/Rendering/ImageLoader.hpp"

namespace Engine
{

// ======================================================
// Names/indexes of each cube-map side:
// ======================================================

namespace /* unnamed */
{

static const FixedArray<const char *, 6> cubeSideName =
{
    "neg_x", // GL_TEXTURE_CUBE_MAP_NEGATIVE_X
    "pos_x", // GL_TEXTURE_CUBE_MAP_POSITIVE_X
    "neg_y", // GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
    "pos_y", // GL_TEXTURE_CUBE_MAP_POSITIVE_Y
    "neg_z", // GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
    "pos_z"  // GL_TEXTURE_CUBE_MAP_POSITIVE_Z
};

static const FixedArray<GLenum, 6> cubeSideGLTarget =
{
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z
};

} // namespace unnamed {}

// ======================================================
// Texture:
// ======================================================

Texture::Texture()
    : textureImageSize(0)
    , textureId(0)
    , textureTarget(0)
    , params(GetDefaultSetupParams())
    , isDefault(false)
{
}

Texture::~Texture()
{
    if (textureId != 0)
    {
        // Clear all TMUs:
        for (int tmu = 0; tmu < GLState::MaxTMUs; ++tmu)
        {
            Unbind(tmu);
        }

        // Delete texture object:
        glDeleteTextures(1, &textureId);
        textureId = 0;
    }
}

bool Texture::InitFromFile(std::string filename, std::string resourceId)
{
    // Cube maps require loading the 6 separate textures.
    if (filename.find(ResourceManager::CubeMapsDirectory) != std::string::npos)
    {
        common->PrintF("Loading cube-map '%s' from file \"%s\"", resourceId.c_str(), filename.c_str());
        if (!CreateGLTexId())
        {
            return false;
        }

        // Set cube-map target and make it current:
        textureTarget = GL_TEXTURE_CUBE_MAP;
        Bind();

        PixelFormat pf;
        Vec2u dimensions;
        size_t imageSize = 0;

        // Load the six cube faces:
        for (int side = 0; side < 6; ++side)
        {
            ImageData img;
            const std::string fullPathName(filename + System::PathSepString + cubeSideName[side] + ResourceManager::StandardCubeMapFileExt);
            if (!ImageLoader::LoadImageFromFile(fullPathName, img))
            {
                common->WarningF("Failed to load cube-map side %d!", side);
                return false; // Don't finish the texture. Make it a default.
            }

            SetupCubeMapSide(side, img);

            // NOTE: We are assuming all cube sides have the same dimensions and PixelFormat!
            imageSize += img.sizeInBytes;
            dimensions = img.dimensions;
            pf = img.pixelFormat;

            common->PrintF("Loaded cube side '%s' (#%d) from image \"%s\"", cubeSideName[side], side, fullPathName.c_str());
        }

        FinishCubeMapSetup(std::move(resourceId), imageSize, pf, dimensions);
        return true;
    }
    else // Standard 2D texture:
    {
        // Load image using available loaders:
        ImageData img;
        if (!ImageLoader::LoadImageFromFile(filename, img))
        {
            common->WarningF("Failed to load texture from image file!");
            return false;
        }

        // New textures have the default parameters, except for the PixelFormat and size:
        SetupParams setupParams = GetDefaultSetupParams();
        setupParams.baseDimensions = img.dimensions;
        setupParams.pixelFormat = img.pixelFormat;
        return InitFromDataEx(std::move(resourceId), setupParams, img.pixels, img.sizeInBytes);
    }
}

bool Texture::InitFromData(const DataBlob blob, std::string filename)
{
    assert(blob.IsValid());

    /*TODO
	// Cube maps require loading the 6 separate textures.
	if (filename.find(ResourceManager::CubeMapsDirectory) != std::string::npos)
	{
		common->PrintF("Loading cube-map '%s' from DataBlob...", filename.c_str());

		// TODO blob would have to contain the 6 images???
	}
	else // Standard 2D texture:
	*/
    {
        // Load image using available loaders:
        ImageData img;
        if (!ImageLoader::LoadImageFromData(blob, filename, img))
        {
            common->WarningF("Failed to load texture from image data!");
            return false;
        }

        // New textures have the default parameters, except for the PixelFormat and size:
        SetupParams setupParams = GetDefaultSetupParams();
        setupParams.baseDimensions = img.dimensions;
        setupParams.pixelFormat = img.pixelFormat;
        return InitFromDataEx(std::move(filename), setupParams, img.pixels, img.sizeInBytes);
    }
}

bool Texture::InitFromDataEx(std::string name, const SetupParams & setupParams, const uint8_t * imageData, const size_t imageSize)
{
    // Validate the input:
    assert(setupParams.pixelFormat != PixelFormat::Invalid);
    assert(setupParams.baseDimensions.width != 0);
    assert(setupParams.baseDimensions.height != 0);
    assert(imageData != nullptr);
    assert(imageSize != 0);

    // This will also ClearGLErrors().
    if (!CreateGLTexId())
    {
        return false;
    }

    // Convert to GL constants:
    unsigned int glInternalFormat, glFormat, glType;
    PixelFormatToGL(setupParams.pixelFormat, glInternalFormat, glFormat, glType);
    textureTarget = TypeToGLTarget(setupParams.type);

    // Make active:
    Bind();

    // Allocate first level:
    glTexImage2D(
        /* target         = */ textureTarget,
        /* level          = */ 0,
        /* internalFormat = */ glInternalFormat,
        /* width          = */ setupParams.baseDimensions.width,
        /* height         = */ setupParams.baseDimensions.height,
        /* border         = */ 0,
        /* format         = */ glFormat,
        /* type           = */ glType,
        /* data           = */ imageData);

    CheckGLErrors();
    UNUSED_VAR(imageSize);

    // Refresh GL-side params:
    RefreshGLTexParams(setupParams);
    CheckGLErrors();

    // Optionally generate mip-map chain:
    if ((setupParams.numMipMapLevels > 1) && setupParams.genMipMaps)
    {
        glGenerateMipmap(textureTarget);
        CheckGLErrors();
    }

    // All went well. Save new values:
    textureImageSize = imageSize;
    textureImageName = std::move(name);
    params = setupParams;

    // We should now update the mip-map counts:
    GLint maxLevel = 0;
    glGetTexParameteriv(textureTarget, GL_TEXTURE_MAX_LEVEL, &maxLevel);
    params.numMipMapLevels = (maxLevel + 1);
    params.maxMipMapLevel = maxLevel;

    Unbind();

    // Return a success, even if GL generate errors.
    // It is too late for a roll-back. The old texture is gone.
    return true;
}

bool Texture::InitFromFunction(std::string name, bool (*initTexture)(std::string, Texture *))
{
    // Fires the callback immediately.
    assert(initTexture != nullptr);
    return initTexture(std::move(name), this);
}

void Texture::FreeAllData()
{
    if (textureId != 0)
    {
        // Clear all TMUs:
        for (int tmu = 0; tmu < GLState::MaxTMUs; ++tmu)
        {
            Unbind(tmu);
        }

        // Delete texture object:
        glDeleteTextures(1, &textureId);
        textureId = 0;
    }

    // Reset everything:
    textureTarget = 0;
    textureImageSize = 0;
    textureImageName.clear();
    params = GetDefaultSetupParams();
}

void Texture::RefreshGLTexParams(const SetupParams & setupParams)
{
    // Texture object must be already bound!

    const unsigned int glTarget = TypeToGLTarget(setupParams.type);
    const unsigned int glTexWrap = AddressingToGL(setupParams.addressing);

    // Set addressing mode:
    glTexParameteri(glTarget, GL_TEXTURE_WRAP_S, glTexWrap);
    glTexParameteri(glTarget, GL_TEXTURE_WRAP_T, glTexWrap);

    // Filtering:
    unsigned int anisotropyAmount;
    unsigned int minFilter, magFilter;

    if (setupParams.filter == Filter::Default)
    {
        // Use renderer values:
        renderMgr->GetDefaultGLTexFilter(&minFilter, &magFilter, &anisotropyAmount);
    }
    else
    {
        // Use setupParams:
        FilterToGL(setupParams.filter, ((setupParams.numMipMapLevels > 1) && setupParams.genMipMaps), minFilter, magFilter);
        anisotropyAmount = setupParams.anisotropyAmount;
    }

    // Set filtering:
    glTexParameteri(glTarget, GL_TEXTURE_MIN_FILTER, minFilter);
    glTexParameteri(glTarget, GL_TEXTURE_MAG_FILTER, magFilter);
    if (anisotropyAmount > 0)
    {
        glTexParameterf(glTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, static_cast<float>(anisotropyAmount));
    }

    // mip-map level ranges:
    assert(setupParams.maxMipMapLevel < setupParams.numMipMapLevels);
    glTexParameteri(glTarget, GL_TEXTURE_BASE_LEVEL, setupParams.baseMipMapLevel);
    glTexParameteri(glTarget, GL_TEXTURE_MAX_LEVEL, setupParams.maxMipMapLevel);
}

bool Texture::CreateGLTexId()
{
    // Clear GL error flags:
    ClearGLErrors();

    // New id? Allow recreating the texture.
    if (textureId == 0)
    {
        glGenTextures(1, &textureId);
        if (textureId == 0)
        {
            CheckGLErrors();
            common->ErrorF("Failed to generate non-zero GL texture id!");
            return false;
        }
    }

    return true;
}

void Texture::SetupCubeMapSide(const int side, const ImageData & img) const
{
    assert(IsCurrent());
    assert(side >= 0 && side < 6);
    assert(img.IsValid());

    unsigned int glInternalFormat, glFormat, glType;
    PixelFormatToGL(img.pixelFormat, glInternalFormat, glFormat, glType);
    glTexImage2D(
        /* target         = */ cubeSideGLTarget[side],
        /* level          = */ 0,
        /* internalFormat = */ glInternalFormat,
        /* width          = */ img.dimensions.width,
        /* height         = */ img.dimensions.height,
        /* border         = */ 0,
        /* format         = */ glFormat,
        /* type           = */ glType,
        /* data           = */ img.pixels);
    CheckGLErrors();
}

void Texture::FinishCubeMapSetup(std::string resourceId, const size_t imageSize, const Texture::PixelFormat pf, const Vec2u dimensions)
{
    assert(IsCurrent());
    assert(textureTarget == GL_TEXTURE_CUBE_MAP);

    // Set standard GL cube-map parameters:
    glTexParameteri(textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(textureTarget, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(textureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(textureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // Update other local parameters:
    textureImageName = std::move(resourceId);
    textureImageSize = imageSize;
    params.baseDimensions = dimensions;
    params.type = Type::CubeMap;
    params.addressing = Addressing::ClampToEdge;
    params.filter = Filter::Linear;
    params.pixelFormat = pf;
    params.anisotropyAmount = 0;
    params.numMipMapLevels = 1;
    params.baseMipMapLevel = 0;
    params.maxMipMapLevel = 0;
    params.genMipMaps = false;

    // Cube-maps are NOT mip-mapped!
    glTexParameteri(textureTarget, GL_TEXTURE_BASE_LEVEL, params.baseMipMapLevel);
    glTexParameteri(textureTarget, GL_TEXTURE_MAX_LEVEL, params.maxMipMapLevel);

    // Some final error checking:
    CheckGLErrors();

    // And unbind.
    Unbind();
}

size_t Texture::GetEstimateMemoryUsage() const
{
    return textureImageSize;
}

bool Texture::IsDefault() const
{
    return isDefault;
}

std::string Texture::GetResourceId() const
{
    return textureImageName;
}

std::string Texture::GetResourceTypeString() const
{
    return "Texture";
}

void Texture::PrintSelf() const
{
    common->PrintF("------- Texture -------");
    common->PrintF("Size............: %ux%u pixels", GetWidth(), GetHeight());
    common->PrintF("Image file......: %s", textureImageName.c_str());
    common->PrintF("Memory usage....: %s", FormatMemoryUnit(textureImageSize, true).c_str());
    common->PrintF("GL texture id...: %u", textureId);
    common->PrintF("Type............: %s", TypeToString(params.type).c_str());
    common->PrintF("Addressing......: %s", AddressingToString(params.addressing).c_str());
    common->PrintF("Filter..........: %s", FilterToString(params.filter).c_str());
    common->PrintF("Pixel format....: %s", PixelFormatToString(params.pixelFormat).c_str());
    common->PrintF("Anisotropy......: %u", params.anisotropyAmount);
    common->PrintF("Num mip levels..: %u", params.numMipMapLevels);
    common->PrintF("Base mip level..: %u", params.baseMipMapLevel);
    common->PrintF("Max mip level...: %u", params.maxMipMapLevel);
    common->PrintF("Gen mip-maps....: %s", (params.genMipMaps ? "yes" : "no"));
    common->PrintF("Is default?.....: %s", (IsDefault() ? "yes" : "no"));
}

bool Texture::IsValid() const
{
    return textureId != 0;
}

bool Texture::IsMipMapped() const
{
    return params.genMipMaps && (params.numMipMapLevels > 1);
}

bool Texture::IsPowerOfTwo() const
{
    return ((params.baseDimensions.width  & (params.baseDimensions.width  - 1)) == 0) &&
           ((params.baseDimensions.height & (params.baseDimensions.height - 1)) == 0);
}

void Texture::Bind(const unsigned int texUnit) const
{
    if (renderMgr->GetState().currentTexture[texUnit] != textureId)
    {
        renderMgr->GetState().currentTexture[texUnit] = textureId;
        glActiveTexture(GL_TEXTURE0 + texUnit);
        glBindTexture(textureTarget, textureId);
    }
}

void Texture::Unbind(const unsigned int texUnit) const
{
    renderMgr->GetState().currentTexture[texUnit] = 0;
    glActiveTexture(GL_TEXTURE0 + texUnit);
    glBindTexture(textureTarget, 0);
}

bool Texture::IsCurrent(const unsigned int texUnit) const
{
    return (textureId != 0) && (textureId == renderMgr->GetState().currentTexture[texUnit]);
}

Texture::SetupParams Texture::GetDefaultSetupParams()
{
    return
    {
        /* baseDimensions   = */ Vec2u(0, 0),
        /* type             = */ Type::Texture2D,
        /* addressing       = */ Addressing::Default,
        /* filter           = */ Filter::Default,
        /* pixelFormat      = */ PixelFormat::Invalid,
        /* anisotropyAmount = */ 0,
        /* numMipMapLevels  = */ MaxMipMapLevels,
        /* baseMipMapLevel  = */ 0,
        /* maxMipMapLevel   = */ (MaxMipMapLevels - 1),
        /* genMipMaps       = */ true
    };
}

unsigned int Texture::TypeToGLTarget(const Texture::Type type)
{
    switch (type)
    {
    case Type::Texture2D:
        return GL_TEXTURE_2D;
    case Type::CubeMap:
        return GL_TEXTURE_CUBE_MAP;
    default:
        common->FatalErrorF("Invalid Texture::Type!");
    } // switch (type)
}

unsigned int Texture::AddressingToGL(const Texture::Addressing addr)
{
    switch (addr)
    {
    case Addressing::ClampToEdge:
        return GL_CLAMP_TO_EDGE;

    case Addressing::Repeat:
        return GL_REPEAT;

    case Addressing::MirroredRepeat:
        return GL_MIRRORED_REPEAT;

    case Addressing::Default:
        return GL_REPEAT;

    default:
        common->FatalErrorF("Invalid Texture::Addressing!");
    } // switch (addr)
}

void Texture::FilterToGL(const Texture::Filter filter, const bool withMipMaps, unsigned int & minFilter, unsigned int & magFilter)
{
    switch (filter)
    {
    case Filter::Nearest:
        minFilter = (withMipMaps ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST);
        magFilter = GL_NEAREST;
        break;

    case Filter::Linear:
        minFilter = (withMipMaps ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
        magFilter = GL_LINEAR;
        break;

    case Filter::Anisotropic:
        // Same as trilinear, but user also have to set GL_TEXTURE_MAX_ANISOTROPY_EXT.
        minFilter = GL_LINEAR_MIPMAP_LINEAR;
        magFilter = GL_LINEAR;
        break;

    case Filter::Default:
        // Query the renderer:
        renderMgr->GetDefaultGLTexFilter(&minFilter, &magFilter, nullptr);
        break;

    default:
        common->FatalErrorF("Invalid Texture::Filter!");
    } // switch (filter)
}

void Texture::PixelFormatToGL(const Texture::PixelFormat pf, unsigned int & internalFormat,
                              unsigned int & format, unsigned int & type)
{
    switch (pf)
    {
    case PixelFormat::RGB_U8:
        internalFormat = GL_RGB8;
        format = GL_RGB;
        type = GL_UNSIGNED_BYTE;
        break;

    case PixelFormat::RGBA_U8:
        internalFormat = GL_RGBA8;
        format = GL_RGBA;
        type = GL_UNSIGNED_BYTE;
        break;

    case PixelFormat::BGR_U8:
        internalFormat = GL_RGB8;
        format = GL_BGR;
        type = GL_UNSIGNED_BYTE;
        break;

    case PixelFormat::BGRA_U8:
        internalFormat = GL_RGBA8;
        format = GL_BGRA;
        type = GL_UNSIGNED_INT_8_8_8_8_REV;
        break;

    default:
        common->FatalErrorF("Invalid Texture::PixelFormat!");
    } // switch (pf)
}

std::string Texture::TypeToString(const Texture::Type type)
{
    switch (type)
    {
    case Type::Texture2D:
        return "Texture2D";
    case Type::CubeMap:
        return "CubeMap";
    default:
        common->FatalErrorF("Invalid Texture::Type!");
    } // switch (type)
}

std::string Texture::AddressingToString(const Texture::Addressing addr)
{
    switch (addr)
    {
    case Addressing::ClampToEdge:
        return "ClampToEdge";
    case Addressing::Repeat:
        return "Repeat";
    case Addressing::MirroredRepeat:
        return "MirroredRepeat";
    case Addressing::Default:
        return "Default";
    default:
        common->FatalErrorF("Invalid Texture::Addressing!");
    } // switch (addr)
}

std::string Texture::FilterToString(const Texture::Filter filter)
{
    switch (filter)
    {
    case Filter::Nearest:
        return "Nearest";
    case Filter::Linear:
        return "Linear";
    case Filter::Anisotropic:
        return "Anisotropic";
    case Filter::Default:
        return "Default";
    default:
        common->FatalErrorF("Invalid Texture::Filter!");
    } // switch (filter)
}

std::string Texture::PixelFormatToString(const Texture::PixelFormat pf)
{
    switch (pf)
    {
    case PixelFormat::Invalid:
        return "Invalid";
    case PixelFormat::RGB_U8:
        return "RGB_U8";
    case PixelFormat::RGBA_U8:
        return "RGBA_U8";
    case PixelFormat::BGR_U8:
        return "BGR_U8";
    case PixelFormat::BGRA_U8:
        return "BGRA_U8";
    default:
        common->FatalErrorF("Invalid Texture::PixelFormat!");
    } // switch (pf)
}

// ======================================================
// Texture factory function for ResourceManager:
// ======================================================

static ResourcePtr Texture_FactoryFunction(const bool forceDefault)
{
    if (forceDefault)
    {
        return renderMgr->defaultDiffuseTexture;
    }
    else
    {
        return renderMgr->NewTexture();
    }
}

// Register the callback:
REGISTER_RESOURCE_FACTORY_FOR_TYPE(Resource::Type::Texture, Texture_FactoryFunction);

} // namespace Engine {}
