
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Basic2DTextRenderer.hpp
// Author: Guilherme R. Lampert
// Created on: 13/09/14
// Brief: Basic 2D text renderer helper. Can be inherited by other classes.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// TextAlign:
// ======================================================

enum class TextAlign
{
    Left,
    Center,
    Right,
    Justify
};

// ======================================================
// Basic2DTextRenderer:
// ======================================================

//
// Text positions are in pixels.
// Standard screen (0,0) origin is the lower-left corner (OpenGL's default).
//
// NOTE: Currently, font outlines are not supported.
//
class Basic2DTextRenderer
{
  public:

    // The references must live for at least as long as this object does!
    // No copies of the input objects are made.
    Basic2DTextRenderer(const Font & fnt, SpriteBatchRenderer & batch, const SpriteLayer layer);

    // Simple draw, without accounting for line breaks.
    void DrawText(const Vec2f pos, const Color4f color, const TextAlign align, const char * text);

    // Draw multi-line text, i.e. handling line breaks.
    void DrawTextML(const Vec2f pos, const Color4f color, const TextAlign align, const char * text);

    // Draw multi-line text constrained to a max line width. Breaks lines that are too long.
    void DrawTextMaxWidth(const Vec2f pos, const float maxWidth, const Color4f color, const TextAlign align, const char * text);

    // Compute the with of a string as if it were drawn,
    // but without actually rendering it (textLength is computed if <= 0).
    float GetTextWidth(const char * text, int textLength) const;

    // Get a fullscreen orthographic projection with origin at the lower-left corner.
    // This matrix can be used to render fullscreen text with the Basic2DTextRenderer.
    static Matrix4 GetFullscreenOrthoMatrix();

    // Get/set sprite text layer:
    SpriteLayer GetTextLayer() const { return textLayer; }
    void SetTextLayer(const SpriteLayer layer) { textLayer = layer; }

  private:

    void InternalWrite(float x, float y, const Color4f color, const char * text,
                       const int textLength, const float spacing = 0.0f);

    // References to the font and a sprite batch.
    // The references must outlive this class!
    const Font & font;
    SpriteBatchRenderer & spriteBatch;

    // Sprite layer used by the renderer. Can be changed during runtime.
    SpriteLayer textLayer;
};

} // namespace Engine {}
