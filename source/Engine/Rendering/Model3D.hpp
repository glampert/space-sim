
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Model3D.hpp
// Author: Guilherme R. Lampert
// Created on: 08/08/14
// Brief: 3D model/mesh/object.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Geometry/AABB.hpp"
#include <vector>

namespace Engine
{

// ======================================================
// Model3D:
// ======================================================

//
// Minimal interface for a 3D renderer model/object.
//
// A renderer model can either be a static geometry model
// or it can be an object generated dynamically in a per-frame/view basis.
//
// NOTE: Model3D instances are created via RenderManager::NewModel3D().
//
class Model3D
    : public Resource
{
  public:

    //
    // DrawVertex structure:
    // Vertex type used by static 3D objects.
    //
    struct DrawVertex
    {

        // Data:
        Vec3f position;
        Vec3f normal;
        Vec3f tangent;
        Vec3f bitangent;
        Vec2f texCoords;

        // Attribute indexes for glEnableVertexAttribArray():
        static constexpr int PositionAttrIndex = 0;
        static constexpr int NormalAttrIndex = 1;
        static constexpr int TangentAttrIndex = 2;
        static constexpr int BiTangentAttrIndex = 3;
        static constexpr int TexCoordsAttrIndex = 4;

        // Offsets for glVertexAttribPointer():
        static constexpr size_t PositionOffset = sizeof(Vec3f) * 0;
        static constexpr size_t NormalOffset = sizeof(Vec3f) * 1;
        static constexpr size_t TangentOffset = sizeof(Vec3f) * 2;
        static constexpr size_t BiTangentOffset = sizeof(Vec3f) * 3;
        static constexpr size_t TexCoordsOffset = sizeof(Vec3f) * 4;
    };

    // Adjust this if DrawVertex is changed!
    static_assert(sizeof(DrawVertex) == (sizeof(Vec3f) * 4 + sizeof(Vec2f)), "Wrong Model3D::DrawVertex size!");

    //
    // Model index data type.
    // This can be changed to uint16_t if models are small.
    // It would save a considerable amount of memory.
    //
    using IndexType = uint32_t;
    static constexpr unsigned int GetGLTypeForIndexType();

    //
    // SubMesh structure.
    // Has the vertex/index range of the mesh plus its material.
    //
    struct SubMesh
    {

        // Material to apply. If null, a default is used.
        ConstMaterialPtr material;

        // System-side data:
        std::vector<DrawVertex> vertexes;
        std::vector<IndexType> indexes;

        // Offsets in VB/IB, needed for rendering:
        unsigned int firstVertexInVB = 0;
        unsigned int firstIndexInIB = 0;

        // Tightest box fitting just this SubMesh.
        // The parent model has one that encompasses the whole mesh set.
        AABB aabb;

        // Compute bounds for this SubMesh.
        const AABB & ComputeBounds()
        {
            aabb.FromMeshVertexes(vertexes.data(), vertexes.size(), sizeof(DrawVertex));
            return aabb;
        }
    };

    // Init the model from a file on disk.
    bool InitFromFile(std::string filename, std::string resourceId);

    // Init the model from a block of data. If the data originated from a file, the filename is should be provided.
    bool InitFromData(const DataBlob blob, std::string filename);

    // Frees all underlaying model data.
    void FreeAllData();

    // Test if the model is a default. This is usually the case if an initialization error occurs.
    bool IsDefault() const;

    // Estimate the amount of memory consumed by the model.
    size_t GetEstimateMemoryUsage() const;

    // Get the name of the file that originated this model.
    std::string GetResourceId() const;

    // Short string describing the resource type.
    std::string GetResourceTypeString() const;

    // Print a description of this model to the developer log.
    void PrintSelf() const;

    // Compute AABBs for the whole model and each of its SubMeshes.
    void ComputeBounds();

    // Draw calls:
    void DrawWithMVP(const Matrix4 & mvp, const Matrix4 & mv) const;
    void DrawWithMaterial(const ConstMaterialPtr & material,
                          const Matrix4 * mvp = nullptr,
                          const Matrix4 * mv = nullptr) const;

    // Debug drawing:
    void DrawModelAABBs(class DebugRenderer & debugRenderer, const Matrix4 * transform,
                        const Color4f color, const uint32_t durationMs = 0,
                        const bool depthEnabled = true) const;

    // Manage SubMeshes:
    void AllocateSubMeshes(const size_t num);
    unsigned int GetSubMeshCount() const;

    // Access SubMeshes with bounds checking:
    SubMesh & GetSubMeshAt(const size_t index);
    const SubMesh & GetSubMeshAt(const size_t index) const;

    // Miscellaneous queries:
    unsigned int GetVertexCount() const;
    unsigned int GetIndexCount() const;
    size_t GetVertexBufferSizeBytes() const;
    size_t GetIndexBufferSizeBytes() const;

    // Get/set BufferStorage:
    BufferStorage GetBufferStorage() const;
    void SetBufferStorage(const BufferStorage storage);

    // Get model bounds:
    AABB & GetAABB() { return aabb; }
    const AABB & GetAABB() const { return aabb; }

  protected:

    // RenderManager is the only object that can create Model3Ds.
    friend class RenderManager;

    // Default constructor. Creates an empty Model3D.
    Model3D() = default;

  private:

    // Internal helpers:
    bool InitBuffers();
    void CopyDataToBuffers(void * pVB, void * pIB) const;

    // OpenGL buffers:
    IndexedVertexBuffer vertexBuffer;

    // Array of SubMeshes. They all share the same VB/IB.
    std::vector<SubMesh> subMeshes;

    // Tightest AABB that encompasses the whole model with all SubMeshes.
    AABB aabb;

    // Name of the file that originated this Model3D.
    std::string modelFileName;

    // No copy allowed. This is a shareable resource.
    DISABLE_COPY_AND_ASSIGN(Model3D)
};

// Strong reference counted pointer to Model3D.
using Model3DPtr = std::shared_ptr<Model3D>;
using ConstModel3DPtr = std::shared_ptr<const Model3D>;

} // namespace Engine {}
