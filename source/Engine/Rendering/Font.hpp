
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Font.hpp
// Author: Guilherme R. Lampert
// Created on: 06/09/14
// Brief: Font resource for text rendering. Can be loaded from a .FNT file created with BMFont.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <vector>
#include <unordered_map>

namespace Engine
{

// ======================================================
// Font:
// ======================================================

//
// Font resource type.
// Font resources can be loaded from a .FNT file created with the BMFont tool.
// A Font can have several Texture resources bundled inside it.
//
// On MacOS/iOS, the Hiero tool can also be used to generate compliant FNT files.
//
// NOTE: Font instances are created via RenderManager::NewFont().
//
class Font
    : public Resource
{
  public:

    // Encoding of the glyph chars of a font (ASCII is the default).
    enum class Encoding
    {
        ASCII,
        UTF8,
        UTF16
    };

    // Data pertaining a font glyph.
    struct GlyphInfo
    {
        // Glyph data:
        int16_t x;
        int16_t y;
        int16_t width;
        int16_t height;
        int16_t xOffset;
        int16_t yOffset;
        int16_t xAdvance;
        int16_t pageNum;
        int16_t channel;
        std::vector<int32_t> kerningPairs;

        GlyphInfo() noexcept; // Construct with everything set to zero.
        GlyphInfo(const int x, const int y, const int w, const int h, const int xOffs,
                  const int yOffs, const int xAdv, const int pageNum, const int channel) noexcept;

        bool operator==(const GlyphInfo & other) const noexcept;
        bool operator!=(const GlyphInfo & other) const noexcept;
    };

    // Dummy constant. All fields set to zero.
    static const GlyphInfo NullGlyphInfo;

    // Init the Font from file. Will attempt to load a FNT file and textures from 'filename' path.
    bool InitFromFile(std::string filename, std::string resourceId);

    // Init the Font from a block of data. The blob has the contents of a FNT file. Texture pages are requested via ResourceManager.
    bool InitFromData(const DataBlob blob, std::string filename);

    // Frees all underlaying font data and textures.
    void FreeAllData();

    // Test if the resource is a default. This is usually the case if an initialization error occurs.
    bool IsDefault() const;

    // Estimate the amount of memory consumed by this font.
    size_t GetEstimateMemoryUsage() const;

    // Resource name/id; The font filename.
    std::string GetResourceId() const;

    // Short string describing the resource type.
    std::string GetResourceTypeString() const;

    // Print a description of the resource to the developer log (Common).
    void PrintSelf() const;

    // Test if this font has a glyph id/char.
    bool HasGlyph(const int glyphId) const;

    // Get the glyph info set for a given glyph id/char. Will return NullGlyphInfo if the id is not found.
    const GlyphInfo & GetGlyphInfo(const int glyphId) const;

    // Number of glyphs available for this font.
    size_t GetGlyphCount() const { return glyphMap.size(); }

    // Number of glyph page textures loaded for this font.
    size_t GetGlyphPageCount() const { return glyphPages.size(); }

    // Get a given texture page containing the font glyphs.
    ConstTexturePtr GetGlyphPageTexture(const size_t index) const { return glyphPages[index]; }

    // Font face name, such as "ArialMT".
    std::string GetFaceName() const { return faceName; }

    // Text encoding of this font's glyphs. ASCII by default.
    Encoding GetTextEncoding() const { return encoding; }
    void SetTextEncoding(const Encoding enc) { encoding = enc; }

    // Get/set font height, with scaling taken into account.
    float GetHeight() const { return (scale * static_cast<float>(fontHeight)); }
    void SetHeight(const float h) { scale = h / static_cast<float>(fontHeight); }

    // Get text offsets, with scaling taken into account.
    float GetBottomOffset() const { return (scale * (base - fontHeight)); }
    float GetTopOffset() const { return (scale * static_cast<float>(base)); }

    // Text scalings:
    float GetScale()  const { return scale;  }
    float GetScaleW() const { return scaleW; }
    float GetScaleH() const { return scaleH; }

    // Unscaled height, as read from the FNT file.
    int GetUnscaledHeight() const { return fontHeight; }

    // Size of the font face, as read from the FNT file.
    int GetSize() const { return fontSize; }

    // Check if this font is to be rendered with outline.
    bool HasOutline() const { return hasOutline; }

    // Overwrite the outline drawing for this font.
    void SetHasOutline(const bool outline) { hasOutline = outline; }

    // Test if font face is bold and italic:
    bool IsBold()   const { return isBold;   }
    bool IsItalic() const { return isItalic; }

    // Aux rendering methods.
    // Get text length/char-info taking into account the encoding.
    int GetTextLength(const char * text) const;
    int GetTextChar(const char * text, const int pos, int * nextPos = nullptr) const;
    int FindTextChar(const char * text, const int start, const int length, const int ch) const;
    float AdjustForKerningPairs(const int first, const int second) const;

  protected:

    // RenderManager is the only object that can create Fonts.
    friend class RenderManager;

    // The font loader sets up new font objects from FNT files.
    friend class FNTLoader;

    // Default constructor. Creates an empty Font.
    Font();

  private:

    // Map of font glyphs indexed by the char number (supports char and wchar_t).
    std::unordered_map<int, GlyphInfo> glyphMap;

    // Textures with the font glyphs. Usually just one, but it can have more.
    std::vector<ConstTexturePtr> glyphPages;

    // Additional data from the FNT file:
    int fontHeight;
    int fontSize;
    int base;
    int scaleW;
    int scaleH;
    float scale;
    Encoding encoding;

    // Font face name, such as "ArialMT".
    std::string faceName;

    // FNT file that originated this font.
    std::string fontFilename;

    // Draw glyphs from this font with outline?
    bool hasOutline;

    // Bold and italic info:
    bool isBold;
    bool isItalic;

    // No copy allowed. This is a shareable resource.
    DISABLE_COPY_AND_ASSIGN(Font)
};

// Strong reference counted pointer to Font.
using FontPtr = std::shared_ptr<Font>;
using ConstFontPtr = std::shared_ptr<const Font>;

} // namespace Engine {}
