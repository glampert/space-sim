
// ================================================================================================
// -*- C++ -*-
// File: InputHandlers.cpp
// Author: Guilherme R. Lampert
// Created on: 24/08/14
// Brief: Basic input handlers, such as keyboard and mouse.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine core:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Application/AppEvent.hpp"

// Input system:
#include "Engine/Input/Keys.hpp"
#include "Engine/Input/InputHandlers.hpp"

namespace Engine
{

// ======================================================
// KeyboardInputHandler:
// ======================================================

KeyboardInputHandler::KeyboardInputHandler()
{
    ZeroArray(keyboard);
}

bool KeyboardInputHandler::OnAppEvent(const AppEvent event)
{
    switch (event.type)
    {
    case AppEvent::Type::KeyPress:
        assert(size_t(event.keyboard.keyCode) < ArrayLength(keyboard));
        keyboard[event.keyboard.keyCode].keyState  = 1;
        keyboard[event.keyboard.keyCode].modifiers = static_cast<uint8_t>(event.keyboard.modifiers); // Actually only 7 bits!
        return true;

    case AppEvent::Type::KeyRelease:
        assert(size_t(event.keyboard.keyCode) < ArrayLength(keyboard));
        keyboard[event.keyboard.keyCode].keyState  = 0;
        keyboard[event.keyboard.keyCode].modifiers = 0;
        return true;

    default:
        // Event not handled here.
        return false;
    } // switch (event.type)
}

// ======================================================
// MouseInputHandler:
// ======================================================

MouseInputHandler::MouseInputHandler()
    : mousePos(0.0f, 0.0f)
    , mouseScroll(0.0f, 0.0f)
{
    ZeroArray(buttonDown);
}

void MouseInputHandler::SetCursorPosition(const Vec2f pos)
{
    mousePos = pos;
    common->WarpSystemCursor(mousePos);
}

bool MouseInputHandler::OnAppEvent(const AppEvent event)
{
    switch (event.type)
    {
    case AppEvent::Type::MouseMove:
        mousePos.x = static_cast<float>(event.mousePos.x);
        mousePos.y = static_cast<float>(event.mousePos.y);
        return true;

    case AppEvent::Type::MouseScroll:
        mouseScroll.xOffset = static_cast<float>(event.mouseScroll.xOffset);
        mouseScroll.yOffset = static_cast<float>(event.mouseScroll.yOffset);
        return true;

    case AppEvent::Type::MouseBtnClick:
        assert(size_t(event.mouseBtn.buttonId) < ArrayLength(buttonDown));
        buttonDown[event.mouseBtn.buttonId] = true;
        return true;

    case AppEvent::Type::MouseBtnRelease:
        assert(size_t(event.mouseBtn.buttonId) < ArrayLength(buttonDown));
        buttonDown[event.mouseBtn.buttonId] = false;
        return true;

    default:
        // Event not handled here.
        return false;
    } // switch (event.type)
}

} // namespace Engine {}
