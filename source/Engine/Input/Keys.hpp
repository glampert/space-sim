
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Keys.hpp
// Author: Guilherme R. Lampert
// Created on: 11/12/13
// Brief: List of keyboard key numbers, modifier flags and mouse buttons.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Keyboard keys:
// ======================================================

//
// Known keys in a standard QWERTY keyboard.
//
// ASCII keys are defined by their equivalent ASCII codes.
// Non-ASCII/function keys are numbered above 255.
//
// New keys may be added as needed, however, a key number should
// always be under 32767 (INT16_MAX)
//
struct Key
{
    // Unknown key / not mapped:
    static constexpr int16_t Unknown = -1;

    // Printable ASCII keys:
    static constexpr int16_t Space = ' ';
    static constexpr int16_t Apostrophe = '\'';
    static constexpr int16_t Comma = ',';
    static constexpr int16_t Minus = '-';
    static constexpr int16_t Dot = '.';
    static constexpr int16_t Slash = '/';
    static constexpr int16_t Num0 = '0';
    static constexpr int16_t Num1 = '1';
    static constexpr int16_t Num2 = '2';
    static constexpr int16_t Num3 = '3';
    static constexpr int16_t Num4 = '4';
    static constexpr int16_t Num5 = '5';
    static constexpr int16_t Num6 = '6';
    static constexpr int16_t Num7 = '7';
    static constexpr int16_t Num8 = '8';
    static constexpr int16_t Num9 = '9';
    static constexpr int16_t Semicolon = ';';
    static constexpr int16_t Equal = '=';
    static constexpr int16_t A = 'A';
    static constexpr int16_t B = 'B';
    static constexpr int16_t C = 'C';
    static constexpr int16_t D = 'D';
    static constexpr int16_t E = 'E';
    static constexpr int16_t F = 'F';
    static constexpr int16_t G = 'G';
    static constexpr int16_t H = 'H';
    static constexpr int16_t I = 'I';
    static constexpr int16_t J = 'J';
    static constexpr int16_t K = 'K';
    static constexpr int16_t L = 'L';
    static constexpr int16_t M = 'M';
    static constexpr int16_t N = 'N';
    static constexpr int16_t O = 'O';
    static constexpr int16_t P = 'P';
    static constexpr int16_t Q = 'Q';
    static constexpr int16_t R = 'R';
    static constexpr int16_t S = 'S';
    static constexpr int16_t T = 'T';
    static constexpr int16_t U = 'U';
    static constexpr int16_t V = 'V';
    static constexpr int16_t W = 'W';
    static constexpr int16_t X = 'X';
    static constexpr int16_t Y = 'Y';
    static constexpr int16_t Z = 'Z';
    static constexpr int16_t LeftBracket = '[';
    static constexpr int16_t RightBracket = ']';
    static constexpr int16_t Backslash = '\\';
    static constexpr int16_t GraveAccent = '`';
    // Non-ASCII char keys should be added in the following gap.
    // 'GraveAccent' (`) is number 96.

    // Function keys / key pad:
    static constexpr int16_t Escape = 256;
    static constexpr int16_t Return = 257;
    static constexpr int16_t Tab = 258;
    static constexpr int16_t Backspace = 259;
    static constexpr int16_t Insert = 260;
    static constexpr int16_t Delete = 261;
    static constexpr int16_t Right = 262;
    static constexpr int16_t Left = 263;
    static constexpr int16_t Down = 264;
    static constexpr int16_t Up = 265;
    static constexpr int16_t PageUp = 266;
    static constexpr int16_t PageDown = 267;
    static constexpr int16_t Home = 268;
    static constexpr int16_t End = 269;
    static constexpr int16_t CapsLock = 280;
    static constexpr int16_t ScrollLock = 281;
    static constexpr int16_t NumLock = 282;
    static constexpr int16_t PrintScreen = 283;
    static constexpr int16_t Pause = 284;
    static constexpr int16_t F1 = 290;
    static constexpr int16_t F2 = 291;
    static constexpr int16_t F3 = 292;
    static constexpr int16_t F4 = 293;
    static constexpr int16_t F5 = 294;
    static constexpr int16_t F6 = 295;
    static constexpr int16_t F7 = 296;
    static constexpr int16_t F8 = 297;
    static constexpr int16_t F9 = 298;
    static constexpr int16_t F10 = 299;
    static constexpr int16_t F11 = 300;
    static constexpr int16_t F12 = 301;
    static constexpr int16_t F13 = 302;
    static constexpr int16_t F14 = 303;
    static constexpr int16_t F15 = 304; // 305 to 319 Reserved for future expansion
    static constexpr int16_t KeyPad0 = 320;
    static constexpr int16_t KeyPad1 = 321;
    static constexpr int16_t KeyPad2 = 322;
    static constexpr int16_t KeyPad3 = 323;
    static constexpr int16_t KeyPad4 = 324;
    static constexpr int16_t KeyPad5 = 325;
    static constexpr int16_t KeyPad6 = 326;
    static constexpr int16_t KeyPad7 = 327;
    static constexpr int16_t KeyPad8 = 328;
    static constexpr int16_t KeyPad9 = 329;
    static constexpr int16_t KeyPadDecimal = 330;
    static constexpr int16_t KeyPadDivide = 331;
    static constexpr int16_t KeyPadMultiply = 332;
    static constexpr int16_t KeyPadSubtract = 333;
    static constexpr int16_t KeyPadAdd = 334;
    static constexpr int16_t KeyPadReturn = 335;
    static constexpr int16_t KeyPadEqual = 336;
    static constexpr int16_t LeftShift = 340;
    static constexpr int16_t LeftControl = 341;
    static constexpr int16_t LeftAlt = 342;
    static constexpr int16_t LeftCommand = 343; // AKA "Apple Key" (Left side)
    static constexpr int16_t RightShift = 344;
    static constexpr int16_t RightControl = 345;
    static constexpr int16_t RightAlt = 346;
    static constexpr int16_t RightCommand = 347; // AKA "Apple Key" (Right side)
    static constexpr int16_t Menu = 348;
    static constexpr int16_t LastKey = Menu; // Sentry value. Internal use

    // Key modifiers:
    struct Modifier
    {
        // If this bit is set one or more Shift keys were held down when a key was pressed.
        static constexpr uint32_t Shift = BITFIELD_ENUM(0);
        // If this bit is set one or more Control keys were held down when a key was pressed.
        static constexpr uint32_t Control = BITFIELD_ENUM(1);
        // If this bit is set one or more Alt keys were held down when a key was pressed.
        static constexpr uint32_t Alt = BITFIELD_ENUM(2);
        // If this bit is set one or more Command (Apple Key) keys were held down when a key was pressed.
        static constexpr uint32_t Command = BITFIELD_ENUM(3);
    };
};

// Key code/id type. Accepts any of the Key constants.
using KeyCode = int16_t;

// ======================================================
// Mouse buttons:
// ======================================================

// List of known mouse buttons:
enum class MouseButton
{
    // Traditional mouse buttons:
    Left,
    Right,
    Middle,
    // Sentry value. Internal use.
    LastButton = Middle
};

} // namespace Engine {}
