
// ================================================================================================
// -*- C++ -*-
// File: XB360Controller.cpp
// Author: Guilherme R. Lampert
// Created on: 02/12/14
// Brief: Minimal interface for an XBOX 360 controller.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Application/AppEvent.hpp"
#include "Engine/Input/XB360Controller.hpp"
#include "Engine/Core/Containers/FixedArray.hpp"

// For the time being, we rely entirely on GLFW to do the
// Controller input for us. If works well enough for our purposes.
#include "Engine/Rendering/OpenGL.hpp"

namespace Engine
{

// Button name strings for printing:
static const FixedArray<const char *, int(XB360Controller::Button::Count)> buttonNames =
{
    "A",
    "B",
    "X",
    "Y",
    "ArrowUp",
    "ArrowDown",
    "ArrowLeft",
    "ArrowRight",
    "LButton",
    "RButton",
    "LThumb",
    "RThumb",
    "LTrigger",
    "RTrigger",
    "Start",
    "Back",
    "XBoxButton"
};

// ======================================================
// XB360Controller:
// ======================================================

XB360Controller::XB360Controller()
{
    ZeroArray(buttonStates);
    ZeroArray(axisStates);
}

bool XB360Controller::IsButtonUp(const Button button) const
{
    assert(size_t(button) < ArrayLength(buttonStates));
    return (buttonStates[int(button)] ? false : true);
}

bool XB360Controller::IsButtonDown(const Button button) const
{
    assert(size_t(button) < ArrayLength(buttonStates));
    return (buttonStates[int(button)] ? true : false);
}

float XB360Controller::GetAxisState(const int axisId) const
{
    assert(size_t(axisId) < ArrayLength(axisStates));
    return axisStates[axisId];
}

void XB360Controller::PollInput()
{
    // FIXME: This will be a problem if the system
    // has more than one controller attached to it...
    const int jsId = 0;

    // Button updates:
    {
        ZeroArray(buttonStates);

        int jsNumButtons = 0;
        const uint8_t * jsButtonStates = glfwGetJoystickButtons(jsId, &jsNumButtons);
        if ((jsButtonStates != nullptr) && (jsNumButtons > 0))
        {
            buttonStates[int(Button::ArrowUp)] = jsButtonStates[0];
            buttonStates[int(Button::ArrowDown)] = jsButtonStates[1];
            buttonStates[int(Button::ArrowLeft)] = jsButtonStates[2];
            buttonStates[int(Button::ArrowRight)] = jsButtonStates[3];
            buttonStates[int(Button::Start)] = jsButtonStates[4];
            buttonStates[int(Button::Back)] = jsButtonStates[5];
            buttonStates[int(Button::LThumb)] = jsButtonStates[6];
            buttonStates[int(Button::RThumb)] = jsButtonStates[7];
            buttonStates[int(Button::LButton)] = jsButtonStates[8];
            buttonStates[int(Button::RButton)] = jsButtonStates[9];
            buttonStates[int(Button::XBoxButton)] = jsButtonStates[10];
            buttonStates[int(Button::A)] = jsButtonStates[11];
            buttonStates[int(Button::B)] = jsButtonStates[12];
            buttonStates[int(Button::X)] = jsButtonStates[13];
            buttonStates[int(Button::Y)] = jsButtonStates[14];
        }
    }

    // Axis (analog sticks) updates:
    {
        ZeroArray(axisStates);

        int jsNumAxes = 0;
        const float * jsAxisStates = glfwGetJoystickAxes(jsId, &jsNumAxes);
        if ((jsAxisStates != nullptr) && (jsNumAxes > 0))
        {
            assert(jsNumAxes >= NumAxes + 2);
            axisStates[0] = jsAxisStates[0];
            axisStates[1] = jsAxisStates[1];
            axisStates[2] = jsAxisStates[2];
            axisStates[3] = jsAxisStates[3];

            // The triggers are actually axes, but we are using them as simple buttons.
            // Any value above 0.1, which should be a nearly half way pressed trigger, is a button down.
            buttonStates[int(Button::LTrigger)] = (jsAxisStates[4] > 0.1f);
            buttonStates[int(Button::RTrigger)] = (jsAxisStates[5] > 0.1f);
        }
    }

    /*
	// This is debug printing only. Causes to much frame spiking if left on.
	for (int b = 0; b < NumButtons; ++b)
	{
		if (buttonStates[b])
		{
			common->PrintF("XB Controller button '%s' (%d) is down!", buttonNames[b], b);
		}
	}
	*/
}

} // namespace Engine {}
