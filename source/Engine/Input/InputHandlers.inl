
// ================================================================================================
// -*- C++ -*-
// File: InputHandlers.inl
// Author: Guilherme R. Lampert
// Created on: 24/08/14
// Brief: Inline methods and definitions for InputHandlers.hpp
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// ======================================================
// KeyboardInputHandler:
// ======================================================

inline bool KeyboardInputHandler::IsKeyUp(const KeyCode key) const
{
    assert(size_t(key) < ArrayLength(keyboard));
    return (keyboard[key].keyState ? false : true);
}

inline bool KeyboardInputHandler::IsKeyDown(const KeyCode key) const
{
    assert(size_t(key) < ArrayLength(keyboard));
    return (keyboard[key].keyState ? true : false);
}

inline void KeyboardInputHandler::GetKeyState(const KeyCode key, bool & isDown, uint32_t & modifiers) const
{
    assert(size_t(key) < ArrayLength(keyboard));
    isDown = (keyboard[key].keyState ? true : false);
    modifiers = static_cast<uint32_t>(keyboard[key].modifiers);
}

// ======================================================
// MouseInputHandler:
// ======================================================

inline bool MouseInputHandler::IsButtonUp(const MouseButton button) const
{
    assert(size_t(button) < ArrayLength(buttonDown));
    return !buttonDown[size_t(button)];
}

inline bool MouseInputHandler::IsButtonDown(const MouseButton button) const
{
    assert(size_t(button) < ArrayLength(buttonDown));
    return buttonDown[size_t(button)];
}

inline Vec2f MouseInputHandler::GetCursorPosition() const
{
    return mousePos;
}

inline Vec2f MouseInputHandler::GetMouseWheelScroll() const
{
    return mouseScroll;
}
