
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: InputHandlers.hpp
// Author: Guilherme R. Lampert
// Created on: 24/08/14
// Brief: Basic input handlers, such as keyboard and mouse.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// KeyboardInputHandler:
// ======================================================

class KeyboardInputHandler
{
  public:

    KeyboardInputHandler();

    // Test for up/down keys.
    bool IsKeyUp(const KeyCode key) const;
    bool IsKeyDown(const KeyCode key) const;

    // Key key down state and modifiers.
    void GetKeyState(const KeyCode key, bool & isDown, uint32_t & modifiers) const;

    // Call this method to feed input events to the keyboard handler.
    // If the event is not handled, this method returns false.
    bool OnAppEvent(const AppEvent event);

  private:

    // Size of the keyboard[] array:
    static constexpr int NumKeyboardKeys = int(Key::LastKey) + 1;

    // Bit field for a key state (0=up, 1=down) plus 7 bits for modifiers.
    struct KeyStatePlusModifiers
    {
        uint8_t keyState : 1;
        uint8_t modifiers : 7;
    };
    static_assert(sizeof(KeyStatePlusModifiers) == 1, "Wrong KeyStatePlusModifiers size!");

    // Array with a slot for every entry in the 'Key' enumeration.
    // Each entry will store the up/down state of the key plus
    // the modifiers that were set when the event was generated.
    KeyStatePlusModifiers keyboard[NumKeyboardKeys];

    // There is no need to copy this around.
    DISABLE_COPY_AND_ASSIGN(KeyboardInputHandler)
};

// ======================================================
// MouseInputHandler:
// ======================================================

// Mouse cursor (0,0) origin is the bottom-left corner of the screen. (OpenGL's default).
class MouseInputHandler
{
  public:

    MouseInputHandler();

    // Test for up/down mouse buttons:
    bool IsButtonUp(const MouseButton button) const;
    bool IsButtonDown(const MouseButton button) const;

    // Mouse cursor position and wheel scroll offsets:
    void SetCursorPosition(const Vec2f pos);
    Vec2f GetCursorPosition() const;
    Vec2f GetMouseWheelScroll() const;

    // Call this method to feed input events to the mouse/pointer handler.
    // If the event is not handled, this method returns false.
    bool OnAppEvent(const AppEvent event);

  private:

    // Current mouse position and wheel scroll offsets.
    // Initially zero.
    Vec2f mousePos;
    Vec2f mouseScroll;

    // Array of mouse button states; false=down, true=up.
    static constexpr int NumMouseButtons = int(MouseButton::LastButton) + 1;
    bool buttonDown[NumMouseButtons];

    // There is no need to copy this around.
    DISABLE_COPY_AND_ASSIGN(MouseInputHandler);
};

// Include the inline definitions in the same namespace:
#include "Engine/Input/InputHandlers.inl"

} // namespace Engine {}
