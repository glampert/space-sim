
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: XB360Controller.hpp
// Author: Guilherme R. Lampert
// Created on: 02/12/14
// Brief: Minimal interface for an XBOX 360 controller.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// XB360Controller:
// ======================================================

class XB360Controller
{
  public:

    enum class Button
    {
        // The four colored buttons:
        A,
        B,
        X,
        Y,

        // Direction arrows:
        ArrowUp,
        ArrowDown,
        ArrowLeft,
        ArrowRight,

        // Left/right buttons:
        LButton,
        RButton,

        // Thumb sticks:
        LThumb,
        RThumb,

        // Triggers:
        LTrigger,
        RTrigger,

        // Interface buttons:
        Start,
        Back,
        XBoxButton,

        // Number of entries in this enumeration. Internal use.
        Count
    };

    enum AxisIds
    {
        // Left-side thumb stick:
        X_Left,
        Y_Left,
        // Right-side thumb stick:
        X_Right,
        Y_Right,
    };

    XB360Controller();

    // Query button states:
    bool IsButtonUp(const Button button) const;
    bool IsButtonDown(const Button button) const;

    // Analog stick axis state. Each axis input state ranges from -1 to +1:
    float GetAxisState(const int axisId) const;
    int GetNumAxes() const { return NumAxes; }

    // Must be called ofter to refresh input states.
    void PollInput();

  private:

    static constexpr int NumButtons = int(Button::Count);
    int buttonStates[NumButtons];

    static constexpr int NumAxes = 4;
    float axisStates[NumAxes];
};

} // namespace Engine {}
