
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: HumanGameView.hpp
// Author: Guilherme R. Lampert
// Created on: 26/08/14
// Brief: GameView implementation for the local human player (could be a robot though :P)
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// HumanGameView:
// ======================================================

class HumanGameView
    : public GameView
{
  public:

    // Constructor / destructor:
    HumanGameView();
    virtual ~HumanGameView() = default;

    // View type:
    Type GetType() const override;

    // View id:
    GameViewId GetViewId() const override;
    void SetViewId(const GameViewId newId) override;

  private:

    // Unique id of this view.
    GameViewId viewId;

    // No need to allow copy for this type.
    DISABLE_COPY_AND_ASSIGN(HumanGameView)
};

} // namespace Engine {}
