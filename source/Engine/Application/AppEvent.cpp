
// ================================================================================================
// -*- C++ -*-
// File: AppEvent.cpp
// Author: Guilherme R. Lampert
// Created on: 24/08/14
// Brief: Application/system events.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Application/AppEvent.hpp"

namespace Engine
{

// ======================================================
// ToString() for AppEvent:
// ======================================================

std::string ToString(const AppEvent & event)
{
    switch (event.type)
    {
    case AppEvent::Type::MouseMove:
        return Format("MouseMove(x=%s, y=%s)",
                      ToString(event.mousePos.x).c_str(),
                      ToString(event.mousePos.y).c_str());

    case AppEvent::Type::MouseScroll:
        return Format("MouseScroll(xOffs=%s, yOffs=%s)",
                      ToString(event.mouseScroll.xOffset).c_str(),
                      ToString(event.mouseScroll.yOffset).c_str());

    case AppEvent::Type::MouseBtnClick:
        return Format("MouseBtnClick(btn=%u, modifiers=0x%X)",
                      static_cast<unsigned int>(event.mouseBtn.buttonId),
                      static_cast<unsigned int>(event.mouseBtn.modifiers));

    case AppEvent::Type::MouseBtnRelease:
        return Format("MouseBtnRelease(btn=%u, modifiers=0x%X)",
                      static_cast<unsigned int>(event.mouseBtn.buttonId),
                      static_cast<unsigned int>(event.mouseBtn.modifiers));

    case AppEvent::Type::KeyPress:
        return Format("KeyPress(keyCode=%u, modifiers=0x%X)",
                      static_cast<unsigned int>(event.keyboard.keyCode),
                      static_cast<unsigned int>(event.keyboard.modifiers));

    case AppEvent::Type::KeyRelease:
        return Format("KeyRelease(keyCode=%u, modifiers=0x%X)",
                      static_cast<unsigned int>(event.keyboard.keyCode),
                      static_cast<unsigned int>(event.keyboard.modifiers));

    case AppEvent::Type::KeyChar:
        return Format("KeyChar(char=%c, modifiers=0x%X)",
                      static_cast<unsigned char>(event.keyboard.keyCode),
                      static_cast<unsigned int>(event.keyboard.modifiers));

    default:
        common->FatalErrorF("Invalid AppEvent::Type!");
    } // switch (event.type)
}

} // namespace Engine {}
