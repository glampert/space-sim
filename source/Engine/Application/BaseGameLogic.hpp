
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: BaseGameLogic.hpp
// Author: Guilherme R. Lampert
// Created on: 26/08/14
// Brief: Common/basic game logic. Can be used by most games.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <vector>

namespace Engine
{

// ======================================================
// BaseGameLogic:
// ======================================================

class BaseGameLogic
    : public GameLogic
{
  public:

    using GameViewPtrArray = std::vector<std::unique_ptr<GameView> >;

    BaseGameLogic();
    virtual ~BaseGameLogic() = default;

    // Implemented from GameLogic, but still overridable:
    virtual void OnUpdate(const GameTime time) override;
    virtual void OnRender(const GameTime time) override;
    virtual bool OnAppEvent(const AppEvent event) override;
    virtual bool ShouldQuit() const override;
    virtual void SendQuitCommand() override;

  protected:

    // Takes exclusive ownership of the pointer.
    void AddGameView(std::unique_ptr<GameView> view);
    void DestroyAllGameViews();

    // Common data:
    GameViewPtrArray gameViews; // Attached game views
    bool shouldQuit;            // Quit flag. Set by SendQuitCommand().

    // No need to allow copy for this type.
    DISABLE_COPY_AND_ASSIGN(BaseGameLogic)
};

} // namespace Engine {}
