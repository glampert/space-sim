
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: GameLogicInterfaces.hpp
// Author: Guilherme R. Lampert
// Created on: 23/08/14
// Brief: Game Application <=> Engine bridge.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// GameLogic interface:
// ======================================================

//
// Glue code between the Engine and a Game.
// This interface is all that the Engine knows about the game application.
//
class GameLogic
{
  public:

    // Frame update tic. Any game logic/scene update goes here, except for rendering calls.
    virtual void OnUpdate(const GameTime time) = 0;

    // Render the game views. Once this method returns, the scene is presented.
    virtual void OnRender(const GameTime time) = 0;

    // Handle an application/system event. Return true of the event was handled, false otherwise.
    virtual bool OnAppEvent(const AppEvent event) = 0;

    // Return true when the application should quit to the host environment.
    virtual bool ShouldQuit() const = 0;

    // Called by the Engine to send a quit command to the game.
    // This happens if the user tries to close the game window, for example.
    // If the game wishes to comply, it must return true on ShouldQuit().
    virtual void SendQuitCommand() = 0;

  protected:

    // Pointers to GameLogic are not to be deleted by the Engine-side.
    ~GameLogic() = default;
};

// ======================================================
// GameView interface:
// ======================================================

// Unique GameView id type.
using GameViewId = uint32_t;

// Not a valid GameViewId.
constexpr GameViewId InvalidGameViewId = 0;

//
// A Game View is an abstraction of the game player(s).
// The Game Logic can have many views. A view can be a local
// player, an AI controlled player, a remote player in the cloud
// or even a playback from a previously recorded game.
//
class GameView
{
  public:

    // Types of Game View:
    enum class Type
    {
        Human,         // Local human player.
        Remote,        // Remote player over the net.
        Recorder,      // Recorder/playback system.
        AI,            // AI controlled player.
        Undefined = -1 // Dummy value. Used internally.
    };

    // Frame update tic. Any game logic/scene update goes here, except for rendering calls.
    virtual void OnUpdate(const GameTime time) = 0;

    // Render this view. Perform rendering call that don't alter game state in here.
    virtual void OnRender(const GameTime time) = 0;

    // Handle an application/system event. Return true of the event was handled, false otherwise.
    virtual bool OnAppEvent(const AppEvent event) = 0;

    // Get view type.
    virtual Type GetType() const = 0;

    // Get the unique view id.
    virtual GameViewId GetViewId() const = 0;

    // Set the unique view id.
    virtual void SetViewId(const GameViewId newId) = 0;

    // No-op virtual destructor:
    virtual ~GameView() = default;
};

} // namespace Engine {}
