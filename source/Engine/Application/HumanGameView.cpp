
// ================================================================================================
// -*- C++ -*-
// File: HumanGameView.cpp
// Author: Guilherme R. Lampert
// Created on: 26/08/14
// Brief: GameView implementation for the local human player (could be a robot though :P)
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine core:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"

// Application interfaces:
#include "Engine/Application/AppEvent.hpp"
#include "Engine/Application/GameLogicInterfaces.hpp"
#include "Engine/Application/HumanGameView.hpp"

namespace Engine
{

// ======================================================
// HumanGameView:
// ======================================================

HumanGameView::HumanGameView()
    : viewId(InvalidGameViewId)
{
}

GameView::Type HumanGameView::GetType() const
{
    return GameView::Type::Human;
}

GameViewId HumanGameView::GetViewId() const
{
    return viewId;
}

void HumanGameView::SetViewId(const GameViewId newId)
{
    viewId = newId;
}

} // namespace Engine {}
