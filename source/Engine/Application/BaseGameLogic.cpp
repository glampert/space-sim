
// ================================================================================================
// -*- C++ -*-
// File: BaseGameLogic.cpp
// Author: Guilherme R. Lampert
// Created on: 26/08/14
// Brief: Common/basic game logic. Can be used by most games.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine core:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"

// Application interfaces:
#include "Engine/Application/AppEvent.hpp"
#include "Engine/Application/GameLogicInterfaces.hpp"
#include "Engine/Application/BaseGameLogic.hpp"

namespace Engine
{

// ======================================================
// BaseGameLogic:
// ======================================================

BaseGameLogic::BaseGameLogic()
    : shouldQuit(false)
{
}

void BaseGameLogic::OnUpdate(const GameTime time)
{
    // Update all views:
    for (auto & it : gameViews)
    {
        it->OnUpdate(time);
    }
}

void BaseGameLogic::OnRender(const GameTime time)
{
    // Render all views:
    for (auto & it : gameViews)
    {
        it->OnRender(time);
    }
}

bool BaseGameLogic::OnAppEvent(const AppEvent event)
{
    // Forward event to the views.
    // The first view to handle the event consumes it.
    for (auto & it : gameViews)
    {
        if (it->OnAppEvent(event))
        {
            return true;
        }
    }

    // Event not handled.
    return false;
}

bool BaseGameLogic::ShouldQuit() const
{
    return shouldQuit;
}

void BaseGameLogic::SendQuitCommand()
{
    shouldQuit = true;
}

void BaseGameLogic::AddGameView(std::unique_ptr<GameView> view)
{
    assert(view != nullptr);
    gameViews.emplace_back(std::move(view));
}

void BaseGameLogic::DestroyAllGameViews()
{
    // Unique pointers will automatically delete the objects.
    // We just need to clear the vector.
    gameViews.clear();
}

} // namespace Engine {}
