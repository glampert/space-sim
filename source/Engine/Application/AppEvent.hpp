
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: AppEvent.hpp
// Author: Guilherme R. Lampert
// Created on: 24/08/14
// Brief: Application/system events.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// AppEvent:
// ======================================================

struct AppEvent
{
    // Pointer type used by events.
    using Ptr = void *;

    // Integer type for event values.
    // Must be the size of a pointer I.e.: sizeof(AppEvent::Int) == sizeof(AppEvent::Ptr)
    using Int = intptr_t;

    // Floating point type for decimal event values.
    // Must be the size of a pointer I.e.: sizeof(AppEvent::Float) == sizeof(AppEvent::Ptr)
    using Float = double;

    // AppEvent type tag:
    enum class Type : int
    {
        // Null event. All event values are meaningless.
        Null = 0,

        // Mouse events:
        MouseMove,
        MouseScroll,
        MouseBtnClick,
        MouseBtnRelease,

        // Keyboard events:
        KeyPress,
        KeyRelease,

        // Char code of a key press.
        // First byte of 'keyboard.keyCode' will have the char.
        KeyChar
    };

    // Event data must be 2 pointers wide:
    union
    {
        Ptr data[2];
        struct
        {
            Float x;
            Float y;
        } mousePos;
        struct
        {
            Int buttonId;
            Int modifiers;
        } mouseBtn;
        struct
        {
            Float xOffset;
            Float yOffset;
        } mouseScroll;
        struct
        {
            Int keyCode;
            Int modifiers;
        } keyboard;
    };

    // Event type identifier/tag:
    Type type;
};

// Make a printable string from an AppEvent's value. Used for debugging.
std::string ToString(const AppEvent & event);

// ======================================================
// Invariants:
// ======================================================

// We mix AppEvent::Ptr and AppEvent::Int in the unions of AppEvent. They must have the same size in bytes.
static_assert(sizeof(AppEvent::Int) == sizeof(AppEvent::Ptr), "AppEvent::Int must have the size of a pointer!");

// AppEvent::Float must also match the size of a pointer so it doesn't break the alignment.
static_assert(sizeof(AppEvent::Float) == sizeof(AppEvent::Ptr), "AppEvent::Float must have the size of a pointer! Try float/double.");

// We force the enum to be the size of a pointer.
static_assert(sizeof(AppEvent::Type) == sizeof(int), "AppEvent::Type constants must be the size of int!");

} // namespace Engine {}
