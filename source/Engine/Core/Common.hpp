
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Common.hpp
// Author: Guilherme R. Lampert
// Created on: 06/08/14
// Brief: Common Engine utilities.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Helper types / structures / forward decls:
// ======================================================

// Forward declarations:
class GameLogic;

// Game tic/time information,
// available in both seconds and milliseconds scale.
struct GameTime
{
    // Absolute time since the application started.
    struct
    {
        int32_t seconds;
        int64_t milliseconds;
    } currentTime;

    // Time elapsed since last frame/update.
    struct
    {
        double seconds;
        double milliseconds;
    } deltaTime;
};

// ======================================================
// Common singleton:
// ======================================================

class Common final
    : public SingleInstanceType<Common>
{
  public:

    // Not directly used.
    // 'common' global is provided instead.
    Common();

    // Initialization and shutdown. Called internally by the Engine.
    void Init();
    void Shutdown();

    // ---- Miscellaneous/application: ----

    // Set the GameLogic instance. It must remain valid until ShutdownEngine() is called.
    // Common does not take ownership of the pointer. Must not be null.
    void SetGameLogicInstance(GameLogic * gameLogic);

    // Access the current GameLogic instance. Will assert if the instance is null.
    GameLogic * GetGameLogicInstance() { return game; }

    // Get an opaque pointer to the underlaying Window management object.
    void * GetWindowObject() { return windowObj; }

    // Init application window, rendering context and whatnot.
    // Called by InitEngine() after running config scripts and command line.
    void CreateAppContext();

    // Runs the application main loop until the user quits.
    void MainLoop();

    // Execute/parse the command line. This is called after config scripts are run.
    void ExecCmdLine(const int argc, const char * argv[]);

    // Execute default configuration scripts. This must happen BEFORE ExecCmdLine().
    void RunEngineConfigScripts();

    // ---- System cursor/pointer/mouse: ----

    // Takes ownership of the system cursor/pointer, hiding it.
    void GrabSystemCursor();

    // Makes the system cursor visible again.
    void RestoreSystemCursor();

    // Sets the position of the system cursor, relative to the client area of the window.
    void WarpSystemCursor(const Vec2f pos);

    // ---- Logging/debugging: ----

    // Print comments to the Engine log:
    void PrintF(const char * fmt, ...) ATTRIBUTE_FORMAT_FUNC(printf, 2, 3);

    // Print warnings to the Engine log:
    void WarningF(const char * fmt, ...) ATTRIBUTE_FORMAT_FUNC(printf, 2, 3);

    // Print errors to the Engine log:
    void ErrorF(const char * fmt, ...) ATTRIBUTE_FORMAT_FUNC(printf, 2, 3);

    // Prints a fatal error to the Engine log and throws an exception of type Engine::FatalErrorException.
    NO_RETURN_FUNC(void FatalErrorF(const char * fmt, ...)) ATTRIBUTE_FORMAT_FUNC(printf, 2, 3);

  private:

    // Log/misc initialization and shutdown:
    void InitLog();
    void ShutdownLog();
    void ShutdownApp();

    // Initialize GLEW and GLFWv3:
    void InitGLFW();
    void InitGLEW();

    // Frame updates:
    void RenderFrame();
    void UpdateGame();

    // Instance to the game logic object. Not owned by Common.
    GameLogic * game;

    // Log output. Can be stdout, stderr or a file.
    FILE * logStream;

    // Handle to the GLFWv3 application window.
    // As a void* so we don't have to make the GLFW API public.
    void * windowObj; // GLFWwindow*

    // Current game time. Updated every main loop iteration.
    GameTime time;

    DISABLE_COPY_AND_ASSIGN(Common)
};

// Globally unique instance:
extern Common * common;

} // namespace Engine {}
