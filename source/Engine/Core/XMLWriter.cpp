
// ================================================================================================
// -*- C++ -*-
// File: XMLWriter.cpp
// Author: Guilherme R. Lampert
// Created on: 26/04/13
// Brief: Simple XML stream writer.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine common:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/Containers/DestructionPolicies.hpp"
#include "Engine/Core/Containers/IntrusiveSList.hpp"
#include "Engine/Core/Containers/FixedArray.hpp"

// XML library:
#include "Engine/Core/XML.hpp"
#include "Engine/Core/XMLReader.hpp"
#include "Engine/Core/XMLWriter.hpp"

namespace Engine
{

// ======================================================
// XMLWriter:
// ======================================================

XMLWriter::XMLWriter(PrinterFunction outputFunc, void * userData, const char * encoding)
    : outputCallback(outputFunc)
    , ud(userData)
{
    assert(outputFunc != nullptr);

    // Print header with version and optional encoding:
    if (encoding == nullptr)
    {
        OutputXML("<?xml version=\"1.0\"?>\n"); // Always v1.0
    }
    else
    {
        // Always v1.0
        OutputXML(Format("<?xml version=\"1.0\" encoding=\"%s\"?>\n", encoding).c_str());
    }
}

void XMLWriter::WriteSubtree(const XMLElement & subtreeRoot)
{
    std::string indent; // Write subtree, starting at indent level 0
    WriteSubtreeRecursive(subtreeRoot, indent);
}

void XMLWriter::WriteComment(const char * comment, const bool multiline)
{
    assert(comment != nullptr);

    if (!multiline) // <!-- comment -->
    {
        OutputXML("<!-- ");
        OutputXML(comment);
        OutputXML(" -->\n");
    }
    else
    {
        // <!--
        // comment
        // -->
        OutputXML("<!--\n");
        OutputXML(comment);
        OutputXML("\n-->\n");
    }
}

void XMLWriter::WriteCDATA(const char * CDATA_contents, const bool multiline)
{
    assert(CDATA_contents != nullptr);

    if (!multiline) // <![CDATA[ CDATA_contents ]]>
    {
        OutputXML("<![CDATA[ ");
        OutputXML(CDATA_contents);
        OutputXML(" ]]>\n");
    }
    else
    {
        // <![CDATA[
        // CDATA_contents
        // ]]>
        OutputXML("<![CDATA[\n");
        OutputXML(CDATA_contents);
        OutputXML("\n]]>\n");
    }
}

void XMLWriter::WriteSubtreeRecursive(const XMLElement & subtreeRoot, std::string & indent)
{
    // Open tag:
    OutputXML("<");
    OutputXML(subtreeRoot.name.c_str());

    // Output attributes of this element/tag:
    WriteAttributes(subtreeRoot);

    if (!subtreeRoot.HasChildren() && !subtreeRoot.HasText() && !subtreeRoot.HasCDATA())
    {
        // No child elements, no text nor CDATA, close tag on the same line.
        OutputXML(" />\n");
        return;
    }

    // Close non-empty tag:
    OutputXML(">\n");

    if (subtreeRoot.HasText())
    {
        // Output text body:
        indent.push_back('\t');
        OutputXML(indent.c_str());
        WriteTextBody(subtreeRoot.Text());
        OutputXML("\n");
        indent.pop_back();
    }

    if (subtreeRoot.HasCDATA())
    {
        // Output CDATA section:
        indent.push_back('\t');
        OutputXML(indent.c_str());
        WriteCDATA(subtreeRoot.CDATA());
        indent.pop_back();
    }

    if (subtreeRoot.HasChildren())
    {
        // Output all child elements:
        XMLElement::ConstChildIterator childIterator = subtreeRoot.FirstChild();
        XMLElement::ConstChildIterator childListEnd = subtreeRoot.ChildrenListEnd();

        indent.push_back('\t');
        while (childIterator != childListEnd)
        {
            OutputXML(indent.c_str());
            const XMLElement * const pChildElem = (*childIterator);
            WriteSubtreeRecursive(*pChildElem, indent);
            ++childIterator;
        }
        indent.pop_back();
    }

    // Close tag:
    OutputXML(indent.c_str());
    OutputXML("</");
    OutputXML(subtreeRoot.name.c_str());
    OutputXML(">\n");
}

void XMLWriter::WriteAttributes(const XMLElement & elem)
{
    if (!elem.HasAttributes())
    {
        return; // Nothing to do here.
    }

    std::string attributeStr;
    XMLElement::ConstAttrIterator attrIterator = elem.FirstAttribute();
    XMLElement::ConstAttrIterator attrListEnd = elem.AttributeListEnd();
    while (attrIterator != attrListEnd)
    {
        attributeStr = Format(" %s=\"%s\"", (*attrIterator)->name.c_str(), (*attrIterator)->value.c_str());
        OutputXML(attributeStr.c_str());
        ++attrIterator;
    }
}

void XMLWriter::WriteTextBody(const char * text)
{
    assert(text != nullptr);
    if (text[0] == '\0')
    {
        return; // Empty text!
    }

    // Write text body fixing all entity references:
    FixedArray<char, MaxTempStringLen + 1> tempBuf;
    size_t i = 0, j = 0;
    for (;;)
    {
        // Possible entity reference:
        if (const XMLEntityReference * entRef = IsXMLEntityReference(text[i]))
        {
            // It is an Entity Ref, replace char with string
            const size_t entNameLen = std::strlen(entRef->ansiName);
            if ((j + entNameLen + 1) >= MaxTempStringLen)
            {
                tempBuf[j] = '\0';
                OutputXML(tempBuf.data());
                j = 0;
            }

            for (size_t k = 0; k < entNameLen; ++k)
            {
                tempBuf[j++] = entRef->ansiName[k];
            }

            ++i;
        }
        else
        {
            tempBuf[j++] = text[i++];
        }

        if (text[i] == '\0')
        {
            tempBuf[j] = '\0';
            OutputXML(tempBuf.data());
            break;
        }

        if (j == MaxTempStringLen)
        {
            tempBuf[j] = '\0';
            OutputXML(tempBuf.data());
            j = 0;
        }
    }
}

void XMLWriter::OutputXML(const char * text)
{
    if (*text != '\0')
    {
        outputCallback(ud, text);
    }
}

} // namespace Engine {}
