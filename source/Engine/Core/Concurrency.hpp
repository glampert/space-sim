
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Concurrency.hpp
// Author: Guilherme R. Lampert
// Created on: 07/09/14
// Brief: Threading and concurrency helpers.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <type_traits>
#include <functional>
#include <utility>
#include <future>
#include <thread>
#include <atomic>
#include <mutex>

// OSX specific:
#include <dispatch/dispatch.h>

namespace Engine
{

// ======================================================
// ThreadRAII:
// ======================================================

//
// std::thread RAII wrapper.
//
// The motivation for this wrapper to std::thread
// it to provide RAII-style finalization for threads.
// If the destructor of a standard thread is called while
// the thread is still running, the program will terminate.
// In other words, you need to keep the thread object alive
// until the thread terminates or call join()/detach()
// before disposing the std::thread object.
// This thin wrapper ensure either join or detach are called
// on destruction by explicitly storing a pointer to one
// of the member functions. Example:
//
// ThreadRAII t1(std::thread(doSomething), &std::thread::join);
//   - will join when t1 is destroyed.
//
// ThreadRAII t2(std::thread(doSomething), &std::thread::detach);
//   - will detach when t2 is destroyed.
//
class ThreadRAII final
{
  public:

    // Pointer to std::thread member.
    // Must be either join() or detach().
    using RAIIAction = void (std::thread::*)();

    ThreadRAII(std::thread && thread, RAIIAction action) noexcept
        : thd(std::move(thread)), act(action)
    { }

    ~ThreadRAII()
    {
        // Join or detach
        if (thd.joinable())
        {
            (thd.*act)();
        }
    }

    std::thread & GetStdThread() noexcept { return thd; }

  private:

    std::thread thd;
    RAIIAction  act;
};

// ======================================================
// AsyncTask() and AsyncPriority:
// ======================================================

//
// Motivation for AsyncTask():
// AsyncTask is compatible and interchangeable with std::async().
// It can be more efficient though. std::async, on MacOS, iOS and
// other Unix system, is currently implemented in terms of std::thread,
// meaning that each std::async call can potentially spawn a new thread.
// Creating a thread can have a significant overhead, therefore,
// AsyncTask implements equivalent functionality using LibDispatch
// for MacOS and iOS. LibDispatch uses a more efficient thread-pool
// task model, giving some performance advantage over std::async.
// On Windows, std::async should already be implemented using Windows Thread Pools,
// so in such case, AsyncTask can be replace with std::async.
//
// This implementation is largely based on the one presented
// by Sean Parent on the GoingNative 2013 event.
// https://github.com/sean-parent/sean-parent.github.io/wiki/Papers-and-Presentations
//

//
// AsyncTask priorities.
//
enum class AsyncPriority : long
{
    High       = DISPATCH_QUEUE_PRIORITY_HIGH,
    Default    = DISPATCH_QUEUE_PRIORITY_DEFAULT,
    Low        = DISPATCH_QUEUE_PRIORITY_LOW,
    Background = DISPATCH_QUEUE_PRIORITY_BACKGROUND
};

//
// AsyncTask() for a function with no arguments
// and a user specified dispatch priority.
//
template <typename F>
auto AsyncTask(const AsyncPriority priority, F && f) -> std::future<typename std::result_of<F()>::type>
{
    using ResultType = typename std::result_of<F()>::type;
    using PackagedType = std::packaged_task<ResultType()>;

    // Will have to allocate the packaged_task dynamically, but this
    // should still be more efficient that creating an OS thread.
    auto task = new PackagedType(std::forward<F>(f));
    auto result = task->get_future();

    dispatch_async_f(
    dispatch_get_global_queue(static_cast<long>(priority), 0),
        task, [](void * param)
        {
            PackagedType * fn = static_cast<PackagedType *>(param);
            (*fn)();
            delete fn;
        });

    return result;
}

//
// AsyncTask() for a function with an argument list
// and a user specified dispatch priority.
//
template <typename F, typename... Args>
auto AsyncTask(const AsyncPriority priority, F && f, Args &&... args) -> std::future<typename std::result_of<F(Args...)>::type>
{
    return AsyncTask(priority, std::bind(std::forward<F>(f), std::forward<Args>(args)...));
}

//
// AsyncTask() for a function with no arguments
// and default dispatch priority.
//
template <typename F>
auto AsyncTask(F && f) -> std::future<typename std::result_of<F()>::type>
{
    return AsyncTask(AsyncPriority::Default, std::forward<F>(f));
}

//
// AsyncTask() for a function with an argument list
// and default dispatch priority.
//
template <typename F, typename... Args>
auto AsyncTask(F && f, Args &&... args) -> std::future<typename std::result_of<F(Args...)>::type>
{
    return AsyncTask(AsyncPriority::Default, std::bind(std::forward<F>(f), std::forward<Args>(args)...));
}

} // namespace Engine {}
