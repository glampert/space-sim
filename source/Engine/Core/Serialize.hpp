
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Serialize.hpp
// Author: Guilherme R. Lampert
// Created on: 17/08/14
// Brief: Basic property/data serialization.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// Forward declarations:
class Serializable;
class SerializerStream;
class ReadSerializer;
class WriteSerializer;

// ======================================================
// Serializable interface:
// ======================================================

//
// Objects that are persistent or need network serialization
// should implement this interface.
//
class Serializable
{
  public:

    // Saves to object to the writable stream.
    virtual bool Serialize(WriteSerializer & serializer) const = 0;

    // Initializes the object from the readable stream.
    virtual bool DeSerialize(ReadSerializer & serializer) = 0;

  protected:

    // "A base class destructor should be either public and virtual, or protected and non-virtual".
    // Serializable is never meant to exist by itself or have its pointers stored or deleted.
    // It is only provided from the semantics of Serialize() and DeSerialize(),
    // therefore, we can leave the destructor protected and non-virtual.
    ~Serializable() = default;
};

// ======================================================
// SerializerStream:
// ======================================================

// Base type for ReadSerializer and WriteSerializer.
class SerializerStream
{
  public:

    class Error : public Exception
    {
      public:
        Error(const char * error)
            : Exception(error) { }

        Error(const std::string & error)
            : Exception(error.c_str()) { }

        const char * ExceptionName() const noexcept
        {
            return "Engine::SerializerStream::Error";
        }
    };

    // Report error, possibly throwing a C++ exception, if the throwExceptions flag is set.
    bool ErrorF(const char * fmt, ...) const ATTRIBUTE_FORMAT_FUNC(printf, 2, 3);

    // Report a warning and continue executing normally.
    void WarningF(const char * fmt, ...) const ATTRIBUTE_FORMAT_FUNC(printf, 2, 3);

    // Total number of errors generated.
    int GetErrorCount() const;

    // Total number of warnings generated.
    int GetWarningCount() const;

    // If the serializer supports the concept of blocks, this method is used to begin a new one.
    virtual bool BeginBlock(const char * label) = 0;

    // End a block previously opened by BeginBlock(). No-op if the serializer doesn't support the concept of blocks.
    virtual bool EndBlock(const char * label) = 0;

    // Test if the last block operated on generated errors.
    virtual bool BlockHadErrors() const = 0;

  protected:

    SerializerStream(const bool disallowErrors, const bool disallowWarnings, const bool throwExceptionOnError);
    ~SerializerStream() = default;

  private:

    // Error/warning counters (only incremented if enabled):
    mutable int errorCount;
    mutable int warningCount;

    // Disables all error/warning messages.
    const bool noErrors;
    const bool noWarnings;

    // Make a call to ErrorF() throw an exception. A non-fatal error otherwise.
    const bool throwExceptions;
};

// ======================================================
// ReadSerializer:
// ======================================================

// Provides reading methods for the basic Engine types.
class ReadSerializer
    : public SerializerStream
{
  public:

    // char/wchar_t/bool:
    virtual bool ReadChar(char & value, const char * label) = 0;
    virtual bool ReadBool(bool & value, const char * label) = 0;
    virtual bool ReadWideChar(wchar_t & value, const char * label) = 0;

    // 8-bits integers:
    virtual bool ReadInt8(int8_t & value, const char * label) = 0;
    virtual bool ReadUInt8(uint8_t & value, const char * label) = 0;

    // 16-bit integers:
    virtual bool ReadInt16(int16_t & value, const char * label) = 0;
    virtual bool ReadUInt16(uint16_t & value, const char * label) = 0;

    // 32-bit integers:
    virtual bool ReadInt32(int32_t & value, const char * label) = 0;
    virtual bool ReadUInt32(uint32_t & value, const char * label) = 0;

    // 64-bit integers:
    virtual bool ReadInt64(int64_t & value, const char * label) = 0;
    virtual bool ReadUInt64(uint64_t & value, const char * label) = 0;

    // Float/double:
    virtual bool ReadFloat(float & value, const char * label) = 0;
    virtual bool ReadDouble(double & value, const char * label) = 0;

    // Strings:
    virtual bool ReadString(std::string & str, const char * label) = 0;
    virtual bool ReadString(char * str, const size_t maxChars, const char * label) = 0;

    // VectorMath types:
    virtual bool ReadQuat(Quat & q, const char * label) = 0;
    virtual bool ReadPoint3(Point3 & p, const char * label) = 0;
    virtual bool ReadVector3(Vector3 & v, const char * label) = 0;
    virtual bool ReadVector4(Vector4 & v, const char * label) = 0;
    virtual bool ReadMatrix4(Matrix4 & m, const char * label) = 0;
    virtual bool ReadMatrix3(Matrix3 & m, const char * label) = 0;

    // Aux vector types:
    virtual bool ReadVec2(Vec2f & v, const char * label) = 0;
    virtual bool ReadVec3(Vec3f & v, const char * label) = 0;
    virtual bool ReadVec4(Vec4f & v, const char * label) = 0;
    virtual bool ReadVec2(Vec2i & v, const char * label) = 0;
    virtual bool ReadVec3(Vec3i & v, const char * label) = 0;
    virtual bool ReadVec4(Vec4i & v, const char * label) = 0;
    virtual bool ReadVec2(Vec2u & v, const char * label) = 0;
    virtual bool ReadVec3(Vec3u & v, const char * label) = 0;
    virtual bool ReadVec4(Vec4u & v, const char * label) = 0;

    // Color[2/3/4]b:
    virtual bool ReadColor(Color2b & c, const char * label) = 0;
    virtual bool ReadColor(Color3b & c, const char * label) = 0;
    virtual bool ReadColor(Color4b & c, const char * label) = 0;

    // Read raw bytes. Returns number of bytes successfully read.
    virtual size_t ReadBytes(void * destBuffer, const size_t numBytesToRead) = 0;

    // No-op.
    virtual ~ReadSerializer() = default;

    // Forward to SerializerStream:
    ReadSerializer(const bool disallowErrors, const bool disallowWarnings, const bool throwExceptionOnError)
        : SerializerStream(disallowErrors, disallowWarnings, throwExceptionOnError) { }
};

// ======================================================
// WriteSerializer:
// ======================================================

// Provides writing methods for the basic Engine types.
class WriteSerializer
    : public SerializerStream
{
  public:

    // char/wchar_t/bool:
    virtual bool WriteChar(const char value, const char * label) = 0;
    virtual bool WriteBool(const bool value, const char * label) = 0;
    virtual bool WriteWideChar(const wchar_t value, const char * label) = 0;

    // 8-bits integers:
    virtual bool WriteInt8(const int8_t value, const char * label) = 0;
    virtual bool WriteUInt8(const uint8_t value, const char * label) = 0;

    // 16-bit integers:
    virtual bool WriteInt16(const int16_t value, const char * label) = 0;
    virtual bool WriteUInt16(const uint16_t value, const char * label) = 0;

    // 32-bit integers:
    virtual bool WriteInt32(const int32_t value, const char * label) = 0;
    virtual bool WriteUInt32(const uint32_t value, const char * label) = 0;

    // 64-bit integers:
    virtual bool WriteInt64(const int64_t value, const char * label) = 0;
    virtual bool WriteUInt64(const uint64_t value, const char * label) = 0;

    // Float/double:
    virtual bool WriteFloat(const float value, const char * label) = 0;
    virtual bool WriteDouble(const double value, const char * label) = 0;

    // Strings:
    virtual bool WriteString(const std::string & str, const char * label) = 0;
    virtual bool WriteString(const char * str, const char * label) = 0;

    // VectorMath types:
    virtual bool WriteQuat(const Quat & q, const char * label) = 0;
    virtual bool WritePoint3(const Point3 & p, const char * label) = 0;
    virtual bool WriteVector3(const Vector3 & v, const char * label) = 0;
    virtual bool WriteVector4(const Vector4 & v, const char * label) = 0;
    virtual bool WriteMatrix4(const Matrix4 & m, const char * label) = 0;
    virtual bool WriteMatrix3(const Matrix3 & m, const char * label) = 0;

    // Aux vector types:
    virtual bool WriteVec2(const Vec2f & v, const char * label) = 0;
    virtual bool WriteVec3(const Vec3f & v, const char * label) = 0;
    virtual bool WriteVec4(const Vec4f & v, const char * label) = 0;
    virtual bool WriteVec2(const Vec2i & v, const char * label) = 0;
    virtual bool WriteVec3(const Vec3i & v, const char * label) = 0;
    virtual bool WriteVec4(const Vec4i & v, const char * label) = 0;
    virtual bool WriteVec2(const Vec2u & v, const char * label) = 0;
    virtual bool WriteVec3(const Vec3u & v, const char * label) = 0;
    virtual bool WriteVec4(const Vec4u & v, const char * label) = 0;

    // Color[2/3/4]b:
    virtual bool WriteColor(const Color2b & c, const char * label) = 0;
    virtual bool WriteColor(const Color3b & c, const char * label) = 0;
    virtual bool WriteColor(const Color4b & c, const char * label) = 0;

    // Write raw bytes. Returns the number of bytes successfully written.
    virtual size_t WriteBytes(const void * srcBuffer, const size_t numBytesToWrite) = 0;

    // No-op.
    virtual ~WriteSerializer() = default;

    // Forward to SerializerStream:
    WriteSerializer(const bool disallowErrors, const bool disallowWarnings, const bool throwExceptionOnError)
        : SerializerStream(disallowErrors, disallowWarnings, throwExceptionOnError) { }
};

// ======================================================
// Helper macros:
// ======================================================

//
// Shorthand for SerializerStream BeginBlock()/EndBlock().
//
#define SER_BEGIN(serializer, blockName) (serializer).BeginBlock(#blockName)
#define SER_END(serializer, blockName) (serializer).EndBlock(#blockName)

//
// Writes a named property to the serializer stream.
//
#define SER_WRITE(serializer, property) SerializerWrite((serializer), (property), #property)
#define SER_WRITE_NAMED(serializer, property, label) SerializerWrite((serializer), (property), (label))

//
// Reads a named property from the serializer stream.
// If the read operation fails or the named property
// cannot be found, the provided variable remains unchanged.
//
#define SER_READ(serializer, property) SerializerRead((serializer), (property), #property)
#define SER_READ_NAMED(serializer, property, label) SerializerRead((serializer), (property), (label))

//
// Tries to read a named property from the serializer stream.
// If the property is not found or cannot be read, the provided
// default value is assigned to it.
//
#define SER_READ_OPT(serializer, property, defaultValue)      \
    if (!SerializerRead((serializer), (property), #property)) \
    {                                                         \
        (property) = (defaultValue);                          \
    }

//
// Same as SER_READ_OPT() but takes a user defined label for the property.
//
#define SER_READ_OPT_NAMED(serializer, property, defaultValue, label) \
    if (!SerializerRead((serializer), (property), (label)))           \
    {                                                                 \
        (property) = (defaultValue);                                  \
    }

//
// For consistency, we declared a global inline SerializerRead()/SerializerWrite()
// for all the native serializable types.
//
// The user is free to extend the serializer to any type he/she wishes by defining
// a new Serializer[Read/Write] for the given custom type, based on the provided primitives.
//

// ======================================================
// Reading:
// ======================================================

// Numerical types:
inline bool SerializerRead(ReadSerializer & s, char & value, const char * label)     { return s.ReadChar(value, label);     }
inline bool SerializerRead(ReadSerializer & s, bool & value, const char * label)     { return s.ReadBool(value, label);     }
inline bool SerializerRead(ReadSerializer & s, wchar_t & value, const char * label)  { return s.ReadWideChar(value, label); }
inline bool SerializerRead(ReadSerializer & s, int8_t & value, const char * label)   { return s.ReadInt8(value, label);     }
inline bool SerializerRead(ReadSerializer & s, uint8_t & value, const char * label)  { return s.ReadUInt8(value, label);    }
inline bool SerializerRead(ReadSerializer & s, int16_t & value, const char * label)  { return s.ReadInt16(value, label);    }
inline bool SerializerRead(ReadSerializer & s, uint16_t & value, const char * label) { return s.ReadUInt16(value, label);   }
inline bool SerializerRead(ReadSerializer & s, int32_t & value, const char * label)  { return s.ReadInt32(value, label);    }
inline bool SerializerRead(ReadSerializer & s, uint32_t & value, const char * label) { return s.ReadUInt32(value, label);   }
inline bool SerializerRead(ReadSerializer & s, int64_t & value, const char * label)  { return s.ReadInt64(value, label);    }
inline bool SerializerRead(ReadSerializer & s, uint64_t & value, const char * label) { return s.ReadUInt64(value, label);   }
inline bool SerializerRead(ReadSerializer & s, float & value, const char * label)    { return s.ReadFloat(value, label);    }
inline bool SerializerRead(ReadSerializer & s, double & value, const char * label)   { return s.ReadDouble(value, label);   }

// VectorMath types:
inline bool SerializerRead(ReadSerializer & s, Quat & value, const char * label)    { return s.ReadQuat(value, label);    }
inline bool SerializerRead(ReadSerializer & s, Point3 & value, const char * label)  { return s.ReadPoint3(value, label);  }
inline bool SerializerRead(ReadSerializer & s, Vector3 & value, const char * label) { return s.ReadVector3(value, label); }
inline bool SerializerRead(ReadSerializer & s, Vector4 & value, const char * label) { return s.ReadVector4(value, label); }
inline bool SerializerRead(ReadSerializer & s, Matrix3 & value, const char * label) { return s.ReadMatrix3(value, label); }
inline bool SerializerRead(ReadSerializer & s, Matrix4 & value, const char * label) { return s.ReadMatrix4(value, label); }

// Aux vector types:
inline bool SerializerRead(ReadSerializer & s, Vec2f & value, const char * label) { return s.ReadVec2(value, label); }
inline bool SerializerRead(ReadSerializer & s, Vec3f & value, const char * label) { return s.ReadVec3(value, label); }
inline bool SerializerRead(ReadSerializer & s, Vec4f & value, const char * label) { return s.ReadVec4(value, label); }
inline bool SerializerRead(ReadSerializer & s, Vec2i & value, const char * label) { return s.ReadVec2(value, label); }
inline bool SerializerRead(ReadSerializer & s, Vec3i & value, const char * label) { return s.ReadVec3(value, label); }
inline bool SerializerRead(ReadSerializer & s, Vec4i & value, const char * label) { return s.ReadVec4(value, label); }
inline bool SerializerRead(ReadSerializer & s, Vec2u & value, const char * label) { return s.ReadVec2(value, label); }
inline bool SerializerRead(ReadSerializer & s, Vec3u & value, const char * label) { return s.ReadVec3(value, label); }
inline bool SerializerRead(ReadSerializer & s, Vec4u & value, const char * label) { return s.ReadVec4(value, label); }

// Color[2/3/4]b:
inline bool SerializerRead(ReadSerializer & s, Color2b & value, const char * label) { return s.ReadColor(value, label); }
inline bool SerializerRead(ReadSerializer & s, Color3b & value, const char * label) { return s.ReadColor(value, label); }
inline bool SerializerRead(ReadSerializer & s, Color4b & value, const char * label) { return s.ReadColor(value, label); }

// Strings:
inline bool SerializerRead(ReadSerializer & s, std::string & str, const char * label)                 { return s.ReadString(str, label); }
inline bool SerializerRead(ReadSerializer & s, char * str, const size_t maxChars, const char * label) { return s.ReadString(str, maxChars, label); }

// STL containers (any compatible with range based iteration):
template <class STLContainer>
inline bool SerializerRead(ReadSerializer & s, STLContainer & container, const char * label)
{
    bool hadError = false;
    for (auto & it : container)
    {
        if (!SerializerRead(s, it, label))
        {
            hadError = true;
        }
    }
    return hadError;
}

// ======================================================
// Writing:
// ======================================================

// Numerical types:
inline bool SerializerWrite(WriteSerializer & s, const char value, const char * label)     { return s.WriteChar(value, label);     }
inline bool SerializerWrite(WriteSerializer & s, const bool value, const char * label)     { return s.WriteBool(value, label);     }
inline bool SerializerWrite(WriteSerializer & s, const wchar_t value, const char * label)  { return s.WriteWideChar(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const int8_t value, const char * label)   { return s.WriteInt8(value, label);     }
inline bool SerializerWrite(WriteSerializer & s, const uint8_t value, const char * label)  { return s.WriteUInt8(value, label);    }
inline bool SerializerWrite(WriteSerializer & s, const int16_t value, const char * label)  { return s.WriteInt16(value, label);    }
inline bool SerializerWrite(WriteSerializer & s, const uint16_t value, const char * label) { return s.WriteUInt16(value, label);   }
inline bool SerializerWrite(WriteSerializer & s, const int32_t value, const char * label)  { return s.WriteInt32(value, label);    }
inline bool SerializerWrite(WriteSerializer & s, const uint32_t value, const char * label) { return s.WriteUInt32(value, label);   }
inline bool SerializerWrite(WriteSerializer & s, const int64_t value, const char * label)  { return s.WriteInt64(value, label);    }
inline bool SerializerWrite(WriteSerializer & s, const uint64_t value, const char * label) { return s.WriteUInt64(value, label);   }
inline bool SerializerWrite(WriteSerializer & s, const float value, const char * label)    { return s.WriteFloat(value, label);    }
inline bool SerializerWrite(WriteSerializer & s, const double value, const char * label)   { return s.WriteDouble(value, label);   }

// VectorMath types:
inline bool SerializerWrite(WriteSerializer & s, const Quat & value, const char * label)    { return s.WriteQuat(value, label);    }
inline bool SerializerWrite(WriteSerializer & s, const Point3 & value, const char * label)  { return s.WritePoint3(value, label);  }
inline bool SerializerWrite(WriteSerializer & s, const Vector3 & value, const char * label) { return s.WriteVector3(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Vector4 & value, const char * label) { return s.WriteVector4(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Matrix3 & value, const char * label) { return s.WriteMatrix3(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Matrix4 & value, const char * label) { return s.WriteMatrix4(value, label); }

// Aux vector types:
inline bool SerializerWrite(WriteSerializer & s, const Vec2f & value, const char * label) { return s.WriteVec2(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Vec3f & value, const char * label) { return s.WriteVec3(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Vec4f & value, const char * label) { return s.WriteVec4(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Vec2i & value, const char * label) { return s.WriteVec2(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Vec3i & value, const char * label) { return s.WriteVec3(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Vec4i & value, const char * label) { return s.WriteVec4(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Vec2u & value, const char * label) { return s.WriteVec2(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Vec3u & value, const char * label) { return s.WriteVec3(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Vec4u & value, const char * label) { return s.WriteVec4(value, label); }

// Color[2/3/4]b:
inline bool SerializerWrite(WriteSerializer & s, const Color2b & value, const char * label) { return s.WriteColor(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Color3b & value, const char * label) { return s.WriteColor(value, label); }
inline bool SerializerWrite(WriteSerializer & s, const Color4b & value, const char * label) { return s.WriteColor(value, label); }

// Strings:
inline bool SerializerWrite(WriteSerializer & s, const std::string & str, const char * label) { return s.WriteString(str, label); }
inline bool SerializerWrite(WriteSerializer & s, const char * str, const char * label)        { return s.WriteString(str, label); }

// STL containers (any compatible with range based iteration):
template <class STLContainer>
inline bool SerializerWrite(WriteSerializer & s, const STLContainer & container, const char * label)
{
    bool hadError = false;
    for (const auto & it : container)
    {
        if (!SerializerWrite(s, it, label))
        {
            hadError = true;
        }
    }
    return hadError;
}

} // namespace Engine {}
