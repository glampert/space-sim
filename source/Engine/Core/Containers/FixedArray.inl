
// ================================================================================================
// -*- C++ -*-
// File: FixedArray.inl
// Author: Guilherme R. Lampert
// Created on: 23/05/13
// Brief: Inline definitions for FixedArray.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

template <class T, size_t N>
FixedArray<T, N>::FixedArray(const std::initializer_list<T> & init)
{
    std::copy(std::begin(init), std::end(init), elements);
}

template <class T, size_t N>
FixedArray<T, N>::FixedArray(const T & fillVal)
{
    std::fill(begin(), end(), fillVal);
}

template <class T, size_t N>
bool FixedArray<T, N>::operator==(const FixedArray<T, N> & rhs) const noexcept
{
    if (this == &rhs)
    {
        // Same object
        return true;
    }

    // Compare each element, stopping on the first difference:
    for (size_t i = 0; i < N; ++i)
    {
        if (elements[i] != rhs.elements[i])
        {
            return false;
        }
    }

    return true;
}

template <class T, size_t N>
bool FixedArray<T, N>::operator!=(const FixedArray<T, N> & rhs) const noexcept
{
    return !((*this) == rhs);
}

template <class T, size_t N>
auto FixedArray<T, N>::begin() noexcept -> iterator
{
    return elements;
}

template <class T, size_t N>
auto FixedArray<T, N>::begin() const noexcept -> const_iterator
{
    return elements;
}

template <class T, size_t N>
auto FixedArray<T, N>::end() noexcept -> iterator
{
    return elements + N;
}

template <class T, size_t N>
auto FixedArray<T, N>::end() const noexcept -> const_iterator
{
    return elements + N;
}

template <class T, size_t N>
T & FixedArray<T, N>::operator[](const size_t index)noexcept
{
    assert(index < N && "Index out-of-range!");
    return elements[index];
}

template <class T, size_t N>
const T & FixedArray<T, N>::operator[](const size_t index) const noexcept
{
    assert(index < N && "Index out-of-range!");
    return elements[index];
}

template <class T, size_t N>
T * FixedArray<T, N>::data() noexcept
{
    return elements;
}

template <class T, size_t N>
const T * FixedArray<T, N>::data() const noexcept
{
    return elements;
}

template <class T, size_t N>
void FixedArray<T, N>::to_vector(std::vector<T> & v) const
{
    v.clear();
    v.reserve(N);
    for (size_t i = 0; i < N; ++i)
    {
        v.push_back(elements[i]);
    }
}

template <class T, size_t N>
void FixedArray<T, N>::to_vector(std::vector<const T> & v) const
{
    v.clear();
    v.reserve(N);
    for (size_t i = 0; i < N; ++i)
    {
        v.push_back(elements[i]);
    }
}

template <class T, size_t N>
void FixedArray<T, N>::fill(const T & value) noexcept
{
    std::fill(begin(), end(), value);
}

template <class T, size_t N>
size_t FixedArray<T, N>::copy_cstr(const T * sourceStr) noexcept
{
    static_assert(std::is_same<T, char>::value     ||
                  std::is_same<T, wchar_t>::value  ||
                  std::is_same<T, char16_t>::value ||
                  std::is_same<T, char32_t>::value,
                  "This method is only available for strings!");

    assert(sourceStr != nullptr && "Null string pointer!");

    size_t i = 0;
    for (; i < N; ++i)
    {
        if (!(elements[i] = sourceStr[i]))
        {
            break;
        }
    }

    if (i == N)
    {
        // Truncate string if overflow:
        elements[--i] = T(0);
    }

    // Number of chars copied minus the null => std::strlen(this->elements)
    return i;
}
