
// ================================================================================================
// -*- C++ -*-
// File: IntrusiveSList.inl
// Author: Guilherme R. Lampert
// Created on: 23/05/13
// Brief: Inline definitions for IntrusiveSList.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// ======================================================
// IntrusiveSList:
// ======================================================

template <class T, class DP>
IntrusiveSList<T, DP>::IntrusiveSList() noexcept
: head(nullptr),
  tail(nullptr),
  numItems(0)
{
}

template <class T, class DP>
void IntrusiveSList<T, DP>::push_front(value_type item) noexcept
{
    assert(item != nullptr);

    if (!empty())
    {
        item->sl.next = head;
        head = item;
    }
    else
    {
        // Empty list, first insertion:
        item->sl.next = head;
        head = item;
        tail = head;
    }
    ++numItems;
}

template <class T, class DP>
void IntrusiveSList<T, DP>::push_back(value_type item) noexcept
{
    assert(item != nullptr);

    if (!empty())
    {
        tail->sl.next = item;
        tail = tail->sl.next;
        item->sl.next = nullptr; // Make sure item's next is null.
    }
    else
    {
        // Empty list, first insertion:
        head = item;
        tail = head;
        head->sl.next = nullptr;
    }
    ++numItems;
}

template <class T, class DP>
void IntrusiveSList<T, DP>::pop_front() noexcept
{
    assert(!empty() && "List already empty!");

    // 1 item left, clear list:
    if (head == tail)
    {
        tail = head = nullptr;
        --numItems;

        assert(numItems == 0);
        return;
    }

    // Shift head by 1:
    head = head->sl.next;
    --numItems;
}

template <class T, class DP>
void IntrusiveSList<T, DP>::pop_back() noexcept
{
    assert(!empty() && "List already empty!");

    // 1 item left, clear list:
    if (head == tail)
    {
        tail = head = nullptr;
        --numItems;

        assert(numItems == 0);
        return;
    }

    // Find the node before the tail:
    value_type it = head;
    value_type tailPrev = nullptr; // Element before tail, which will become the new tail.

    while (it != tail)
    {
        tailPrev = it;
        it = it->sl.next;
    }

    // Unlink tail node:
    assert(it != nullptr);
    tailPrev->sl.next = nullptr;
    tail = tailPrev;
    --numItems;
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::front() noexcept -> value_type
{
    assert(!empty() && "Calling front() with an empty list!");
    return head;
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::front() const noexcept -> const_value_type
{
    assert(!empty() && "Calling front() with an empty list!");
    return head;
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::back() noexcept -> value_type
{
    assert(!empty() && "Calling back() with an empty list!");
    return tail;
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::back() const noexcept -> const_value_type
{
    assert(!empty() && "Calling back() with an empty list!");
    return tail;
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::begin() noexcept -> iterator
{
    return iterator(head);
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::begin() const noexcept -> const_iterator
{
    return const_iterator(head);
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::end() noexcept -> iterator
{
    // tail == nullptr when the list it empty, tail->sl.next is always nullptr.
    return iterator(nullptr);
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::end() const noexcept -> const_iterator
{
    // tail == nullptr when the list it empty, tail->sl.next is always nullptr.
    return const_iterator(nullptr);
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::empty() const noexcept -> bool
{
    return numItems == 0;
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::size() const noexcept -> size_t
{
    return numItems;
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::num_items() const noexcept -> size_t
{
    return numItems;
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::insert(iterator position, value_type item) noexcept -> iterator
{
    assert(item != nullptr);

    // Special cases:
    if (position == begin())
    {
        push_front(item);
        return iterator(head);
    }
    if (position == end())
    {
        push_back(item);
        return iterator(tail);
    }

    // Somewhere in between:
    value_type it = head;
    value_type posNode = *position;
    value_type posPrev = nullptr; // Element before 'position'

    while (it != posNode)
    {
        posPrev = it;
        it = it->sl.next;
    }

    assert(it != nullptr);
    posPrev->sl.next = item;
    item->sl.next = posNode;
    ++numItems;

    return iterator(item);
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::erase(iterator position) noexcept -> iterator
{
    // Can't erase end(), that is just a marker to the element after the last.
    assert(position != end());

    // Can't erase from empty list either.
    assert(!empty() && "List already empty!");

    value_type posNode = *position;

    if (head == tail) // Last element, empty list:
    {
        assert(numItems == 1 && posNode == head);
        tail = head = nullptr;
        --numItems;
        return end();
    }

    if (posNode == head) // Remove head:
    {
        head = head->sl.next;
        --numItems;
        return iterator(head);
    }

    // Find the element before 'position':
    value_type it = head;
    value_type posPrev = nullptr;

    while (it != posNode)
    {
        posPrev = it;
        it = it->sl.next;
    }

    assert(it != nullptr);

    // Unlink 'position' by pointing its previous element to what was after it:
    posPrev->sl.next = posNode->sl.next;

    // Make sure tail pointer is up-to-date:
    if (posNode == tail)
    {
        tail = posPrev;
    }

    // Decrement item count:
    --numItems;

    // Return the element that followed 'position':
    return iterator(posNode->sl.next);
}

template <class T, class DP>
template <class Pred>
void IntrusiveSList<T, DP>::remove_if(Pred pred, const bool destroyObjects) noexcept
{
    auto it = begin();
    while (it != end())
    {
        auto obj = *it;
        if (pred(*obj))
        {
            it = erase(it);
            if (destroyObjects)
            {
                DP::Destroy(obj);
            }
            continue;
        }
        ++it;
    }
}

template <class T, class DP>
void IntrusiveSList<T, DP>::clear(const bool destroyObjects) noexcept
{
    if (destroyObjects) // Unlink and destroy all objects:
    {
        for (auto it = head; it != nullptr;)
        {
            auto tmp = it;
            it = it->sl.next;
            DP::Destroy(tmp);
        }
    }
    else // Unlink objects but don't destroy 'em:
    {
        // Make sure all pointers are nullified in a debug build
        // to help detecting programmer errors:
        #if ENGINE_DEBUG_BUILD
        for (auto it = head; it != nullptr;)
        {
            auto tmp = it;
            it = it->sl.next;
            tmp->sl.next = nullptr;
        }
        #endif // ENGINE_DEBUG_BUILD
    }

    tail = head = nullptr;
    numItems = 0;
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::find(const T & elemRef) noexcept -> iterator
{
    for (auto it = head; it != nullptr; it = it->sl.next)
    {
        if (*it == elemRef)
        {
            return iterator(it);
        }
    }
    return end(); // Element not found
}

template <class T, class DP>
auto IntrusiveSList<T, DP>::find(const T & elemRef) const noexcept -> const_iterator
{
    for (auto it = head; it != nullptr; it = it->sl.next)
    {
        if (*it == elemRef)
        {
            return const_iterator(it);
        }
    }
    return end(); // Element not found
}

template <class T, class DP>
template <class Pred, class Param>
auto IntrusiveSList<T, DP>::find(Pred pred, const Param & parm) noexcept -> iterator
{
    for (auto it = head; it != nullptr; it = it->sl.next)
    {
        if (pred(*it, parm))
        {
            return iterator(it);
        }
    }
    return end(); // Element not found
}

template <class T, class DP>
template <class Pred, class Param>
auto IntrusiveSList<T, DP>::find(Pred pred, const Param & parm) const noexcept -> const_iterator
{
    for (auto it = head; it != nullptr; it = it->sl.next)
    {
        if (pred(*it, parm))
        {
            return const_iterator(it);
        }
    }
    return end(); // Element not found
}

template <class T, class DP>
void IntrusiveSList<T, DP>::sort()
{
    sort(std::less<T>());
}

template <class T, class DP>
template <class Cmp>
void IntrusiveSList<T, DP>::sort(Cmp cmp)
{
    // Convert to a temp vector:
    std::vector<T *> tmp;
    to_vector(tmp);

    // Sort the vector using a fast std::sort:
    std::sort(std::begin(tmp), std::end(tmp), sort_vec<Cmp, T>(cmp));
    const size_t count = tmp.size();
    assert(count == numItems);

    // Rebuild the list:
    clear(/* destroyObjects = */ false);
    for (size_t i = 0; i < count; ++i)
    {
        push_back(tmp[i]);
    }

    assert(numItems == count);
}

template <class T, class DP>
void IntrusiveSList<T, DP>::to_vector(std::vector<T *> & v)
{
    v.clear();
    v.reserve(numItems);

    auto it = head;
    while (it != nullptr)
    {
        v.push_back(it);
        it = it->sl.next;
    }
}

template <class T, class DP>
void IntrusiveSList<T, DP>::to_vector(std::vector<const T *> & v) const
{
    v.clear();
    v.reserve(numItems);

    auto it = head;
    while (it != nullptr)
    {
        v.push_back(it);
        it = it->sl.next;
    }
}

template <class T, class DP>
IntrusiveSList<T, DP>::~IntrusiveSList()
{
    // Clear list and destroy all objects currently in it.
    clear(/* destroyObjects = */ true);
}

// ======================================================
// IntrusiveSList::forward_iterator_base:
// ======================================================

template <class T, class DP>
template <class NodeT>
IntrusiveSList<T, DP>::forward_iterator_base<NodeT>::forward_iterator_base(value_type nod) noexcept
: node(nod)
{
}

template <class T, class DP>
template <class NodeT>
IntrusiveSList<T, DP>::forward_iterator_base<NodeT> &
IntrusiveSList<T, DP>::forward_iterator_base<NodeT>::
operator=(const forward_iterator_base & other) noexcept
{
    this->node = other.node;
    return *this;
}

template <class T, class DP>
template <class NodeT>
auto IntrusiveSList<T, DP>::forward_iterator_base<NodeT>::operator*() const noexcept -> value_type
{
    assert(node != nullptr && "Invalid iterator dereference!");
    return node;
}

template <class T, class DP>
template <class NodeT>
auto IntrusiveSList<T, DP>::forward_iterator_base<NodeT>::operator -> () const noexcept -> value_type
{
    assert(node != nullptr && "Invalid iterator dereference!");
    return node;
}

template <class T, class DP>
template <class NodeT>
IntrusiveSList<T, DP>::forward_iterator_base<NodeT> &
IntrusiveSList<T, DP>::forward_iterator_base<NodeT>::
operator++() noexcept
{
    assert(node != nullptr && "Out-of-bounds iterator increment!");
    node = node->sl.next;
    return *this;
}

template <class T, class DP>
template <class NodeT>
IntrusiveSList<T, DP>::forward_iterator_base<NodeT>
IntrusiveSList<T, DP>::forward_iterator_base<NodeT>::operator++(int)noexcept
{
    assert(node != nullptr && "Out-of-bounds iterator increment!");
    forward_iterator_base tmp(*this);
    node = node->sl.next;
    return tmp; // Return old, pre-increment value.
}

template <class T, class DP>
template <class NodeT>
bool IntrusiveSList<T, DP>::forward_iterator_base<NodeT>::operator==(const forward_iterator_base & other) const noexcept
{
    return node == other.node;
}

template <class T, class DP>
template <class NodeT>
bool IntrusiveSList<T, DP>::forward_iterator_base<NodeT>::operator!=(const forward_iterator_base & other) const noexcept
{
    return node != other.node;
}

template <class T, class DP>
template <class NodeT>
IntrusiveSList<T, DP>::forward_iterator_base<NodeT>::operator IntrusiveSList<T, DP>::forward_iterator_base<NodeT const>() const noexcept
{
    return forward_iterator_base<NodeT const>(node);
}

// ======================================================
// Global friend functions:
// ======================================================

template <class Tp, class Dp>
void MergeLists(IntrusiveSList<Tp, Dp> & listA, IntrusiveSList<Tp, Dp> & listB, IntrusiveSList<Tp, Dp> & result) noexcept
{
    // listA and listB should both have something:
    assert(!listA.empty());
    assert(!listB.empty());

    // 'result' must be a new, empty list:
    assert(result.empty());

    // Attack b to a's tail:
    listA.tail->sl.next = listB.head;
    listA.tail = listB.tail;

    // Set result:
    result.head = listA.head;
    result.tail = listA.tail;
    result.numItems = (listA.numItems + listB.numItems);

    // Invalidate listA and listB:
    listA.head = listB.head = nullptr;
    listA.tail = listB.tail = nullptr;
    listA.numItems = listB.numItems = 0;
}

template <class Tp, class Dp>
void MoveList(IntrusiveSList<Tp, Dp> & source, IntrusiveSList<Tp, Dp> & dest) noexcept
{
    assert(dest.empty() && "dest contents would be lost!");

    dest.head = source.head;
    dest.tail = source.tail;
    dest.numItems = source.numItems;

    source.head = nullptr;
    source.tail = source.head;
    source.numItems = 0;
}
