
// ================================================================================================
// -*- C++ -*-
// File: IntrusiveDList.inl
// Author: Guilherme R. Lampert
// Created on: 26/07/13
// Brief: Inline definitions for IntrusiveDList.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// ======================================================
// IntrusiveDList:
// ======================================================

template <class T, class DP>
IntrusiveDList<T, DP>::IntrusiveDList() noexcept
: head(nullptr),
  numItems(0)
{
}

template <class T, class DP>
void IntrusiveDList<T, DP>::push_front(value_type item) noexcept
{
    assert(item != nullptr);

    if (!empty())
    {
        // head->dl.prev points the tail, and vice-versa:
        auto tail = head->dl.prev;
        item->dl.next = head;
        head->dl.prev = item;
        item->dl.prev = tail;
        head = item;
    }
    else
    {
        // Empty list, first insertion:
        head = item;
        head->dl.prev = head;
        head->dl.next = head;
    }
    ++numItems;
}

template <class T, class DP>
void IntrusiveDList<T, DP>::push_back(value_type item) noexcept
{
    assert(item != nullptr);

    if (!empty())
    {
        // head->dl.prev points the tail, and vice-versa:
        auto tail = head->dl.prev;
        item->dl.prev = tail;
        tail->dl.next = item;
        item->dl.next = head;
        head->dl.prev = item;
    }
    else
    {
        // Empty list, first insertion:
        head = item;
        head->dl.prev = head;
        head->dl.next = head;
    }
    ++numItems;
}

template <class T, class DP>
void IntrusiveDList<T, DP>::pop_front() noexcept
{
    assert(!empty() && "List already empty!");
    auto tail = head->dl.prev;
    head = head->dl.next;
    head->dl.prev = tail;
    --numItems;
}

template <class T, class DP>
void IntrusiveDList<T, DP>::pop_back() noexcept
{
    assert(!empty() && "List already empty!");
    auto tail = head->dl.prev;
    head->dl.prev = tail->dl.prev;
    tail->dl.prev->dl.next = head;
    --numItems;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::front() noexcept -> value_type
{
    assert(!empty() && "Calling front() with an empty list!");
    return head;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::front() const noexcept -> const_value_type
{
    assert(!empty() && "Calling front() with an empty list!");
    return head;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::back() noexcept -> value_type
{
    assert(!empty() && "Calling back() with an empty list!");
    return head->dl.prev;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::back() const noexcept -> const_value_type
{
    assert(!empty() && "Calling back() with an empty list!");
    return head->dl.prev;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::begin() noexcept -> iterator
{
    return iterator(this, 0, head);
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::begin() const noexcept -> const_iterator
{
    return const_iterator(this, 0, head);
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::end() noexcept -> iterator
{
    return iterator(this, numItems, head);
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::end() const noexcept -> const_iterator
{
    return const_iterator(this, numItems, head);
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::empty() const noexcept -> bool
{
    return numItems == 0;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::size() const noexcept -> size_t
{
    return numItems;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::num_items() const noexcept -> size_t
{
    return numItems;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::insert(iterator position, value_type item) noexcept -> iterator
{
    assert(item != nullptr);

    // Special cases:
    if (position == begin())
    {
        push_front(item);
        return iterator(this, 0, head);
    }
    if (position == end())
    {
        push_back(item);
        return iterator(this, numItems, head->dl.prev);
    }

    // Iterators of a d-list hold a reference to the owning list,
    // we can assert here to improve safety:
    assert((position.myList == this) && "Iterator does not belong to this list!");

    // Insert somewhere between head and tail (before 'position'):
    auto posNode = *position;
    auto posPrev = posNode->dl.prev;
    posPrev->dl.next = item;
    posNode->dl.prev = item;
    item->dl.prev = posPrev;
    item->dl.next = posNode;
    ++numItems;

    return iterator(this, position.traversalPos, item);
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::erase(iterator position) noexcept -> iterator
{
    // Can't erase end(), that is just a marker to the element after the last.
    assert(position != end());

    // Can't erase from empty list either.
    assert(!empty() && "List already empty!");

    // Iterators of a d-list hold a reference to the owning list,
    // we can assert here to improve safety:
    assert((position.myList == this) && "Iterator does not belong to this list!");

    // Special case, erase head:
    if (position == begin())
    {
        pop_front();
        return begin();
    }

    // Remove node:
    auto posNode = *position;
    auto posPrev = posNode->dl.prev;
    auto posNext = posNode->dl.next;
    posPrev->dl.next = posNext;
    posNext->dl.prev = posPrev;
    --numItems;

    // Return the element that followed 'position':
    return iterator(this, position.traversalPos + 1, posNode->dl.next);
}

template <class T, class DP>
template <class Pred>
void IntrusiveDList<T, DP>::remove_if(Pred pred, const bool destroyObjects) noexcept
{
    auto it = begin();
    while (it != end())
    {
        auto obj = *it;
        if (pred(*obj))
        {
            it = erase(it);
            if (destroyObjects)
            {
                DP::Destroy(obj);
            }
            continue;
        }
        ++it;
    }
}

template <class T, class DP>
void IntrusiveDList<T, DP>::clear(const bool destroyObjects) noexcept
{
    if (destroyObjects) // Unlink and destroy all objects:
    {
        auto it = head;
        for (size_t i = 0; i < numItems; ++i)
        {
            auto tmp = it;
            it = it->dl.next;
            DP::Destroy(tmp);
        }
    }
    else // Unlink objects but don't destroy 'em:
    {
        // Make sure all pointers are nullified in a debug build
        // to help detecting programmer errors:
        #if ENGINE_DEBUG_BUILD
        auto it = head;
        for (size_t i = 0; i < numItems; ++i)
        {
            auto tmp = it;
            it = it->dl.next;
            tmp->dl.prev = nullptr;
            tmp->dl.next = nullptr;
        }
        #endif // ENGINE_DEBUG_BUILD
    }

    head = nullptr;
    numItems = 0;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::find(const T & elemRef) noexcept -> iterator
{
    auto it = begin();
    while (it != end())
    {
        if (*(*it) == elemRef)
        {
            return it;
        }
        ++it;
    }
    return it;
}

template <class T, class DP>
auto IntrusiveDList<T, DP>::find(const T & elemRef) const noexcept -> const_iterator
{
    auto it = begin();
    while (it != end())
    {
        if (*(*it) == elemRef)
        {
            return it;
        }
        ++it;
    }
    return it;
}

template <class T, class DP>
template <class Pred, class Param>
auto IntrusiveDList<T, DP>::find(Pred pred, const Param & parm) noexcept -> iterator
{
    auto it = begin();
    while (it != end())
    {
        if (pred(*(*it), parm))
        {
            return it;
        }
        ++it;
    }
    return it;
}

template <class T, class DP>
template <class Pred, class Param>
auto IntrusiveDList<T, DP>::find(Pred pred, const Param & parm) const noexcept -> const_iterator
{
    auto it = begin();
    while (it != end())
    {
        if (pred(*(*it), parm))
        {
            return it;
        }
        ++it;
    }
    return it;
}

template <class T, class DP>
void IntrusiveDList<T, DP>::sort()
{
    sort(std::less<T>());
}

template <class T, class DP>
template <class Cmp>
void IntrusiveDList<T, DP>::sort(Cmp cmp)
{
    // Convert to a temp vector:
    std::vector<T *> tmp;
    to_vector(tmp);

    // Sort the vector using a fast std::sort:
    std::sort(std::begin(tmp), std::end(tmp), sort_vec<Cmp, T>(cmp));
    const size_t count = tmp.size();
    assert(count == numItems);

    // Rebuild the list:
    clear(/* destroyObjects = */ false);
    for (size_t i = 0; i < count; ++i)
    {
        push_back(tmp[i]);
    }

    assert(numItems == count);
}

template <class T, class DP>
void IntrusiveDList<T, DP>::to_vector(std::vector<T *> & v)
{
    v.clear();
    v.reserve(numItems);

    auto it = begin();
    while (it != end())
    {
        v.push_back(*it);
        ++it;
    }
}

template <class T, class DP>
void IntrusiveDList<T, DP>::to_vector(std::vector<const T *> & v) const
{
    v.clear();
    v.reserve(numItems);

    auto it = begin();
    while (it != end())
    {
        v.push_back(*it);
        ++it;
    }
}

template <class T, class DP>
IntrusiveDList<T, DP>::~IntrusiveDList()
{
    // Clear list and destroy all objects currently in it.
    clear(/* destroyObjects = */ true);
}

// ======================================================
// IntrusiveDList::bidirectional_iterator_base:
// ======================================================

template <class T, class DP>
template <class NodeT, class ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::bidirectional_iterator_base() noexcept
: myList(nullptr),
  traversalPos(0),
  node(nullptr)
{
    // Constructs an invalid iterator.
}

template <class T, class DP>
template <class NodeT, class ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::bidirectional_iterator_base(const ListT * list, const size_t pos, value_type nod) noexcept
: myList(list),
  traversalPos(pos),
  node(nod)
{
    assert(myList != nullptr);
}

template <class T, class DP>
template <class NodeT, class ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::bidirectional_iterator_base(const bidirectional_iterator_base & other) noexcept
: myList(other.myList),
  traversalPos(other.traversalPos),
  node(other.node)
{
    assert(myList != nullptr);
}

template <class T, class DP>
template <class NodeT, class ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT> &
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::
operator=(const bidirectional_iterator_base & other) noexcept
{
    this->myList = other.myList;
    this->traversalPos = other.traversalPos;
    this->node = other.node;

    assert(myList != nullptr);
    return *this;
}

template <class T, class DP>
template <class NodeT, class ListT>
auto IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::operator*() const noexcept -> value_type
{
    assert((node != nullptr && traversalPos < myList->num_items()) && "Invalid iterator dereference!");
    return node;
}

template <class T, class DP>
template <class NodeT, class ListT>
auto IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::operator -> () const noexcept -> value_type
{
    assert((node != nullptr && traversalPos < myList->num_items()) && "Invalid iterator dereference!");
    return node;
}

template <class T, class DP>
template <class NodeT, class ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT> &
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::
operator--() noexcept
{
    assert(node != nullptr && "Invalid iterator!");
    assert(traversalPos > 0 && "Out-of-bounds iterator decrement!");

    node = node->dl.prev;
    --traversalPos;
    return *this;
}

template <class T, class DP>
template <class NodeT, class ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::operator--(int)noexcept
{
    assert(node != nullptr && "Invalid iterator!");
    assert(traversalPos > 0 && "Out-of-bounds iterator decrement!");

    bidirectional_iterator_base tmp(*this);
    node = node->dl.prev;
    --traversalPos;
    return tmp; // Return old, pre-increment value.
}

template <class T, class DP>
template <class NodeT, class ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT> &
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::
operator++() noexcept
{
    assert(node != nullptr && "Invalid iterator!");
    assert(traversalPos < myList->num_items() && "Out-of-bounds iterator increment!");

    node = node->dl.next;
    ++traversalPos;
    return *this;
}

template <class T, class DP>
template <class NodeT, class ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::operator++(int)noexcept
{
    assert(node != nullptr && "Invalid iterator!");
    assert(traversalPos < myList->num_items() && "Out-of-bounds iterator increment!");

    bidirectional_iterator_base tmp(*this);
    node = node->dl.next;
    ++traversalPos;
    return tmp; // Return old, pre-increment value.
}

template <class T, class DP>
template <class NodeT, class ListT>
bool IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::operator==(const bidirectional_iterator_base & other) const noexcept
{
    return traversalPos == other.traversalPos;
}

template <class T, class DP>
template <class NodeT, class ListT>
bool IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::operator!=(const bidirectional_iterator_base & other) const noexcept
{
    return traversalPos != other.traversalPos;
}

template <class T, class DP>
template <class NodeT, class ListT>
IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT, ListT>::operator IntrusiveDList<T, DP>::bidirectional_iterator_base<NodeT const, ListT>() const noexcept
{
    return bidirectional_iterator_base<NodeT const, ListT>(myList, traversalPos, node);
}

// ======================================================
// Global friend functions:
// ======================================================

template <class Tp, class Dp>
void MergeLists(IntrusiveDList<Tp, Dp> & listA, IntrusiveDList<Tp, Dp> & listB, IntrusiveDList<Tp, Dp> & result) noexcept
{
    // listA and listB should both have something:
    assert(!listA.empty());
    assert(!listB.empty());

    // 'result' must be a new, empty list:
    assert(result.empty());

    // Attack b to a's tail:
    Tp * aTail = listA.head->dl.prev;
    Tp * bTail = listB.head->dl.prev;
    aTail->dl.next = listB.head;
    listB.head->dl.prev = aTail;
    listA.head->dl.prev = bTail;

    // Set result:
    result.head = listA.head;
    result.numItems = (listA.numItems + listB.numItems);

    // Invalidate listA and listB:
    listA.head = listB.head = nullptr;
    listA.numItems = listB.numItems = 0;
}

template <class Tp, class Dp>
void MoveList(IntrusiveDList<Tp, Dp> & source, IntrusiveDList<Tp, Dp> & dest) noexcept
{
    assert(dest.empty() && "dest contents would be lost!");

    dest.head = source.head;
    dest.numItems = source.numItems;

    source.head = nullptr;
    source.numItems = 0;
}
