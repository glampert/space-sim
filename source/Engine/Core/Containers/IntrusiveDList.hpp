
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: IntrusiveDList.hpp
// Author: Guilherme R. Lampert
// Created on: 26/07/13
// Brief: Intrusive, doubly-linked, list template class.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// IntrusiveDList can be copied into a vector.
#include <vector>
#include <iterator>

namespace Engine
{

// ======================================================
// DListNode:
// ======================================================

//
// List node type for the intrusive D-list.
// Types that wish to have its objects inserted into an
// IntrusiveDList must inherit form this type.
//
template <class T>
struct DListNode
{
    struct
    {
        T * prev;
        T * next;
    } dl;

    DListNode()
    {
        dl.prev = nullptr;
        dl.next = nullptr;
    }
};

// ======================================================
// IntrusiveDList:
// ======================================================

//
// Intrusive doubly-liked list template class.
//
// An intrusive data structure is a structure that stores house
// keeping information necessary to its functioning directly inside
// the object being stored.
//
// This kind of storage scheme forces types that wish to be stored inside
// the list to inherit from DListNode<T>.
// Only object pointers can actually be stored inside the structure,
// thus the objective of the intrusive list is to eliminate one memory allocation
// per new object, instead of allocating a node structure per object, the object
// itself is the node. Consequently, memory allocations are all external to the structure.
//
// NOTE: The naming convention of member methods follows the standard
// C++ convention for std::containers, that is: snake_case.
//
template
<
    class T,                         // Linked type. Must inherit from DListNode
    class DP = DestroyUsingDelete<T> // Node Destruction Policy
>
class IntrusiveDList
{
  public:

    // Bidirectional iterator for a doubly-linked list.
    // Allows bidirectional list traversal (++ and --).
    template <class NodeT, class ListT>
    class bidirectional_iterator_base
        : public std::iterator<std::bidirectional_iterator_tag, NodeT>
    {
      public:

        using value_type = NodeT *;

        // Constructors:
        bidirectional_iterator_base() noexcept;
        bidirectional_iterator_base(const ListT * list, const size_t pos, value_type nod) noexcept;
        bidirectional_iterator_base(const bidirectional_iterator_base & other) noexcept;

        // Assignment:
        bidirectional_iterator_base & operator=(const bidirectional_iterator_base & other) noexcept;

        // Pointer operations:
        value_type operator*() const noexcept;
        value_type operator->() const noexcept;

        // --/++ operators:
        bidirectional_iterator_base & operator--() noexcept;
        bidirectional_iterator_base & operator++() noexcept;
        bidirectional_iterator_base operator--(int)noexcept;
        bidirectional_iterator_base operator++(int)noexcept;

        // Comparison:
        bool operator==(const bidirectional_iterator_base & other) const noexcept;
        bool operator!=(const bidirectional_iterator_base & other) const noexcept;

        // One way conversion: iterator -> const_iterator
        operator bidirectional_iterator_base<NodeT const, ListT>() const noexcept;

      private:

        friend class IntrusiveDList;

        const ListT * myList; // List this iterator belongs to.
        size_t traversalPos;  // Traversal position. Needed due to the circular format of the list.
        value_type node;      // Actual node pointing to a list item.
    };

    // Nested types:
    using value_type = T *;
    using const_value_type = const T *;
    using iterator = bidirectional_iterator_base<T, IntrusiveDList<T, DP> >;
    using const_iterator = bidirectional_iterator_base<T const, IntrusiveDList<T, DP> >;

    // Construct an empty list.
    IntrusiveDList() noexcept;

    // Insert a new item at the head of the list. The new item becomes the list head.
    void push_front(value_type item) noexcept;

    // Insert a new item at the tail of the list. The new element becomes the list tail.
    void push_back(value_type item) noexcept;

    // Removes the head element of the list, without destroying the object.
    void pop_front() noexcept;

    // Removes the tail element of the list, without destroying the object.
    void pop_back() noexcept;

    // Returns the first element in the list container.
    // Calling this method in an empty list will cause a debug assertion.
    value_type front() noexcept;

    // Returns the first element in the list container. Const overload.
    // Calling this method in an empty list will cause a debug assertion.
    const_value_type front() const noexcept;

    // Returns the last element in the list container.
    // Calling this method in an empty list will cause a debug assertion.
    value_type back() noexcept;

    // Returns the last element in the list container. Const overload.
    // Calling this method in an empty list will cause a debug assertion.
    const_value_type back() const noexcept;

    // Get an iterator to the first element of the list.
    iterator begin() noexcept;

    // Get an iterator to the first element of the list. Const overload.
    const_iterator begin() const noexcept;

    // Get an iterator to the element past the end of the list.
    // This iterator is only a marker to the end of the list. It is not valid to dereference it.
    iterator end() noexcept;

    // Get an iterator to the element past the end of the list. Const overload.
    // This iterator is only a marker to the end of the list. It is not valid to dereference it.
    const_iterator end() const noexcept;

    // Test if the list is empty (num_items() == 0). Constant time.
    bool empty() const noexcept;

    // Returns the length in elements of the list. Constant time.
    size_t size() const noexcept;

    // Get the list length in items. Constant time.
    size_t num_items() const noexcept;

    // Insert element into list.
    // The container is extended by inserting a new element before the element at the specified position.
    // No memory allocation takes place.
    iterator insert(iterator position, value_type item) noexcept;

    // Removes one item from the list.
    // Removing doesn't destroy the item, only unlinks it from the
    // data structure. The user can save the item before removing it
    // and then destroy it after removing, if that is the case.
    // Returns an iterator pointing to the new location of the element that followed the element unlinked.
    // This is the list end if the operation removed the last element in it.
    iterator erase(iterator position) noexcept;

    // Removes from the container all the elements for which Pred 'pred' returns true.
    // Can optionally destroy or just unlink the objects. Destroys by default.
    template <class Pred>
    void remove_if(Pred pred, const bool destroyObjects = true) noexcept;

    // Unlink all objects from the list and destroys them.
    // The normal behavior of the list it to destroy all object
    // with its Node Destruction Policy. If 'destroyObjects' is set to false however,
    // all objects are unlinked from the list without being destroyed.
    void clear(const bool destroyObjects = true) noexcept;

    // Performs linear search in the list and returns an iterator to the first occurrence of 'elemRef', or end() if 'elemRef' in not present.
    // Search is performed by applying operator == in the elements themselves, not the stored pointers.
    iterator find(const T & elemRef) noexcept;

    // Const overload of Find().
    const_iterator find(const T & elemRef) const noexcept;

    // Performs linear search in the list and returns an iterator to the first element matching 'pred', or end() if there is no match.
    template <class Pred, class Param>
    iterator find(Pred pred, const Param & parm) noexcept;

    // Const overload of Find().
    template <class Pred, class Param>
    const_iterator find(Pred pred, const Param & parm) const noexcept;

    // Sorts the elements in the list, altering their position within the container.
    // The sorting is performed by applying an algorithm that uses operator < in the elements
    // themselves, not the stored pointers.
    void sort();

    // Sorts the elements in the list, altering their position within the container.
    // The sorting is performed by applying an algorithm that uses comparator 'Cmp'
    // in the elements themselves, not the stored pointers.
    // 'cmp' is a binary predicate that, taking two values of the same type of those contained in the list,
    // returns true if the first argument goes before the second argument in the strict ordering it defines, and false otherwise.
    template <class Cmp>
    void sort(Cmp cmp);

    // Copy all the object pointers stored in the list to an unsorted std::vector<T *>.
    // The list still keeps ownership of all the pointer copied to the vector.
    void to_vector(std::vector<T *> & v);

    // Copy all the object pointers stored in the list to an unsorted std::vector<const T *>.
    // The list still keeps ownership of all the pointer copied to the vector.
    void to_vector(std::vector<const T *> & v) const;

    // Clears the list, destroying all objects currently in it with its Node Destruction Policy.
    // If the user wishes to keep the objects alive after
    // the list is destroyed, he can call list.clear(true) to force the
    // list to unlink all objects, relinquishing ownership of the pointers.
    ~IntrusiveDList();

    // Merges two lists, clearing them, and returning a new list which is a combination of the two inputs.
    template <class Tp, class Dp>
    friend void MergeLists(IntrusiveDList<Tp, Dp> & listA, IntrusiveDList<Tp, Dp> & listB, IntrusiveDList<Tp, Dp> & result) noexcept;

    // Moves the contents of 'source' list into the 'dest' list, making dest equal to source and clearing the source.
    template <class Tp, class Dp>
    friend void MoveList(IntrusiveDList<Tp, Dp> & source, IntrusiveDList<Tp, Dp> & dest) noexcept;

  private:

    // Used internally as functor for std::sort() and vector.
    template <class Cmp, class Tp>
    struct sort_vec
    {
        Cmp original;
        sort_vec(Cmp cmp)
            : original(cmp)
        { }

        bool operator()(const Tp * a, const Tp * b) { return original(*a, *b); }
    };

    // List cannot be copied, but it can be moved.
    DISABLE_COPY_AND_ASSIGN(IntrusiveDList)

    value_type head; // Head of list. Circularly referenced.
    size_t numItems; // Number of items in the list.
};

// ======================================================
// Standard begin/end for range-based iteration:
// ======================================================

// Const:
template <class T, class DP>
typename IntrusiveDList<T, DP>::const_iterator begin(const IntrusiveDList<T, DP> & list) noexcept { return list.begin(); }
template <class T, class DP>
typename IntrusiveDList<T, DP>::const_iterator end(const IntrusiveDList<T, DP> & list) noexcept { return list.end(); }

// Non-const:
template <class T, class DP>
typename IntrusiveDList<T, DP>::iterator begin(IntrusiveDList<T, DP> & list) noexcept { return list.begin(); }
template <class T, class DP>
typename IntrusiveDList<T, DP>::iterator end(IntrusiveDList<T, DP> & list) noexcept { return list.end(); }

// Include the inline definitions in the same namespace:
#include "Engine/Core/Containers/IntrusiveDList.inl"

} // namespace Engine {}
