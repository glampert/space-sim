
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: DestructionPolicies.hpp
// Author: Guilherme R. Lampert
// Created on: 16/08/14
// Brief: Object destruction policies for the custom Engine containers.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Standard destruction policies for containers/ptrs:
// ======================================================

//
// Destroy an object using the 'delete' operator.
//
template <class T>
class DestroyUsingDelete
{
  public:
    // Call the default destructor for the given object.
    static void Destroy(T * object) noexcept
    {
        delete object;
    }
    // Make it compatible with the STL 'deleter'.
    void operator()(T * object) const noexcept
    {
        delete object;
    }
};

//
// Call the object destructor but does not deallocates memory.
//
template <class T>
class DestroyNoDealloc
{
  public:
    // Call the default destructor for the given object.
    static void Destroy(T * object) noexcept
    {
        if (object != nullptr)
        {
            object->~T();
        }
    }
    // Make it compatible with the STL 'deleter'.
    void operator()(T * object) const noexcept
    {
        if (object != nullptr)
        {
            object->~T();
        }
    }
};

//
// Does nothing with the object. No-op destruction policy.
//
template <class T>
class DestroyNoOp
{
  public:
    // No-op; does nothing.
    static void Destroy(T *) noexcept
    {
    }
    // Make it compatible with the STL 'deleter'.
    void operator()(T *) const noexcept
    {
    }
};

} // namespace Engine {}
