
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: FixedArray.hpp
// Author: Guilherme R. Lampert
// Created on: 10/11/12
// Brief: Defines the FixedArray template class, a fixed size std::vector like array.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <initializer_list> // For list-style construction
#include <vector>           // FixedArray can be copied into a vector

namespace Engine
{

// ======================================================
// FixedArray:
// ======================================================

//
// Simple wrapper around a statically allocated, fixed size array.
//
// This class provides array access via the [] operator,
// which checks for out-of-bounds access, giving some
// extra level of safety in our daily array manipulations.
//
// Why should you prefer FixedArray over std::array?
// Unfortunately the Clang compiler does not perform any bounds checking
// in the [] operator of std::array, not even in debug mode
// (defining _LIBCPP_DEBUG doesn't work either), rendering std::array
// not a bit safer than a plain C array if you access its elements via operator[].
// FixedArray will assert on out-of-bounds access, defaulting to an unchecked
// fast access for release builds.
//
// NOTE: The naming convention of member methods follows the standard
// C++ convention for std::containers, that is: snake_case.
//
template <class T, size_t N>
class FixedArray
{
  public:

    using value_type     = T;
    using iterator       = T *;
    using const_iterator = const T *;

    // Allows "array = {1,2,3,4};" style initialization.
    FixedArray(const std::initializer_list<T> & init);

    // Init every element to 'fillVal'.
    FixedArray(const T & fillVal);

    // Explicitly defaulted:
    FixedArray() = default;
    FixedArray(const FixedArray<T, N> & other) = default;
    FixedArray<T, N> & operator=(const FixedArray<T, N> & other) = default;

    // "Equal to" operator; element wise.
    bool operator==(const FixedArray<T, N> & rhs) const noexcept;

    // "Not equal to" operator; element wise.
    bool operator!=(const FixedArray<T, N> & rhs) const noexcept;

    // Array access, with assertion to guard against out of bounds index.
    T & operator[](const size_t index)noexcept;
    const T & operator[](const size_t index) const noexcept;

    // Iterators (no iterator debugging provided!):
    iterator begin() noexcept;
    iterator end() noexcept;
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // Returns the length in elements of the FixedArray.
    static constexpr size_t size() noexcept { return N; };

    // Get the array length in items. Same as size(); Provided for consistency the intrusive lists.
    static constexpr size_t num_items() noexcept { return N; }

    // Returns a raw pointer to the base address of the array (&array[0]).
    // This method is provided to allow better compatibility with legacy APIs that require raw pointers.
    T * data() noexcept;
    const T * data() const noexcept;

    // Copy all the elements stored in the array to an unsorted std::vector<T>.
    // Any existing elements in the vector are overwritten.
    void to_vector(std::vector<T> & v) const;
    void to_vector(std::vector<const T> & v) const;

    // Assigns the given value value to all elements in the array.
    void fill(const T & value) noexcept;

    // Eventually the FixedArray class is used to store a sequence of chars,
    // representing a C string. This function provides easy and safe copy of C strings
    // into the array, with insurance of a null terminator at the end of the array.
    // If the actual length of the source string, plus an extra null character at the end,
    // is greater that the size of the array, the source string is truncated and a null is always appended at the end.
    // Returns the number of character copied to the array, not including the null terminator.
    size_t copy_cstr(const T * sourceStr) noexcept;

  private:

    T elements[N];
};

// ======================================================
// ArrayLength() for FixedArray:
// ======================================================

// Overloads ArrayLength() to work with FixedArrays.
template <class T, size_t N>
constexpr size_t ArrayLength(const FixedArray<T, N> &) noexcept
{
    return N;
}

// ======================================================
// ZeroArray() for FixedArray:
// ======================================================

// Zero fills (memsets) the array. This method performs a bitwise zero-fill
// on the array, therefore, should only be used on arrays of POD objects.
template <typename T, size_t N>
void ZeroArray(FixedArray<T, N> & arr) noexcept
{
    static_assert(std::is_pod<T>::value, "Type must be Plain Old Data!");
    std::memset(arr.data(), 0, sizeof(T) * N);
}

// ======================================================
// Standard begin/end for range-based iteration:
// ======================================================

// Const:
template <class T, size_t N>
typename FixedArray<T, N>::const_iterator begin(const FixedArray<T, N> & arr) noexcept { return arr.begin(); }
template <class T, size_t N>
typename FixedArray<T, N>::const_iterator end(const FixedArray<T, N> & arr) noexcept { return arr.end(); }

// Non-const:
template <class T, size_t N>
typename FixedArray<T, N>::iterator begin(FixedArray<T, N> & arr) noexcept { return arr.begin(); }
template <class T, size_t N>
typename FixedArray<T, N>::iterator end(FixedArray<T, N> & arr) noexcept { return arr.end(); }

// Include the inline definitions in the same namespace:
#include "Engine/Core/Containers/FixedArray.inl"

} // namespace Engine {}
