
// ================================================================================================
// -*- C++ -*-
// File: Common.cpp
// Author: Guilherme R. Lampert
// Created on: 07/08/14
// Brief: Common Engine utilities.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

namespace Engine
{

// ======================================================
// Common singleton:
// ======================================================

Common::Common()
    : game(nullptr)
    , logStream(stdout)
    , windowObj(nullptr)
{
    ZeroPodObject(time);
}

void Common::Init()
{
    InitLog();
    PrintF("---------- Common::Init() ----------");
}

void Common::Shutdown()
{
    PrintF("-------- Common::Shutdown() --------");

    ShutdownApp();
    ShutdownLog();
    game = nullptr;
}

void Common::SetGameLogicInstance(GameLogic * gameLogic)
{
    assert(gameLogic != nullptr);
    game = gameLogic;
}

void Common::ExecCmdLine(const int argc, const char * argv[])
{
    (void)argc;
    (void)argv;
    // TODO handle cmd line (supersedes the lua script values!)
}

void Common::RunEngineConfigScripts()
{
    // FIXME hardcoded for now.
    scriptMgr->RunConfigFile("resources/scripts/engine_config.lua", { "rendererConfig", "resourcesConfig" });
}

// ======================================================
// Global Common instance:
// ======================================================

static Common theCommon;
Common * common = &theCommon;

} // namespace Engine {}
