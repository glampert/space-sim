
// ================================================================================================
// -*- C++ -*-
// File: XMLSerializer.cpp
// Author: Guilherme R. Lampert
// Created on: 20/08/14
// Brief: Implementation of ReadSerializer and WriteSerializer that reads/produces XML text.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine common:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Serialize.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/Containers/DestructionPolicies.hpp"
#include "Engine/Core/Containers/IntrusiveSList.hpp"

// XML library:
#include "Engine/Core/XML.hpp"
#include "Engine/Core/XMLReader.hpp"
#include "Engine/Core/XMLWriter.hpp"
#include "Engine/Core/XMLSerializer.hpp"

// For std::iota()/std::isxdigit()/etc:
#include <numeric>
#include <cctype>

// TODO: This files need some refactoring.
// Too many macros and copy-pasting!

namespace Engine
{
namespace /* unnamed */
{

// ======================================================
// Local constants:
// ======================================================

// These are used by the XMLWriteSerializer when printing boolean attribute values.
// Change them if you prefer another flavor, such as "true"/"false".
static const char xmlBoolTrue[]  = "yes";
static const char xmlBoolFalse[] = "no";

// Name of the XML tag used by WriteBytes()/ReadBytes().
// <ByteBuffer> </ByteBuffer>
static const char rawByteBufferTagName[] = "ByteBuffer";

// Max length of temporary stack buffers used for string building.
// These buffers are used mainly for number => string conversion via
// snprintf(), so they don't need to be very big.
static constexpr int TempCharBufMax = 256;

// ======================================================
// TrimFloatStr():
// ======================================================

static char * TrimFloatStr(char * str, size_t * newLen = nullptr)
{
    size_t n = 0;
    for (char * p = str; *p != '\0'; ++p, ++n)
    {
        if (*p == '.')
        {
            while (*++p) // Find the end of the string
            {
                ++n;
            }
            ++n;
            while (*--p == '0') // Remove trailing zeros
            {
                *p = '\0', --n;
            }
            if (*p == '.') // If the dot was left alone at the end, remove it to
            {
                *p = '\0', --n;
            }
            break;
        }
    }

    if (newLen != nullptr)
    {
        (*newLen) = n;
    }

    return str;
}

// ======================================================
// FloatTupleToStr():
// ======================================================

static char * FloatTupleToStr(char * destBuf, const float * data, const int numElements)
{
    size_t n;
    char * ptr = destBuf;
    for (int i = 0; i < numElements; ++i)
    {
        std::sprintf(ptr, "%f", data[i]);
        TrimFloatStr(ptr, &n); // Remove trailing zeros
        ptr += n;

        // Append a comma and a space, if not the last one:
        if (i != (numElements - 1))
        {
            *ptr++ = ',';
            *ptr++ = ' ';
        }
    }

    *ptr = '\0'; // Ensure null terminated
    return destBuf;
}

} // namespace unnamed {}

// ======================================================
// Helper macros used by XMLReadSerializer:
// ======================================================

//
// Validate the XMLReadSerializer and return an error is invalid.
// Must be called inside a method that returns a boolean.
//
#define XML_READ_SERIALIZER_VALIDATE_SELF()                          \
    if (!IsValid())                                                  \
    {                                                                \
        return ErrorF("XMLReadSerializer is not in a valid state!"); \
    }

//
// Convenient way to avoid code repetition in the Read*() methods.
// This will generate basic input validation code which is the same for every Read*().
// 'parseAction' is a code block to execute if the attribute is found.
//
#define XML_READ_SERIALIZER_PARSE_XATTR(parseAction)                      \
    assert(label != nullptr);                                             \
    assert(IsValidXMLElementAttrName(label));                             \
    XML_READ_SERIALIZER_VALIDATE_SELF();                                  \
    assert(currentElement != nullptr);                                    \
    if (const XMLAttribute * attr = currentElement->FindAttribute(label)) \
    {                                                                     \
        parseAction;                                                      \
    }                                                                     \
    return ErrorF("Attribute \"%s\" not found for element <%s>!", label, currentElement->name.c_str());

//
// Call this directly inside one of the Read*() methods that read an integer value
// to generate all the code needed to validate the input and read the number.
// 'integerType' is a type name, such as 'int', 'int32_t', 'uint8_t', etc.
//
#define XML_READ_SERIALIZER_PARSE_INTEGER(integerType) \
    XML_READ_SERIALIZER_PARSE_XATTR( \
    { \
        bool success; \
        const integerType v = static_cast< integerType >(ParseInt(attr->value, &success)); \
        if (success) \
        { \
            value = v; \
            return true; \
        } \
        return ErrorF("Unable to parse integer from attribute \"%s\" value \"%s\"!", \
                      label, attr->value.c_str()); \
    });

//
// Same as XML_READ_SERIALIZER_PARSE_INTEGER() but with explicit
// support for 64bit integer values.
//
#define XML_READ_SERIALIZER_PARSE_INTEGER_64(integerType) \
    XML_READ_SERIALIZER_PARSE_XATTR( \
    { \
        bool success; \
        const integerType v = static_cast< integerType >(ParseInt64(attr->value, &success)); \
        if (success) \
        { \
            value = v; \
            return true; \
        } \
        return ErrorF("Unable to parse integer (64bit) from attribute \"%s\" value \"%s\"!", \
                      label, attr->value.c_str()); \
    });

// ======================================================
// XMLReadSerializer:
// ======================================================

XMLReadSerializer::XMLReadSerializer(const DataBlob blob, const bool disallowErrors,
                                     const bool disallowWarnings, const bool throwExceptionOnError)
    : ReadSerializer(disallowErrors, disallowWarnings, throwExceptionOnError)
    , currentElement(nullptr)
    , savedErrorCount(0)
{
    rootNode = BuildXMLTree(blob);
    if (rootNode == nullptr)
    {
        ErrorF("Provided XML data was not valid! Couldn't build XML tree.");
    }
    else
    {
        currentElement = rootNode.get();
    }
}

XMLReadSerializer::XMLReadSerializer(XMLReader<char> & xmlReader, const bool disallowErrors,
                                     const bool disallowWarnings, const bool throwExceptionOnError)
    : ReadSerializer(disallowErrors, disallowWarnings, throwExceptionOnError)
    , currentElement(nullptr)
    , savedErrorCount(0)
{
    rootNode = BuildXMLTree(xmlReader);
    if (rootNode == nullptr)
    {
        ErrorF("Provided XMLReader instance was not in a valid state! Couldn't build XML tree.");
    }
    else
    {
        currentElement = rootNode.get();
    }
}

bool XMLReadSerializer::IsValid() const
{
    return rootNode != nullptr;
}

bool XMLReadSerializer::BeginBlock(const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));

    XML_READ_SERIALIZER_VALIDATE_SELF();

    assert(currentElement != nullptr);
    if ((currentElement == rootNode.get()) && (currentElement->name == label)) // This is the root. Do nothing.
    {
        return true;
    }

    const XMLElement * element = currentElement->FindChild(label);
    if (element == nullptr)
    {
        return ErrorF("XML element/tag <%s> not found!", label);
    }

    // Advance one element / save error count:
    currentElement = element;
    savedErrorCount = GetErrorCount();
    return true;
}

bool XMLReadSerializer::EndBlock(const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));

    XML_READ_SERIALIZER_VALIDATE_SELF();

    if (currentElement == nullptr)
    {
        return ErrorF("\'currentElement\' is null! Missing BeginBlock()?");
    }

    // Rollback one element:
    if (currentElement->name != label)
    {
        ErrorF("Closing tag </%s> name differs from opening tag <%s>!", label, currentElement->name.c_str());
        currentElement = currentElement->GetParent();
        if (currentElement == nullptr)
        {
            currentElement = rootNode.get();
        }
        return false;
    }
    else
    {
        currentElement = currentElement->GetParent();
        if (currentElement == nullptr)
        {
            currentElement = rootNode.get();
        }
        return true;
    }
}

bool XMLReadSerializer::BlockHadErrors() const
{
    // The error count never decrements.
    // savedErrorCount is the current error count
    // saved at every new block/tag. If this method
    // is called at the end of a block and the current
    // error count differs from the value remembered
    // when the block/tag was opened, the block had errors in it.
    return savedErrorCount != GetErrorCount();
}

bool XMLReadSerializer::ReadChar(char & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        if (!attr->value.empty())
        {
            value = attr->value[0];
        }
        else
        {
            value = '\0';
        }
        return true;
    });
}

bool XMLReadSerializer::ReadBool(bool & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        if ((attr->value == "yes") || (attr->value == "true") || (attr->value == "1"))
        {
            value = true;
            return true;
        }
        if ((attr->value == "no") || (attr->value == "false") || (attr->value == "0"))
        {
            value = false;
            return true;
        }
        return ErrorF("Value \"%s\" from attribute \"%s\" is not convertible to a boolean value!",
                      attr->value.c_str(), label);
    });
}

bool XMLReadSerializer::ReadWideChar(wchar_t & value, const char * label)
{
    // Currently we don't have support for wide strings,
    // so convert directly from multi-byte to wchar_t:
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        if (!attr->value.empty())
        {
            value = static_cast<wchar_t>(attr->value[0]);
        }
        else
        {
            value = L'\0';
        }
        return true;
    });
}

bool XMLReadSerializer::ReadInt8(int8_t & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_INTEGER(int8_t);
}

bool XMLReadSerializer::ReadUInt8(uint8_t & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_INTEGER(uint8_t);
}

bool XMLReadSerializer::ReadInt16(int16_t & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_INTEGER(int16_t);
}

bool XMLReadSerializer::ReadUInt16(uint16_t & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_INTEGER(uint16_t);
}

bool XMLReadSerializer::ReadInt32(int32_t & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_INTEGER(int32_t);
}

bool XMLReadSerializer::ReadUInt32(uint32_t & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_INTEGER(uint32_t);
}

bool XMLReadSerializer::ReadInt64(int64_t & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_INTEGER_64(int64_t);
}

bool XMLReadSerializer::ReadUInt64(uint64_t & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_INTEGER_64(uint64_t);
}

bool XMLReadSerializer::ReadFloat(float & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        bool success;
        const float v = ParseFloat(attr->value, &success);
        if (success)
        {
            value = v;
            return true;
        }
        return ErrorF("Unable to parse float from attribute \"%s\" value \"%s\"!", label, attr->value.c_str());
    });
}

bool XMLReadSerializer::ReadDouble(double & value, const char * label)
{
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        bool success;
        const double v = ParseDouble(attr->value, &success);
        if (success)
        {
            value = v;
            return true;
        }
        return ErrorF("Unable to parse double from attribute \"%s\" value \"%s\"!", label, attr->value.c_str());
    });
}

bool XMLReadSerializer::ReadString(std::string & str, const char * label)
{
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        str = attr->value;
        return true;
    });
}

bool XMLReadSerializer::ReadString(char * str, const size_t maxChars, const char * label)
{
    assert(str != nullptr);
    assert(maxChars != 0);

    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        if (!attr->value.empty())
        {
            CopyCString(str, maxChars, attr->value.c_str());
        }
        else
        {
            ZeroArray(str, maxChars);
        }
        return true;
    });
}

bool XMLReadSerializer::ReadQuat(Quat & q, const char * label)
{
    // Format: "Quat(x, y, z, w)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        float tmp[4];
        if (!StringStartsWith(attr->value, "Quat") || !ParseFloatTuple(attr->value.substr(4), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Quaternion!",
                          attr->value.c_str(), label);
        }
        q = Quat(tmp[0], tmp[1], tmp[2], tmp[3]);
        return true;
    });
}

bool XMLReadSerializer::ReadPoint3(Point3 & p, const char * label)
{
    // Format: "Point3(x, y, z)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        float tmp[3];
        if (!StringStartsWith(attr->value, "Point3") || !ParseFloatTuple(attr->value.substr(6), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Point3!",
                          attr->value.c_str(), label);
        }
        p = Point3(tmp[0], tmp[1], tmp[2]);
        return true;
    });
}

bool XMLReadSerializer::ReadVector3(Vector3 & v, const char * label)
{
    // Format: "Vector3(x, y, z)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        float tmp[3];
        if (!StringStartsWith(attr->value, "Vector3") || !ParseFloatTuple(attr->value.substr(7), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vector3!",
                          attr->value.c_str(), label);
        }
        v = Vector3(tmp[0], tmp[1], tmp[2]);
        return true;
    });
}

bool XMLReadSerializer::ReadVector4(Vector4 & v, const char * label)
{
    // Format: "Vector4(x, y, z, w)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        float tmp[4];
        if (!StringStartsWith(attr->value, "Vector4") || !ParseFloatTuple(attr->value.substr(7), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vector4!",
                          attr->value.c_str(), label);
        }
        v = Vector4(tmp[0], tmp[1], tmp[2], tmp[3]);
        return true;
    });
}

bool XMLReadSerializer::ReadMatrix4(Matrix4 & m, const char * label)
{
    // Format: "Matrix4{ (r0.x, r0.y, r0.z, r0.w), (r1.x, r1.y, r1.z, r1.w), (r2.x, r2.y, r2.z, r2.w), (r3.x, r3.y, r3.z, r3.w) }"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        float row[4][4];
        const int got = std::sscanf(attr->value.c_str(),
            "Matrix4{ (%f, %f, %f, %f), (%f, %f, %f, %f), (%f, %f, %f, %f), (%f, %f, %f, %f) }",
            &row[0][0], &row[0][1], &row[0][2], &row[0][3],
            &row[1][0], &row[1][1], &row[1][2], &row[1][3],
            &row[2][0], &row[2][1], &row[2][2], &row[2][3],
            &row[3][0], &row[3][1], &row[3][2], &row[3][3]);

        if (got != 16)
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Matrix4!",
                          attr->value.c_str(), label);
        }

        m.setRow(0, Vector4(row[0][0], row[0][1], row[0][2], row[0][3]));
        m.setRow(1, Vector4(row[1][0], row[1][1], row[1][2], row[1][3]));
        m.setRow(2, Vector4(row[2][0], row[2][1], row[2][2], row[2][3]));
        m.setRow(3, Vector4(row[3][0], row[3][1], row[3][2], row[3][3]));
        return true;
    });
}

bool XMLReadSerializer::ReadMatrix3(Matrix3 & m, const char * label)
{
    // Format: "Matrix3{ (r0.x, r0.y, r0.z), (r1.x, r1.y, r1.z), (r2.x, r2.y, r2.z) }"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        float row[3][3];
        const int got = std::sscanf(attr->value.c_str(),
            "Matrix3{ (%f, %f, %f), (%f, %f, %f), (%f, %f, %f) }",
            &row[0][0], &row[0][1], &row[0][2],
            &row[1][0], &row[1][1], &row[1][2],
            &row[2][0], &row[2][1], &row[2][2]);

        if (got != 9)
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Matrix3!",
                          attr->value.c_str(), label);
        }

        m.setRow(0, Vector3(row[0][0], row[0][1], row[0][2]));
        m.setRow(1, Vector3(row[1][0], row[1][1], row[1][2]));
        m.setRow(2, Vector3(row[2][0], row[2][1], row[2][2]));
        return true;
    });
}

bool XMLReadSerializer::ReadVec2(Vec2f & v, const char * label)
{
    // Format: "Vec2f(x, y)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        float tmp[2];
        if (!StringStartsWith(attr->value, "Vec2f") || !ParseFloatTuple(attr->value.substr(5), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vec2f!",
                          attr->value.c_str(), label);
        }
        v = Vec2f(tmp[0], tmp[1]);
        return true;
    });
}

bool XMLReadSerializer::ReadVec3(Vec3f & v, const char * label)
{
    // Format: "Vec3f(x, y, z)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        float tmp[3];
        if (!StringStartsWith(attr->value, "Vec3f") || !ParseFloatTuple(attr->value.substr(5), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vec3f!",
                          attr->value.c_str(), label);
        }
        v = Vec3f(tmp[0], tmp[1], tmp[2]);
        return true;
    });
}

bool XMLReadSerializer::ReadVec4(Vec4f & v, const char * label)
{
    // Format: "Vec4f(x, y, z, w)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        float tmp[4];
        if (!StringStartsWith(attr->value, "Vec4f") || !ParseFloatTuple(attr->value.substr(5), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vec4f!",
                          attr->value.c_str(), label);
        }
        v = Vec4f(tmp[0], tmp[1], tmp[2], tmp[3]);
        return true;
    });
}

bool XMLReadSerializer::ReadVec2(Vec2i & v, const char * label)
{
    // Format: "Vec2i(x, y)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        int tmp[2];
        if (!StringStartsWith(attr->value, "Vec2i") || !ParseIntTuple(attr->value.substr(5), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vec2i!",
                          attr->value.c_str(), label);
        }
        v = Vec2i(tmp[0], tmp[1]);
        return true;
    });
}

bool XMLReadSerializer::ReadVec3(Vec3i & v, const char * label)
{
    // Format: "Vec3i(x, y, z)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        int tmp[3];
        if (!StringStartsWith(attr->value, "Vec3i") || !ParseIntTuple(attr->value.substr(5), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vec3i!",
                          attr->value.c_str(), label);
        }
        v = Vec3i(tmp[0], tmp[1], tmp[2]);
        return true;
    });
}

bool XMLReadSerializer::ReadVec4(Vec4i & v, const char * label)
{
    // Format: "Vec4i(x, y, z, w)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        int tmp[4];
        if (!StringStartsWith(attr->value, "Vec4i") || !ParseIntTuple(attr->value.substr(5), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vec4i!",
                          attr->value.c_str(), label);
        }
        v = Vec4i(tmp[0], tmp[1], tmp[2], tmp[3]);
        return true;
    });
}

bool XMLReadSerializer::ReadVec2(Vec2u & v, const char * label)
{
    // Format: "Vec2u(x, y)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        int tmp[2];
        if (!StringStartsWith(attr->value, "Vec2u") || !ParseIntTuple(attr->value.substr(5), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vec2u!",
                          attr->value.c_str(), label);
        }
        v = Vec2u(tmp[0], tmp[1]);
        return true;
    });
}

bool XMLReadSerializer::ReadVec3(Vec3u & v, const char * label)
{
    // Format: "Vec3u(x, y, z)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        int tmp[3];
        if (!StringStartsWith(attr->value, "Vec3u") || !ParseIntTuple(attr->value.substr(5), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vec3u!",
                          attr->value.c_str(), label);
        }
        v = Vec3u(tmp[0], tmp[1], tmp[2]);
        return true;
    });
}

bool XMLReadSerializer::ReadVec4(Vec4u & v, const char * label)
{
    // Format: "Vec4u(x, y, z, w)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        int tmp[4];
        if (!StringStartsWith(attr->value, "Vec4u") || !ParseIntTuple(attr->value.substr(5), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Vec4u!",
                          attr->value.c_str(), label);
        }
        v = Vec4u(tmp[0], tmp[1], tmp[2], tmp[3]);
        return true;
    });
}

bool XMLReadSerializer::ReadColor(Color2b & c, const char * label)
{
    // Format: "Color2b(r, g, b)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        int tmp[2];
        if (!StringStartsWith(attr->value, "Color2b") || !ParseIntTuple(attr->value.substr(7), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Color2b!",
                          attr->value.c_str(), label);
        }
        c = Color2b(static_cast<uint8_t>(tmp[0]), static_cast<uint8_t>(tmp[1]));
        return true;
    });
}

bool XMLReadSerializer::ReadColor(Color3b & c, const char * label)
{
    // Format: "Color3b(r, g, b)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        int tmp[3];
        if (!StringStartsWith(attr->value, "Color3b") || !ParseIntTuple(attr->value.substr(7), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Color3b!",
                          attr->value.c_str(), label);
        }
        c = Color3b(static_cast<uint8_t>(tmp[0]), static_cast<uint8_t>(tmp[1]), static_cast<uint8_t>(tmp[2]));
        return true;
    });
}

bool XMLReadSerializer::ReadColor(Color4b & c, const char * label)
{
    // Format: "Color4b(r, g, b, a)"
    XML_READ_SERIALIZER_PARSE_XATTR(
    {
        int tmp[4];
        if (!StringStartsWith(attr->value, "Color4b") || !ParseIntTuple(attr->value.substr(7), tmp, ArrayLength(tmp)))
        {
            return ErrorF("Value \"%s\" from attribute \"%s\" cannot be parsed as a Color4b!",
                          attr->value.c_str(), label);
        }
        c = Color4b(static_cast<uint8_t>(tmp[0]), static_cast<uint8_t>(tmp[1]),
                    static_cast<uint8_t>(tmp[2]), static_cast<uint8_t>(tmp[3]));
        return true;
    });
}

size_t XMLReadSerializer::ReadBytes(void * destBuffer, const size_t numBytesToRead)
{
    assert(destBuffer != nullptr);
    assert(numBytesToRead != 0);

    XML_READ_SERIALIZER_VALIDATE_SELF();
    assert(currentElement != nullptr);

    const XMLElement * byteBufferElement = currentElement->FindChild(rawByteBufferTagName);
    if (byteBufferElement == nullptr)
    {
        return ErrorF("No <%s> child element found on current XML element!", rawByteBufferTagName);
    }

    // Not considered an error, but still outputs a warning.
    if (!byteBufferElement->HasText())
    {
        WarningF("<%s> element has no data!", rawByteBufferTagName);
        std::memset(destBuffer, 0, numBytesToRead); // Clear the output buffer.
        return true;
    }

    // Parse the hexadecimal string.
    // Format:
    //   "1a, 1b, 1c, 1d, 1e, 1f, 20, 21, ..."
    //
    size_t bytesRead = 0;
    uint8_t * output = reinterpret_cast<uint8_t *>(destBuffer);
    for (const char * ptr = byteBufferElement->Text(); ((*ptr != '\0') && (bytesRead < numBytesToRead)); ++ptr)
    {
        if (std::isxdigit(*ptr))
        {
            char * endptr = nullptr;
            const int num = static_cast<int>(std::strtol(ptr, &endptr, 16));
            if (endptr > ptr)
            {
                output[bytesRead++] = static_cast<uint8_t>(num);
                ptr = endptr;
                continue;
            }
            else
            {
                return ErrorF("Problems while parsing <%s> element!", rawByteBufferTagName);
            }
        }
    }

    return true;
}

// Clear the namespace:
#undef XML_READ_SERIALIZER_VALIDATE_SELF
#undef XML_READ_SERIALIZER_PARSE_XATTR
#undef XML_READ_SERIALIZER_PARSE_INTEGER
#undef XML_READ_SERIALIZER_PARSE_INTEGER_64

// ======================================================
// XMLWriteSerializer:
// ======================================================

XMLWriteSerializer::XMLWriteSerializer(FILE * anOpenfile, const bool disallowErrors, const bool disallowWarnings, const bool throwExceptionOnError)
    : WriteSerializer(disallowErrors, disallowWarnings, throwExceptionOnError)
    , indent(0)
    , savedErrorCount(0)
    , emptyElement(false)
    , openElement(false)
    , file(anOpenfile)
{
    if ((anOpenfile == nullptr) || std::ferror(anOpenfile))
    {
        ErrorF("Input file stream is invalid!");
        file = nullptr;
    }
    else
    {
        // Add default XML version tag:
        XMLPrintF("<?xml version=\"1.0\"?>\n");
    }
}

bool XMLWriteSerializer::IsValid() const
{
    return (file != nullptr) && !std::ferror(file);
}

void XMLWriteSerializer::XMLPrintIndent()
{
    if (!IsValid())
    {
        ErrorF("XML file handle is not valid!");
        return; // File never opened
    }

    for (int i = 0; i < indent; ++i)
    {
        if (std::fputc('\t', file) != '\t')
        {
            ErrorF("Failed to indent XML with fputc()!");
        }
    }
}

bool XMLWriteSerializer::XMLPrintF(const char * fmt, ...)
{
    if (!IsValid())
    {
        return ErrorF("XML file handle is not valid!");
    }

    va_list vaList;
    va_start(vaList, fmt);
    const int result = std::vfprintf(file, fmt, vaList);
    va_end(vaList);

    if (result < 0)
    {
        return ErrorF("Failed to print XML text with vfprintf()! result = %d", result);
    }

    return true;
}

bool XMLWriteSerializer::BeginBlock(const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));

    currentElementName = label;

    // If nesting an element, close the current one:
    if (openElement)
    {
        if (emptyElement)
        {
            if (!XMLPrintF(" />\n"))
            {
                WarningF("Failed to open XML tag/block!");
                return false;
            }
        }
        else
        {
            if (!XMLPrintF(">\n"))
            {
                WarningF("Failed to open XML tag/block!");
                return false;
            }
        }
    }

    // Increase indent level:
    XMLPrintIndent();
    ++indent;

    // Open new element tag:
    openElement = true;
    savedErrorCount = GetErrorCount();
    return XMLPrintF("<%s", label);
}

bool XMLWriteSerializer::EndBlock(const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));

    if (currentElementName == label)
    {
        emptyElement = true;
    }
    else
    {
        emptyElement = false;
    }

    // Decrease indent and close element tag:
    --indent;
    openElement = false;
    if (emptyElement)
    {
        return XMLPrintF(" />\n");
    }
    else
    {
        XMLPrintIndent();
        return XMLPrintF("</%s>\n", label);
    }
}

bool XMLWriteSerializer::BlockHadErrors() const
{
    // The error count never decrements.
    // savedErrorCount is the current error count
    // saved at every new block/tag. If this method
    // is called at the end of a block and the current
    // error count differs from the value remembered
    // when the block/tag was opened, the block had errors in it.
    return savedErrorCount != GetErrorCount();
}

bool XMLWriteSerializer::WriteChar(const char value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%c\"", label, value);
}

bool XMLWriteSerializer::WriteBool(const bool value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%s\"", label, (value ? xmlBoolTrue : xmlBoolFalse));
}

bool XMLWriteSerializer::WriteWideChar(const wchar_t value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%lc\"", label, value);
}

bool XMLWriteSerializer::WriteInt8(const int8_t value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%i\"", label, static_cast<int>(value));
}

bool XMLWriteSerializer::WriteUInt8(const uint8_t value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%u\"", label, static_cast<unsigned int>(value));
}

bool XMLWriteSerializer::WriteInt16(const int16_t value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%i\"", label, static_cast<int>(value));
}

bool XMLWriteSerializer::WriteUInt16(const uint16_t value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%u\"", label, static_cast<unsigned int>(value));
}

bool XMLWriteSerializer::WriteInt32(const int32_t value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%i\"", label, value);
}

bool XMLWriteSerializer::WriteUInt32(const uint32_t value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%u\"", label, value);
}

bool XMLWriteSerializer::WriteInt64(const int64_t value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%lli\"", label, value);
}

bool XMLWriteSerializer::WriteUInt64(const uint64_t value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%llu\"", label, value);
}

bool XMLWriteSerializer::WriteFloat(const float value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    char floatStr[TempCharBufMax];
    SNPrintF(floatStr, "%f", value);
    return XMLPrintF(" %s=\"%s\"", label, TrimFloatStr(floatStr));
}

bool XMLWriteSerializer::WriteDouble(const double value, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    char floatStr[TempCharBufMax];
    SNPrintF(floatStr, "%lf", value);
    return XMLPrintF(" %s=\"%s\"", label, TrimFloatStr(floatStr));
}

bool XMLWriteSerializer::WriteString(const std::string & str, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"%s\"", label, str.c_str());
}

bool XMLWriteSerializer::WriteString(const char * str, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    assert(str != nullptr);
    return XMLPrintF(" %s=\"%s\"", label, str);
}

bool XMLWriteSerializer::WriteQuat(const Quat & q, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    char tempStr[TempCharBufMax];
    return XMLPrintF(" %s=\"Quat(%s)\"", label, FloatTupleToStr(tempStr, ToFloatPtr(q), 4));
}

bool XMLWriteSerializer::WritePoint3(const Point3 & p, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    char tempStr[TempCharBufMax];
    return XMLPrintF(" %s=\"Point3(%s)\"", label, FloatTupleToStr(tempStr, ToFloatPtr(p), 3));
}

bool XMLWriteSerializer::WriteVector3(const Vector3 & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    char tempStr[TempCharBufMax];
    return XMLPrintF(" %s=\"Vector3(%s)\"", label, FloatTupleToStr(tempStr, ToFloatPtr(v), 3));
}

bool XMLWriteSerializer::WriteVector4(const Vector4 & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    char tempStr[TempCharBufMax];
    return XMLPrintF(" %s=\"Vector4(%s)\"", label, FloatTupleToStr(tempStr, ToFloatPtr(v), 4));
}

bool XMLWriteSerializer::WriteMatrix4(const Matrix4 & m, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));

    // XML output format:
    //
    //   "Matrix4{ (r0.x, r0.y, r0.z, r0.w), (r1.x, r1.y, r1.z, r1.w), (r2.x, r2.y, r2.z, r2.w), (r3.x, r3.y, r3.z, r3.w) }"
    //
    // Where each vector/tuple is a row of the matrix.
    //
    //   | r0.x, r0.y, r0.z, r0.w |
    //   | r1.x, r1.y, r1.z, r1.w |
    //   | r2.x, r2.y, r2.z, r2.w |
    //   | r3.x, r3.y, r3.z, r3.w |
    //
    char tempStr[4][TempCharBufMax];
    const Vector4 r0 = m.getRow(0);
    const Vector4 r1 = m.getRow(1);
    const Vector4 r2 = m.getRow(2);
    const Vector4 r3 = m.getRow(3);
    return XMLPrintF(" %s=\"Matrix4{ (%s), (%s), (%s), (%s) }\"", label,
                     FloatTupleToStr(tempStr[0], ToFloatPtr(r0), 4),
                     FloatTupleToStr(tempStr[1], ToFloatPtr(r1), 4),
                     FloatTupleToStr(tempStr[2], ToFloatPtr(r2), 4),
                     FloatTupleToStr(tempStr[3], ToFloatPtr(r3), 4));
}

bool XMLWriteSerializer::WriteMatrix3(const Matrix3 & m, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));

    // XML output format:
    //
    //   "Matrix3{ (r0.x, r0.y, r0.z), (r1.x, r1.y, r1.z), (r2.x, r2.y, r2.z) }"
    //
    // Where each vector/tuple is a row of the matrix.
    //
    //   | r0.x, r0.y, r0.z |
    //   | r1.x, r1.y, r1.z |
    //   | r2.x, r2.y, r2.z |
    //
    char tempStr[3][TempCharBufMax];
    const Vector3 r0 = m.getRow(0);
    const Vector3 r1 = m.getRow(1);
    const Vector3 r2 = m.getRow(2);
    return XMLPrintF(" %s=\"Matrix3{ (%s), (%s), (%s) }\"", label,
                     FloatTupleToStr(tempStr[0], ToFloatPtr(r0), 3),
                     FloatTupleToStr(tempStr[1], ToFloatPtr(r1), 3),
                     FloatTupleToStr(tempStr[2], ToFloatPtr(r2), 3));
}

bool XMLWriteSerializer::WriteVec2(const Vec2f & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    char tempStr[TempCharBufMax];
    return XMLPrintF(" %s=\"Vec2f(%s)\"", label, FloatTupleToStr(tempStr, ToFloatPtr(v), 2));
}

bool XMLWriteSerializer::WriteVec3(const Vec3f & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    char tempStr[TempCharBufMax];
    return XMLPrintF(" %s=\"Vec3f(%s)\"", label, FloatTupleToStr(tempStr, ToFloatPtr(v), 3));
}

bool XMLWriteSerializer::WriteVec4(const Vec4f & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    char tempStr[TempCharBufMax];
    return XMLPrintF(" %s=\"Vec4f(%s)\"", label, FloatTupleToStr(tempStr, ToFloatPtr(v), 4));
}

bool XMLWriteSerializer::WriteVec2(const Vec2i & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"Vec2i(%i, %i)\"", label, v.x, v.y);
}

bool XMLWriteSerializer::WriteVec3(const Vec3i & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"Vec3i(%i, %i, %i)\"", label, v.x, v.y, v.z);
}

bool XMLWriteSerializer::WriteVec4(const Vec4i & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"Vec4i(%i, %i, %i, %i)\"", label, v.x, v.y, v.z, v.w);
}

bool XMLWriteSerializer::WriteVec2(const Vec2u & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"Vec2u(%u, %u)\"", label, v.x, v.y);
}

bool XMLWriteSerializer::WriteVec3(const Vec3u & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"Vec3u(%u, %u, %u)\"", label, v.x, v.y, v.z);
}

bool XMLWriteSerializer::WriteVec4(const Vec4u & v, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"Vec4u(%u, %u, %u, %u)\"", label, v.x, v.y, v.z, v.w);
}

bool XMLWriteSerializer::WriteColor(const Color2b & c, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"Color2b(%u, %u)\"", label,
                     static_cast<unsigned int>(c.r),
                     static_cast<unsigned int>(c.g));
}

bool XMLWriteSerializer::WriteColor(const Color3b & c, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"Color3b(%u, %u, %u)\"", label,
                     static_cast<unsigned int>(c.r),
                     static_cast<unsigned int>(c.g),
                     static_cast<unsigned int>(c.b));
}

bool XMLWriteSerializer::WriteColor(const Color4b & c, const char * label)
{
    assert(label != nullptr);
    assert(IsValidXMLElementAttrName(label));
    return XMLPrintF(" %s=\"Color4b(%u, %u, %u, %u)\"", label,
                     static_cast<unsigned int>(c.r),
                     static_cast<unsigned int>(c.g),
                     static_cast<unsigned int>(c.b),
                     static_cast<unsigned int>(c.a));
}

size_t XMLWriteSerializer::WriteBytes(const void * srcBuffer, const size_t numBytesToWrite)
{
    assert(srcBuffer != nullptr);
    assert(numBytesToWrite != 0);

    // Max length (in hexadecimal byte words) of a line before breaking it:
    constexpr int MaxBytesPerLine = 25;

    // Open another child tag that will belong to the current tag:
    BeginBlock(rawByteBufferTagName);

    // But close the tag '>' immediately:
    openElement = false;
    emptyElement = false;
    currentElementName.clear();
    XMLPrintF(">\n");

    // Build a tabs string for proper indentation:
    size_t lvl = 0;
    char indentStr[TempCharBufMax];
    for (; ((lvl < static_cast<size_t>(indent)) && (lvl < ArrayLength(indentStr) - 1)); ++lvl)
    {
        indentStr[lvl] = '\t';
    }
    indentStr[lvl] = '\0';

    // Locals:
    const uint8_t * bytes = reinterpret_cast<const uint8_t *>(srcBuffer);
    size_t bytesWritten = 0;
    int bytesInCurrentLine = 0;

    // Write every byte as an hexadecimal word,
    // in a comma separated list:
    XMLPrintF("%s", indentStr);
    for (size_t i = 0; i < numBytesToWrite; ++i)
    {
        if (!XMLPrintF("%02x", static_cast<unsigned int>(bytes[i])))
        {
            // Stop if an error occurs.
            break;
        }

        ++bytesWritten;

        if (i != (numBytesToWrite - 1))
        {
            if (bytesInCurrentLine < MaxBytesPerLine)
            {
                ++bytesInCurrentLine;
                XMLPrintF(", ");
            }
            else
            {
                bytesInCurrentLine = 0;
                XMLPrintF(",\n%s", indentStr);
            }
        }
        else
        {
            XMLPrintF("\n");
        }
    }

    EndBlock(rawByteBufferTagName);
    return bytesWritten;
}

// ======================================================
// Unit Tests:
// ======================================================

#if ENGINE_BUILD_UNIT_TESTS

//
// Helper macros used locally for testing:
//
#define TEST_ASSERT assert

#define TEST_ASSERT_VEC2_EQ(a, b)     \
    TEST_ASSERT(((a)[0] == (b)[0]) && \
                ((a)[1] == (b)[1]))

#define TEST_ASSERT_VEC3_EQ(a, b)     \
    TEST_ASSERT(((a)[0] == (b)[0]) && \
                ((a)[1] == (b)[1]) && \
                ((a)[2] == (b)[2]))

#define TEST_ASSERT_VEC4_EQ(a, b)     \
    TEST_ASSERT(((a)[0] == (b)[0]) && \
                ((a)[1] == (b)[1]) && \
                ((a)[2] == (b)[2]) && \
                ((a)[3] == (b)[3]))

//
// File generated and referenced by the tests:
//
static const char testXMLfilename[] = "xml_serializer_test.xml";

//
// Test for XMLWriteSerializer.
// The output file is used by TestXMLReadSerializer().
// NOTE: Will attempt to write the output file to the current directory!
//
void TestXMLWriteSerializer()
{
    common->PrintF("----- TestXMLWriteSerializer() -----");

    // (Input can also be a standard stream, such as 'stdout')
    FILE * xmlFile = std::fopen(testXMLfilename, "wt");

    XMLWriteSerializer xmlSerializer(xmlFile,
                                     /* disallowErrors        = */ false,
                                     /* disallowWarnings      = */ false,
                                     /* throwExceptionOnError = */ true);
    TEST_ASSERT(xmlSerializer.IsValid() == true);

    //
    // Try every method of XMLWriteSerializer:
    //
    // NOTE: All elements must reside under a top-level
    // parent node, in this case <aaa>.
    //
    xmlSerializer.BeginBlock("aaa");
    {
        // Char/bool/strings:
        xmlSerializer.WriteChar('x', "char");
        xmlSerializer.WriteWideChar(L'$', "wchar");
        xmlSerializer.WriteBool(true, "bool");
        xmlSerializer.WriteString(std::string("Hello"), "cpp_string");
        xmlSerializer.WriteString("World", "c_string");

        xmlSerializer.BeginBlock("bbb");
        {
            // Numerical types:
            xmlSerializer.WriteInt8(-127, "i8");
            xmlSerializer.WriteUInt8(255, "u8");
            xmlSerializer.WriteInt16(-42, "i16");
            xmlSerializer.WriteUInt16(42, "u16");
            xmlSerializer.WriteInt32(-1337, "i32");
            xmlSerializer.WriteUInt32(1337, "u32");
            xmlSerializer.WriteInt64(-7671, "i64");
            xmlSerializer.WriteUInt64(7671, "u64");
            xmlSerializer.WriteFloat(3.1415f, "pi_f");
            xmlSerializer.WriteDouble(3.1415, "pi_d");

            xmlSerializer.BeginBlock("ccc");
            {
                // VectorMath types:
                xmlSerializer.WriteQuat(QuatIdentity, "q4");
                xmlSerializer.WritePoint3(Point3(1.0f, 2.0f, 3.0f), "p3");
                xmlSerializer.WriteVector3(Vector3(1.0f, 2.0f, 3.0f), "v3");
                xmlSerializer.WriteVector4(Vector4(1.0f, 2.0f, 3.0f, 4.0f), "v4");
                xmlSerializer.WriteMatrix3(Matrix3Identity, "mat3");
                xmlSerializer.WriteMatrix4(Matrix4Identity, "mat4");
            }
            xmlSerializer.EndBlock("ccc");
            TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // ccc

            xmlSerializer.BeginBlock("ddd");
            {
                // Aux vectors/colors:
                xmlSerializer.WriteVec2(Vec2f(1.0f, 2.0f), "v2f");
                xmlSerializer.WriteVec3(Vec3f(1.0f, 2.0f, 3.0f), "v3f");
                xmlSerializer.WriteVec4(Vec4f(1.0f, 2.0f, 3.0f, 4.0f), "v4f");
                xmlSerializer.WriteVec2(Vec2i(-1, 2), "v2i");
                xmlSerializer.WriteVec3(Vec3i(-1, 2, 3), "v3i");
                xmlSerializer.WriteVec4(Vec4i(-1, 2, 3, 4), "v4i");
                xmlSerializer.WriteVec2(Vec2u(1, 2), "v2u");
                xmlSerializer.WriteVec3(Vec3u(1, 2, 3), "v3u");
                xmlSerializer.WriteVec4(Vec4u(1, 2, 3, 4), "v4u");
                xmlSerializer.WriteColor(Color2b(127, 255), "rg");
                xmlSerializer.WriteColor(Color3b(0, 127, 255), "rgb");
                xmlSerializer.WriteColor(Color4b(0, 64, 127, 255), "rgba");
            }
            xmlSerializer.EndBlock("ddd");
            TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // ddd
        }
        xmlSerializer.EndBlock("bbb");
        TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // bbb

        // Write some raw bytes:
        xmlSerializer.BeginBlock("eee");
        {
            uint8_t bytes[256];

            // Init with value range from 0 to 255:
            std::iota(std::begin(bytes), std::end(bytes), uint8_t(0));

            // Print:
            xmlSerializer.WriteBytes(bytes, sizeof(bytes));
        }
        xmlSerializer.EndBlock("eee");
        TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // eee
    }
    xmlSerializer.EndBlock("aaa");
    TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // aaa

    //
    // XMLWriteSerializer doe not assume ownership of the file.
    // We need to close it when the serializer is done.
    //
    std::fclose(xmlFile);

    common->PrintF("TestXMLWriteSerializer() completed successfully!");
}

//
// Test for XMLReadSerializer.
// Uses the file generated by TestXMLWriteSerializer() as input.
//
void TestXMLReadSerializer()
{
    common->PrintF("----- TestXMLReadSerializer() ------");

    DataBlob fileContents = DataBlob::LoadFile(testXMLfilename, /* isBinary = */ false);
    SCOPE_EXIT({ fileContents.FreeData(); });

    XMLReadSerializer xmlSerializer(fileContents,
                                    /* disallowErrors        = */ false,
                                    /* disallowWarnings      = */ false,
                                    /* throwExceptionOnError = */ true);
    TEST_ASSERT(xmlSerializer.IsValid() == true);

    //
    // Read all the elements/attributes that
    // XMLWriteSerializer wrote:
    //
    xmlSerializer.BeginBlock("aaa");
    {
        // Read char/bool/strings:

        char c;
        xmlSerializer.ReadChar(c, "char");
        TEST_ASSERT(c == 'x');

        wchar_t wc;
        xmlSerializer.ReadWideChar(wc, "wchar");
        TEST_ASSERT(wc == '$');

        bool b;
        xmlSerializer.ReadBool(b, "bool");
        TEST_ASSERT(b == true);

        std::string cppStr;
        xmlSerializer.ReadString(cppStr, "cpp_string");
        TEST_ASSERT(cppStr == "Hello");

        char cStr[256];
        xmlSerializer.ReadString(cStr, ArrayLength(cStr), "c_string");
        TEST_ASSERT(std::strcmp(cStr, "World") == 0);

        xmlSerializer.BeginBlock("bbb");
        {
            // Read the numerical types:

            int8_t i8;
            xmlSerializer.ReadInt8(i8, "i8");
            TEST_ASSERT(i8 == -127);

            uint8_t u8;
            xmlSerializer.ReadUInt8(u8, "u8");
            TEST_ASSERT(u8 == 255);

            int16_t i16;
            xmlSerializer.ReadInt16(i16, "i16");
            TEST_ASSERT(i16 == -42);

            uint16_t u16;
            xmlSerializer.ReadUInt16(u16, "u16");
            TEST_ASSERT(u16 == 42);

            int32_t i32;
            xmlSerializer.ReadInt32(i32, "i32");
            TEST_ASSERT(i32 == -1337);

            uint32_t u32;
            xmlSerializer.ReadUInt32(u32, "u32");
            TEST_ASSERT(u32 == 1337);

            int64_t i64;
            xmlSerializer.ReadInt64(i64, "i64");
            TEST_ASSERT(i64 == -7671);

            uint64_t u64;
            xmlSerializer.ReadUInt64(u64, "u64");
            TEST_ASSERT(u64 == 7671);

            float pi_f;
            xmlSerializer.ReadFloat(pi_f, "pi_f");
            TEST_ASSERT(pi_f == 3.1415f);

            double pi_d;
            xmlSerializer.ReadDouble(pi_d, "pi_d");
            TEST_ASSERT(pi_d == 3.1415);

            xmlSerializer.BeginBlock("ccc");
            {
                // Read the VectorMath types:
                Quat q4;
                xmlSerializer.ReadQuat(q4, "q4");
                TEST_ASSERT_VEC4_EQ(q4, QuatIdentity);

                Point3 p3;
                const Point3 p3Expected(1.0f, 2.0f, 3.0f);
                xmlSerializer.ReadPoint3(p3, "p3");
                TEST_ASSERT_VEC3_EQ(p3, p3Expected);

                Vector3 v3;
                const Vector3 v3Expected(1.0f, 2.0f, 3.0f);
                xmlSerializer.ReadVector3(v3, "v3");
                TEST_ASSERT_VEC3_EQ(v3, v3Expected);

                Vector4 v4;
                const Vector4 v4Expected(1.0f, 2.0f, 3.0f, 4.0f);
                xmlSerializer.ReadVector4(v4, "v4");
                TEST_ASSERT_VEC4_EQ(v4, v4Expected);

                // Matrices:
                {
                    Matrix3 mat3;
                    xmlSerializer.ReadMatrix3(mat3, "mat3");
                    TEST_ASSERT_VEC3_EQ(mat3.getRow(0), Vector3(1.0f, 0.0f, 0.0f));
                    TEST_ASSERT_VEC3_EQ(mat3.getRow(1), Vector3(0.0f, 1.0f, 0.0f));
                    TEST_ASSERT_VEC3_EQ(mat3.getRow(2), Vector3(0.0f, 0.0f, 1.0f));
                }
                {
                    Matrix4 mat4;
                    xmlSerializer.ReadMatrix4(mat4, "mat4");
                    TEST_ASSERT_VEC4_EQ(mat4.getRow(0), Vector4(1.0f, 0.0f, 0.0f, 0.0f));
                    TEST_ASSERT_VEC4_EQ(mat4.getRow(1), Vector4(0.0f, 1.0f, 0.0f, 0.0f));
                    TEST_ASSERT_VEC4_EQ(mat4.getRow(2), Vector4(0.0f, 0.0f, 1.0f, 0.0f));
                    TEST_ASSERT_VEC4_EQ(mat4.getRow(3), Vector4(0.0f, 0.0f, 0.0f, 1.0f));
                }
            }
            xmlSerializer.EndBlock("ccc");
            TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // ccc

            xmlSerializer.BeginBlock("ddd");
            {
                // Read the aux vectors/colors:

                // ---- VecXf ----
                {
                    Vec2f v2f;
                    const Vec2f v2fExpected(1.0f, 2.0f);
                    xmlSerializer.ReadVec2(v2f, "v2f");
                    TEST_ASSERT_VEC2_EQ(v2f, v2fExpected);

                    Vec3f v3f;
                    const Vec3f v3fExpected(1.0f, 2.0f, 3.0f);
                    xmlSerializer.ReadVec3(v3f, "v3f");
                    TEST_ASSERT_VEC3_EQ(v3f, v3fExpected);

                    Vec4f v4f;
                    const Vec4f v4fExpected(1.0f, 2.0f, 3.0f, 4.0f);
                    xmlSerializer.ReadVec4(v4f, "v4f");
                    TEST_ASSERT_VEC4_EQ(v4f, v4fExpected);
                }

                // ---- VecXi ----
                {
                    Vec2i v2i;
                    const Vec2i v2iExpected(-1, 2);
                    xmlSerializer.ReadVec2(v2i, "v2i");
                    TEST_ASSERT_VEC2_EQ(v2i, v2iExpected);

                    Vec3i v3i;
                    const Vec3i v3iExpected(-1, 2, 3);
                    xmlSerializer.ReadVec3(v3i, "v3i");
                    TEST_ASSERT_VEC3_EQ(v3i, v3iExpected);

                    Vec4i v4i;
                    const Vec4i v4iExpected(-1, 2, 3, 4);
                    xmlSerializer.ReadVec4(v4i, "v4i");
                    TEST_ASSERT_VEC4_EQ(v4i, v4iExpected);
                }

                // ---- VecXu ----
                {
                    Vec2u v2u;
                    const Vec2u v2uExpected(1, 2);
                    xmlSerializer.ReadVec2(v2u, "v2u");
                    TEST_ASSERT_VEC2_EQ(v2u, v2uExpected);

                    Vec3u v3u;
                    const Vec3u v3uExpected(1, 2, 3);
                    xmlSerializer.ReadVec3(v3u, "v3u");
                    TEST_ASSERT_VEC3_EQ(v3u, v3uExpected);

                    Vec4u v4u;
                    const Vec4u v4uExpected(1, 2, 3, 4);
                    xmlSerializer.ReadVec4(v4u, "v4u");
                    TEST_ASSERT_VEC4_EQ(v4u, v4uExpected);
                }

                // ---- ColorXb ----
                {
                    Color2b rg;
                    const Color2b rgExpected(127, 255);
                    xmlSerializer.ReadColor(rg, "rg");
                    TEST_ASSERT_VEC2_EQ(rg, rgExpected);

                    Color3b rgb;
                    const Color3b rgbExpected(0, 127, 255);
                    xmlSerializer.ReadColor(rgb, "rgb");
                    TEST_ASSERT_VEC3_EQ(rgb, rgbExpected);

                    Color4b rgba;
                    const Color4b rgbaExpected(0, 64, 127, 255);
                    xmlSerializer.ReadColor(rgba, "rgba");
                    TEST_ASSERT_VEC4_EQ(rgba, rgbaExpected);
                }
            }
            xmlSerializer.EndBlock("ddd");
            TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // ddd
        }
        xmlSerializer.EndBlock("bbb");
        TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // bbb

        // Read the raw byte buffer:
        xmlSerializer.BeginBlock("eee");
        {
            uint8_t bytesExpected[256];
            std::iota(std::begin(bytesExpected), std::end(bytesExpected), uint8_t(0));

            uint8_t bytesRead[256];
            xmlSerializer.ReadBytes(bytesRead, sizeof(bytesRead));

            // Make sure they are binary identical:
            TEST_ASSERT(std::memcmp(bytesRead, bytesExpected, 256) == 0);
        }
        xmlSerializer.EndBlock("eee");
        TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // eee
    }
    xmlSerializer.EndBlock("aaa");
    TEST_ASSERT(xmlSerializer.BlockHadErrors() == false); // aaa

    common->PrintF("TestXMLReadSerializer() completed successfully!");
}

//
// Very basic test for XMLWriter and BuildXMLTree().
// No actual validation is done. It just reads the file
// pointed by 'testXMLfilename' and writes it back to
// stdout using the XMLWriter.
//
void TestXMLWriter()
{
    common->PrintF("---------- TestXMLWriter() ---------");

    struct PrintToStdOut
    {
        static void Func(void *, const char * text)
        {
            TEST_ASSERT(text != nullptr);
            std::printf("%s", text);
        }
    };

    DataBlob fileContents = DataBlob::LoadFile(testXMLfilename, /* isBinary = */ false);
    SCOPE_EXIT(
    { fileContents.FreeData();
    });

    // Read the file, testing BuildXMLTree():
    ConstXMLRootPtr rootNode = BuildXMLTree(fileContents);

    // Now dump it to stdout.
    // We should add some validation to the read contents sometime in the future...
    //
    XMLWriter xmlWriter(&PrintToStdOut::Func, nullptr);
    xmlWriter.WriteSubtree(*rootNode);

    common->PrintF("TestXMLWriter() completed successfully!");
}

// Clear the namespace:
#undef TEST_ASSERT
#undef TEST_ASSERT_VEC2_EQ
#undef TEST_ASSERT_VEC3_EQ
#undef TEST_ASSERT_VEC4_EQ

#endif // ENGINE_BUILD_UNIT_TESTS

} // namespace Engine {}
