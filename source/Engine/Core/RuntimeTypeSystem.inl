
// ================================================================================================
// -*- C++ -*-
// File: RuntimeTypeSystem.inl
// Author: Guilherme R. Lampert
// Created on: 23/07/14
// Brief: Inline definitions for the Runtime Type System.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// ======================================================
// TypeInfo inline methods:
// ======================================================

inline Class * TypeInfo::CallCreateFunc() const
{
    assert(createInstanceFunc != nullptr);
    return createInstanceFunc();
}

inline void TypeInfo::CallDestroyFunc(Class * instance) const
{
    assert(destroyInstanceFunc != nullptr);
    destroyInstanceFunc(instance);
}

// ======================================================
// Class inline methods:
// ======================================================

inline const char * Class::GetClassName() const
{
    return GetTypeInfo().GetClassName();
}

inline const char * Class::GetSuperclassName() const
{
    return GetTypeInfo().GetSuperclassName();
}

inline HashIndex Class::GetClassHashIndex() const
{
    return GetTypeInfo().GetClassHashIndex();
}

inline HashIndex Class::GetSuperclassHashIndex() const
{
    return GetTypeInfo().GetSuperclassHashIndex();
}

inline bool Class::IsSubclassOf(const TypeInfo & typeInfo) const
{
    return GetTypeInfo().IsSubclassOf(typeInfo);
}

inline bool Class::IsSuperclassOf(const TypeInfo & typeInfo) const
{
    return GetTypeInfo().IsSuperclassOf(typeInfo);
}

inline bool Class::IsExactly(const TypeInfo & typeInfo) const
{
    return GetTypeInfo().IsExactly(typeInfo);
}

inline bool Class::IsKindOf(const TypeInfo & typeInfo) const
{
    return GetTypeInfo().IsKindOf(typeInfo);
}

// ======================================================
// Default templated instancers and deleters:
// ======================================================

template <class T>
inline T * DefaultClassInstancer()
{
    return new T;
}

template <class T>
inline T * DefaultClassInstancer(const T & copy)
{
    return new T(copy);
}

template <class T>
inline void DefaultClassDeleter(T * instance)
{
    delete instance;
}
