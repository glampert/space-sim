
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: SingleInstanceType.hpp
// Author: Guilherme R. Lampert
// Created on: 06/08/14
// Brief: Base type for classes and structs that shall not be instantiated more than once.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// SingleInstanceType template:
// ======================================================

//
// Classes derived from this type can only have a single alive instance at a time.
// The primary use of this type is to ensure the Engine singletons
// (which are global variables) are not instantiated by the user.
//
template <class Derived>
class SingleInstanceType
{
  public:

    // Constructor will increment the instance count
    // for every instantiation, triggering an assertion if the
    // instance count is greater than one.
    SingleInstanceType()
    {
        ++instanceCount;
        assert(instanceCount == 1 && "Multiple instantiation of this type is not allowed!");
    }

  protected:

    // Destructor will automatically decrement the instance count.
    ~SingleInstanceType()
    {
        --instanceCount;
        assert(instanceCount == 0 && "Corrupted instance count!");
    }

  private:

    static int instanceCount;
};

// Static shared instance counter:
template <class Derived>
int SingleInstanceType<Derived>::instanceCount(0);

} // namespace Engine {}
