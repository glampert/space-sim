
// ================================================================================================
// -*- C++ -*-
// File: XMLReader.inl
// Author: Guilherme R. Lampert
// Created on: 05/05/13
// Brief: Inline definitions for XMLReader.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

template <typename CharType>
XMLReader<CharType>::XMLReader(const DataBlob blob)
    : currentNodeType(XMLNodeType::None)
    , sourceFormat(XMLTextFormat::ASCII)
    , textChars(0)
    , textData(nullptr)
    , textBegin(nullptr)
    , ptr(nullptr)
    , isEmptyElement(false)
    , inputData(blob)
{
    if (inputData.IsValid())
    {
        InitParser();
    }
    else
    {
        common->WarningF("DataBlob provided to XML reader is not valid!");
    }
}

template <typename CharType>
bool XMLReader<CharType>::IsValid() const
{
    return inputData.IsValid() && (ptr != nullptr) && (textData != nullptr);
}

template <typename CharType>
bool XMLReader<CharType>::ReadNextNode()
{
    // If not at the end, read next node:
    if (ptr && ((ptr - textBegin) < static_cast<ptrdiff_t>(textChars - 1)) && (*ptr != 0))
    {
        ParseCurrentNode();
        return true;
    }
    return false;
}

template <typename CharType>
const typename XMLReader<CharType>::StringType & XMLReader<CharType>::NodeName() const
{
    return nodeNameData;
}

template <typename CharType>
XMLNodeType XMLReader<CharType>::NodeType() const
{
    return currentNodeType;
}

template <typename CharType>
const typename XMLReader<CharType>::StringType & XMLReader<CharType>::NodeData() const
{
    return nodeNameData;
}

template <typename CharType>
bool XMLReader<CharType>::IsEmptyElement() const
{
    return isEmptyElement;
}

template <typename CharType>
unsigned int XMLReader<CharType>::AttributeCount() const
{
    return static_cast<unsigned int>(attributes.size());
}

template <typename CharType>
const CharType * XMLReader<CharType>::AttributeName(const unsigned int idx) const
{
    if (idx < static_cast<unsigned int>(attributes.size()))
    {
        return attributes[idx].name.c_str();
    }
    return nullptr;
}

template <typename CharType>
const CharType * XMLReader<CharType>::AttributeValue(const unsigned int idx) const
{
    if (idx < static_cast<unsigned int>(attributes.size()))
    {
        return attributes[idx].value.c_str();
    }
    return nullptr;
}

template <typename CharType>
const CharType * XMLReader<CharType>::AttributeValue(const CharType * name) const
{
    assert(name != nullptr);

    const Attribute * attr = FindAttributeByName(name);
    if (attr != nullptr)
    {
        return attr->value.c_str();
    }
    return nullptr; // If no attribute with 'name' exists, return null.
}

template <typename CharType>
typename XMLReader<CharType>::StringType XMLReader<CharType>::AttributeValueSafe(const CharType * name) const
{
    assert(name != nullptr);

    const Attribute * attr = FindAttributeByName(name);
    if (attr != nullptr)
    {
        return attr->value;
    }
    return StringType();
}

template <typename CharType>
int XMLReader<CharType>::AttributeValueAsInt(const CharType * name) const
{
    assert(name != nullptr);

    const Attribute * attr = FindAttributeByName(name);
    if (attr != nullptr)
    {
        return std::stoi(attr->value);
    }
    return 0;
}

template <typename CharType>
int XMLReader<CharType>::AttributeValueAsInt(const unsigned int idx) const
{
    const CharType * attrVal = AttributeValue(idx);
    if (attrVal != nullptr)
    {
        return std::stoi(attrVal);
    }
    return 0;
}

template <typename CharType>
float XMLReader<CharType>::AttributeValueAsFloat(const CharType * name) const
{
    assert(name != nullptr);

    const Attribute * attr = FindAttributeByName(name);
    if (attr != nullptr)
    {
        return std::stof(attr->value);
    }
    return 0.0f;
}

template <typename CharType>
float XMLReader<CharType>::AttributeValueAsFloat(const unsigned int idx) const
{
    const CharType * attrVal = AttributeValue(idx);
    if (attrVal != nullptr)
    {
        return std::stof(attrVal);
    }
    return 0.0f;
}

template <typename CharType>
XMLTextFormat XMLReader<CharType>::SourceFormat() const
{
    return sourceFormat;
}

template <typename CharType>
XMLTextFormat XMLReader<CharType>::ReaderFormat() const
{
    switch (sizeof(CharType))
    {
    case 1:
        return XMLTextFormat::UTF8;
    case 2:
        return XMLTextFormat::UTF16_LE;
    case 4:
        return XMLTextFormat::UTF32_LE;
    default:
        return XMLTextFormat::ASCII;
    } // switch (sizeof(CharType))
}

template <typename CharType>
const typename XMLReader<CharType>::Attribute * XMLReader<CharType>::FindAttributeByName(const CharType * name) const
{
    assert(name != nullptr);

    const size_t numAttribs = attributes.size();
    for (size_t i = 0; i < numAttribs; ++i)
    {
        if (attributes[i].name == name)
        {
            return &attributes[i];
        }
    }
    return nullptr; // Attribute not found
}

template <typename CharType>
void XMLReader<CharType>::ParseCurrentNode()
{
    const CharType * start = ptr;

    // Move forward until a '<' is found
    while ((*ptr != L'<') && *ptr)
    {
        ++ptr;
    }

    if (!*ptr)
    {
        return;
    }

    if ((ptr - start) > 0)
    {
        // Found some text, store it:
        if (SetText(start, ptr))
        {
            return;
        }
    }

    ++ptr;

    // Based on current token, parse next element:
    switch (*ptr)
    {
    case L'/':
        ParseClosingXMLElement();
        break;

    case L'?':
        IgnoreDefinition();
        break;

    case L'!':
        if (!ParseCDATA())
        {
            ParseComment();
        }
        break;

    default:
        ParseOpeningXMLElement();
        break;
    } // switch (*ptr)
}

template <typename CharType>
bool XMLReader<CharType>::SetText(const CharType * start, const CharType * end)
{
    assert(start != nullptr);
    assert(end != nullptr);

    // Check if text is more than 2 characters, and if not, check if there is
    // only white space, so that this text won't be reported:
    if ((end - start) < 3)
    {
        const CharType * pStart = start;

        for (; pStart != end; ++pStart)
        {
            if (!std::isspace(*pStart))
            {
                break;
            }
        }

        if (pStart == end)
        {
            return false;
        }
    }

    // Set current text to the parsed text, and replace XML entity references:
    nodeNameData.assign(start, (end - start));
    ReplaceEntityReferences(nodeNameData);

    // current XML node type is text
    currentNodeType = XMLNodeType::Text;

    return true;
}

template <typename CharType>
void XMLReader<CharType>::ParseClosingXMLElement()
{
    currentNodeType = XMLNodeType::ElementEnd;
    isEmptyElement = false;

    attributes.clear();

    ++ptr;
    const CharType * pBeginClose = ptr;

    while (*ptr && (*ptr != L'>'))
    {
        ++ptr;
    }

    if (!*ptr)
    {
        common->ErrorF("Broken XML data! Missing closing \'>\' on a tag!");
        return;
    }

    // Remove trailing white spaces, if any:
    while (std::isspace(ptr[-1]))
    {
        --ptr;
    }

    nodeNameData.assign(pBeginClose, (ptr - pBeginClose));
    ++ptr;
}

template <typename CharType>
void XMLReader<CharType>::IgnoreDefinition()
{
    currentNodeType = XMLNodeType::Unknown;

    while (*ptr && (*ptr != L'>'))
    {
        ++ptr;
    }

    if (!*ptr)
    {
        common->ErrorF("Broken XML data! Missing closing \'>\' on a tag!");
        return;
    }

    ++ptr;
}

template <typename CharType>
bool XMLReader<CharType>::ParseCDATA()
{
    if (*(ptr + 1) != L'[')
    {
        return false;
    }

    currentNodeType = XMLNodeType::CDATA;

    // Skip "<![CDATA[":
    int count = 0;

    while (*ptr && (count < 8))
    {
        ++ptr, ++count;
    }

    if (!*ptr) // Empty CDATA?
    {
        return true;
    }

    const CharType * cDataBegin = ptr;
    const CharType * cDataEnd = nullptr;

    // Find end of CDATA section:
    while (*ptr && !cDataEnd)
    {
        if ((*ptr == L'>') && (*(ptr - 1) == L']') && (*(ptr - 2) == L']'))
        {
            cDataEnd = (ptr - 2);
        }
        ++ptr;
    }

    if (cDataEnd)
    {
        nodeNameData.assign(cDataBegin, (cDataEnd - cDataBegin));
    }
    else
    {
        nodeNameData.clear();
    }

    return true;
}

template <typename CharType>
void XMLReader<CharType>::ParseComment()
{
    currentNodeType = XMLNodeType::Comment;
    ++ptr;

    const CharType * pCommentBegin = ptr;
    int count = 1;

    // Move until end of comment:
    while (count)
    {
        if (*ptr == L'>')
        {
            --count;
        }
        else
        {
            if (*ptr == L'<')
            {
                ++count;
            }
        }
        ++ptr;
    }

    ptr -= 3;
    nodeNameData.assign(pCommentBegin + 2, (ptr - pCommentBegin - 2));
    ptr += 3;
}

template <typename CharType>
void XMLReader<CharType>::ParseOpeningXMLElement()
{
    currentNodeType = XMLNodeType::Element;
    isEmptyElement = false;

    attributes.clear();

    const CharType * startName = ptr;
    while ((*ptr != L'>') && !std::isspace(*ptr)) // Find end of element:
    {
        ++ptr;
    }
    const CharType * endName = ptr;

    // Find attributes:
    while (*ptr != L'>')
    {
        if (std::isspace(*ptr))
        {
            ++ptr;
        }
        else
        {
            if (*ptr != L'/') // We've got an attribute!
            {
                // Read the attribute names:
                const CharType * attributeNameBegin = ptr;
                while (!std::isspace(*ptr) && (*ptr != L'='))
                {
                    ++ptr;
                }
                const CharType * attributeNameEnd = ptr++;

                // Read the attribute value, check for quotes and single quotes:
                while ((*ptr != L'\"') && (*ptr != L'\'') && *ptr)
                {
                    ++ptr;
                }

                if (!*ptr) // Malformed XML!
                {
                    common->ErrorF("Malformed XML... Stopping reader...");
                    return;
                }

                const CharType attributeQuoteChar = *ptr++;
                const CharType * attributeValueBegin = ptr;

                while ((*ptr != attributeQuoteChar) && *ptr)
                {
                    ++ptr;
                }

                if (!*ptr) // Malformed XML!
                {
                    common->ErrorF("Malformed XML... Stopping reader...");
                    return;
                }

                const CharType * attributeValueEnd = ptr++;

                Attribute attr;
                attr.name.assign(attributeNameBegin, (attributeNameEnd - attributeNameBegin));
                attr.value.assign(attributeValueBegin, (attributeValueEnd - attributeValueBegin));
                ReplaceEntityReferences(attr.value); // Attribute values may have entity references too!
                attributes.push_back(attr);
            }
            else // Empty body tag:
            {
                ++ptr;
                isEmptyElement = true;
                break;
            }
        }
    }

    // Check if this tag has an empty body:
    if ((endName > startName) && (*(endName - 1) == L'/'))
    {
        isEmptyElement = true;
        --endName;
    }

    nodeNameData.assign(startName, (endName - startName));
    ++ptr;
}

template <typename CharType>
void XMLReader<CharType>::InitParser()
{
    assert(inputData.IsValid());
    const size_t size = inputData.sizeInBytes;

    // Test pointers:
    const uint8_t  * data8  = reinterpret_cast<const uint8_t *>(inputData.data);
    const uint16_t * data16 = reinterpret_cast<const uint16_t *>(inputData.data);
    const uint32_t * data32 = reinterpret_cast<const uint32_t *>(inputData.data);

    // Constants for byte order checking:
    static const uint8_t  UTF8[]   = { 0xEF, 0xBB, 0xBF }; // 0xEFBBBF
    static const uint16_t UTF16_BE = 0xFFFE;
    static const uint16_t UTF16_LE = 0xFEFF;
    static const uint32_t UTF32_BE = 0xFFFE0000;
    static const uint32_t UTF32_LE = 0x0000FEFF;

    // Check source for all UTF versions and convert to target data format:
    if ((size >= 4) && (data32[0] == UTF32_BE))
    {
        // UTF-32, big endian
        sourceFormat = XMLTextFormat::UTF32_BE;
        SetParseData((data32 + 1), data8, (size / 4)); // data32 + 1 to skip the header
    }
    else if ((size >= 4) && (data32[0] == UTF32_LE))
    {
        // UTF-32, little endian
        sourceFormat = XMLTextFormat::UTF32_LE;
        SetParseData((data32 + 1), data8, (size / 4)); // data32 + 1 to skip the header
    }
    else if ((size >= 2) && (data16[0] == UTF16_BE))
    {
        // UTF-16, big endian
        sourceFormat = XMLTextFormat::UTF16_BE;
        SetParseData((data16 + 1), data8, (size / 2)); // data16 + 1 to skip the header
    }
    else if ((size >= 2) && (data16[0] == UTF16_LE))
    {
        // UTF-16, little endian
        sourceFormat = XMLTextFormat::UTF16_LE;
        SetParseData((data16 + 1), data8, (size / 2)); // data16 + 1 to skip the header
    }
    else if ((size >= 3) && (data8[0] == UTF8[0]) && (data8[1] == UTF8[1]) && (data8[2] == UTF8[2]))
    {
        // UTF-8
        sourceFormat = XMLTextFormat::UTF8;
        SetParseData((data8 + 3), data8, size); // data8 + 3 to skip the header
    }
    else
    {
        // ASCII
        sourceFormat = XMLTextFormat::ASCII;
        SetParseData(data8, data8, size);
    }

    // Set pointer to text begin:
    assert(textBegin != nullptr);
    ptr = textBegin;
}

template <typename CharType>
template <class SrcCharType>
void XMLReader<CharType>::SetParseData(const SrcCharType * source, const uint8_t * pointerToStore, const size_t sizeInChars)
{
    assert(source != nullptr);
    assert(pointerToStore != nullptr);
    assert(sizeInChars != 0);

    // Test if same byte-order (endianess):
    if (sizeof(SrcCharType) > 1)
    {
        if (IsLittleEndian(ReaderFormat()) != IsLittleEndian(SourceFormat()))
        {
            common->WarningF("XML input data endianess differs from what the reader expected...");
        }
    }

    // Test char sizes:
    if (sizeof(SrcCharType) != sizeof(CharType))
    {
        common->WarningF("XML input data char size differs from what the reader expected! src=%zu, reader=%zu",
                         sizeof(SrcCharType), sizeof(CharType));
    }

    textBegin = reinterpret_cast<const CharType *>(source);
    textData = reinterpret_cast<const CharType *>(pointerToStore);
    textChars = sizeInChars;
}

template <typename CharType>
bool XMLReader<CharType>::IsLittleEndian(const XMLTextFormat fmt)
{
    return (fmt == XMLTextFormat::ASCII)    ||
           (fmt == XMLTextFormat::UTF8)     ||
           (fmt == XMLTextFormat::UTF16_LE) ||
           (fmt == XMLTextFormat::UTF32_LE);
}

template <typename CharType>
void XMLReader<CharType>::ReplaceEntityReferences(std::string & str)
{
    for (size_t i = 0; i < str.length(); ++i)
    {
        if (str[i] == '&') // Possible XML entity reference:
        {
            if (const XMLEntityReference * entRef = IsXMLEntityReference(&str[i]))
            {
                // Replace entity ref token by its equivalent ANSI char:
                str.replace(i, std::strlen(entRef->ansiName), 1, entRef->trChar);
            }
        }
    }
}

template <typename CharType>
void XMLReader<CharType>::ReplaceEntityReferences(std::wstring & str)
{
    for (size_t i = 0; i < str.length(); ++i)
    {
        if (str[i] == L'&') // Possible XML entity reference:
        {
            if (const XMLEntityReference * entRef = IsXMLEntityReference(&str[i]))
            {
                // Replace entity ref token by its equivalent wide char:
                str.replace(i, std::wcslen(entRef->wideName), 1, entRef->trChar);
            }
        }
    }
}

// ======================================================
// XMLTextFormatToString():
// ======================================================

inline std::string XMLTextFormatToString(const XMLTextFormat fmt)
{
    switch (fmt)
    {
    case XMLTextFormat::ASCII:
        return "ASCII";
    case XMLTextFormat::UTF8:
        return "UTF8";
    case XMLTextFormat::UTF16_BE:
        return "UTF16_BE";
    case XMLTextFormat::UTF16_LE:
        return "UTF16_LE";
    case XMLTextFormat::UTF32_BE:
        return "UTF32_BE";
    case XMLTextFormat::UTF32_LE:
        return "UTF32_LE";
    default:
        common->FatalErrorF("Invalid XMLTextFormat flag!");
    } // switch (fmt)
}

// ======================================================
// XMLNodeTypeToString():
// ======================================================

inline std::string XMLNodeTypeToString(const XMLNodeType type)
{
    switch (type)
    {
    case XMLNodeType::None:
        return "None";
    case XMLNodeType::Element:
        return "Element";
    case XMLNodeType::ElementEnd:
        return "ElementEnd";
    case XMLNodeType::Text:
        return "Text";
    case XMLNodeType::Comment:
        return "Comment";
    case XMLNodeType::CDATA:
        return "CDATA";
    case XMLNodeType::Unknown:
        return "Unknown";
    default:
        common->FatalErrorF("Invalid XMLNodeType flag!");
    } // switch (type)
}
