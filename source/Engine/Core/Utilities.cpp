
// ================================================================================================
// -*- C++ -*-
// File: Utilities.cpp
// Author: Guilherme R. Lampert
// Created on: 07/08/14
// Brief: Miscellaneous Engine helpers and utilities.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"

#include <algorithm>
#include <functional>
#include <fstream>
#include <cctype>
#include <cctype>
#include <locale>
#include <cerrno>

namespace Engine
{

// ======================================================
// DataBlob::LoadFile() (static method):
// ======================================================

DataBlob DataBlob::LoadFile(const std::string & filename, const bool isBinary)
{
    assert(!filename.empty());

    // Query file length, aborting if it is empty or doesn't exits.
    std::string errorStr;
    const uint64_t fileLength = system->QueryFileLength(filename, &errorStr);
    if (fileLength == 0)
    {
        if (!errorStr.empty())
        {
            common->ErrorF("Failed to get file length: %s", errorStr.c_str());
        }
        return DataBlob();
    }

    std::ifstream file;
    const std::ifstream::openmode openMode =
    (isBinary ? (std::ifstream::in | std::ifstream::binary) : std::ifstream::in);

    errno = 0;          // Clear errno so that we can use on strerror() if the file can't be opened.
    file.exceptions(0); // Don't throw exception if file can't be open.
    file.open(filename, openMode);

    if (!file.is_open())
    {
        common->ErrorF("Failed to open file \"%s\" for reading: %s", filename.c_str(), std::strerror(errno));
        return DataBlob();
    }

    // Text files will get 4 extra bytes at the end.
    // This will ensure a null terminator for ASCII and UNICODE text files.
    const size_t extraBytes = (isBinary ? 0 : 4);

    // Allocate:
    uint8_t * data = new uint8_t[fileLength + extraBytes];

    // Read the data:
    file.read(reinterpret_cast<char *>(data), static_cast<std::streamsize>(fileLength));

    // Null terminate if text:
    if (!isBinary)
    {
        data[fileLength + 0] = 0;
        data[fileLength + 1] = 0;
        data[fileLength + 2] = 0;
        data[fileLength + 3] = 0;
    }

    return DataBlob(data, static_cast<size_t>(fileLength));
}

// ======================================================
// DupCString():
// ======================================================

char * DupCString(const char * input)
{
    assert(input != nullptr);

    const size_t len = std::strlen(input);
    if (len == 0)
    {
        return nullptr;
    }

    return std::strcpy(new char[len + 1], input); // Caller must free with delete[]
}

// ======================================================
// CopyCString():
// ======================================================

size_t CopyCString(char * destStr, const size_t maxCount, const char * srcStr) noexcept
{
    assert(destStr != nullptr);
    assert(srcStr != nullptr);
    assert(maxCount != 0);

    char * p = destStr;
    size_t available = maxCount;
    while ((*p++ = *srcStr++) && (--available > 0))
    {
    }

    if (available == 0)
    {
        *(--p) = '\0'; // Truncate
        return static_cast<size_t>(p - destStr);
    }

    return static_cast<size_t>((p - 1) - destStr);
}

// ======================================================
// TokenizeCString():
// ======================================================

char * TokenizeCString(char * str, const char * delimiters, char ** remaining) noexcept
{
    // Validation:
    assert(remaining != nullptr);
    assert(delimiters != nullptr);
    assert(str != nullptr || *remaining != nullptr);

    char * token;
    const char * ctl;

    // If str is null, continue with previous string:
    if (!str)
    {
        str = *remaining;
    }

    while (*str)
    {
        for (ctl = delimiters; *ctl && *ctl != *str; ++ctl)
        {
        }
        if (!*ctl)
        {
            break;
        }
        ++str;
    }

    token = str;

    // Find the end of the token.
    // If it is not the end of the string, put a null there.
    for (; *str; ++str)
    {
        for (ctl = delimiters; *ctl && *ctl != *str; ++ctl)
        {
        }
        if (*ctl)
        {
            *str++ = '\0';
            break;
        }
    }

    // Update remaining string pointer:
    *remaining = str;

    // Determine if a token has been found:
    return (token == str) ? nullptr : token;
}

// ======================================================
// SplitString():
// ======================================================

StringArray SplitString(const std::string & source, const std::string & delimiters)
{
    assert(!delimiters.empty());
    StringArray tokenList;

    if (!source.empty())
    {
        const char * delim = delimiters.c_str();

        // Need the local copy since TokenizeCString() will modify
        // the input. This should be optimized at some point in the future...
        std::unique_ptr<char[]> tempString(DupCString(source.c_str()));

        // Tokenize the C string:
        char * remaining = nullptr;
        char * tk = TokenizeCString(tempString.get(), delim, &remaining);

        while (tk != nullptr)
        {
            tokenList.push_back(tk);
            tk = TokenizeCString(nullptr, delim, &remaining);
        }
    }

    return tokenList;
}

// ======================================================
// CaseInsensitiveStringCompare():
// ======================================================

int CaseInsensitiveStringCompare(const std::string & str1, const std::string & str2)
{
    struct Cmp
    {
        static int CharDiff(const int c1, const int c2)
        {
            if (c1 != c2)
            {
                const int lc1 = std::tolower(c1);
                const int lc2 = std::tolower(c2);
                if (lc1 < lc2)
                {
                    return -1;
                }
                if (lc1 > lc2)
                {
                    return +1;
                }
            }
            return 0;
        }
    };

    int diff = 0;
    const size_t str1Length = str1.length();
    const size_t str2Length = str2.length();
    const size_t minLength = std::min(str1Length, str2Length);

    for (size_t idx = 0; ((idx < minLength) && (diff == 0)); ++idx)
    {
        diff += Cmp::CharDiff(str1[idx], str2[idx]);
    }

    if (diff != 0)
    {
        return Clamp(diff, -1, +1);
    }
    if (str2Length == str1Length)
    {
        return 0;
    }
    if (str2Length > str1Length)
    {
        return 1;
    }
    return -1;
}

// ======================================================
// StringStartsWith():
// ======================================================

bool StringStartsWith(const std::string & str, const std::string & prefix)
{
    const size_t prefixLen = prefix.length();
    if (prefixLen == 0)
    {
        return false;
    }

    return str.compare(0, prefixLen, prefix) == 0;
}

// ======================================================
// StringEndsWith():
// ======================================================

bool StringEndsWith(const std::string & str, const std::string & postfix)
{
    const size_t strLen = str.length();
    const size_t postfixLen = postfix.length();

    if (postfixLen == 0)
    {
        return false;
    }

    if (strLen < postfixLen)
    {
        return false;
    }

    if (strLen == postfixLen)
    {
        // Same length, simple compare:
        return str.compare(postfix) == 0;
    }

    // Compare tail of the string. Postfix > str.
    return str.compare((strLen - postfixLen), postfixLen, postfix) == 0;
}

// ======================================================
// ExtractFilenameExtension():
// ======================================================

std::string ExtractFilenameExtension(const std::string & filename, const bool includeDot)
{
    const size_t lastDot = filename.find_last_of('.');
    if (lastDot != std::string::npos)
    {
        return filename.substr(includeDot ? lastDot : (lastDot + 1));
    }

    return std::string(); // No dot in the filename, no extension.
}

// ======================================================
// RemoveTrailingFloatZeros():
// ======================================================

std::string RemoveTrailingFloatZeros(const std::string & floatStr)
{
    // Only process if the number is decimal (has a dot somewhere):
    if (floatStr.find_last_of('.') == std::string::npos)
    {
        return floatStr;
    }

    std::string trimmed(floatStr);

    // Remove trailing zeros:
    while (!trimmed.empty() && (trimmed.back() == '0'))
    {
        trimmed.pop_back();
    }

    // If the dot was left alone at the end, remove it too:
    if (!trimmed.empty() && (trimmed.back() == '.'))
    {
        trimmed.pop_back();
    }

    return trimmed;
}

// ======================================================
// Format();
// ======================================================

std::string Format(const char * format, ...)
{
    assert(format != nullptr);

    va_list vaList;
    char buf[MaxTempStringLen];

    va_start(vaList, format);
    const int result = std::vsnprintf(buf, MaxTempStringLen, format, vaList);
    va_end(vaList);

    if (result < 0)
    {
        assert(false && "vsnprintf() failed!");
        ZeroArray(buf); // Clear the string.
        return buf;
    }
    if (result >= MaxTempStringLen)
    {
        assert(false && "vsnprintf() overflowed buffer!");
        buf[MaxTempStringLen - 1] = '\0'; // Truncate the string.
        return buf;
    }

    buf[result] = '\0';
    return buf;
}

// ======================================================
// VFormat():
// ======================================================

std::string VFormat(const char * format, va_list vaList)
{
    assert(format != nullptr);

    char buf[MaxTempStringLen];
    const int result = std::vsnprintf(buf, MaxTempStringLen, format, vaList);

    if (result < 0)
    {
        assert(false && "vsnprintf() failed!");
        ZeroArray(buf); // Clear the string.
        return buf;
    }
    if (result >= MaxTempStringLen)
    {
        assert(false && "vsnprintf() overflowed buffer!");
        buf[MaxTempStringLen - 1] = '\0'; // Truncate the string.
        return buf;
    }

    buf[result] = '\0';
    return buf;
}

// ======================================================
// FormatMemoryUnit():
// ======================================================

std::string FormatMemoryUnit(const uint64_t memorySizeInBytes, const bool abbreviated)
{
    const char * memUnitStr;
    double adjustedSize;
    char numStrBuf[128];

    if (memorySizeInBytes < 1024)
    {
        memUnitStr = (abbreviated ? "B" : "Bytes");
        adjustedSize = static_cast<double>(memorySizeInBytes);
    }
    else if (memorySizeInBytes < (1024 * 1024))
    {
        memUnitStr = (abbreviated ? "KB" : "Kilobytes");
        adjustedSize = (memorySizeInBytes / 1024.0);
    }
    else if (memorySizeInBytes < (1024 * 1024 * 1024))
    {
        memUnitStr = (abbreviated ? "MB" : "Megabytes");
        adjustedSize = (memorySizeInBytes / 1024.0 / 1024.0);
    }
    else
    {
        memUnitStr = (abbreviated ? "GB" : "Gigabytes");
        adjustedSize = (memorySizeInBytes / 1024.0 / 1024.0 / 1024.0);
    }

    // We only care about the first 2 decimal digits.
    SNPrintF(numStrBuf, "%.2f", adjustedSize);

    // Remove trailing zeros if no significant decimal digits:
    std::string str = RemoveTrailingFloatZeros(numStrBuf);
    str += " ";
    str += memUnitStr;

    return str;
}

// ======================================================
// LTrimString():
// ======================================================

std::string & LTrimString(std::string & s)
{
    s.erase(std::begin(s), std::find_if(std::begin(s), std::end(s), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// ======================================================
// RTrimString():
// ======================================================

std::string & RTrimString(std::string & s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), std::end(s));
    return s;
}

// ======================================================
// TrimString():
// ======================================================

std::string & TrimString(std::string & s)
{
    return LTrimString(RTrimString(s));
}

// ======================================================
// ParseIntCommonValidation():
// ======================================================

static bool ParseIntCommonValidation(const std::string & s, bool * success, const int base) noexcept
{
    if (base != 0)
    {
        // Validate base:
        if ((base < 0) || (base == 1) || (base > 36))
        {
            common->ErrorF("Invalid numerical base (%d) for ParseInt()!", base);
            if (success != nullptr)
            {
                (*success) = false;
            }
            return false;
        }
    }
    // Passing base=0 will allow strtol() to infer
    // the numerical base from the string format.

    if (s.empty())
    {
        // Bail right away if string is empty.
        // Since nothing would be read, we consider this case a failure.
        if (success != nullptr)
        {
            (*success) = false;
        }
        return false;
    }

    // Continue parsing.
    return true;
}

// ======================================================
// ParseInt():
// ======================================================

int ParseInt(const std::string & s, bool * success, const int base) noexcept
{
    if (!ParseIntCommonValidation(s, success, base))
    {
        // 'success' was already set by the validation.
        return 0;
    }

    char * endptr = nullptr;
    const char * nptr = s.c_str();
    const int result = static_cast<int>(std::strtol(nptr, &endptr, base));

    if (success != nullptr)
    {
        // Successful if anything was parsed (any char advanced from the input).
        (*success) = ((endptr != nptr) ? true : false);
    }

    return result;
}

// ======================================================
// ParseInt64():
// ======================================================

int64_t ParseInt64(const std::string & s, bool * success, const int base) noexcept
{
    if (!ParseIntCommonValidation(s, success, base))
    {
        // 'success' was already set by the validation.
        return 0;
    }

    char * endptr = nullptr;
    const char * nptr = s.c_str();
    const int64_t result = std::strtoll(nptr, &endptr, base);

    if (success != nullptr)
    {
        // Successful if anything was parsed (any char advanced from the input).
        (*success) = ((endptr != nptr) ? true : false);
    }

    return result;
}

// ======================================================
// ParseFloat():
// ======================================================

float ParseFloat(const std::string & s, bool * success) noexcept
{
    if (s.empty())
    {
        // Bail right away if string is empty.
        // Since nothing would be read, we consider this case a failure.
        if (success != nullptr)
        {
            (*success) = false;
        }
        return 0.0f;
    }

    char * endptr = nullptr;
    const char * nptr = s.c_str();
    const float result = static_cast<float>(std::strtod(nptr, &endptr));

    if (success != nullptr)
    {
        // Successful if anything was parsed (any char advanced from the input).
        (*success) = ((endptr != nptr) ? true : false);
    }

    return result;
}

// ======================================================
// ParseDouble():
// ======================================================

double ParseDouble(const std::string & s, bool * success) noexcept
{
    if (s.empty())
    {
        // Bail right away if string is empty.
        // Since nothing would be read, we consider this case a failure.
        if (success != nullptr)
        {
            (*success) = false;
        }
        return 0.0;
    }

    char * endptr = nullptr;
    const char * nptr = s.c_str();
    const double result = std::strtod(nptr, &endptr);

    if (success != nullptr)
    {
        // Successful if anything was parsed (any char advanced from the input).
        (*success) = ((endptr != nptr) ? true : false);
    }

    return result;
}

// ======================================================
// ParseIntTuple():
// ======================================================

bool ParseIntTuple(const std::string & data, int * dest, const int numElements) noexcept
{
    assert(dest != nullptr);

    switch (numElements)
    {
    case 2:
        if (std::sscanf(data.c_str(), "(%i,%i)", &dest[0], &dest[1]) == 2)
        {
            return true;
        }
        break;
    case 3:
        if (std::sscanf(data.c_str(), "(%i,%i,%i)", &dest[0], &dest[1], &dest[2]) == 3)
        {
            return true;
        }
        break;
    case 4:
        if (std::sscanf(data.c_str(), "(%i,%i,%i,%i)", &dest[0], &dest[1], &dest[2], &dest[3]) == 4)
        {
            return true;
        }
        break;
    default:
        common->ErrorF("ParseIntTuple() => \'numElements\' must be either 2, 3 or 4!");
    } // switch (numElements)

    return false;
}

// ======================================================
// ParseFloatTuple():
// ======================================================

bool ParseFloatTuple(const std::string & data, float * dest, const int numElements) noexcept
{
    assert(dest != nullptr);

    switch (numElements)
    {
    case 2:
        if (std::sscanf(data.c_str(), "(%f,%f)", &dest[0], &dest[1]) == 2)
        {
            return true;
        }
        break;
    case 3:
        if (std::sscanf(data.c_str(), "(%f,%f,%f)", &dest[0], &dest[1], &dest[2]) == 3)
        {
            return true;
        }
        break;
    case 4:
        if (std::sscanf(data.c_str(), "(%f,%f,%f,%f)", &dest[0], &dest[1], &dest[2], &dest[3]) == 4)
        {
            return true;
        }
        break;
    default:
        common->ErrorF("ParseFloatTuple() => \'numElements\' must be either 2, 3 or 4!");
    } // switch (numElements)

    return false;
}

// ======================================================
// DecodeUTF8():
// ======================================================

int DecodeUTF8(const char * encodedBuffer, unsigned int * outCharLength)
{
    // Reference: http://en.wikipedia.org/wiki/Utf8
    //
    // Adapted from code found on the "AngelCode Tool Box Library"
    //  http://www.angelcode.com/dev/bmfonts/
    //
    assert(encodedBuffer != nullptr);
    const unsigned char * __restrict buf = reinterpret_cast<const unsigned char *>(encodedBuffer);

    int value = 0;
    int length = -1;
    unsigned char byte = buf[0];

    if ((byte & 0x80) == 0)
    {
        // This is the only byte
        if (outCharLength != nullptr)
        {
            *outCharLength = 1;
        }
        return byte;
    }
    else if ((byte & 0xE0) == 0xC0)
    {
        // There is one more byte
        value = static_cast<int>(byte & 0x1F);
        length = 2;

        // The value at this moment must not be less than 2, because
        // that should have been encoded with one byte only.
        if (value < 2)
        {
            length = -1;
        }
    }
    else if ((byte & 0xF0) == 0xE0)
    {
        // There are two more bytes
        value = static_cast<int>(byte & 0x0F);
        length = 3;
    }
    else if ((byte & 0xF8) == 0xF0)
    {
        // There are three more bytes
        value = static_cast<int>(byte & 0x07);
        length = 4;
    }

    int n = 1;
    for (; n < length; ++n)
    {
        byte = buf[n];
        if ((byte & 0xC0) == 0x80)
        {
            value = (value << 6) + static_cast<int>(byte & 0x3F);
        }
        else
        {
            break;
        }
    }

    if (n == length)
    {
        if (outCharLength != nullptr)
        {
            *outCharLength = static_cast<unsigned int>(length);
        }
        return value;
    }

    // The byte sequence isn't a valid UTF-8 sequence.
    return -1;
}

// ======================================================
// DecodeUTF16():
// ======================================================

int DecodeUTF16(const char * encodedBuffer, unsigned int * outCharLength, const UnicodeByteOrder byteOrder)
{
    // Reference: http://en.wikipedia.org/wiki/Utf8
    //
    // Adapted from code found on the "AngelCode Tool Box Library"
    //  http://www.angelcode.com/dev/bmfonts/
    //
    assert(encodedBuffer != nullptr);
    const unsigned char * __restrict buf = reinterpret_cast<const unsigned char *>(encodedBuffer);

    int value = 0;
    if (byteOrder == UnicodeByteOrder::LittleEndian)
    {
        value += buf[0];
        value += static_cast<unsigned int>(buf[1]) << 8;
    }
    else
    {
        value += buf[1];
        value += static_cast<unsigned int>(buf[0]) << 8;
    }

    if ((value < 0xD800) || (value > 0xDFFF))
    {
        if (outCharLength != nullptr)
        {
            *outCharLength = 2;
        }
        return value;
    }
    else if (value < 0xDC00)
    {
        // We've found the first surrogate word
        value = ((value & 0x3FF) << 10);

        // Read the second surrogate word
        int value2 = 0;
        if (byteOrder == UnicodeByteOrder::LittleEndian)
        {
            value2 += buf[2];
            value2 += static_cast<unsigned int>(buf[3]) << 8;
        }
        else
        {
            value2 += buf[3];
            value2 += static_cast<unsigned int>(buf[2]) << 8;
        }

        // The second surrogate word must be in the 0xDC00 - 0xDFFF range
        if ((value2 < 0xDC00) || (value2 > 0xDFFF))
        {
            return -1;
        }

        value = value + (value2 & 0x3FF) + 0x10000;
        if (outCharLength != nullptr)
        {
            *outCharLength = 4;
        }
        return value;
    }

    // It is an illegal sequence if a character in the 0xDC00-0xDFFF range comes first
    return -1;
}

// ======================================================
// ToString() helpers:
// ======================================================

std::string ToString(const float f)
{
    char temp[128];
    return RemoveTrailingFloatZeros(SNPrintF(temp, "%f", f));
}

std::string ToString(const double d)
{
    char temp[128];
    return RemoveTrailingFloatZeros(SNPrintF(temp, "%lf", d));
}

std::string ToString(const Vec2i & v)
{
    return Format("(%i, %i)", v.x, v.y);
}

std::string ToString(const Vec3i & v)
{
    return Format("(%i, %i, %i)", v.x, v.y, v.z);
}

std::string ToString(const Vec4i & v)
{
    return Format("(%i, %i, %i, %i)", v.x, v.y, v.z, v.w);
}

std::string ToString(const Vec2u & v)
{
    return Format("(%u, %u)", v.x, v.y);
}

std::string ToString(const Vec3u & v)
{
    return Format("(%u, %u, %u)", v.x, v.y, v.z);
}

std::string ToString(const Vec4u & v)
{
    return Format("(%u, %u, %u, %u)", v.x, v.y, v.z, v.w);
}

std::string ToString(const Vec2f & v)
{
    return Format("(%s, %s)", ToString(v.x).c_str(), ToString(v.y).c_str());
}

std::string ToString(const Vec3f & v)
{
    return Format("(%s, %s, %s)", ToString(v.x).c_str(),
            ToString(v.y).c_str(), ToString(v.z).c_str());
}

std::string ToString(const Vec4f & v)
{
    return Format("(%s, %s, %s, %s)",
            ToString(v.x).c_str(), ToString(v.y).c_str(),
            ToString(v.z).c_str(), ToString(v.w).c_str());
}

std::string ToString(const Vector3 & v)
{
    return Format("(%s, %s, %s)", ToString(v[0]).c_str(),
            ToString(v[1]).c_str(), ToString(v[2]).c_str());
}

std::string ToString(const Vector4 & v)
{
    return Format("(%s, %s, %s, %s)",
            ToString(v[0]).c_str(), ToString(v[1]).c_str(),
            ToString(v[2]).c_str(), ToString(v[3]).c_str());
}

// ======================================================
// Compile-time checks:
// ======================================================

// These assumptions are made through the Engine:
static_assert(sizeof(uint64_t) == 8, "sizeof(uint64_t) is not 8 bytes!");
static_assert(sizeof(uint32_t) == 4, "sizeof(uint32_t) is not 4 bytes!");
static_assert(sizeof(uint16_t) == 2, "sizeof(uint16_t) is not 2 bytes!");
static_assert(sizeof(uint8_t)  == 1, "sizeof(uint8_t) is not 1 byte!");
static_assert(sizeof(char)     == 1, "sizeof(char) is not 1 byte!");
static_assert(sizeof(float)    == 4, "float is expected to be 4 bytes long!");
static_assert(sizeof(size_t)   == sizeof(void *), "size_t is expected to be the same size as a void* pointer!");
static_assert(sizeof(Vec2f)    == (sizeof(float) * 2), "Vec2f should be exactly 2 floats in size!");
static_assert(sizeof(Vec3f)    == (sizeof(float) * 3), "Vec3f should be exactly 3 floats in size!");
static_assert(sizeof(Vec4f)    == (sizeof(float) * 4), "Vec4f should be exactly 4 floats in size!");

} // namespace Engine {}
