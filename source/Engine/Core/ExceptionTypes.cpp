
// ================================================================================================
// -*- C++ -*-
// File: ExceptionTypes.cpp
// Author: Guilherme R. Lampert
// Created on: 14/08/14
// Brief: Exception types used by the Engine. Base Exception class inherits from std::exception.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"

namespace Engine
{

// ======================================================
// Exception:
// ======================================================

char Exception::errorMessage[Exception::MaxExceptionMessageLen];

Exception::Exception()
{
    // Empty exception.
    ClearString();
}

Exception::Exception(const char * error)
{
    if (error != nullptr)
    {
        CopyCString(errorMessage, ArrayLength(errorMessage), error);
    }
    else
    {
        ClearString();
    }
}

const char * Exception::ErrorMessage() const noexcept
{
    return errorMessage;
}

bool Exception::HasErrorMessage() const noexcept
{
    return errorMessage[0] != '\0';
}

const char * Exception::what() const noexcept
{
    return errorMessage;
}

const char * Exception::ExceptionName() const noexcept
{
    return "Engine::Exception";
}

void Exception::ClearString() noexcept
{
    ZeroArray(errorMessage);
}

} // namespace Engine {}
