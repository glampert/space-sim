
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: RuntimeTypeSystem.hpp
// Author: Guilherme R. Lampert
// Created on: 16/08/14
// Brief: Runtime Type System for basic type inference and reflection.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/RuntimeTypeSystemMacros.hpp"

namespace Engine
{

// Forward declarations:
class Class;
class TypeInfo;

// Function pointer types:
using CreateInstanceFunction  = Class * (*)();
using DestroyInstanceFunction = void (*)(Class *);

// ======================================================
// TypeInfo:
// ======================================================

//
// Information about a C++ type, such as a class or struct.
//
// Every programmer defined C++ type that utilizes the Runtime Type System must have a
// static instance of this class associated with it. It holds basic information
// for runtime object instantiation by name/id plus some type testing facilities.
//
class TypeInfo
{
  public:

    // Default constructor. Should only be used by the RUNTIME_* macros.
    TypeInfo(const char * superName, const char * thisName,
             CreateInstanceFunction createInstance,
             DestroyInstanceFunction destroyInstance);

    // Printable string with the name of the class that owns this type.
    const char * GetClassName() const;

    // Printable string with the name of the direct superclass of this type.
    const char * GetSuperclassName() const;

    // String hash index of the class name that owns this type.
    HashIndex GetClassHashIndex() const;

    // String hash index of the name of the direct superclass of this type.
    HashIndex GetSuperclassHashIndex() const;

    // Get a pointer to the TypeInfo belonging to the direct superclass of this type. Null if this type has no superclass.
    const TypeInfo * GetSuperclassTypeInfo() const;

    // Test if the given type is a direct subclass of this type.
    bool IsSubclassOf(const TypeInfo & typeInfo) const;

    // Test if the given type is the first superclass above this type.
    bool IsSuperclassOf(const TypeInfo & typeInfo) const;

    // Test if the given type is the same as this type.
    bool IsExactly(const TypeInfo & typeInfo) const;

    // Test if the given type is in the inheritance chain of this type.
    bool IsKindOf(const TypeInfo & typeInfo) const;

    // Internal helpers used by the type system:
    Class * CallCreateFunc() const;
    void CallDestroyFunc(Class * instance) const;
    int GetHierarchyDepth() const;

  private:

    friend void InitRuntimeTypeSystem();

    // Type-info data:
    const TypeInfo * superclassPtr;              // Null if no superclass.
    const char * const className;                // Name of class that owns this TypeInfo. Can't be null.
    const char * const superclassName;           // The 'official' superclass name or null if a root class.
    CreateInstanceFunction createInstanceFunc;   // Instance factory. Never null.
    DestroyInstanceFunction destroyInstanceFunc; // Instance deleter. Never null.
    const HashIndex classHash;                   // Hash(className).
    const HashIndex superclassHash;              // Hash(superclassName). Zero if a root class.

    // Avoid accidental copies:
    DISABLE_COPY_AND_ASSIGN(TypeInfo)
};

// ======================================================
// Class:
// ======================================================

//
// Base interface for all types that wish to use the Runtime Type System.
//
// Any class that uses the type system must have Engine::Class as its base, directly or indirectly.
// Doing so will expose the required system methods to the subclass.
// The subclass must also make use of the RUNTIME_[CLASS|INTERFACE]_DECL()
// and RUNTIME_[CLASS|INTERFACE]_DEFINITION() macros to ensure any additional
// data and methods are added correctly and as transparently as possible.
//
// It is important to note that the Runtime Type System only provides support for a single
// superclass per subclass. This does not mean that the subclass cannot inherit
// from more the one class (mixins are common), however, the programmer will have
// to choose one of the parent classes to be the "official" superclass, and pass
// its name to the RUNTIME_[CLASS|INTERFACE]_DEFINITION macro in the implementation file.
//
class Class
{
    RUNTIME_INTERFACE_DECL(Class)

  public:

    // ---- Static class methods: ----

    // Creates a class instance by its name. The class must have been
    // registered with the type system at startup.
    static Class * CreateInstance(const char * className);

    // Creates a class instance by the hash index of its name.
    // The class must have been registered with the type system at startup.
    static Class * CreateInstance(const HashIndex classHash);

    // Destroy the instance of a class previously created with Class::CreateInstance().
    // This is equivalent to calling the DestroyInstance() static method of a class
    // that registered itself with the type system via RUNTIME_CLASS_DECL().
    static void DestroyInstance(Class * instance);

    // ---- Instance methods: ----

    // Get the printable class (type) name and superclass name.
    const char * GetClassName() const;
    const char * GetSuperclassName() const;

    // Get the class and superclass hash indexes, generated from their names.
    HashIndex GetClassHashIndex() const;
    HashIndex GetSuperclassHashIndex() const;

    // Constant-time type queries, for exact comparisons. See also: TypeInfo.
    bool IsSubclassOf(const TypeInfo & typeInfo) const;
    bool IsSuperclassOf(const TypeInfo & typeInfo) const;
    bool IsExactly(const TypeInfo & typeInfo) const;

    // Test if the given type is in the superclass hierarchy of this type. See also: TypeInfo.
    bool IsKindOf(const TypeInfo & typeInfo) const;

    // No-op virtual destructor.
    virtual ~Class() = default;
};

// ======================================================
// Global subsystem functions. Mostly for internal use.
// ======================================================

// InitRuntimeTypeSystem() will finish setting up the Runtime Type System,
// doing initialization that cannot be done by object constructors.
// It should always be called by the application after main() is entered
// or after the dynamic module is loaded and C++ constructors have been called.
void InitRuntimeTypeSystem();

// Text dump of the Runtime Type System database.
// Useful for debugging and console printing. Can safely be called
// during runtime. Execution time is linear according to the database size.
// Param 'print' is a pointer to a function that will receive the text dump for printing.
// If null, the data is printed to the default log with Common::PrintF().
void DumpTypeDatabase(void (*print)(const char *));

// Registers a TypeInfo instance belonging to a class with the system. Used internally.
void RegisterNewType(TypeInfo * typeInfo);

// Finds a TypeInfo instance that belongs to a previously registered class, by the class name.
const TypeInfo * FindTypeByName(const char * className);

// Finds a TypeInfo instance that belongs to a previously registered class, by the class hash index.
const TypeInfo * FindTypeByHashIndex(const HashIndex classHash);

// ======================================================
// Default templated instancers and deleters:
// ======================================================

template <class T> T * DefaultClassInstancer();
template <class T> T * DefaultClassInstancer(const T & copy);
template <class T> void DefaultClassDeleter(T * instance);

// Include the inline definitions in the same namespace:
#include "Engine/Core/RuntimeTypeSystem.inl"

} // namespace Engine {}
