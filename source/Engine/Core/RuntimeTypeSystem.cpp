
// ================================================================================================
// -*- C++ -*-
// File: RuntimeTypeSystem.cpp
// Author: Guilherme R. Lampert
// Created on: 16/08/14
// Brief: Runtime Type System for basic type inference and reflection.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine front-end:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"

// Runtime Type System:
#include "Engine/Core/RuntimeTypeSystemMacros.hpp"
#include "Engine/Core/RuntimeTypeSystem.hpp"

#include <map>
#include <vector>
#include <algorithm>

namespace Engine
{
namespace /* unnamed */
{

// ======================================================
// Local data:
// ======================================================

// The entire type database in kept inside the 'theTypeMap' std map.
// Memory is allocated from the standard heap. No custom allocator provided.
using TypeMapValueType = std::pair<const HashIndex, TypeInfo *>;
using TypeMap = std::map<const HashIndex, TypeInfo *>;

// Ensures the map is initialized on its first use.
static TypeMap * GetTypeMap()
{
    static TypeMap theTypeMap;
    return &theTypeMap;
}

// Set once when InitRuntimeTypeSystem() is first called.
// InitRuntimeTypeSystem() must be called after main() enters
// or after the dynamic library is loaded.
static bool typeSysInitialized = false;

} // namespace unnamed {}

// ======================================================
// InitRuntimeTypeSystem():
// ======================================================

void InitRuntimeTypeSystem()
{
    if (typeSysInitialized)
    {
        common->WarningF("Attention! Multiple initializations of the Runtime Type System detected!");
        return;
    }

    common->PrintF("Initializing the Runtime Type System and type database...");

    TypeMap * types = GetTypeMap();
    TypeMap::iterator it = types->begin();
    TypeMap::iterator end = types->end();

    while (it != end)
    {
        TypeInfo * typeInfo = it->second;
        assert(typeInfo != nullptr);

        if ((typeInfo->superclassPtr == nullptr) && (typeInfo->superclassHash != 0))
        {
            TypeMap::const_iterator super = types->find(typeInfo->superclassHash);
            if (super != types->end())
            {
                typeInfo->superclassPtr = super->second;
            }
        }

        ++it;
    }

    typeSysInitialized = true;
    common->PrintF("Runtime Type System initialized. Type database ready.");
}

// ======================================================
// DumpTypeDatabase():
// ======================================================

void DumpTypeDatabase(void (*print)(const char *))
{
    struct PrintToCommonLog
    {
        static void Func(const char * str)
        {
            common->PrintF("%s", str);
        }
    };

    if (print == nullptr)
    {
        print = &PrintToCommonLog::Func;
    }

    if (!typeSysInitialized)
    {
        print("Warning! Runtime Type System not initialized!");
        // Allow it to continue...
    }

    // Locals:
    unsigned int longestClassName = 0;
    unsigned int longestSuperclassName = 0;
    const TypeMap * types = GetTypeMap();
    std::vector<const TypeInfo *> typeList;
    std::string tempString;

    //
    // First pass is to compute the correct padding for names
    // and add everything to a sortable array for the second pass.
    //
    typeList.reserve(types->size());
    TypeMap::const_iterator it = types->begin();
    TypeMap::const_iterator end = types->end();

    while (it != end)
    {
        const TypeInfo * typeInfo = it->second;
        assert(typeInfo != nullptr);

        longestClassName = std::max(longestClassName,
                                    static_cast<unsigned int>(std::strlen(typeInfo->GetClassName())));

        longestSuperclassName = std::max(longestSuperclassName,
                                         static_cast<unsigned int>(std::strlen(typeInfo->GetSuperclassName())));

        typeList.push_back(typeInfo);
        ++it;
    }

    // Sort typeList alphabetically, by class name:
    std::sort(std::begin(typeList), std::end(typeList),
              [](const TypeInfo * a, const TypeInfo * b) -> bool
              {
			      return (CaseInsensitiveStringCompare(a->GetClassName(), b->GetClassName()) < 0);
              });

    //
    // Now print everything:
    //
    print("--------------- Runtime Type Database ----------------");

    // Min padding:
    if (longestClassName < 10)
    {
        longestClassName = 10;
    }
    if (longestSuperclassName < 10)
    {
        longestSuperclassName = 10;
    }

    tempString = Format("%-*s | class hash | %-*s | super hash | h-depth | super TypeInfo",
                        longestClassName, "class name", longestSuperclassName, "super name");
    print(tempString.c_str());

    for (size_t i = 0; i < typeList.size(); ++i)
    {
        const TypeInfo * typeInfo = typeList[i];
        tempString = Format("%-*s | 0x%08x | %-*s | 0x%08x | %-7d | %p",
                            longestClassName, typeInfo->GetClassName(), typeInfo->GetClassHashIndex(),
                            longestSuperclassName, typeInfo->GetSuperclassName(), typeInfo->GetSuperclassHashIndex(),
                            typeInfo->GetHierarchyDepth(), typeInfo->GetSuperclassTypeInfo());
        print(tempString.c_str());
    }

    //
    // Print some extra statistics:
    //
    tempString = Format("Printed %zu classes.", types->size());
    print(tempString.c_str());

    const size_t dynBytesUsed = (types->size() * sizeof(TypeMapValueType));
    tempString = Format("Approximately %s of dynamic memory used by the type database.", FormatMemoryUnit(dynBytesUsed, false).c_str());
    print(tempString.c_str());

    const size_t staticBytesUsed = (types->size() * sizeof(TypeInfo));
    tempString = Format("Approximately %s of static memory used by the type database.", FormatMemoryUnit(staticBytesUsed, false).c_str());
    print(tempString.c_str());

    print("------------------------------------------------------");
}

// ======================================================
// RegisterNewType():
// ======================================================

void RegisterNewType(TypeInfo * typeInfo)
{
    assert(typeInfo != nullptr);
    TypeMap * types = GetTypeMap();

    // Disallow duplicates:
    TypeMap::const_iterator it = types->find(typeInfo->GetClassHashIndex());
    if (it != types->end())
    {
        assert(false && "Native type is already registered in the Runtime Type System database!");
        return;
    }

    types->insert(TypeMap::value_type(typeInfo->GetClassHashIndex(), typeInfo));
}

// ======================================================
// FindTypeByName():
// ======================================================

const TypeInfo * FindTypeByName(const char * className)
{
    assert(className != nullptr);

    if (!typeSysInitialized)
    {
        common->WarningF("Calling FindTypeByName() before Runtime Type System initialization!");
    }

    const HashIndex classHash = HashString(className);
    const TypeMap * types = GetTypeMap();

    TypeMap::const_iterator it = types->find(classHash);
    if (it != types->end())
    {
        return it->second;
    }

    common->PrintF("Native type '%s' not found!", className);
    return nullptr;
}

// ======================================================
// FindTypeByHashIndex():
// ======================================================

const TypeInfo * FindTypeByHashIndex(const HashIndex classHash)
{
    if (!typeSysInitialized)
    {
        common->WarningF("Calling FindTypeByName() before Runtime Type System initialization!");
    }

    const TypeMap * types = GetTypeMap();

    TypeMap::const_iterator it = types->find(classHash);
    if (it != types->end())
    {
        return it->second;
    }

    common->PrintF("Native type (id 0x%08x) not found!", classHash);
    return nullptr;
}

// ======================================================
// TypeInfo implementation:
// ======================================================

TypeInfo::TypeInfo(const char * superName, const char * thisName, CreateInstanceFunction createInstance, DestroyInstanceFunction destroyInstance)
    : superclassPtr(nullptr)
    , className(thisName)
    , superclassName(superName)
    , createInstanceFunc(createInstance)
    , destroyInstanceFunc(destroyInstance)
    , classHash(HashString(className))
    , superclassHash((std::strcmp(superName, "NULL") != 0) ? HashString(superName) : 0) // "NULL" is used by Class, which has no superclass above it. Don't generate a hash in this case.
{
    // Validate inputs:
    assert(className != nullptr);
    assert(superclassName != nullptr);
    assert(createInstanceFunc != nullptr);
    assert(destroyInstanceFunc != nullptr);

    // Register type with the runtime type system:
    RegisterNewType(this);
}

const char * TypeInfo::GetClassName() const
{
    return className;
}

const char * TypeInfo::GetSuperclassName() const
{
    return superclassName;
}

HashIndex TypeInfo::GetClassHashIndex() const
{
    return classHash;
}

HashIndex TypeInfo::GetSuperclassHashIndex() const
{
    return superclassHash;
}

const TypeInfo * TypeInfo::GetSuperclassTypeInfo() const
{
    return superclassPtr;
}

bool TypeInfo::IsSubclassOf(const TypeInfo & typeInfo) const
{
    return this->superclassHash == typeInfo.classHash;
}

bool TypeInfo::IsSuperclassOf(const TypeInfo & typeInfo) const
{
    return this->classHash == typeInfo.superclassHash;
}

bool TypeInfo::IsExactly(const TypeInfo & typeInfo) const
{
    return this->classHash == typeInfo.classHash;
}

bool TypeInfo::IsKindOf(const TypeInfo & typeInfo) const
{
    if (IsExactly(typeInfo))
    {
        return true;
    }

    // Linear search in the local class hierarchy.
    const TypeInfo * c = superclassPtr;
    while (c != nullptr)
    {
        if (c->classHash == typeInfo.classHash)
        {
            return true;
        }
        c = c->superclassPtr;
    }

    return false;
}

int TypeInfo::GetHierarchyDepth() const
{
    // Count the number of classes above this one + itself.
    const TypeInfo * c = superclassPtr;
    int numClasses = 1;

    while (c != nullptr)
    {
        c = c->superclassPtr;
        ++numClasses;
    }

    return numClasses;
}

// ======================================================
// Class implementation:
// ======================================================

// The type 'Class' is the root of any class hierarchy,
// therefore its superclass is 'NULL'.
RUNTIME_INTERFACE_DEFINITION(NULL, Engine::Class)

Class * Class::CreateInstance(const char * className)
{
    const TypeInfo * type = FindTypeByName(className);
    if (type != nullptr)
    {
        return type->CallCreateFunc();
    }
    else
    {
        return nullptr;
    }
}

Class * Class::CreateInstance(const HashIndex classHash)
{
    const TypeInfo * type = FindTypeByHashIndex(classHash);
    if (type != nullptr)
    {
        return type->CallCreateFunc();
    }
    else
    {
        return nullptr;
    }
}

void Class::DestroyInstance(Class * instance)
{
    if (instance != nullptr)
    {
        const TypeInfo & type = instance->GetTypeInfo();
        type.CallDestroyFunc(instance);
    }
}

} // namespace Engine {}
