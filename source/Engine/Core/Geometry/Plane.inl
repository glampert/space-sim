
// ================================================================================================
// -*- C++ -*-
// File: Plane.inl
// Author: Guilherme R. Lampert
// Created on: 15/12/13
// Brief: Inline definitions for Plane class.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

inline Plane::Plane(const Plane & p)
    : normal(p.normal)
    , d(p.d)
{
}

inline Plane::Plane(const float p[])
    : normal(p[0], p[1], p[2])
    , d(p[3])
{
}

inline Plane::Plane(const Vector3 & n, const float dist)
    : normal(n)
    , d(dist)
{
}

inline Plane::Plane(const float nx, const float ny, const float nz, const float dist)
    : normal(nx, ny, nz)
    , d(dist)
{
}

inline Plane::Plane(const Vector3 & n, const Vector3 & point)
    : normal(n)
    , d(0)
{
    RecalculateDist(point);
}

inline Plane::Plane(const Vector3 & point1, const Vector3 & point2, const Vector3 & point3)
{
    Set(point1, point2, point3);
}

inline Plane & Plane::operator=(const Plane & p)
{
    normal = p.normal;
    d = p.d;
    return *this;
}

inline Plane & Plane::Set(const Vector3 & n, const float dist)
{
    normal = n;
    d = dist;
    return *this;
}

inline Plane & Plane::Set(const Vector3 & n, const Vector3 & point)
{
    normal = n;
    RecalculateDist(point);
    return *this;
}

inline Plane & Plane::Set(const Vector3 & point1, const Vector3 & point2, const Vector3 & point3)
{
    // Creates the plane from 3 member points:
    normal = normalize(cross((point2 - point1), (point3 - point1)));
    RecalculateDist(point1);
    return *this;
}

inline Plane & Plane::RecalculateDist(const Vector3 & point)
{
    d = -dot(point, normal);
    return *this;
}

inline Plane & Plane::Normalize()
{
    const float invLen = 1.0f / length(normal);
    normal *= invLen;
    d *= invLen;
    return *this;
}

inline Vector3 Plane::GetMemberPoint() const
{
    return normal * -d;
}

inline float Plane::GetDistanceTo(const Vector3 & point) const
{
    return dot(point, normal) + d;
}

inline bool Plane::PlaneIntersection(const Plane & p) const
{
    return length(cross(p.normal, normal)) > 0.0f;
}

inline Vector3 Plane::ProjectPoint(const Vector3 & point) const
{
    return point - normal * GetDistanceTo(point);
}

inline Plane::Side Plane::WhichSide(const Vector3 & point) const
{
    const float distance = GetDistanceTo(point);
    if (distance > 0.0f)
    {
        return SideFront;
    }
    if (distance < 0.0f)
    {
        return SideBack;
    }
    return SideOn; // Point on the plane.
}
