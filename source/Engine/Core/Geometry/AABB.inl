
// ================================================================================================
// -*- C++ -*-
// File: AABB.inl
// Author: Guilherme R. Lampert
// Created on: 21/02/14
// Brief: Inline methods of the AABB class.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

inline AABB::AABB()
    : mins(0.0f, 0.0f, 0.0f)
    , maxs(0.0f, 0.0f, 0.0f)
{
}

inline AABB::AABB(const AABB & other)
    : mins(other.mins)
    , maxs(other.maxs)
{
}

inline AABB::AABB(const float min[], const float max[])
    : mins(min[0], min[1], min[2])
    , maxs(max[0], max[1], max[2])
{
}

inline AABB::AABB(const Vec3f & min, const Vec3f & max)
    : mins(min.x, min.y, min.z)
    , maxs(max.x, max.y, max.z)
{
}

inline AABB::AABB(const Vector3 & min, const Vector3 & max)
    : mins(min)
    , maxs(max)
{
}

inline AABB::AABB(const void * __restrict vertexes, const size_t numVertexes, const size_t vertexStide)
{
    FromMeshVertexes(vertexes, numVertexes, vertexStide);
}

inline void AABB::Clear()
{
    mins = Vector3Infinity;
    maxs = Vector3NegInfinity;
}

inline void AABB::Zero()
{
    mins = Vector3Zero;
    maxs = Vector3Zero;
}

inline bool AABB::IsCleared() const
{
    return mins[0] > maxs[0];
}

inline float AABB::Volume() const
{
    if ((mins[0] >= maxs[0]) || (mins[1] >= maxs[1]) || (mins[2] >= maxs[2]))
    {
        return 0.0f;
    }

    return ((maxs[0] - mins[0]) * (maxs[1] - mins[1]) * (maxs[2] - mins[2]));
}

inline Vector3 AABB::Center() const
{
    return (maxs + mins) * 0.5f;
}

inline bool AABB::ContainsPoint(const Vector3 & p) const
{
    if ((p[0] < mins[0]) || (p[1] < mins[1]) || (p[2] < mins[2]) ||
        (p[0] > maxs[0]) || (p[1] > maxs[1]) || (p[2] > maxs[2]))
    {
        return false;
    }
    return true;
}

inline bool AABB::LineIntersection(const Vector3 & start, const Vector3 & end) const
{
    const Vector3 center = (mins + maxs) * 0.5f;
    const Vector3 extents = maxs - center;
    const Vector3 lineDir = 0.5f * (end - start);
    const Vector3 lineCenter = start + lineDir;
    const Vector3 dir = lineCenter - center;

    const float ld0 = std::fabs(lineDir[0]);
    if (std::fabs(dir[0]) > (extents[0] + ld0))
    {
        return false;
    }

    const float ld1 = std::fabs(lineDir[1]);
    if (std::fabs(dir[1]) > (extents[1] + ld1))
    {
        return false;
    }

    const float ld2 = std::fabs(lineDir[2]);
    if (std::fabs(dir[2]) > (extents[2] + ld2))
    {
        return false;
    }

    const Vector3 vCross = cross(lineDir, dir);

    if (std::fabs(vCross[0]) > (extents[1] * ld2 + extents[2] * ld1))
    {
        return false;
    }

    if (std::fabs(vCross[1]) > (extents[0] * ld2 + extents[2] * ld0))
    {
        return false;
    }

    if (std::fabs(vCross[2]) > (extents[0] * ld1 + extents[1] * ld0))
    {
        return false;
    }

    return true;
}

inline AABB & AABB::AddBounds(const AABB & other)
{
    mins = minPerElem(other.mins, mins);
    maxs = maxPerElem(other.maxs, maxs);
    return *this;
}

inline AABB & AABB::FromMeshVertexes(const void * __restrict vertexes, const size_t numVertexes, const size_t vertexStide)
{
    assert(vertexes != nullptr);
    assert(numVertexes != 0 && vertexStide != 0);

    mins = Vector3Infinity;
    maxs = Vector3NegInfinity;
    const uint8_t * __restrict ptr = reinterpret_cast<const uint8_t *>(vertexes);

    for (size_t i = 0; i < numVertexes; ++i)
    {
        const Vec3f * v = reinterpret_cast<const Vec3f *>(ptr + i * vertexStide);
        const Vector3 xyz(v->x, v->y, v->z);

        mins = minPerElem(xyz, mins);
        maxs = maxPerElem(xyz, maxs);
    }

    return *this;
}

inline AABB & AABB::Transform(const Matrix4 & mat)
{
    Vector4 out;

    out = mat * Point3(mins);
    mins[0] = out[0];
    mins[1] = out[1];
    mins[2] = out[2];

    out = mat * Point3(maxs);
    maxs[0] = out[0];
    maxs[1] = out[1];
    maxs[2] = out[2];

    return *this;
}

inline AABB AABB::Transformed(const Matrix4 & mat) const
{
    AABB copy(*this);
    copy.Transform(mat);
    return copy;
}

inline void AABB::ToPoints(Vector3 points[8]) const
{
    const Vector3 b[2] = { mins, maxs };
    for (int i = 0; i < 8; ++i)
    {
        points[i][0] = b[(i ^ (i >> 1)) & 1][0];
        points[i][1] = b[(i >> 1) & 1][1];
        points[i][2] = b[(i >> 2) & 1][2];
    }
}

inline AABB AABB::Lerp(const float t, const AABB & a, const AABB & b)
{
    return AABB(lerp(t, a.mins, b.mins), lerp(t, a.maxs, b.maxs));
}
