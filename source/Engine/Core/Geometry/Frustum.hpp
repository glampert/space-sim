
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Frustum.hpp
// Author: Guilherme R. Lampert
// Created on: 18/09/14
// Brief: Frustum culling.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Geometry/AABB.hpp"

namespace Engine
{

// ======================================================
// Frustum:
// ======================================================

//
// Frustum class that provides frustum culling checks
// for common computational geometry objects.
//
class Frustum
{
  public:

    // Frustum planes:
    enum
    {
        A,
        B,
        C,
        D
    };
    _VECTORMATH_ALIGNED(float p[6][4]);

    // projection * view:
    Matrix4 clipMatrix;

    // Sets everything to zero / identity.
    Frustum();

    // Construct from camera matrices.
    Frustum(const Matrix4 & view, const Matrix4 & projection);

    // Compute frustum planes from camera matrices. Also sets 'clipMatrix' by multiplying projection * view.
    void ComputeClippingPlanes(const Matrix4 & view, const Matrix4 & projection);

    // ---- Bounding volume => frustum testing: ----

    // Point:
    bool TestPoint(const float x, const float y, const float z) const;
    bool TestPoint(const Vector3 & v) const { return TestPoint(v[0], v[1], v[2]); }
    bool TestPoint(const Vec3f & v) const   { return TestPoint(v.x, v.y, v.z); }

    // Bounding sphere:
    bool TestSphere(const float x, const float y, const float z, const float radius) const;
    bool TestSphere(const Vector3 & center, const float radius) const { return TestSphere(center[0], center[1], center[2], radius); }
    bool TestSphere(const Vec3f & center, const float radius) const   { return TestSphere(center.x, center.y, center.z, radius); }

    // A cube:
    bool TestCube(const float x, const float y, const float z, const float size) const;
    bool TestCube(const Vector3 & center, const float size) const { return TestCube(center[0], center[1], center[2], size); }
    bool TestCube(const Vec3f & center, const float size) const   { return TestCube(center.x, center.y, center.z, size); }

    // Axis-aligned bounding box. True if box is partly intersecting or fully contained in the frustum.
    bool TestAABB(const Vector3 & mins, const Vector3 & maxs) const;
    bool TestAABB(const AABB & aabb) const { return TestAABB(aabb.mins, aabb.maxs); }
};

} // namespace Engine {}
