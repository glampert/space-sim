
// ================================================================================================
// -*- C++ -*-
// File: Frustum.cpp
// Author: Guilherme R. Lampert
// Created on: 18/09/14
// Brief: Frustum culling.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Geometry/Frustum.hpp"

namespace Engine
{

// ======================================================
// NormalizePlane():
// ======================================================

namespace /* unnamed */
{

static void NormalizePlane(float p[4])
{
    // plane *= 1/sqrt(p.a * p.a + p.b * p.b + p.c * p.c);
    const float invLen = 1.0f / std::sqrt((p[0] * p[0]) + (p[1] * p[1]) + (p[2] * p[2]));
    p[0] *= invLen;
    p[1] *= invLen;
    p[2] *= invLen;
    p[3] *= invLen;
}

} // namespace unnamed {}

// ======================================================
// Frustum:
// ======================================================

Frustum::Frustum()
    : clipMatrix(Matrix4::identity())
{
    // Zero fill:
    for (int x = 0; x < 6; ++x)
    {
        for (int y = 0; y < 4; ++y)
        {
            p[x][y] = 0.0f;
        }
    }
}

Frustum::Frustum(const Matrix4 & view, const Matrix4 & projection)
{
    ComputeClippingPlanes(view, projection);
}

void Frustum::ComputeClippingPlanes(const Matrix4 & view, const Matrix4 & projection)
{
    // Compute a clip matrix:
    clipMatrix = projection * view;

    // Compute and normalize the 6 frustum planes:
    const float * m = ToFloatPtr(clipMatrix);
    p[0][A] = m[3]  - m[0];
    p[0][B] = m[7]  - m[4];
    p[0][C] = m[11] - m[8];
    p[0][D] = m[15] - m[12];
    NormalizePlane(p[0]);
    p[1][A] = m[3]  + m[0];
    p[1][B] = m[7]  + m[4];
    p[1][C] = m[11] + m[8];
    p[1][D] = m[15] + m[12];
    NormalizePlane(p[1]);
    p[2][A] = m[3]  + m[1];
    p[2][B] = m[7]  + m[5];
    p[2][C] = m[11] + m[9];
    p[2][D] = m[15] + m[13];
    NormalizePlane(p[2]);
    p[3][A] = m[3]  - m[1];
    p[3][B] = m[7]  - m[5];
    p[3][C] = m[11] - m[9];
    p[3][D] = m[15] - m[13];
    NormalizePlane(p[3]);
    p[4][A] = m[3]  - m[2];
    p[4][B] = m[7]  - m[6];
    p[4][C] = m[11] - m[10];
    p[4][D] = m[15] - m[14];
    NormalizePlane(p[4]);
    p[5][A] = m[3]  + m[2];
    p[5][B] = m[7]  + m[6];
    p[5][C] = m[11] + m[10];
    p[5][D] = m[15] + m[14];
    NormalizePlane(p[5]);
}

bool Frustum::TestPoint(const float x, const float y, const float z) const
{
    for (int i = 0; i < 6; ++i)
    {
        if ((p[i][A] * x + p[i][B] * y + p[i][C] * z + p[i][D]) <= 0.0f)
        {
            return false;
        }
    }
    return true;
}

bool Frustum::TestSphere(const float x, const float y, const float z, const float radius) const
{
    for (int i = 0; i < 6; ++i)
    {
        if ((p[i][A] * x + p[i][B] * y + p[i][C] * z + p[i][D]) <= -radius)
        {
            return false;
        }
    }
    return true;
}

bool Frustum::TestCube(const float x, const float y, const float z, const float size) const
{
    for (int i = 0; i < 6; ++i)
    {
        if ((p[i][A] * (x - size) + p[i][B] * (y - size) + p[i][C] * (z - size) + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * (x + size) + p[i][B] * (y - size) + p[i][C] * (z - size) + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * (x - size) + p[i][B] * (y + size) + p[i][C] * (z - size) + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * (x + size) + p[i][B] * (y + size) + p[i][C] * (z - size) + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * (x - size) + p[i][B] * (y - size) + p[i][C] * (z + size) + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * (x + size) + p[i][B] * (y - size) + p[i][C] * (z + size) + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * (x - size) + p[i][B] * (y + size) + p[i][C] * (z + size) + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * (x + size) + p[i][B] * (y + size) + p[i][C] * (z + size) + p[i][D]) > 0.0f)
        {
            continue;
        }
        return false;
    }
    return true;
}

bool Frustum::TestAABB(const Vector3 & mins, const Vector3 & maxs) const
{
    for (int i = 0; i < 6; ++i)
    {
        if ((p[i][A] * mins[0] + p[i][B] * mins[1] + p[i][C] * mins[2] + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * maxs[0] + p[i][B] * mins[1] + p[i][C] * mins[2] + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * mins[0] + p[i][B] * maxs[1] + p[i][C] * mins[2] + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * maxs[0] + p[i][B] * maxs[1] + p[i][C] * mins[2] + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * mins[0] + p[i][B] * mins[1] + p[i][C] * maxs[2] + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * maxs[0] + p[i][B] * mins[1] + p[i][C] * maxs[2] + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * mins[0] + p[i][B] * maxs[1] + p[i][C] * maxs[2] + p[i][D]) > 0.0f)
        {
            continue;
        }
        if ((p[i][A] * maxs[0] + p[i][B] * maxs[1] + p[i][C] * maxs[2] + p[i][D]) > 0.0f)
        {
            continue;
        }
        return false;
    }
    return true;
}

} // namespace Engine {}
