
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: LineSegment.hpp
// Author: Guilherme R. Lampert
// Created on: 21/02/14
// Brief: Line segment representation. Has a starting point and an end point.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// LineSegment:
// ======================================================

//
// Line segment representation.
// Has a starting point and an end point.
//
class LineSegment
{
  public:

    // Line endpoints:
    Vector3 start;
    Vector3 end;

    LineSegment(); // Constructs at (0,0,0) origin.
    LineSegment(const LineSegment & other);
    LineSegment(const float s[], const float e[]);
    LineSegment(const Vec3f & s, const Vec3f & e);
    LineSegment(const Vector3 & s, const Vector3 & e);

    // Set as a point at the origin.
    void Zero();

    // Infinite line.
    void MakeInfinite();

    // Returns true if line is close to infinity.
    bool IsInfinite() const;

    // Length of the line.
    float Length() const;

    // Squared length of the line.
    float LengthSqr() const;

    // Get a point on the line by linear interpolation. NOTE: 't' is not clamped.
    Vector3 GetPointOnLine(const float t) const;

    // Transform the points defining this line by a given matrix.
    LineSegment & Transform(const Matrix4 & mat);
};

// Include the inline definitions in the same namespace:
#include "Engine/Core/Geometry/LineSegment.inl"

} // namespace Engine {}
