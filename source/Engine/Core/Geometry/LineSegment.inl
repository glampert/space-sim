
// ================================================================================================
// -*- C++ -*-
// File: LineSegment.inl
// Author: Guilherme R. Lampert
// Created on: 21/02/14
// Brief: Inline methods of the LineSegment class.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

inline LineSegment::LineSegment()
    : start(0.0f, 0.0f, 0.0f)
    , end(0.0f, 0.0f, 0.0f)
{
}

inline LineSegment::LineSegment(const LineSegment & other)
    : start(other.start)
    , end(other.end)
{
}

inline LineSegment::LineSegment(const float s[], const float e[])
    : start(s[0], s[1], s[2])
    , end(e[0], e[1], e[2])
{
}

inline LineSegment::LineSegment(const Vec3f & s, const Vec3f & e)
    : start(s.x, s.y, s.z)
    , end(e.x, e.y, e.z)
{
}

inline LineSegment::LineSegment(const Vector3 & s, const Vector3 & e)
    : start(s)
    , end(e)
{
}

inline void LineSegment::Zero()
{
    start = Vector3Zero;
    end   = Vector3Zero;
}

inline void LineSegment::MakeInfinite()
{
    start = Vector3NegInfinity;
    end   = Vector3Infinity;
}

inline bool LineSegment::IsInfinite() const
{
    if ((start[0] >= MathConst::Infinity) || !std::isfinite(float(start[0])) ||
        (end[0]   >= MathConst::Infinity) || !std::isfinite(float(end[0])))
    {
        return false;
    }

    return true;
}

inline float LineSegment::Length() const
{
    return length(end - start);
}

inline float LineSegment::LengthSqr() const
{
    return lengthSqr(end - start);
}

inline Vector3 LineSegment::GetPointOnLine(const float t) const
{
    return lerp(t, start, end);
}

inline LineSegment & LineSegment::Transform(const Matrix4 & mat)
{
    Vector4 out;

    out = mat * Point3(start);
    start[0] = out[0];
    start[1] = out[1];
    start[2] = out[2];

    out = mat * Point3(end);
    end[0] = out[0];
    end[1] = out[1];
    end[2] = out[2];

    return *this;
}
