
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: AABB.hpp
// Author: Guilherme R. Lampert
// Created on: 20/02/14
// Brief: Axis Aligned Bounding Box (AABB).
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// AABB:
// ======================================================

//
// Simple Axis Aligned Bounding Box (AABB).
// Has a min point and a max point for the two extents of a box.
//
class AABB
{
  public:

    // Minimum and maximum extents of the box.
    Vector3 mins;
    Vector3 maxs;

    AABB(); // Constructs at (0,0,0) origin.
    AABB(const AABB & other);
    AABB(const float min[], const float max[]);
    AABB(const Vec3f & min, const Vec3f & max);
    AABB(const Vector3 & min, const Vector3 & max);
    AABB(const void * vertexes, const size_t numVertexes, const size_t vertexStide);

    // Inside-out bounds.
    void Clear();

    // Set as a point at the origin.
    void Zero();

    // Returns true if bounds are inside-out.
    bool IsCleared() const;

    // Returns the volume of the bounds.
    float Volume() const;

    // Returns the center point of the bounds as a Vector3.
    Vector3 Center() const;

    // Test if a point is touching or is inside the box bounds.
    bool ContainsPoint(const Vector3 & p) const;

    // Returns true if the line intersects the bounds between the start and end points.
    bool LineIntersection(const Vector3 & start, const Vector3 & end) const;

    // Add two bounds, only expanding this if other is greater.
    AABB & AddBounds(const AABB & other);

    // Build tightest bounds for a set of mesh vertexes. First element of the vertex must be a Vec3 (the vertex position).
    AABB & FromMeshVertexes(const void * vertexes, const size_t numVertexes, const size_t vertexStide);

    // Transform the points defining this bounds by a given matrix.
    AABB & Transform(const Matrix4 & mat);

    // Get a transformed copy of this AABB.
    AABB Transformed(const Matrix4 & mat) const;

    // Turn the AABB into a set of points defining a box. Useful for debug visualization.
    void ToPoints(Vector3 points[8]) const;

    // Linearly interpolate two AABBs.
    static AABB Lerp(const float t, const AABB & a, const AABB & b);
};

// Include the inline definitions in the same namespace:
#include "Engine/Core/Geometry/AABB.inl"

} // namespace Engine {}
