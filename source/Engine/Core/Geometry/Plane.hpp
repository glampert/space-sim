
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Plane.hpp
// Author: Guilherme R. Lampert
// Created on: 15/12/13
// Brief: Representation of a plane in 3D space.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Plane:
// ======================================================

//
// Representation of a plane in the 3D space.
// Plane equation used: a*x + b*y + c*z + d = 0
//
// This class is built on top of the VectorMath library.
//
class Plane
{
  public:

    Vector3 normal; // Plane normal vector (ax, by, cz)
    float d;        // Distance (d)

    // Plane "sides":
    enum Side
    {
        SideFront, // Point is in front of the plane.
        SideBack,  // Point is behind.
        SideOn     // Point lies on the plane.
    };

    Plane() = default; // Left uninitialized
    Plane(const Plane & p);
    Plane(const float p[]);
    Plane(const Vector3 & n, const float dist);
    Plane(const float nx, const float ny, const float nz, const float dist);
    Plane(const Vector3 & n, const Vector3 & point);
    Plane(const Vector3 & point1, const Vector3 & point2, const Vector3 & point3);

    // Assignment operator.
    Plane & operator=(const Plane & p);

    // Rebuild the plane from a normal and a distance.
    Plane & Set(const Vector3 & n, const float dist);

    // Rebuild the plane from a normal and a point.
    Plane & Set(const Vector3 & n, const Vector3 & point);

    // Rebuild the plane from 3 points.
    Plane & Set(const Vector3 & point1, const Vector3 & point2, const Vector3 & point3);

    // Recalculates the distance from origin by applying a new member point to the plane.
    Plane & RecalculateDist(const Vector3 & point);

    // Normalize this plane.
    Plane & Normalize();

    // Get a member point of the plane.
    Vector3 GetMemberPoint() const;

    // Get the distance to a point. Note that this only works if the plane normal is normalized.
    float GetDistanceTo(const Vector3 & point) const;

    // Test if there is an intersection with another plane.
    bool PlaneIntersection(const Plane & p) const;

    // Project a point into the plane.
    Vector3 ProjectPoint(const Vector3 & point) const;

    // Test in which side of the plane a point is.
    // The "positive side" of the plane is the half space to which the plane
    // normal points (SideFront). The "negative side" is the other half space (SideBack).
    // This function returns 'SideFront' when the point is on the positive side, 'SideBack'
    // when the point is on the negative side, or 'SideOn' when the point is on the plane.
    Side WhichSide(const Vector3 & point) const;
};

// Plane defined as (x:0, y:0, z:0, d:0)
extern const Plane PlaneZero;

// Plane defined as (x:0, y:1, z:0) and a point at the origin.
extern const Plane PlaneYUp;

// Include the inline definitions in the same namespace:
#include "Engine/Core/Geometry/Plane.inl"

} // namespace Engine {}
