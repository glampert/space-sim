
// ================================================================================================
// -*- C++ -*-
// File: CommonAppGLFW.cpp
// Author: Guilherme R. Lampert
// Created on: 23/08/14
// Brief: GLFW application management.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine core:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"
#include "Engine/Application/AppEvent.hpp"
#include "Engine/Application/GameLogicInterfaces.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

// Direct access to the renderer module + GL/GLFW:
#include "Engine/Rendering/RenderManager.hpp"
#include "Engine/Rendering/OpenGL.hpp"

// Title displayed on the menu bar of the window.
// Provide a default if a custom was not defined.
#ifndef APP_NAME_TITLE
#define APP_NAME_TITLE "Demo Game"
#endif // APP_NAME_TITLE

namespace Engine
{

// GLFW is a C API, so it is best to define the
// functions we pass pointers to it as C functions.
extern "C"
{

// ======================================================
// GLFWErrCodeStr():
// ======================================================

static const char * GLFWErrCodeStr(const int errCode)
{
    switch (errCode)
    {
    case GLFW_NOT_INITIALIZED:
        return "GLFW_NOT_INITIALIZED";
    case GLFW_NO_CURRENT_CONTEXT:
        return "GLFW_NO_CURRENT_CONTEXT";
    case GLFW_INVALID_ENUM:
        return "GLFW_INVALID_ENUM";
    case GLFW_INVALID_VALUE:
        return "GLFW_INVALID_VALUE";
    case GLFW_OUT_OF_MEMORY:
        return "GLFW_OUT_OF_MEMORY";
    case GLFW_API_UNAVAILABLE:
        return "GLFW_API_UNAVAILABLE";
    case GLFW_VERSION_UNAVAILABLE:
        return "GLFW_VERSION_UNAVAILABLE";
    case GLFW_PLATFORM_ERROR:
        return "GLFW_PLATFORM_ERROR";
    case GLFW_FORMAT_UNAVAILABLE:
        return "GLFW_FORMAT_UNAVAILABLE";
    default:
        return "Unknown GLFW error";
    } // End switch (errCode)
}

// ======================================================
// GLFWErrorCallback():
// ======================================================

static void GLFWErrorCallback(const int errCode, const char * message)
{
    common->ErrorF("GLFW reported error '%s': %s", GLFWErrCodeStr(errCode), message);
}

// ======================================================
// GLFWKeyCallback():
// ======================================================

static void GLFWKeyCallback(GLFWwindow * win, const int glfwKeyCode, const int sysKeyCode, const int action, const int mods)
{
    assert(common->GetWindowObject() == win);
    UNUSED_VAR(sysKeyCode);
    UNUSED_VAR(win);

    AppEvent event;
    switch (action)
    {
    case GLFW_RELEASE:
        event.type = AppEvent::Type::KeyRelease;
        break;

    case GLFW_PRESS:
        event.type = AppEvent::Type::KeyPress;
        break;

    default:
        // GLFW_REPEAT ignored...
        return;
    } // switch (action)

    // Key codes used by the engine match the ones defined by GLFWv3.
    event.keyboard.keyCode = static_cast<AppEvent::Int>(glfwKeyCode);
    // Key modifiers used by the engine also match the GLFWv3 values.
    event.keyboard.modifiers = static_cast<AppEvent::Int>(mods);

    // Send event to the game layer:
    common->GetGameLogicInstance()->OnAppEvent(event);
}

// ======================================================
// GLFWCharCallback():
// ======================================================

static void GLFWCharCallback(GLFWwindow * win, const unsigned int c)
{
    assert(common->GetWindowObject() == win);
    UNUSED_VAR(win);

    AppEvent event;
    event.type = AppEvent::Type::KeyChar;
    event.keyboard.keyCode = c;
    event.keyboard.modifiers = 0;

    // Send event to the game layer:
    common->GetGameLogicInstance()->OnAppEvent(event);
}

// ======================================================
// GLFWCursorPosCallback():
// ======================================================

static void GLFWCursorPosCallback(GLFWwindow * win, const double x, const double y)
{
    assert(common->GetWindowObject() == win);
    UNUSED_VAR(win);

    // GLFW actually works with the screen origin at the top-left corner.
    // We however, follow the GL notation, with origin at the bottom-left.
    const double invY = static_cast<double>(renderMgr->GetWindowDimensions().height) - y;

    AppEvent event;
    event.type = AppEvent::Type::MouseMove;
    event.mousePos.x = static_cast<AppEvent::Float>(x);
    event.mousePos.y = static_cast<AppEvent::Float>(invY);

    // Send event to the game layer:
    common->GetGameLogicInstance()->OnAppEvent(event);
}

// ======================================================
// GLFWScrollCallback():
// ======================================================

static void GLFWScrollCallback(GLFWwindow * win, const double xOffset, const double yOffset)
{
    assert(common->GetWindowObject() == win);
    UNUSED_VAR(win);

    AppEvent event;
    event.type = AppEvent::Type::MouseScroll;
    event.mouseScroll.xOffset = static_cast<AppEvent::Float>(xOffset);
    event.mouseScroll.yOffset = static_cast<AppEvent::Float>(yOffset);

    // Send event to the game layer:
    common->GetGameLogicInstance()->OnAppEvent(event);
}

// ======================================================
// GLFWMouseButtonCallback():
// ======================================================

static void GLFWMouseButtonCallback(GLFWwindow * win, const int btn, const int action, const int mods)
{
    assert(common->GetWindowObject() == win);
    UNUSED_VAR(win);

    AppEvent event;
    if (action == GLFW_PRESS)
    {
        event.type = AppEvent::Type::MouseBtnClick;
    }
    else
    {
        assert(action == GLFW_RELEASE);
        event.type = AppEvent::Type::MouseBtnRelease;
    }

    // Mouse button numbers used by the engine match GLFWv3 values. No conversion needed.
    event.mouseBtn.buttonId  = static_cast<AppEvent::Int>(btn);
    event.mouseBtn.modifiers = static_cast<AppEvent::Int>(mods);

    // Send event to the game layer:
    common->GetGameLogicInstance()->OnAppEvent(event);
}

} // extern "C"

// ======================================================
// Common singleton: Application management with GLFW.
// ======================================================

void Common::CreateAppContext()
{
    // Initialize the GLFW window and the GLEW:
    InitGLFW();
    InitGLEW();

    // Set GLFW application callbacks:
    assert(windowObj != nullptr);
    GLFWwindow * win = reinterpret_cast<GLFWwindow *>(windowObj);
    glfwSetKeyCallback(win, &GLFWKeyCallback);
    glfwSetCharCallback(win, &GLFWCharCallback);
    glfwSetCursorPosCallback(win, &GLFWCursorPosCallback);
    glfwSetScrollCallback(win, &GLFWScrollCallback);
    glfwSetMouseButtonCallback(win, &GLFWMouseButtonCallback);
    /*
	 * TODO
	glfwSetWindowIconifyCallback   ( win, &GLFWWindowIconifyCallback   );
	glfwSetFramebufferSizeCallback ( win, &GLFWFramebufferSizeCallback );
	*/
}

void Common::ShutdownApp()
{
    PrintF("Shutting down GLFW application...");

    // Destroy app window:
    if (windowObj != nullptr)
    {
        glfwDestroyWindow(reinterpret_cast<GLFWwindow *>(windowObj));
        windowObj = nullptr;
    }

    glfwSetErrorCallback(nullptr); // Remove the callback
    glfwTerminate();
}

void Common::InitGLFW()
{
    PrintF("Initializing GL Framework Library (GLFW) ...");

    // Initialize:
    glfwSetErrorCallback(&GLFWErrorCallback);
    if (!glfwInit())
    {
        FatalErrorF("Failed to initialize GLFW! Unable to continue.");
    }

    PrintF("GLFW version: %s", glfwGetVersionString());

    // Require Core OpenGL 3.2 or higher:
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);

    // Set other window hints:
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_RED_BITS,   8);
    glfwWindowHint(GLFW_GREEN_BITS, 8);
    glfwWindowHint(GLFW_BLUE_BITS,  8);
    glfwWindowHint(GLFW_ALPHA_BITS, 8);

    // Stencil & depth buffer formats:
    int stencilBits = 0, depthBits = 24;
    scriptMgr->GetConfigValue("rendererConfig.stencilBits", stencilBits);
    scriptMgr->GetConfigValue("rendererConfig.depthBits", depthBits);
    glfwWindowHint(GLFW_STENCIL_BITS, stencilBits);
    glfwWindowHint(GLFW_DEPTH_BITS, depthBits);

    // Multisampling AA:
    int msaa = 0;
    scriptMgr->GetConfigValue("rendererConfig.msaa", msaa);
    glfwWindowHint(GLFW_SAMPLES, msaa);

    // Get and validate window dimension:
    int winW = 0, winH = 0;
    scriptMgr->GetConfigValue("rendererConfig.windowWidth", winW);
    scriptMgr->GetConfigValue("rendererConfig.windowHeight", winH);
    if (winW <= 0)
    {
        winW = 1024;
    }
    if (winH <= 0)
    {
        winH = 768;
    }

    bool fullscreen = false;
    scriptMgr->GetConfigValue("rendererConfig.fullscreen", fullscreen);

    // Create a window and its OpenGL context:
    GLFWwindow * windowObj = glfwCreateWindow(winW, winH, APP_NAME_TITLE,
                                              (fullscreen ? glfwGetPrimaryMonitor() : nullptr), nullptr);

    if (windowObj == nullptr)
    {
        glfwTerminate();
        FatalErrorF("Failed to create a GLFW window or OpenGL rendering context!"
                    "Make sure your graphics driver is up-to-date.");
    }

    // Make the window's GL context current:
    glfwMakeContextCurrent(windowObj);

    // Set V-sync:
    bool vsync = false;
    scriptMgr->GetConfigValue("rendererConfig.vsync", vsync);
    if (vsync)
    {
        glfwSwapInterval(1);
    }
    else
    {
        glfwSwapInterval(0);
    }

    // Done. Save window object:
    this->windowObj = windowObj;
    PrintF("GLFW initialized!");
}

void Common::InitGLEW()
{
    PrintF("Initializing the GL Extension Wrangler (GLEW) ...");

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        FatalErrorF("Failed to initialize GLEW! Unable to continue.");
    }

    PrintF("GL_VERSION....: %s", glGetString(GL_VERSION));
    PrintF("GL_RENDERER...: %s", glGetString(GL_RENDERER));
    PrintF("GL_VENDOR.....: %s", glGetString(GL_VENDOR));
    PrintF("GLSL_VERSION..: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));

    // Send the window pointer to the renderer now:
    renderMgr->SetWindowObject(windowObj);
}

void Common::GrabSystemCursor()
{
    assert(windowObj != nullptr);
    glfwSetInputMode(reinterpret_cast<GLFWwindow *>(windowObj), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Common::RestoreSystemCursor()
{
    assert(windowObj != nullptr);
    glfwSetInputMode(reinterpret_cast<GLFWwindow *>(windowObj), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Common::WarpSystemCursor(const Vec2f pos)
{
    assert(windowObj != nullptr);

    // GLFW actually works with the screen origin at the top-left corner.
    // We however, follow the GL notation, with origin at the bottom-left.
    const double invY = static_cast<double>(renderMgr->GetWindowDimensions().height) - pos.y;

    glfwSetCursorPos(reinterpret_cast<GLFWwindow *>(windowObj), pos.x, invY);
}

void Common::MainLoop()
{
    // Must have a GameLogic by now!
    assert(game != nullptr);
    assert(windowObj != nullptr);

    int64_t t0ms, t1ms;
    double deltaTimeMs = (1.0 / 30.0); // Assume initial frame-rate of 60fps

    // Loop until the game wants to quit:
    while (!game->ShouldQuit())
    {
        t0ms = system->ClockMillisec();

        time.currentTime.seconds = MsecToSec(t0ms);
        time.currentTime.milliseconds = t0ms;

        time.deltaTime.seconds = MsecToSec(deltaTimeMs);
        time.deltaTime.milliseconds = deltaTimeMs;

        // User is attempting to close the window. Notify the application.
        // If the GameLogic wishes to comply, it must return true on the next ShouldQuit().
        if (glfwWindowShouldClose(reinterpret_cast<GLFWwindow *>(windowObj)))
        {
            game->SendQuitCommand();
        }

        // Update GameLogic:
        UpdateGame();

        // Render a frame / refresh the screen:
        RenderFrame();

        // Pool system and input events:
        glfwPollEvents();

        t1ms = system->ClockMillisec();
        deltaTimeMs = static_cast<double>(t1ms - t0ms);
    }
}

void Common::UpdateGame()
{
    // This function will be called once at the beginning of every frame. This is the
    // best location for the game application to handle updates to the scene, but is not
    // intended to contain actual rendering calls, which should instead be placed in the
    // RenderFrame() method.
    //
    game->OnUpdate(time);
}

void Common::RenderFrame()
{
    // All rendering calls should be done in here.
    // This function will be called at the end of every frame (after UpdateGame) to perform all
    // the rendering calls for the scene, and it will also be called if the window needs to be
    // repainted. This function will swap buffers and present the rendered frame at the end.
    //
    renderMgr->ClearScreen();
    game->OnRender(time);
    renderMgr->SwapBuffers();
}

} // namespace Engine {}
