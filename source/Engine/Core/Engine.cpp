
// ================================================================================================
// -*- C++ -*-
// File: Engine.cpp
// Author: Guilherme R. Lampert
// Created on: 07/08/14
// Brief: Engine startup and shutdown.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"

namespace Engine
{

// ======================================================
// InitEngine():
// ======================================================

void InitEngine(const int argc, const char * argv[])
{
    // Basic initialization:
    common->Init();
    system->Init();

    // Register runtime type database now:
    InitRuntimeTypeSystem();

    // After ScriptManager startup, run default config scripts
    // and parse the command line, so that command args can
    // overwrite script configurations.
    scriptMgr->Init();
    common->RunEngineConfigScripts();
    common->ExecCmdLine(argc, argv);

    // Create application window/context, now that we have the script configs:
    common->CreateAppContext();

    // Init other subsystems:
    renderMgr->Init();
    resourceMgr->Init();

    // Engine startup completed successfully.
    common->PrintF("Vector/Matrix library: %s", VectorMathImplementationString().c_str());
    common->PrintF("----- Engine startup completed -----");
}

// ======================================================
// ShutdownEngine():
// ======================================================

void ShutdownEngine()
{
    common->PrintF("----- Engine shutting down... ------");

    // Shutdown in reverse order:
    resourceMgr->Shutdown();
    renderMgr->Shutdown();
    scriptMgr->Shutdown();
    system->Shutdown();
    common->Shutdown();
}

// ======================================================
// RunMainLoop():
// ======================================================

void RunMainLoop(GameLogic * game)
{
    // A raw pointer is used here for GameLogic since
    // the Engine will not take ownership of the object
    // and the Engine client might wish to declare it as a
    // global/stack variable, making the use of a smart pointer
    // more trouble than it is worth it.
    //
    common->SetGameLogicInstance(game);

    // Enter main game loop:
    common->PrintF("Running main loop...");
    common->MainLoop();
}

} // namespace Engine {}
