
// ================================================================================================
// -*- C++ -*-
// File: CommonLog.cpp
// Author: Guilherme R. Lampert
// Created on: 23/08/14
// Brief: Methods of the Common singleton related to the Engine log and developer printing.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"

namespace Engine
{

// ======================================================
// Common singleton: Log and printing.
// ======================================================

void Common::InitLog()
{
    // No-op for now.
    // Constructor already sets logStream to stdout.
}

void Common::ShutdownLog()
{
    // Close log stream if needed:
    if (logStream != nullptr)
    {
        if ((logStream != stdout) && (logStream != stderr))
        {
            std::fclose(logStream);
            logStream = nullptr;
        }
    }
}

void Common::PrintF(const char * fmt, ...)
{
    assert(fmt != nullptr);
    if (logStream == nullptr)
    {
        return;
    }

    std::fprintf(logStream, "<c> "); // Message tag (normal color)

    va_list vaList;
    va_start(vaList, fmt);
    std::vfprintf(logStream, fmt, vaList);
    va_end(vaList);

    std::fprintf(logStream, "\n");
}

void Common::WarningF(const char * fmt, ...)
{
    assert(fmt != nullptr);
    if (logStream == nullptr)
    {
        return;
    }

    std::fprintf(logStream, "\033[33;1m<w> "); // Message tag (yellow text)

    va_list vaList;
    va_start(vaList, fmt);
    std::vfprintf(logStream, fmt, vaList);
    va_end(vaList);

    std::fprintf(logStream, "\033[0;1m\n");
}

void Common::ErrorF(const char * fmt, ...)
{
    assert(fmt != nullptr);
    if (logStream == nullptr)
    {
        return;
    }

    std::fprintf(logStream, "\033[31;1m<e> "); // Message tag (red text)

    va_list vaList;
    va_start(vaList, fmt);
    std::vfprintf(logStream, fmt, vaList);
    va_end(vaList);

    std::fprintf(logStream, "\033[0;1m\n");

    // Errors will also flush the log stream:
    std::fflush(logStream);
}

void Common::FatalErrorF(const char * fmt, ...)
{
    assert(fmt != nullptr);

    va_list vaList;
    char errorMessage[MaxTempStringLen];

    va_start(vaList, fmt);
    std::vsnprintf(errorMessage, ArrayLength(errorMessage), fmt, vaList);
    errorMessage[ArrayLength(errorMessage) - 1] = '\0'; // Ensure null terminated
    va_end(vaList);

    if (logStream != nullptr)
    {
        // Log error message:
        std::fprintf(logStream, "\033[31;1m<f> %s\033[0;1m\n", errorMessage);

        // Fatal errors will also flush the log stream:
        std::fflush(logStream);
    }

    // And raise an Engine::FatalErrorException:
    throw FatalErrorException(errorMessage);
}

} // namespace Engine {}
