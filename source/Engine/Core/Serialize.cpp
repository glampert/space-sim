
// ================================================================================================
// -*- C++ -*-
// File: Serialize.cpp
// Author: Guilherme R. Lampert
// Created on: 17/08/14
// Brief: Basic property/data serialization.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Serialize.hpp"
#include "Engine/Core/Common.hpp"

namespace Engine
{

// ======================================================
// SerializerStream:
// ======================================================

SerializerStream::SerializerStream(const bool disallowErrors, const bool disallowWarnings, const bool throwExceptionOnError)
    : errorCount(0)
    , warningCount(0)
    , noErrors(disallowErrors)
    , noWarnings(disallowWarnings)
    , throwExceptions(throwExceptionOnError)
{
}

bool SerializerStream::ErrorF(const char * fmt, ...) const
{
    if (noErrors)
    {
        return true; // Error printing is disabled.
    }

    ++errorCount;

    va_list vaList;
    va_start(vaList, fmt);
    const std::string message = VFormat(fmt, vaList);
    va_end(vaList);

    if (!throwExceptions)
    {
        // Print to default error output and proceed normally.
        common->ErrorF("SerializerStream error: %s", message.c_str());
    }
    else
    {
        // Fatal errors are allowed. Every error throws an exception.
        throw SerializerStream::Error(message);
    }

    // Always return false to allow the idiom:
    //   ...
    //   return SerializerStream::ErrorF("Problems...");
    //
    return false;
}

void SerializerStream::WarningF(const char * fmt, ...) const
{
    if (noWarnings)
    {
        return; // Warning printing is disabled.
    }

    ++warningCount;

    va_list vaList;
    va_start(vaList, fmt);
    const std::string message = VFormat(fmt, vaList);
    va_end(vaList);

    common->WarningF("SerializerStream warning: %s", message.c_str());
}

int SerializerStream::GetErrorCount() const
{
    return errorCount;
}

int SerializerStream::GetWarningCount() const
{
    return warningCount;
}

} // namespace Engine {}
