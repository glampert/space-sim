
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Utilities.hpp
// Author: Guilherme R. Lampert
// Created on: 07/08/14
// Brief: Miscellaneous Engine helpers and utilities.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// ======================================================
// #includes:
// ======================================================

#include <cstdint>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <memory>
#include <string>
#include <vector>
#include <exception>
#include <type_traits>

#include "Engine/Core/SingleInstanceType.hpp"
#include "Engine/Core/ExceptionTypes.hpp"
#include "Engine/Core/Math/MathUtils.hpp"
#include "Engine/Core/Hash.hpp"

// ======================================================
// Global macros and definitions:
// ======================================================

//
// GCC/Clang defines __func__, whereas Visual Studio defines __FUNCTION__.
// NOTE: On GCC/Clang __func__ is actually a local variable, thus only valid inside a function.
//
#ifndef __FUNCTION__
#ifdef __GNUC__
#define __FUNCTION__ __func__
#else // !__GNUC__
#define __FUNCTION__ "???"
#endif // __GNUC__
#endif // __FUNCTION__

//
// GCC only extension that helps the compiler validate
// format strings: __attribute__((format...))
//
#ifdef __GNUC__
#define ATTRIBUTE_FORMAT_FUNC(baseFunc, stringIndex, firstToCheck) __attribute__((format(baseFunc, stringIndex, firstToCheck)))
#else // !__GNUC__
#define ATTRIBUTE_FORMAT_FUNC(baseFunc, stringIndex, firstToCheck) /* Nothing */
#endif // __GNUC__

//
// This attribute tells the compiler that a function does not return.
// As a consequence, the compiler knows that any code following a call
// to that function is unreachable and don't issue a warning.
//
#if defined(__GNUC__)
#define NO_RETURN_FUNC(prototype) prototype __attribute__((noreturn))
#elif defined(_MSC_VER)
#define NO_RETURN_FUNC(prototype) __declspec(noreturn) prototype
#else // !__GNUC__ && !_MSC_VER
#define NO_RETURN_FUNC(prototype) prototype
#endif

//
// Disables copy and assignment for a user defined type.
// NOTE: The '= delete;' can be removed if not compiling C++11.
//
#define DISABLE_COPY_AND_ASSIGN(className) \
  private:                                 \
    className(const className &) = delete; \
    className & operator=(const className &) = delete;

//
// Makes a class non-instantiable.
// Useful for implementing the "Monostate" pattern.
//
#define NON_INSTANTIABLE(className)        \
  private:                                 \
    className() = delete;                  \
    className(const className &) = delete; \
    className & operator=(const className &) = delete;

//
// Set a given bit of an integer.
// Used with bitfield enums.
//
#define BITFIELD_ENUM(n) (1 << (n))

//
// Avoid compiler warnings about unused variables.
//
#define UNUSED_VAR(x) (void)x

//
// Appends two tokens into a single name/identifier.
// Normally used to declared internal/built-in functions and variables.
//
#define HELPER_TOKEN_APPEND2(x, y) x ## y
#define TOKEN_APPEND2(x, y) HELPER_TOKEN_APPEND2(x, y)

//
// Appends three tokens into a single name/identifier.
// Normally used to declared internal/built-in functions and variables.
//
#define HELPER_TOKEN_APPEND3(x, y, z) x ## y ## z
#define TOKEN_APPEND3(x, y, z) HELPER_TOKEN_APPEND3(x, y, z)

//
// All the Engine stuff will be inside this namespace.
//
namespace Engine
{

// ======================================================
// Inline functions / miscellaneous helpers:
// ======================================================

// Length of statically allocated C++ arrays.
template <typename T, size_t N>
constexpr size_t ArrayLength(const T(&)[N]) noexcept
{
    return N;
}

// Zero fills an object. This will not enforce that the type in question is POD. Use with caution.
template <typename T>
void ZeroObjectUnchecked(T & s) noexcept
{
    std::memset(&s, 0, sizeof(T));
}

// Zero fills a POD type, such as a C struct or union.
template <typename T>
void ZeroPodObject(T & s) noexcept
{
    static_assert(std::is_pod<T>::value, "Type must be Plain Old Data!");
    std::memset(&s, 0, sizeof(T));
}

// Zero fills a statically allocated array of POD or built-in types. Array length inferred by the compiler.
template <typename T, size_t N>
void ZeroArray(T(&arr)[N]) noexcept
{
    static_assert(std::is_pod<T>::value, "Type must be Plain Old Data!");
    std::memset(arr, 0, sizeof(T) * N);
}

// Zero fills an array of POD or built-in types, with array length provided by the caller.
template <typename T>
void ZeroArray(T * arrayPtr, const size_t arrayLength) noexcept
{
    static_assert(std::is_pod<T>::value, "Type must be Plain Old Data!");
    assert(arrayPtr != nullptr && arrayLength != 0);
    std::memset(arrayPtr, 0, sizeof(T) * arrayLength);
}

// ======================================================
// DataBlob struct and friends:
// ======================================================

//
// Pair of void pointer and its size in bytes.
// DataBlob DOES NOT free any memory in the destructor!
// This type is not meant to replace a smart pointer.
//
struct DataBlob
{
    // Public data:
    const uint8_t * data;
    size_t sizeInBytes;

    // Set everything to null/zero.
    DataBlob() noexcept
        : data(nullptr)
        , sizeInBytes(0)
    { }

    // Constructor with params:
    DataBlob(const uint8_t * d, const size_t s) noexcept
        : data(d)
        , sizeInBytes(s)
    { }

    // Test if both the data pointer and size are valid.
    bool IsValid() const noexcept
    {
        return (data != nullptr) && (sizeInBytes != 0);
    }

    // Explicitly frees the data blob. Note that this is NOT done automatically by the destructor!
    void FreeData() noexcept
    {
        delete[] data;
        data = nullptr;
        sizeInBytes = 0;
    }

    // Load the contents of a file into main memory and return it as a DataBlob.
    static DataBlob LoadFile(const std::string & filename, const bool isBinary);
};

// ======================================================
// String utilities:
// ======================================================

// std::vector (array) of std::strings.
using StringArray = std::vector<std::string>;

// Max length for temp strings, format strings and stack declared char buffers.
constexpr int MaxTempStringLen = 4096;

// Duplicate string on heap memory. Free returned pointer with delete[].
char * DupCString(const char * input);

// This is a safer version of the strcpy() routine that only copy up to 'maxCount' characters to the dest buffer,
// ensuring the output string is always null terminated, even in case of overflow.
// Returns the number of character written to 'destStr'.
size_t CopyCString(char * destStr, const size_t maxCount, const char * srcStr) noexcept;

// A sequence of calls to this function splits 'str' into tokens, which are sequences
// of contiguous characters separated by any of the characters that are part of delimiters.
char * TokenizeCString(char * str, const char * delimiters, char ** remaining) noexcept;

// Split a string into a list of tokens according to the provided delimiters.
StringArray SplitString(const std::string & source, const std::string & delimiters);

// Compare to strings ignored the word case. Result is the same of strcmp().
int CaseInsensitiveStringCompare(const std::string & str1, const std::string & str2);

// Test for prefix/postfix (case sensitive):
bool StringStartsWith(const std::string & str, const std::string & prefix);
bool StringEndsWith(const std::string & str, const std::string & postfix);

// Returns the extension of a filename (everything after the last dot '.') or an empty string if no dot in the name.
std::string ExtractFilenameExtension(const std::string & filename, const bool includeDot);

// Trims a string representing a floating-point number to remove unnecessary trailing zeros.
std::string RemoveTrailingFloatZeros(const std::string & floatStr);

// Memory unit/size to printable string:
std::string FormatMemoryUnit(const uint64_t memorySizeInBytes, const bool abbreviated = false);

// Safer string formatting:
std::string Format(const char * format, ...) ATTRIBUTE_FORMAT_FUNC(printf, 1, 2);
std::string VFormat(const char * format, va_list vaList) ATTRIBUTE_FORMAT_FUNC(printf, 1, 0);

// Trim strings (in-place):
std::string & LTrimString(std::string & s); // Trim from start
std::string & RTrimString(std::string & s); // Trim from end
std::string & TrimString(std::string & s);  // Trim from both ends

// Misc to-string helpers:
std::string ToString(const float f);  // Trims zeros at the end
std::string ToString(const double d); // Trims zeros at the end
std::string ToString(const Vec2i & v);
std::string ToString(const Vec3i & v);
std::string ToString(const Vec4i & v);
std::string ToString(const Vec2u & v);
std::string ToString(const Vec3u & v);
std::string ToString(const Vec4u & v);
std::string ToString(const Vec2f & v);
std::string ToString(const Vec3f & v);
std::string ToString(const Vec4f & v);
std::string ToString(const Vector3 & v);
std::string ToString(const Vector4 & v);

// Parse numbers/tuples from strings.
// Return zero on failure, setting 'success' to false if flag is provided. No exceptions raised.
// For ParseInt(), 'base' is optional. It will be inferred from the string if you pass 0.
int ParseInt(const std::string & s, bool * success = nullptr, const int base = 0) noexcept;
int64_t ParseInt64(const std::string & s, bool * success = nullptr, const int base = 0) noexcept;
float ParseFloat(const std::string & s, bool * success = nullptr) noexcept;
double ParseDouble(const std::string & s, bool * success = nullptr) noexcept;

// Tries to parse a tuple of floating-point or integer values from a string.
// The expected format of the string is, for example:
//   "(1,2,3)"
// or
//   "( 1.1, 2.2, 3.3 )"
// Any number of spaces is accepted between the values.
// The string must begin with a parenthesis '(' and end with the corresponding closing one.
// 'numElements' must be either 2, 3 or 4. Larger sizes are currently not supported.
// If this functions fails, the input buffer remains unaltered.
bool ParseIntTuple(const std::string & data, int * dest, const int numElements) noexcept;
bool ParseFloatTuple(const std::string & data, float * dest, const int numElements) noexcept;

// Better option to sprintf()/snprintf().
// Does not allocate memory directly. No std::string used.
// Format()/VFormat() should be preferred though.
template <size_t N>
ATTRIBUTE_FORMAT_FUNC(printf, 2, 3)
char * SNPrintF(char(&buf)[N], const char * format, ...) noexcept
{
    assert(format != nullptr);

    va_list vaList;
    va_start(vaList, format);
    const int result = std::vsnprintf(buf, N, format, vaList);
    va_end(vaList);

    if (result < 0)
    {
        assert(false && "vsnprintf() failed!");
        ZeroArray(buf); // Clear the string.
        return buf;
    }
    if (static_cast<size_t>(result) >= N)
    {
        assert(false && "vsnprintf() overflowed buffer!");
        buf[N - 1] = '\0'; // Truncate the string.
        return buf;
    }

    buf[result] = '\0'; // Ensure null terminated.
    return buf;
}

// ======================================================
// UTF text handling.
// ======================================================

// Multibyte character byte-order:
enum class UnicodeByteOrder
{
    LittleEndian,
    BigEndian,
    // This will need to be changed if porting
    // to a BigEndian system!
    Native = LittleEndian
};

// This function will attempt to decode a UTF-8 encoded character in the buffer.
// If the encoding is invalid, the function returns -1. 'outCharLength' is optional.
int DecodeUTF8(const char * encodedBuffer, unsigned int * outCharLength);

// This function will attempt to decode a UTF-16 encoded character in the buffer.
// If the encoding is invalid, the function returns -1. 'outCharLength' is optional.
int DecodeUTF16(const char * encodedBuffer, unsigned int * outCharLength,
                const UnicodeByteOrder byteOrder = UnicodeByteOrder::Native);

// ======================================================
// checked_cast<T>() / checked_pointer_cast<T>():
// ======================================================

//
// checked_cast/check_type -> Safer version of static_cast() for polymorphic types.
// Does runtime checking on debug builds. Uses a normal fast static_cast on release builds.
//

template <typename T, typename U>
bool check_type(U * u)
{
    return dynamic_cast<T>(u) != nullptr;
}

template <typename T, typename U>
bool check_type(U & u)
{
    try
    {
        (void)dynamic_cast<T>(u);
        return true;
    }
    catch (const std::bad_cast &)
    {
    }
    return false;
}

// Overload for raw pointer types:
template <typename T, typename U>
T checked_cast(U * u)
{
// dynamic_cast<> is slower but fails with a null pointer
// if the cast is not possible. (check_type)
#if ENGINE_DEBUG_BUILD
    if (!check_type<T, U>(u))
    {
        throw Exception("dynamic_cast() failed!");
    }
#endif // ENGINE_DEBUG_BUILD

    return static_cast<T>(u);
}

// Overload for reference types:
template <typename T, typename U>
T checked_cast(U & u)
{
// dynamic_cast<> is slower but fails with an exception
// if the cast is not possible. (check_type)
#if ENGINE_DEBUG_BUILD
    if (!check_type<T, U>(u))
    {
        throw Exception("dynamic_cast() failed!");
    }
#endif // ENGINE_DEBUG_BUILD

    return static_cast<T>(u);
}

// Operates on std::shared_ptr:
template <typename T, typename U>
std::shared_ptr<T> checked_pointer_cast(const std::shared_ptr<U> & u)
{
// dynamic_pointer_cast<> is slower but fails with a null pointer if the cast is not possible.
#if ENGINE_DEBUG_BUILD
    if (std::dynamic_pointer_cast<T, U>(u) == nullptr)
    {
        throw Exception("dynamic_pointer_cast() failed!");
    }
#endif // ENGINE_DEBUG_BUILD

    return std::static_pointer_cast<T, U>(u);
}

// ======================================================
// D-like scope(exit):
// ======================================================

// Scope-exit helpers:
template <typename F>
struct ScopeExitType
{
    F fn;
    ScopeExitType(F f)
        : fn(f) {}
    ~ScopeExitType() { fn(); }
};
template <typename F>
ScopeExitType<F> MakeScopeExit(F f) { return ScopeExitType<F>(f); }

//
// Actually declare the scope exit block.
// You can add as many of these as you want inside the scope.
// It should only be used inside functions, for obvious reasons.
//
// Usage example:
//   SCOPE_EXIT( { printf("exiting a scope..."); } );
//
#define SCOPE_EXIT(codeBlock) auto TOKEN_APPEND2(_scope_exit_, __LINE__) = MakeScopeExit([=]() mutable codeBlock)

} // namespace Engine {}
