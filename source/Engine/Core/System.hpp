
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: System.hpp
// Author: Guilherme R. Lampert
// Created on: 11/08/14
// Brief: Operating System / platform dependent services.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Helper types / structures:
// ======================================================

// File status and information.
struct FileStats
{
    uint64_t fileLength;         // File length in bytes.
    time_t creationTime;         // Creation timestamp.
    time_t lastAccessTime;       // Last access on file.
    time_t lastModificationTime; // Last modification on file.
    bool isDirectory;            // True if the path provided actually pointed to a directory.
    bool isNormalFile;           // True is the path provided pointed to a file.
};

// ======================================================
// System singleton:
// ======================================================

class System final
    : public SingleInstanceType<System>
{
  public:

    // Standard path separator char and string (public):
    static const std::string PathSepString;
    static const char PathSepChar;

    // Not directly used.
    // 'system' global is provided instead.
    System();

    // Initialization and shutdown. Called internally by the Engine.
    void Init();
    void Shutdown();

    // Get the number of milliseconds elapsed since the application started.
    int64_t ClockMillisec() const;

    // Fixes a non-standard file path. I.e.: Replaces path separators that are different from the used one ('/'), etc.
    std::string FixFilePath(const std::string & path) const;

    // Miscellaneous file system queries:
    bool PathExist(const std::string & pathname);
    bool PathExistAndIsFile(const std::string & pathname);
    bool PathExistAndIsDirectory(const std::string & pathname);
    bool FileStatsForPath(const std::string & pathname, FileStats & fileStats, std::string * errorStr = nullptr);
    uint64_t QueryFileLength(const std::string & filename, std::string * errorStr = nullptr);

  private:

    DISABLE_COPY_AND_ASSIGN(System)
};

// Globally unique instance:
extern System * system;

} // namespace Engine {}
