
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Engine.hpp
// Author: Guilherme R. Lampert
// Created on: 06/08/14
// Brief: Main header for the Engine module.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Core/common Engine subsystems and utilities:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"
#include "Engine/Core/Serialize.hpp"
#include "Engine/Core/RuntimeTypeSystem.hpp"

// Resource management:
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourceManager.hpp"

// Renderer subsystem:
#include "Engine/Rendering/Camera.hpp"
#include "Engine/Rendering/Buffers.hpp"
#include "Engine/Rendering/ShaderProgram.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/Font.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Rendering/Model3D.hpp"
#include "Engine/Rendering/BillboardBatchRenderer.hpp"
#include "Engine/Rendering/SpriteBatchRenderer.hpp"
#include "Engine/Rendering/Basic2DTextRenderer.hpp"
#include "Engine/Rendering/ParticleRenderer.hpp"
#include "Engine/Rendering/SkyboxRenderer.hpp"
#include "Engine/Rendering/DebugRenderer.hpp"
#include "Engine/Rendering/RenderManager.hpp"

// Scripting subsystem:
#include "Engine/Scripting/ScriptManager.hpp"

// Client game application <=> Engine bridge:
#include "Engine/Application/AppEvent.hpp"
#include "Engine/Application/GameLogicInterfaces.hpp"
#include "Engine/Application/HumanGameView.hpp"
#include "Engine/Application/BaseGameLogic.hpp"

// Input subsystem:
#include "Engine/Input/Keys.hpp"
#include "Engine/Input/InputHandlers.hpp"
#include "Engine/Input/XB360Controller.hpp"

// Networking:
#include "Engine/Networking/NetCommon.hpp"
#include "Engine/Networking/Sockets.hpp"

//
// All the Engine stuff will be inside this namespace.
//
namespace Engine
{

// ======================================================
// General Engine startup and shutdown:
// ======================================================

void InitEngine(const int argc, const char * argv[]);
void RunMainLoop(GameLogic * game);
void ShutdownEngine();

} // namespace Engine {}
