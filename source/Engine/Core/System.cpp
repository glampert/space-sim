
// ================================================================================================
// -*- C++ -*-
// File: System.cpp
// Author: Guilherme R. Lampert
// Created on: 11/08/14
// Brief: Operating System / platform dependent services.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"

// POSIX includes:
#include <sys/types.h> // System types
#include <sys/stat.h>  // stat()/fstat()
#include <cerrno>      // 'errno' and error codes
#include <unistd.h>

// mach_absolute_time()/mach_timebase_info() (OSX an iOS).
#include <mach/mach_time.h>

namespace Engine
{

// ======================================================
// Mac OSX and iOS high precision timer:
// ======================================================

namespace /* unnamed */
{

static int64_t clockFreq;
static int64_t baseTime;

static void InitHiResClock()
{
    baseTime = static_cast<int64_t>(mach_absolute_time());

    // Get the clock frequency once, at startup.
    // This value doesn't change while the system is running.
    mach_timebase_info_data_t timeBaseInfo;
    const kern_return_t result = mach_timebase_info(&timeBaseInfo);

    if (result == KERN_SUCCESS)
    {
        clockFreq = (timeBaseInfo.numer / timeBaseInfo.denom);
    }
    else
    {
        common->FatalErrorF("High resolution clock is unavailable!");
    }
}

} // namespace unnamed {}

// ======================================================
// System singleton:
// ======================================================

// Standard path separator char and string (public):
const std::string System::PathSepString = "/";
const char System::PathSepChar = '/';

System::System()
{
}

void System::Init()
{
    common->PrintF("---------- System::Init() ----------");
    InitHiResClock();

    // FIXME this is only needed when we run from "build/"
    // while the game resources are on the parent dir, under "resources/"
    chdir("..");
}

void System::Shutdown()
{
    common->PrintF("-------- System::Shutdown() --------");
}

int64_t System::ClockMillisec() const
{
    // clockFreq is in nanosecond precision (Not Bad!)
    return (((static_cast<int64_t>(mach_absolute_time()) - baseTime) * clockFreq) / 1000000);
}

std::string System::FixFilePath(const std::string & path) const
{
    std::string result;
    const size_t len = path.length();

    result.reserve(len + 1);
    for (size_t i = 0; i < len; ++i)
    {
        if (path[i] != '\\')
        {
            result.push_back(path[i]);
        }
        else
        {
            result.push_back(PathSepChar);
        }
    }

    return result;
}

bool System::PathExist(const std::string & pathname)
{
    FileStats fileStats;
    if (!FileStatsForPath(pathname, fileStats))
    {
        return false; // Path doesn't exist
    }
    return true; // Path is there
}

bool System::PathExistAndIsFile(const std::string & pathname)
{
    FileStats fileStats;
    if (!FileStatsForPath(pathname, fileStats))
    {
        return false; // Path doesn't exist
    }
    return fileStats.isNormalFile && !fileStats.isDirectory;
}

bool System::PathExistAndIsDirectory(const std::string & pathname)
{
    FileStats fileStats;
    if (!FileStatsForPath(pathname, fileStats))
    {
        return false; // Path doesn't exist
    }
    return fileStats.isDirectory && !fileStats.isNormalFile;
}

uint64_t System::QueryFileLength(const std::string & filename, std::string * errorStr)
{
    FileStats fileStats;
    if (!FileStatsForPath(filename, fileStats, errorStr))
    {
        return 0; // Path doesn't exist. 'errorStr' will carry additional info.
    }
    return fileStats.fileLength;
}

bool System::FileStatsForPath(const std::string & pathname, FileStats & fileStats, std::string * errorStr)
{
    assert(!pathname.empty());

    // Clear output first:
    ZeroPodObject(fileStats);

    // System call:
    errno = 0;
    struct stat statBuf;
    const int result = stat(pathname.c_str(), &statBuf);

    if (result == 0)
    {
        fileStats.fileLength = static_cast<uint64_t>(statBuf.st_size);
        fileStats.creationTime = statBuf.st_ctime;
        fileStats.lastAccessTime = statBuf.st_atime;
        fileStats.lastModificationTime = statBuf.st_mtime;
        fileStats.isDirectory = S_ISDIR(statBuf.st_mode) ? true : false;
        fileStats.isNormalFile = S_ISREG(statBuf.st_mode) ? true : false;
        return true;
    }
    else // stat() failed. Return error string is object was provided:
    {
        if (errorStr != nullptr)
        {
            errorStr->assign(Format("stat(\'%s\') failed! Reason: %s", pathname.c_str(), std::strerror(errno)));
        }
        return false;
    }
}

// ======================================================
// Global System instance:
// ======================================================

static System theSystem;
System * system = &theSystem;

} // namespace Engine {}
