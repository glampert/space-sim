
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: XMLReader.hpp
// Author: Guilherme R. Lampert
// Created on: 05/05/13
// Brief: Tiny XML reader.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// XMLTextFormat:
// ======================================================

// Enumeration of all supported XML source text file formats.
enum class XMLTextFormat
{
    ASCII,    // ASCII, file without byte order mark, or not a text file at all!.
    UTF8,     // UTF-8 format.
    UTF16_BE, // UTF-16 format, big endian.
    UTF16_LE, // UTF-16 format, little endian.
    UTF32_BE, // UTF-32 format, big endian.
    UTF32_LE  // UTF-32 format, little endian.
};

// Converts a XMLTextFormat flag to a printable string, for debugging purposes.
std::string XMLTextFormatToString(const XMLTextFormat fmt);

// ======================================================
// XMLNodeType:
// ======================================================

// Enumeration for all XML nodes which are parsed by the XMLReader.
enum class XMLNodeType
{
    None,       // No XML node. This is usually the node if you didn't read anything yet.
    Element,    // A XML element: <foo>
    ElementEnd, // End of a XML element: </foo>
    Text,       // Text within a XML element: <foo> this is the text. </foo>
    Comment,    // A XML comment: <!-- I am a comment -->
    CDATA,      // A XML CDATA section: <![CDATA[ this is some CDATA ]]>
    Unknown     // Unknown/unrecognized element: <?xml ... ?> or something else.
};

// Converts a XMLNodeType flag to a printable string, for debugging purposes.
std::string XMLNodeTypeToString(const XMLNodeType type);

// ======================================================
// XMLReader:
// ======================================================

//
// Tiny XML reader, designed to be fast.
//
// This XML reader is designed to be used as a "trow away" object,
// that is, use the object once, usually as a stack variable, and destroy it.
// The constructor is the only method that accepts input data.
//
// Note: The input XML file is not validated and assumed to be correct.
//
template <typename CharType>
class XMLReader
{
  public:

    // Usually the same as std::string or std::wstring.
    using StringType = std::basic_string<CharType>;

    // Input data blob passed to constructor must remain valid for as long
    // as the reader lives! It will not copy the data, but keep a reference to it.
    XMLReader(const DataBlob blob);

    // Check if the data provided at creation was in good state and was successfully read.
    bool IsValid() const;

    // Read forward to the next XML node.
    // Returns false if there are no more nodes to read.
    bool ReadNextNode();

    // Returns the name of the current node.
    const StringType & NodeName() const;

    // Returns the type of the current XML node.
    XMLNodeType NodeType() const;

    // Returns the text or CDATA section of the current node.
    const StringType & NodeData() const;

    // Returns if the current element (node) is an empty element, like <foo />
    bool IsEmptyElement() const;

    // Returns the attribute count of the current XML node.
    unsigned int AttributeCount() const;

    // Returns the name of the attribute, null if an attribute with this index does not exist.
    // 'idx' is a zero based index between 0 and AttributeCount()-1.
    const CharType * AttributeName(const unsigned int idx) const;

    // Returns the value of the attribute, null if an attribute with this index does not exist.
    // 'idx' is a zero based index between 0 and AttributeCount()-1.
    const CharType * AttributeValue(const unsigned int idx) const;

    // Returns the value of the attribute, null if an attribute with this name does not exist.
    const CharType * AttributeValue(const CharType * name) const;

    // Like AttributeValue(), but does not return null if the attribute doesn't not exist.
    // An empty string ("") is returned in such case.
    StringType AttributeValueSafe(const CharType * name) const;

    // Returns the value of the attribute as integer or 0 if an attribute with this
    // name does not exist or the value could not be interpreted as an integer.
    int AttributeValueAsInt(const CharType * name) const;

    // Returns the value of the attribute as integer or 0 if an attribute with this
    // index does not exist or the value could not be interpreted as an integer.
    int AttributeValueAsInt(const unsigned int idx) const;

    // Returns the value of the attribute as float or 0 if an attribute with this
    // name does not exist or the value could not be interpreted as float.
    float AttributeValueAsFloat(const CharType * name) const;

    // Returns the value of the attribute as float, and 0 if an attribute with this
    // index does not exist or the value could not be interpreted as float.
    float AttributeValueAsFloat(const unsigned int idx) const;

    // Returns the format of the source XML file.
    // It is not necessary to use this method because the parser will convert the input file format
    // to the format wanted by the user when creating the parser. This method is useful to get/display additional information.
    XMLTextFormat SourceFormat() const;

    // Returns format of the strings returned by the parser.
    // This will be UTF8 for example when you created a reader with
    // char8 and UTF32 when it has been created using char32.
    XMLTextFormat ReaderFormat() const;

  private:

    // Helper structure for storing <attribute, name> pairs:
    struct Attribute
    {
        StringType name;
        StringType value;
    };

    // Array of attributes:
    using AttribArray = std::vector<Attribute>;

    // Helper methods:
    const Attribute * FindAttributeByName(const CharType * name) const;
    void ParseCurrentNode();
    bool SetText(const CharType * start, const CharType * end);
    void ParseClosingXMLElement();
    void IgnoreDefinition();
    bool ParseCDATA();
    void ParseComment();
    void ParseOpeningXMLElement();
    void InitParser();
    static bool IsLittleEndian(const XMLTextFormat fmt);
    static void ReplaceEntityReferences(std::string & str);
    static void ReplaceEntityReferences(std::wstring & str);
    template <class SrcCharType>
    void SetParseData(const SrcCharType * source, const uint8_t * pointerToStore, const size_t sizeInChars);

    // Attributes of current node:
    AttribArray attributes;

    // Type of the currently parsed node:
    XMLNodeType currentNodeType;

    // Source format of the XML data:
    XMLTextFormat sourceFormat;

    // Name of the node currently in or text/CDATA section:
    StringType nodeNameData;

    // Other reader states:
    size_t textChars;           // Size of text to parse in characters.
    const CharType * textData;  // Data block of the text file.
    const CharType * textBegin; // Start of text to parse.
    const CharType * ptr;       // Current point in text to parse.
    bool isEmptyElement;        // Is the currently parsed node empty?

    // Keep a reference just to test in IsValid().
    const DataBlob inputData;

    DISABLE_COPY_AND_ASSIGN(XMLReader)
};

// Handy aliases:
using XMLReaderUTF8     = XMLReader<char>;
using XMLReaderWideChar = XMLReader<wchar_t>;

// Include the inline definitions in the same namespace:
#include "Engine/Core/XMLReader.inl"

} // namespace Engine {}
