
// ================================================================================================
// -*- C++ -*-
// File: XML.cpp
// Author: Guilherme R. Lampert
// Created on: 28/04/13
// Brief: Data structures and functions used for both XML reading and writing.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Engine common:
#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/Containers/DestructionPolicies.hpp"
#include "Engine/Core/Containers/IntrusiveSList.hpp"

// XML library:
#include "Engine/Core/XML.hpp"
#include "Engine/Core/XMLReader.hpp"

#include <cwchar>
#include <cctype>

namespace Engine
{

// ======================================================
// XMLAttribute:
// ======================================================

XMLAttribute::XMLAttribute(const std::string & attrName, const std::string & attrVal)
    : name(attrName)
    , value(attrVal)
{
    if (!IsValidXMLElementAttrName(name))
    {
        common->FatalErrorF("Invalid XML attribute name: \"%s\"", name.c_str());
    }
}

XMLAttribute * XMLAttribute::NewAttribute(const std::string & attrName, const std::string & attrVal)
{
    return new XMLAttribute(attrName, attrVal);
}

// ======================================================
// XMLElement:
// ======================================================

XMLElement::XMLElement(const std::string & elemName)
    : name(elemName)
    , parent(nullptr)
    , cdata(nullptr)
    , textBody(nullptr)
{
    if (!IsValidXMLElementAttrName(name))
    {
        common->FatalErrorF("Invalid XML element name: \"%s\"", name.c_str());
    }
}

XMLElement::~XMLElement()
{
    // Free text body and CDATA, if any:
    delete[] textBody;
    delete[] cdata;

    // Lists will delete children/attributes automatically.
}

XMLRoot * XMLElement::NewRoot(const std::string & elemName)
{
    return new XMLRoot(elemName);
}

XMLElement * XMLElement::NewElement(const std::string & elemName)
{
    return new XMLElement(elemName);
}

XMLElement * XMLElement::AddChild(XMLElement * child)
{
    assert(child != nullptr);
    children.push_back(child);
    return child;
}

bool XMLElement::RemoveChild(const std::string & childName)
{
    // Find child's iterator, then remove it:
    auto childIterator = begin(children);
    auto childListEnd = end(children);

    while (childIterator != childListEnd)
    {
        if ((*childIterator)->name == childName)
        {
            XMLElement * child = (*childIterator);
            // Remove unlinks from the data structure, but does not deletes the object.
            children.erase(childIterator);
            ElementDisposer::Destroy(child);
            return true;
        }
        ++childIterator;
    }

    return false; // Child not found
}

XMLElement * XMLElement::FindChild(const std::string & childName)
{
    auto childIterator = begin(children);
    auto childListEnd = end(children);

    while (childIterator != childListEnd)
    {
        if ((*childIterator)->name == childName)
        {
            return *childIterator;
        }
        ++childIterator;
    }

    return nullptr;
}

const XMLElement * XMLElement::FindChild(const std::string & childName) const
{
    auto childIterator = begin(children);
    auto childListEnd = end(children);

    while (childIterator != childListEnd)
    {
        if ((*childIterator)->name == childName)
        {
            return *childIterator;
        }
        ++childIterator;
    }

    return nullptr;
}

bool XMLElement::HasChildren() const
{
    return !children.empty();
}

size_t XMLElement::NumChildren() const
{
    return children.num_items();
}

XMLElement::ChildIterator XMLElement::FirstChild()
{
    return begin(children);
}

XMLElement::ConstChildIterator XMLElement::FirstChild() const
{
    return begin(children);
}

XMLElement::ChildIterator XMLElement::ChildrenListEnd()
{
    return end(children);
}

XMLElement::ConstChildIterator XMLElement::ChildrenListEnd() const
{
    return end(children);
}

const XMLElement * XMLElement::GetParent() const
{
    return parent; // Const access
}

XMLElement * XMLElement::GetParent()
{
    return parent; // Mutable access
}

void XMLElement::SetParent(XMLElement * p)
{
    parent = p; // May be null
}

XMLAttribute * XMLElement::AddAttribute(XMLAttribute * attr)
{
    assert(attr != nullptr);
    attributes.push_back(attr);
    return attr;
}

bool XMLElement::RemoveAttribute(const std::string & attrName)
{
    // Find iterator, then remove it:
    auto attrIterator = begin(attributes);
    auto attrListEnd = end(attributes);

    while (attrIterator != attrListEnd)
    {
        if ((*attrIterator)->name == attrName)
        {
            XMLAttribute * attr = (*attrIterator);
            // Remove unlinks from the data structure, but does not deletes the object.
            attributes.erase(attrIterator);
            AttributeDisposer::Destroy(attr);
            return true;
        }
        ++attrIterator;
    }

    return false; // Attribute not found
}

XMLAttribute * XMLElement::FindAttribute(const std::string & attrName)
{
    auto attrIterator = begin(attributes);
    auto attrListEnd = end(attributes);

    while (attrIterator != attrListEnd)
    {
        if ((*attrIterator)->name == attrName)
        {
            return *attrIterator;
        }
        ++attrIterator;
    }

    return nullptr;
}

const XMLAttribute * XMLElement::FindAttribute(const std::string & attrName) const
{
    auto attrIterator = begin(attributes);
    auto attrListEnd = end(attributes);

    while (attrIterator != attrListEnd)
    {
        if ((*attrIterator)->name == attrName)
        {
            return *attrIterator;
        }
        ++attrIterator;
    }

    return nullptr;
}

bool XMLElement::HasAttributes() const
{
    return !attributes.empty();
}

size_t XMLElement::NumAttributes() const
{
    return attributes.num_items();
}

XMLElement::AttrIterator XMLElement::FirstAttribute()
{
    return begin(attributes);
}

XMLElement::ConstAttrIterator XMLElement::FirstAttribute() const
{
    return begin(attributes);
}

XMLElement::AttrIterator XMLElement::AttributeListEnd()
{
    return end(attributes);
}

XMLElement::ConstAttrIterator XMLElement::AttributeListEnd() const
{
    return end(attributes);
}

void XMLElement::AddText(const char * text)
{
    if (textBody != nullptr)
    {
        delete[] textBody;
    }

    textBody = DupCString(text);
}

bool XMLElement::HasText() const
{
    return textBody != nullptr;
}

const char * XMLElement::Text() const
{
    return textBody;
}

void XMLElement::AddCDATA(const char * CDATA_contents)
{
    if (cdata != nullptr)
    {
        delete[] cdata;
    }

    cdata = DupCString(CDATA_contents);
}

bool XMLElement::HasCDATA() const
{
    return cdata != nullptr;
}

const char * XMLElement::CDATA() const
{
    return cdata;
}

// ======================================================
// Miscellaneous:
// ======================================================

const XMLEntityReference xmlEntityReferences[] =
{
    { "&lt;",   L"&lt;",   '<'  }, // Less than
    { "&gt;",   L"&gt;",   '>'  }, // Greater than
    { "&amp;",  L"&amp;",  '&'  }, // Ampersand
    { "&apos;", L"&apos;", '\'' }, // Apostrophe
    { "&quot;", L"&quot;", '"'  }  // Quotation mark
};

// ASCII char to XML Entity Ref text:
const XMLEntityReference * IsXMLEntityReference(const int c)
{
    const size_t numEnts = ArrayLength(xmlEntityReferences);
    for (size_t i = 0; i < numEnts; ++i)
    {
        const XMLEntityReference * entRef = &xmlEntityReferences[i];
        if (entRef->trChar == c)
        {
            return entRef;
        }
    }
    return nullptr;
}

// XML Entity Ref text to ANSI-char:
const XMLEntityReference * IsXMLEntityReference(const char * text)
{
    assert(text != nullptr);

    const size_t numEnts = ArrayLength(xmlEntityReferences);
    for (size_t i = 0; i < numEnts; ++i)
    {
        const XMLEntityReference * entRef = &xmlEntityReferences[i];
        if (std::strncmp(entRef->ansiName, text, std::strlen(entRef->ansiName)) == 0)
        {
            return entRef;
        }
    }
    return nullptr;
}

// XML Entity Ref text to Wide-char:
const XMLEntityReference * IsXMLEntityReference(const wchar_t * text)
{
    assert(text != nullptr);

    const size_t numEnts = ArrayLength(xmlEntityReferences);
    for (size_t i = 0; i < numEnts; ++i)
    {
        const XMLEntityReference * entRef = &xmlEntityReferences[i];
        if (std::wcsncmp(entRef->wideName, text, std::wcslen(entRef->wideName)) == 0)
        {
            return entRef;
        }
    }
    return nullptr;
}

// XML name validation:
bool IsValidXMLElementAttrName(const std::string & name)
{
    //
    // XML elements and attributes must follow these naming rules:
    // - Names can contain letters, numbers, and other characters;
    // - Names cannot start with a number or punctuation character;
    // - Names cannot start with the letters xml (or XML, or Xml, etc);
    // - Names cannot contain spaces;
    //

    // Must have at least 1 char, empty strings not accepted.
    if (name.empty())
    {
        return false;
    }

    size_t i = 0;
    int c = name[i++];

    // Must start with a letter or an underscore:
    if (((c < 'a') || (c > 'z')) && ((c < 'A') || (c > 'Z')) && (c != '_'))
    {
        return false;
    }

    // Cannot start with "XML", case independent:
    if ((std::strncmp(name.c_str(), "xml", 3) == 0) || (std::strncmp(name.c_str(), "XML", 3) == 0))
    {
        return false;
    }

    // Check for white spaces:
    while (i < name.length())
    {
        c = name[i++];
        if (std::isspace(c))
        {
            // XML rule says a name can contain any char, except a white space.
            return false;
        }
    }

    return true;
}

// ======================================================
// BuildXMLTree() & helpers:
// ======================================================

// Checks whether a XML text or CDATA section is NOT composed entirely of non-graphical (blank) chars:
static bool HasGraphChars(const char * str)
{
    if (str == nullptr)
    {
        return false;
    }

    while (*str)
    {
        if (std::isgraph(*str))
        {
            return true; // Found a non-blank printable char!
        }
        ++str;
    }

    return false;
}

// Reads all XML attributes for the current node:
static void ReadXMLAttributes(XMLReader<char> & xmlReader, XMLElement & node)
{
    assert(xmlReader.IsValid());
    const unsigned int attrCount = xmlReader.AttributeCount();
    for (unsigned int i = 0; i < attrCount; ++i)
    {
        node.AddAttribute(
        XMLAttribute::NewAttribute(
        xmlReader.AttributeName(i),
        xmlReader.AttributeValue(i)));
    }
}

// Builds the XML tree using provided reader. NOTE: A root, non-empty element is required!
XMLRootPtr BuildXMLTree(const DataBlob blob)
{
    assert(blob.IsValid());
    XMLReader<char> xmlReader(blob);
    return BuildXMLTree(xmlReader);
}

// Builds the XML tree from data. NOTE: A root, non-empty element is required!
XMLRootPtr BuildXMLTree(XMLReader<char> & xmlReader)
{
    if (!xmlReader.IsValid())
    {
        common->WarningF("XML input data was not valid!");
        return XMLRootPtr();
    }

    // Locals:
    XMLElement * rootNode = nullptr;
    XMLElement * currentNode = nullptr;
    std::string textCDATA; // Used to copy text of CDATA section

    while (xmlReader.ReadNextNode())
    {
        switch (xmlReader.NodeType())
        {
        case XMLNodeType::Element:
        {
            if ((rootNode == nullptr) && (!xmlReader.IsEmptyElement()))
            {
                rootNode = XMLElement::NewRoot(xmlReader.NodeName());
                ReadXMLAttributes(xmlReader, *rootNode);
                currentNode = rootNode; // XMLRoot is just an alias to XMLElement
            }
            else
            {
                XMLElement * newElement = XMLElement::NewElement(xmlReader.NodeName());
                ReadXMLAttributes(xmlReader, *newElement);
                if (currentNode != nullptr)
                {
                    currentNode->AddChild(newElement);
                }

                newElement->SetParent(currentNode);
                if (!xmlReader.IsEmptyElement())
                {
                    currentNode = newElement;
                }
            }
            break;
        }
        case XMLNodeType::ElementEnd:
        {
            if (currentNode != nullptr)
            {
                currentNode = currentNode->GetParent();
            }
            break;
        }
        case XMLNodeType::Text:
        {
            const std::string & textSeg = xmlReader.NodeData();
            if (currentNode && HasGraphChars(textSeg.c_str()))
            {
                textCDATA = textSeg;
                currentNode->AddText(TrimString(textCDATA).c_str());
            }
            break;
        }
        case XMLNodeType::CDATA:
        {
            const std::string & CDATASeg = xmlReader.NodeData();
            if (currentNode && HasGraphChars(CDATASeg.c_str()))
            {
                textCDATA = CDATASeg;
                currentNode->AddCDATA(TrimString(textCDATA).c_str());
            }
            break;
        }
        default:
            break;
        } // switch (xmlReader.NodeType())
    }

    return XMLRootPtr(rootNode);
}

} // namespace Engine {}
