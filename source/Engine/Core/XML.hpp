
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: XML.hpp
// Author: Guilherme R. Lampert
// Created on: 28/04/13
// Brief: Data structures and functions used for both XML reading and writing.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

class XMLElement;
class XMLAttribute;
template <typename CharType> class XMLReader;

using XMLRoot = XMLElement;
using XMLRootPtr = std::unique_ptr<XMLRoot>;
using ConstXMLRootPtr = std::unique_ptr<const XMLRoot>;

// ======================================================
// XMLAttribute:
// ======================================================

//
// XML element attribute.
// Attributes are sometimes referred to as "properties".
//
// A XML element attribute is a pair of strings
// representing a key (attrib name) and a value (attrib value).
//
// Example:
//   <MyElement Attrib_0="Hello" Attrib_1="World" />
//
class XMLAttribute
    : public SListNode<XMLAttribute>
{
  public:

    // Creates a new XML attribute.
    // If the attribute is attached to an element, then the element becomes the
    // owner of the attribute and will free it when destroyed.
    static XMLAttribute * NewAttribute(const std::string & attrName, const std::string & attrVal);

    // Attribute name.
    std::string name;

    // Attribute value string.
    std::string value;

  private:

    // Constructor is reserved for internal use. Create objects via NewAttribute().
    XMLAttribute(const std::string & attrName, const std::string & attrVal);
};

// ======================================================
// XMLElement:
// ======================================================

//
// XML element type.
//
// A XML element basically translates into a XML tag in a file.
// Elements can be nested, forming a XML tree, can contain a
// text body or a CDATA section.
//
// Example:
//   <EmptyElement />
//
//   <ElemWithText>
//     Hello XML!
//   </ElemWithText>
//
//   <ElemWithAttrib name="John" surname="Galt" />
//
class XMLElement
    : public SListNode<XMLElement>
{
    // Object deleters:
    using ElementDisposer   = DestroyUsingDelete<XMLElement>;
    using AttributeDisposer = DestroyUsingDelete<XMLAttribute>;

    // Private list types:
    using ElementList   = IntrusiveSList<XMLElement, ElementDisposer>;
    using AttributeList = IntrusiveSList<XMLAttribute, AttributeDisposer>;

  public:

    // Public iterator types:
    using ChildIterator = ElementList::iterator;
    using ConstChildIterator = ElementList::const_iterator;
    using AttrIterator = AttributeList::iterator;
    using ConstAttrIterator = AttributeList::const_iterator;

    // Creates a new XML root element. Deleting the root will also delete all elements and attributes.
    // Caller is responsible for freeing the memory. Using a smart pointer is recommended.
    static XMLRoot * NewRoot(const std::string & elemName);

    // Creates a new XML element.
    // Elements added to a parent (by AddChild) should never be free. The parent will free its children
    // when destroyed. If the element is never parented/made a child, then it must be freed by the caller.
    static XMLElement * NewElement(const std::string & elemName);

    // ---- Child elements / nodes: ----

    // Add/remove child element. Parent element takes ownership of child's pointer.
    XMLElement * AddChild(XMLElement * child);
    bool RemoveChild(const std::string & childName);

    // Find child by name:
    XMLElement * FindChild(const std::string & childName);
    const XMLElement * FindChild(const std::string & childName) const;
    bool HasChildren() const;
    size_t NumChildren() const;

    // Children list iteration:
    ChildIterator FirstChild();
    ConstChildIterator FirstChild() const;
    ChildIterator ChildrenListEnd();
    ConstChildIterator ChildrenListEnd() const;

    // Get/set parent node:
    XMLElement * GetParent();
    const XMLElement * GetParent() const;
    void SetParent(XMLElement * p);

    // ---- Attributes: ----

    // Add/remove attributes. Element takes ownership of attribute pointers.
    XMLAttribute * AddAttribute(XMLAttribute * attr);
    bool RemoveAttribute(const std::string & attrName);

    // Find attributes by name.
    XMLAttribute * FindAttribute(const std::string & attrName);
    const XMLAttribute * FindAttribute(const std::string & attrName) const;
    bool HasAttributes() const;
    size_t NumAttributes() const;

    // Attribute list iteration:
    AttrIterator FirstAttribute();
    ConstAttrIterator FirstAttribute() const;
    AttrIterator AttributeListEnd();
    ConstAttrIterator AttributeListEnd() const;

    // ---- Text/CDATA access: ----

    // Tag text body:
    void AddText(const char * text);
    bool HasText() const;
    const char * Text() const;

    // CDATA - (Unparsed) Character Data:
    void AddCDATA(const char * CDATA_contents);
    bool HasCDATA() const;
    const char * CDATA() const;

    // Destructor recursively destroys the element and all of its children.
    // It also frees all attached attributes.
    ~XMLElement();

    // ---- Public properties: ----

    // Element name. Public.
    std::string name;

  private:

    // Elements are created via NewElement() and NewRoot().
    XMLElement(const std::string & elemName);

    // List of child elements, oldest at the front:
    ElementList children;

    // List of attributes, oldest at the front:
    AttributeList attributes;

    // Parent node. Null if root.
    XMLElement * parent;

    // CDATA contents and text body:
    char * cdata;
    char * textBody;

    DISABLE_COPY_AND_ASSIGN(XMLElement)
};

// ======================================================
// Miscellaneous helpers:
// ======================================================

//
// A so called 'Entity Reference' in a XML file is a token
// that translates to some other character, or vice-versa.
// Entity References can only exist inside text bodies.
// They are tokens that start with the ampersand '&' character and end with a semicolon ';'.
// Each Entity Reference is then replaced by its equivalent ASCII char when the XML is parsed.
//
struct XMLEntityReference
{
    const char * ansiName;    // Entity name, with the '&' at the beginning, ANSI-char version.
    const wchar_t * wideName; // Entity name, with the '&' at the beginning, Wide-char version.
    int trChar;               // Char which the entity translates to.
};

// Array of Entity References. Immutable.
extern const XMLEntityReference xmlEntityReferences[];

// Translates a character into a XMLEntityReference or
// return null if the char does not represent an entity reference.
const XMLEntityReference * IsXMLEntityReference(const int c);

// Translates a string into a XMLEntityReference or
// return null if the string does not represent an entity reference.
const XMLEntityReference * IsXMLEntityReference(const char * text);

// Translates a string into a XMLEntityReference or
// return null if the string does not represent an entity reference (wchar_t overload).
const XMLEntityReference * IsXMLEntityReference(const wchar_t * text);

// Validate an attribute or element name.
bool IsValidXMLElementAttrName(const std::string & name);

// Builds a XML tree from the contents of a data blob.
// Currently, the XMLReader<char> is used to build the tree.
XMLRootPtr BuildXMLTree(const DataBlob blob);

// Builds a XML tree using an already initialized XMLReader.
XMLRootPtr BuildXMLTree(XMLReader<char> & xmlReader);

} // namespace Engine {}
