
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Random.hpp
// Author: Guilherme R. Lampert
// Created on: 24/03/13
// Brief: Pseudo-random number generators.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <cmath>
#include <limits>

namespace Engine
{

// ======================================================
// LCGEngine:
// ======================================================

// Implements a Linear Congruential Generator pseudo-random engine.
// This engine is very lightweight and fast, but not very reliable in its distribution and period.
class LCGEngine
{
  public:

    LCGEngine();
    explicit LCGEngine(const unsigned int seed);

    // Reset (seed) the pseudo-random generator engine.
    void Seed(const unsigned int seed);

    // Get the next value in the pseudo-random sequence.
    unsigned int NextValue();

    // Maximum value returned by NextValue().
    static constexpr unsigned int GetMaxValue() { return std::numeric_limits<unsigned int>::max(); }

  private:

    // LCG state:
    unsigned int x;
};

// ======================================================
// XORShiftEngine:
// ======================================================

// Implements a XOR-Shift pseudo-random engine.
// A little better period than the LCG and still blazing fast.
class XORShiftEngine
{
  public:

    XORShiftEngine();
    explicit XORShiftEngine(const unsigned int seed);

    // Reset (seed) the pseudo-random generator engine.
    void Seed(const unsigned int seed);

    // Get the next value in the pseudo-random sequence.
    unsigned int NextValue();

    // Maximum value returned by NextValue().
    static constexpr unsigned int GetMaxValue() { return std::numeric_limits<unsigned int>::max(); }

  private:

    // XOR-Shift states:
    unsigned int x, y, z, w;
};

// ======================================================
// MT19937Engine:
// ======================================================

// Mersenne Twister engine.
// Very good distribution and period but at a higher performance and storage cost.
class MT19937Engine
{
  public:

    MT19937Engine();
    explicit MT19937Engine(const unsigned int seed);

    // Reset (seed) the pseudo-random generator engine.
    void Seed(const unsigned int seed);

    // Get the next value in the pseudo-random sequence.
    unsigned int NextValue();

    // Maximum value returned by NextValue().
    static constexpr unsigned int GetMaxValue() { return std::numeric_limits<unsigned int>::max(); }

  private:

    // Sizes:
    static constexpr int N = 624;
    static constexpr int M = 397;

    // Mersenne Twister tempering constants:
    static constexpr unsigned long A = 0x9908b0dfUL;
    static constexpr unsigned long U = 0x80000000UL;
    static constexpr unsigned long L = 0x7fffffffUL;

    // PRNG states:
    int next;
    unsigned long x[N];
};

// ======================================================
// Random:
// ======================================================

//
// Pseudo-random number generator class with several distribution functions.
//
// The Random class is a template that accepts any compliant
// random generator engine. This class implements the most common
// distribution functions for the engines.
//
template <class ENGINE>
class Random
{
  public:

    // Type of the underlaying random generator engine.
    using EngineType = ENGINE;

    // Construct with default seed.
    Random();

    // Construct with user defined seed.
    explicit Random(const unsigned int seed);

    // Construct with a preexistent random generator engine.
    explicit Random(const ENGINE & eng);

    // Reset (seed) the pseudo-random generator.
    void Seed(const unsigned int seed);

    // Pseudo-random boolean value.
    bool NextBoolean();

    // Pseudo-random floating-point number in range [0, 1].
    float NextFloat();

    // Pseudo-random integer in range [0, GetMaxValue()].
    unsigned int NextInteger();

    // Pseudo-random floating-point number in range [lowerBound, upperBound].
    float NextFloat(const float lowerBound, const float upperBound);

    // Pseudo-random integer in range [lowerBound, upperBound].
    int NextInteger(const int lowerBound, const int upperBound);

    // Pseudo-random floating-point number in range [-1, 1].
    float NextSymmetric();

    // Pseudo-random number with exponential distribution.
    float NextExponential(const float average);

    // Pseudo-random number with normal (Gaussian) distribution.
    float NextGaussian(const float average, const float stdDeviation);

    // Access the underlaying random generator engine.
    ENGINE & GetRandomEngine();

    // Access the underlaying random generator engine (const overload).
    const ENGINE & GetRandomEngine() const;

    // Max integer value that can be generated. Defined by the underlaying engine.
    static constexpr unsigned int GetMaxValue() { return ENGINE::GetMaxValue(); }

  private:

    // The pseudo-random generator engine.
    ENGINE engine;

    // Helper variables used by the normal distribution calculation.
    float leftover;
    bool  useLast;
};

// ======================================================
// Template aliases:
// ======================================================

// Linear Congruential Generator:
// http://en.wikipedia.org/wiki/Linear_congruential_generator
using LCGRandom = Random<LCGEngine>;

// XOR-shift:
// http://en.wikipedia.org/wiki/Xorshift
using XORShiftRandom = Random<XORShiftEngine>;

// Mersenne Twister:
// http://en.wikipedia.org/wiki/Mersenne_twister
using MT19937Random = Random<MT19937Engine>;

// ======================================================
// Helpers / globals:
// ======================================================

// Get a hash value based on the current time. Useful for seeding a pseudo-random generator.
unsigned int GenTimeSeed(const unsigned int seed);

// Include the inline definitions in the same namespace:
#include "Engine/Core/Math/Random.inl"

} // namespace Engine {}
