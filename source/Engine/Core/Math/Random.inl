
// ================================================================================================
// -*- C++ -*-
// File: Random.inl
// Author: Guilherme R. Lampert
// Created on: 17/06/14
// Brief: Inline definitions for the pseudo-random number generators.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

template <class ENGINE>
inline Random<ENGINE>::Random()
    : leftover(0.0f)
    , useLast(false)
{
    // Init with defaults.
}

template <class ENGINE>
inline Random<ENGINE>::Random(const unsigned int seed)
    : engine(seed)
    , leftover(0.0f)
    , useLast(false)
{
    // Init with user provided seed.
}

template <class ENGINE>
inline Random<ENGINE>::Random(const ENGINE & eng)
    : engine(eng)
    , leftover(0.0f)
    , useLast(false)
{
    // Init with existing Engine.
}

template <class ENGINE>
inline void Random<ENGINE>::Seed(const unsigned int seed)
{
    engine.Seed(seed);
}

template <class ENGINE>
inline bool Random<ENGINE>::NextBoolean()
{
    return NextFloat() > 0.5f;
}

template <class ENGINE>
inline float Random<ENGINE>::NextFloat()
{
    return static_cast<float>(engine.NextValue()) / ENGINE::GetMaxValue();
}

template <class ENGINE>
inline unsigned int Random<ENGINE>::NextInteger()
{
    return engine.NextValue();
}

template <class ENGINE>
inline float Random<ENGINE>::NextFloat(const float lowerBound, const float upperBound)
{
    return (NextFloat() * (upperBound - lowerBound)) + lowerBound;
}

template <class ENGINE>
inline int Random<ENGINE>::NextInteger(const int lowerBound, const int upperBound)
{
    if (lowerBound == upperBound)
    {
        return lowerBound;
    }
    return (NextInteger() % (upperBound - lowerBound)) + lowerBound;
}

template <class ENGINE>
inline float Random<ENGINE>::NextSymmetric()
{
    return (2.0f * NextFloat()) - 1.0f;
}

template <class ENGINE>
inline float Random<ENGINE>::NextExponential(const float average)
{
    return -average * std::log(1.0f - NextFloat());
}

template <class ENGINE>
inline float Random<ENGINE>::NextGaussian(const float average, const float stdDeviation)
{
    // See http://www.taygeta.com/random/gaussian.html
    // for a full explanation.
    //
    float x1, x2, y1, y2, w;
    if (useLast) // Use value from previous call?
    {
        y1 = leftover;
        useLast = false;
    }
    else
    {
        do
        {
            x1 = (2.0f * NextFloat()) - 1.0f;
            x2 = (2.0f * NextFloat()) - 1.0f;
            w = (x1 * x1) + (x2 * x2);
        }
        while (w >= 1.0f);

        w = std::sqrt((-2.0f * std::log(w)) / w);
        y1 = (x1 * w);
        y2 = (x2 * w);

        leftover = y2;
        useLast = true;
    }

    return average + (y1 * stdDeviation);
}

template <class ENGINE>
inline ENGINE & Random<ENGINE>::GetRandomEngine()
{
    return engine;
}

template <class ENGINE>
inline const ENGINE & Random<ENGINE>::GetRandomEngine() const
{
    return engine;
}
