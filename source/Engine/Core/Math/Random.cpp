
// ================================================================================================
// -*- C++ -*-
// File: Random.cpp
// Author: Guilherme R. Lampert
// Created on: 24/03/13
// Brief: Pseudo-random number generators.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Math/Random.hpp"
#include <ctime>

namespace Engine
{

// ======================================================
// LCGEngine:
// ======================================================

LCGEngine::LCGEngine()
    : x(48271U)
{
    // Initial constant from Wikipedia:
    // http://en.wikipedia.org/wiki/Lehmer_random_number_generator
}

LCGEngine::LCGEngine(const unsigned int seed)
{
    Seed(seed);
}

void LCGEngine::Seed(const unsigned int seed)
{
    x = seed ^ 48271U;
}

unsigned int LCGEngine::NextValue()
{
    // This is Lehmer's LCG formula:
    x = ((static_cast<uint64_t>(x) * 279470273UL) % 4294967291UL);
    return x;
}

// ======================================================
// XORShiftEngine:
// ======================================================

XORShiftEngine::XORShiftEngine()
    : x(123456789U)
    , y(362436069U)
    , z(521288629U)
    , w(88675123U)
{
    // Initial constants presented in this paper:
    // http://www.jstatsoft.org/v08/i14/paper
}

XORShiftEngine::XORShiftEngine(const unsigned int seed)
{
    Seed(seed);
}

void XORShiftEngine::Seed(const unsigned int seed)
{
    x = seed ^ 123456789U;
    y = seed ^ 362436069U;
    z = seed ^ 521288629U;
    w = seed ^ 88675123U;
}

unsigned int XORShiftEngine::NextValue()
{
    // XOR-Shift-128 RNG:
    unsigned int t = x ^ (x << 11);
    x = y;
    y = z;
    z = w;
    w = w ^ (w >> 19) ^ t ^ (t >> 8);
    return w;
}

// ======================================================
// MT19937Engine:
// ======================================================

MT19937Engine::MT19937Engine()
    : next(N) // Will shuffle on the first run
{
    Seed(5489); // Same seed used by std random.
}

MT19937Engine::MT19937Engine(const unsigned int seed)
    : next(N) // Will shuffle on the first run
{
    Seed(seed);
}

void MT19937Engine::Seed(const unsigned int seed)
{
    x[0] = seed & 0xFFFFFFFFUL;
    for (int i = 1; i < N; ++i)
    {
        x[i] = (1812433253UL * (x[i - 1] ^ (x[i - 1] >> 30)) + i);
        x[i] &= 0xFFFFFFFFUL;
    }
}

unsigned int MT19937Engine::NextValue()
{
    unsigned long y, a;

    // Refill x if exhausted:
    if (next == N)
    {
        next = 0;

        for (int i = 0; i < (N - 1); ++i)
        {
            y = (x[i] & U) | (x[i + 1] & L);
            a = (y & 0x1UL) ? A : 0x0UL;
            x[i] = x[(i + M) % N] ^ (y >> 1) ^ a;
        }

        y = (x[N - 1] & U) | (x[0] & L);
        a = (y & 0x1UL) ? A : 0x0UL;
        x[N - 1] = x[M - 1] ^ (y >> 1) ^ a;
    }

    y = x[next++];

    // Improve distribution:
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9D2C5680UL;
    y ^= (y << 15) & 0xEFC60000UL;
    y ^= (y >> 18);

    return y;
}

// ======================================================
// GenTimeSeed():
// ======================================================

unsigned int GenTimeSeed(const unsigned int seed)
{
    // Adapted from code found here:
    // http://eternallyconfuzzled.com/arts/jsw_art_rand.aspx
    //
    // Zero is a valid initial value for 'seed'
    //
    const time_t now = std::time(nullptr);
    const unsigned char * p = reinterpret_cast<const unsigned char *>(&now);
    unsigned int s = seed;

    for (size_t i = 0; i < sizeof(now); ++i)
    {
        s = s * (std::numeric_limits<unsigned char>::max() + 2U) + p[i];
    }

    return s;
}

} // namespace Engine {}
