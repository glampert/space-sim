
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Vec234.hpp
// Author: Guilherme R. Lampert
// Created on: 04/06/13
// Brief: Vector tuples similar to the ones available in GLSL. Also aliases for color types.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Vec2T<T>:
// ======================================================

// Tuple of 2 values, with a few access aliases.
template <typename T>
union Vec2T
{
    T data[2];
    // Vectors:
    struct
    {
        T x;
        T y;
    };
    // Colors:
    struct
    {
        T r;
        T g;
    };
    // Offsets:
    struct
    {
        T xOffset;
        T yOffset;
    };
    // Texture coordinates:
    struct
    {
        T u;
        T v;
    };
    // Ranges:
    struct
    {
        T start;
        T end;
    };
    // Sizes/dimensions:
    struct
    {
        T width;
        T height;
    };

    // ---- Construction: ----

    Vec2T() {} // Uninitialized

    Vec2T(const T arg) // Propagates the argument to all elements
    : x(arg),
      y(arg)
    {
    }

    Vec2T(const T arg0, const T arg1) // From args
    : x(arg0),
      y(arg1)
    {
    }

    Vec2T(const Vec2T<T> & other) // From copy
    : x(other.x),
      y(other.y)
    {
    }

    Vec2T(const T array[]) // From array of T[2]
    : x(array[0]),
      y(array[1])
    {
    }

    // Set x,y from values.
    void Set(const T arg0, const T arg1)
    {
        x = arg0;
        y = arg1;
    }

    // ---- operator[]: ----

    T operator[](const size_t index) const
    {
        assert(index < 2);
        return data[index];
    }
    T & operator[](const size_t index)
    {
        assert(index < 2);
        return data[index];
    }
};

// ======================================================
// Vec3T<T>:
// ======================================================

// Tuple of 3 values, with a few access aliases.
template <typename T>
union Vec3T
{
    T data[3];
    // Vectors:
    struct
    {
        T x;
        T y;
        T z;
    };
    // Colors:
    struct
    {
        T r;
        T g;
        T b;
    };

    // ---- Construction: ----

    Vec3T() {} // Uninitialized

    Vec3T(const T arg) // Propagates the argument to all elements
    : x(arg),
      y(arg),
      z(arg)
    {
    }

    Vec3T(const T arg0, const T arg1, const T arg2) // From args
    : x(arg0),
      y(arg1),
      z(arg2)
    {
    }

    Vec3T(const Vec3T<T> & other) // From copy
    : x(other.x),
      y(other.y),
      z(other.z)
    {
    }

    Vec3T(const T array[]) // From array of T[3]
    : x(array[0]),
      y(array[1]),
      z(array[2])
    {
    }

    Vec3T(const Vector3 & v3) // Conversion from VectorMath Vector3
    : x(T(v3[0])),
      y(T(v3[1])),
      z(T(v3[2]))
    {
    }

    Vec3T(const Point3 & p3) // Conversion from VectorMath Point3
    : x(T(p3[0])),
      y(T(p3[1])),
      z(T(p3[2]))
    {
    }

    Vec3T(const Vector4 & v4) // Conversion from VectorMath Vector4, discarding the W component
    : x(T(v4[0])),
      y(T(v4[1])),
      z(T(v4[2]))
    {
    }

    // Set from contents of a Vector3.
    void Set(const Vector3 & v3)
    {
        x = T(v3[0]);
        y = T(v3[1]);
        z = T(v3[2]);
    }

    // ---- operator[]: ----

    T operator[](const size_t index) const
    {
        assert(index < 3);
        return data[index];
    }
    T & operator[](const size_t index)
    {
        assert(index < 3);
        return data[index];
    }
};

// ======================================================
// Vec4T<T>:
// ======================================================

// Tuple of 4 values, with a few access aliases.
template <typename T>
union Vec4T
{
    T data[4];
    // Vectors:
    struct
    {
        T x;
        T y;
        T z;
        T w;
    };
    // Colors:
    struct
    {
        T r;
        T g;
        T b;
        T a;
    };
    // Rectangles/boxes:
    struct
    {
        T xOrigin;
        T yOrigin;
        T width;
        T height;
    };
    // Vertex/index ranges:
    struct
    {
        T vStart;
        T vCount;
        T iStart;
        T iCount;
    };

    // ---- Construction: ----

    Vec4T() {} // Uninitialized

    Vec4T(const T arg) // Propagates the argument to all elements
    : x(arg),
      y(arg),
      z(arg),
      w(arg)
    {
    }

    Vec4T(const T arg0, const T arg1, const T arg2, const T arg3) // From args
    : x(arg0),
      y(arg1),
      z(arg2),
      w(arg3)
    {
    }

    Vec4T(const Vec4T<T> & other) // From copy
    : x(other.x),
      y(other.y),
      z(other.z),
      w(other.w)
    {
    }

    Vec4T(const T array[]) // From array of T[4]
    : x(array[0]),
      y(array[1]),
      z(array[2]),
      w(array[3])
    {
    }

    Vec4T(const Vector4 & v4) // Conversion from VectorMath Vector4
    : x(T(v4[0])),
      y(T(v4[1])),
      z(T(v4[2])),
      w(T(v4[3]))
    {
    }

    Vec4T(const Vec3T<T> & v3, const T vw = T(1)) // Conversion from Vec3f/Color. Sets w to 1 by default
    : x(v3.x),
      y(v3.y),
      z(v3.z),
      w(vw)
    {
    }

    // Set from contents of a Vector4.
    void Set(const Vector4 & v4)
    {
        x = T(v4[0]);
        y = T(v4[1]);
        z = T(v4[2]);
        w = T(v4[3]);
    }

    // Set every element:
    void Set(const T arg0, const T arg1, const T arg2, const T arg3)
    {
        x = arg0;
        y = arg1;
        z = arg2;
        w = arg3;
    }

    // ---- operator[]: ----

    T operator[](const size_t index) const
    {
        assert(index < 4);
        return data[index];
    }
    T & operator[](const size_t index)
    {
        assert(index < 4);
        return data[index];
    }
};

// ======================================================
// Typedefs:
// ======================================================

// Unpadded float vectors:
using Vec2f = Vec2T<float>; // 2 component vector of floats.
using Vec3f = Vec3T<float>; // 3 component vector of floats.
using Vec4f = Vec4T<float>; // 4 component vector of floats.

// Unpadded integers vectors:
using Vec2i = Vec2T<int32_t>; // 2 component vector of integers.
using Vec3i = Vec3T<int32_t>; // 3 component vector of integers.
using Vec4i = Vec4T<int32_t>; // 4 component vector of integers.

// Unpadded unsigned integer vectors:
using Vec2u = Vec2T<uint32_t>; // 2 component vector of unsigned integers.
using Vec3u = Vec3T<uint32_t>; // 3 component vector of unsigned integers.
using Vec4u = Vec4T<uint32_t>; // 4 component vector of unsigned integers.

// Colors:
using Color2f = Vec2T<float>;   // RG float32 [0,1] color.
using Color3f = Vec3T<float>;   // RGB float32 [0,1] color.
using Color4f = Vec4T<float>;   // RGBA float32 [0,1] color.
using Color2b = Vec2T<uint8_t>; // RG uint8 [0,255] color.
using Color3b = Vec3T<uint8_t>; // RGB uint8 [0,255] color.
using Color4b = Vec4T<uint8_t>; // RGBA uint8 [0,255] color.

// To be sure...
static_assert(sizeof(Color2b) == 2, "Bad size! Expected 2");
static_assert(sizeof(Color3b) == 3, "Bad size! Expected 3");
static_assert(sizeof(Color4b) == 4, "Bad size! Expected 4");

// ======================================================
// Vec2T operators and helper functions:
// ======================================================

// Multiply vector by vector.
template <typename T>
inline Vec2T<T> operator*(const Vec2T<T> & a, const Vec2T<T> & b)
{
    return Vec2T<T>(a.x * b.x, a.y * b.y);
}

// Multiply vector by scalar.
template <typename T>
inline Vec2T<T> operator*(const Vec2T<T> & a, const float s)
{
    return Vec2T<T>(a.x * s, a.y * s);
}

// Multiply scalar with vector.
template <typename T>
inline Vec2T<T> operator*(const float s, const Vec2T<T> & a)
{
    return Vec2T<T>(s * a.x, s * a.y);
}

// Divide vector by scalar.
template <typename T>
inline Vec2T<T> operator/(const Vec2T<T> & a, const float s)
{
    return Vec2T<T>(a.x / s, a.y / s);
}

// Component wise addition.
template <typename T>
inline Vec2T<T> operator+(const Vec2T<T> & a, const Vec2T<T> & b)
{
    return Vec2T<T>(a.x + b.x, a.y + b.y);
}

// Component wise subtraction.
template <typename T>
inline Vec2T<T> operator-(const Vec2T<T> & a, const Vec2T<T> & b)
{
    return Vec2T<T>(a.x - b.x, a.y - b.y);
}

// Normalize a 2D vector.
inline Vec2f normalize(const Vec2f & v)
{
    const float invLen = 1.0f / std::sqrt((v.x * v.x) + (v.y * v.y));
    return Vec2f(v.x * invLen, v.y * invLen);
}

// Length, or magnitude, of a 2D vector.
inline float length(const Vec2f & v)
{
    return std::sqrt((v.x * v.x) + (v.y * v.y));
}

// Squared length, or magnitude, of a 2D vector.
inline float lengthSqr(const Vec2f & v)
{
    return (v.x * v.x) + (v.y * v.y);
}

// Dot product (or scalar product) of two 2D vectors.
inline float dot(const Vec2f & a, const Vec2f & b)
{
    return (a.x * b.x) + (a.y * b.y);
}

// ======================================================
// Vec3T operators and helper functions:
// ======================================================

// Multiply vector by vector.
template <typename T>
inline Vec3T<T> operator*(const Vec3T<T> & a, const Vec3T<T> & b)
{
    return Vec3T<T>(a.x * b.x, a.y * b.y, a.z * b.z);
}

// Multiply vector by scalar.
template <typename T>
inline Vec3T<T> operator*(const Vec3T<T> & a, const float s)
{
    return Vec3T<T>(a.x * s, a.y * s, a.z * s);
}

// Multiply scalar with vector.
template <typename T>
inline Vec3T<T> operator*(const float s, const Vec3T<T> & a)
{
    return Vec3T<T>(s * a.x, s * a.y, s * a.z);
}

// Divide vector by scalar.
template <typename T>
inline Vec3T<T> operator/(const Vec3T<T> & a, const float s)
{
    return Vec3T<T>(a.x / s, a.y / s, a.z / s);
}

// Component wise addition.
template <typename T>
inline Vec3T<T> operator+(const Vec3T<T> & a, const Vec3T<T> & b)
{
    return Vec3T<T>(a.x + b.x, a.y + b.y, a.z + b.z);
}

// Add vector and scalar.
template <typename T>
inline Vec3T<T> operator+(const Vec3T<T> & a, const float s)
{
    return Vec3T<T>(a.x + s, a.y + s, a.z + s);
}

// Component wise subtraction.
template <typename T>
inline Vec3T<T> operator-(const Vec3T<T> & a, const Vec3T<T> & b)
{
    return Vec3T<T>(a.x - b.x, a.y - b.y, a.z - b.z);
}

// Subtract vector an scalar.
template <typename T>
inline Vec3T<T> operator-(const Vec3T<T> & a, const float s)
{
    return Vec3T<T>(a.x - s, a.y - s, a.z - s);
}

// Normalize a 3D vector.
inline Vec3f normalize(const Vec3f & v)
{
    const float invLen = 1.0f / std::sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
    return Vec3f(v.x * invLen, v.y * invLen, v.z * invLen);
}

// Length, or magnitude, of a 3D vector.
inline float length(const Vec3f & v)
{
    return std::sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
}

// Squared length, or magnitude, of a 3D vector.
inline float lengthSqr(const Vec3f & v)
{
    return (v.x * v.x) + (v.y * v.y) + (v.z * v.z);
}

// Dot product (or scalar product) of two 3D vectors.
inline float dot(const Vec3f & a, const Vec3f & b)
{
    return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
}

// Linear interpolation between two vectors.
inline Vec3f lerp(const float t, const Vec3f & a, const Vec3f & b)
{
    return (b - a) * t + a;
}

// Compute cross product of two 3D vectors.
inline Vec3f cross(const Vec3f & a, const Vec3f & b)
{
    Vec3f result;
    result.x = (a.y * b.z) - (a.z * b.y);
    result.y = (a.z * b.x) - (a.x * b.z);
    result.z = (a.x * b.y) - (a.y * b.x);
    return result;
}

// ======================================================
// Vec4T operators and helper functions:
// ======================================================

// Multiply vector by vector.
template <typename T>
inline Vec4T<T> operator*(const Vec4T<T> & a, const Vec4T<T> & b)
{
    return Vec4T<T>(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
}

// Multiply vector by scalar.
template <typename T>
inline Vec4T<T> operator*(const Vec4T<T> & a, const float s)
{
    return Vec4T<T>(a.x * s, a.y * s, a.z * s, a.w * s);
}

// Divide vector by scalar.
template <typename T>
inline Vec4T<T> operator/(const Vec4T<T> & a, const float s)
{
    return Vec4T<T>(a.x / s, a.y / s, a.z / s, a.w / s);
}

// Component wise addition.
template <typename T>
inline Vec4T<T> operator+(const Vec4T<T> & a, const Vec4T<T> & b)
{
    return Vec4T<T>(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
}

// Add vector and scalar.
template <typename T>
inline Vec4T<T> operator+(const Vec4T<T> & a, const float s)
{
    return Vec4T<T>(a.x + s, a.y + s, a.z + s, a.w + s);
}

// Component wise subtraction.
template <typename T>
inline Vec4T<T> operator-(const Vec4T<T> & a, const Vec4T<T> & b)
{
    return Vec4T<T>(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
}

// Subtract vector and scalar.
template <typename T>
inline Vec4T<T> operator-(const Vec4T<T> & a, const float s)
{
    return Vec4T<T>(a.x - s, a.y - s, a.z - s, a.w - s);
}

// Normalize a 4D vector.
inline Vec4f normalize(const Vec4f & v)
{
    const float invLen = 1.0f / std::sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z) + (v.w * v.w));
    return Vec4f(v.x * invLen, v.y * invLen, v.z * invLen, v.w * invLen);
}

// Length, or magnitude, of a 4D vector.
inline float length(const Vec4f & v)
{
    return std::sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z) + (v.w * v.w));
}

// Squared length, or magnitude, of a 4D vector.
inline float lengthSqr(const Vec4f & v)
{
    return (v.x * v.x) + (v.y * v.y) + (v.z * v.z) + (v.w * v.w);
}

// Linear interpolation between two vectors.
inline Vec4f lerp(const float t, const Vec4f & a, const Vec4f & b)
{
    return (b - a) * t + a;
}

// ======================================================
// Misc inline functions:
// ======================================================

//
// Vec2f/3/4 to float array pointer:
//
inline float * ToFloatPtr(Vec2f & v) { return v.data; }
inline float * ToFloatPtr(Vec3f & v) { return v.data; }
inline float * ToFloatPtr(Vec4f & v) { return v.data; }
inline const float * ToFloatPtr(const Vec2f & v) { return v.data; }
inline const float * ToFloatPtr(const Vec3f & v) { return v.data; }
inline const float * ToFloatPtr(const Vec4f & v) { return v.data; }

//
// Vec3f conversion to Point3/Vector3/Vector4:
//
inline Point3 ToPoint3(const Vec3f & v)   { return Point3(v.x, v.y, v.z);  }
inline Vector3 ToVector3(const Vec3f & v) { return Vector3(v.x, v.y, v.z); }
inline Vector4 ToVector4(const Vec3f & v) { return Vector4(v.x, v.y, v.z, 1.0f); }

//
// Vec4f conversion to Vector3/Vector4:
//
inline Vector3 ToVector3(const Vec4f & v) { return Vector3(v.x, v.y, v.z); }
inline Vector4 ToVector4(const Vec4f & v) { return Vector4(v.x, v.y, v.z, v.w); }

} // namespace Engine {}
