
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: MathUtils.hpp
// Author: Guilherme R. Lampert
// Created on: 16/08/14
// Brief: Miscellaneous math/geometry utilities.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <cmath>
#include <cfloat>

#include "Engine/Core/Math/VectorMath.hpp"
#include "Engine/Core/Math/Vec234.hpp"

namespace Engine
{

// ======================================================
// Useful mathematical constants:
// ======================================================

struct MathConst
{
    static constexpr float PI = 3.1415926535897931f;      // 4*atan(1)
    static constexpr float TwoPI = 2.0f * PI;             // 2*pi
    static constexpr float HalfPI = 0.5f * PI;            // pi/2
    static constexpr float InvPI = 1.0f / PI;             // 1/pi
    static constexpr float InvTwoPI = 1.0f / TwoPI;       // 1/(2*pi)
    static constexpr float LogOf2 = 0.6931471805599453f;  // log(2)
    static constexpr float LogOf10 = 2.3025850929940459f; // log(10)
    static constexpr float InvLogOf2 = 1.0f / LogOf2;     // 1/log(2)
    static constexpr float InvLogOf10 = 1.0f / LogOf10;   // 1/log(10)
    static constexpr float DegToRad = PI / 180.0f;        // pi/180
    static constexpr float RadToDeg = 180.0f / PI;        // 180/pi
    static constexpr float SecToMsec = 1000.0f;           // Seconds to milliseconds multiplier
    static constexpr float MsecToSec = 0.001f;            // Milliseconds to seconds multiplier
    static constexpr float Infinity = INFINITY;           // Nothing lasts forever, except this number :P
    static constexpr float FloatEpsilon = FLT_EPSILON;    // Epsilon/tolerance for floating point comparison
};

// ======================================================
// DegToRad/RadToDeg: degrees <=> radians conversion
// ======================================================

inline constexpr float DegToRad(const float degrees) noexcept
{
    return degrees * (MathConst::PI / 180.0f);
}
inline constexpr float RadToDeg(const float radians) noexcept
{
    return radians * (180.0f / MathConst::PI);
}

// ======================================================
// Time scale conversion:
// ======================================================

inline constexpr double MsecToSec(const double ms) noexcept
{
    return ms * 0.001;
}
inline constexpr double SecToMsec(const double sec) noexcept
{
    return sec * 1000.0;
}

// ======================================================
// Floating-point comparisons with epsilon:
// ======================================================

// Compare floating-point values with an epsilon/tolerance.
inline bool FloatEqualsWithinTol(const float x, const float y, const float tolerance) noexcept
{
    return std::fabs(x - y) < tolerance;
}
inline bool FloatEquals(const float x, const float y) noexcept
{
    return std::fabs(x - y) < MathConst::FloatEpsilon;
}

// Test if a floating-point number is zero, within a tolerance.
inline bool FloatIsZeroWithinTol(const float val, const float tolerance) noexcept
{
    return std::fabs(val) < tolerance;
}
inline bool FloatIsZero(const float val) noexcept
{
    return std::fabs(val) < MathConst::FloatEpsilon;
}

// ======================================================
// Miscellaneous:
// ======================================================

// Clamp value between minimum and maximum, inclusive.
template <typename T>
inline constexpr T Clamp(const T x, const T minimum, const T maximum) noexcept
{
    return ((x < minimum) ? minimum : (x > maximum) ? maximum : x);
}

// Map scalar value from any range to any other range.
template <typename T>
inline constexpr T MapRange(const T x, const T inMin, const T inMax, const T outMin, const T outMax) noexcept
{
    return ((x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin);
}

// Map scalar value to [0,1] range.
template <typename T>
inline constexpr T ValueNormalize(const T x, const T minimum, const T maximum) noexcept
{
    return (x - minimum) / (maximum - minimum);
}

// Build a quaternion from a normalized axis and an angle in radians.
inline Quat QuatFromAngleAxis(const float radians, const Vector3 & axis) noexcept
{
    // The quaternion representing the rotation is:
    // q = cos(A/2) + sin(A/2) * (x * i + y * j + z * k)
    const float halfAngle = (radians * 0.5f);
    return Quat(axis * std::sin(halfAngle), std::cos(halfAngle));
}

// Angle in radians between the two vectors, defined by: cos(x) = a.b / |a||b|
inline float VectorAngle(const Vector3 & a, const Vector3 & b) noexcept
{
    const float m = std::sqrt(lengthSqr(a) * lengthSqr(b));
    return std::acos(dot(a, b) / m);
}

// Left and down pointing vectors for normal. 'v' should be already normalized.
inline void NormalVectors(const Vec3f & v, Vec3f & left, Vec3f & down)
{
    float d = (v.x * v.x + v.y * v.y);
    if (FloatIsZero(d))
    {
        left.x = 1.0f;
        left.y = 0.0f;
        left.z = 0.0f;
    }
    else
    {
        d = 1.0f / std::sqrt(d);
        left.x = -v.y * d;
        left.y = v.x * d;
        left.z = 0.0f;
    }
    down = cross(left, v);
}

// Produce two orthogonal vectors for normal 'v'.
inline void OrthogonalBasis(const Vec3f & v, Vec3f & left, Vec3f & up)
{
    float l, s;
    if (std::fabs(v.z) > 0.7f)
    {
        l = v.y * v.y + v.z * v.z;
        s = 1.0f / std::sqrt(l);
        up.x = 0.0f;
        up.y = v.z * s;
        up.z = -v.y * s;
        left.x = l * s;
        left.y = -v.x * up.z;
        left.z = v.x * up.y;
    }
    else
    {
        l = v.x * v.x + v.y * v.y;
        s = 1.0f / std::sqrt(l);
        left.x = -v.y * s;
        left.y = v.x * s;
        left.z = 0.0f;
        up.x = -v.z * left.y;
        up.y = v.z * left.x;
        up.z = l * s;
    }
}

// Compute Field-of-View Y (vertical) from the projection far plane and its width.
inline float FOVyForProjection(const float height, const float farClip) noexcept
{
    // From http://paulbourke.net/miscellaneous/lens/
    //
    // vertical_field_of_view = 2 atan(0.5 height / focal_length)
    //
    // focal_length can be just the distance to the far plane,
    // since in the synthetic camera, the eye is on/behind the near plane.
    //
    return 2.0f * std::atan((height * 0.5f) / farClip);
}

// ======================================================
// Matrix projections:
// ======================================================

// 3D world space to window/screen coordinates.
bool Project(const float worldX, const float worldY, const float worldZ,
             const Matrix4 & modelView, const Matrix4 & projection,
             const Vec4u viewport, Vector3 & windowCoordinates) noexcept;

// 2D window/screen coordinates to 3D world space.
bool UnProject(const float winX, const float winY, const float winZ,
               const Matrix4 & modelView, const Matrix4 & projection,
               const Vec4u viewport, Vector3 & worldCoordinates) noexcept;

} // namespace Engine {}
