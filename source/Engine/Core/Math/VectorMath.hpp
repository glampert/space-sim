
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: VectorMath.hpp
// Author: Guilherme R. Lampert
// Created on: 07/08/14
// Brief: Interface header for Sony's VectorMath library.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <cmath>
#include "ThirdParty/vectormath/cpp/vectormath_aos.h"

namespace Engine
{

// Makes all VectorMath types visible inside the Engine namespace.
using namespace ::Vectormath::Aos;

// Get the name of the VectorMath implementation currently in use.
inline std::string VectorMathImplementationString()
{
#if defined(VMATH_SPU)
    return "Sony's VectorMath, SPU - AoS format";
#elif defined(VMATH_ALTIVEC)
    return "Sony's VectorMath, ALTIVEC - AoS format";
#elif defined(VMATH_SSE)
    return "Sony's VectorMath, SSE - AoS format";
#else // VMATH_SCALAR
    return "Sony's VectorMath, Scalar - AoS format";
#endif
}

//
// Cast vectors/matrices/quaternions to float pointers (float *):
//
inline float * ToFloatPtr(Point3 & p)  { return reinterpret_cast<float *>(&p); }
inline float * ToFloatPtr(Vector3 & v) { return reinterpret_cast<float *>(&v); }
inline float * ToFloatPtr(Vector4 & v) { return reinterpret_cast<float *>(&v); }
inline float * ToFloatPtr(Quat & q)    { return reinterpret_cast<float *>(&q); }
inline float * ToFloatPtr(Matrix3 & m) { return reinterpret_cast<float *>(&m); }
inline float * ToFloatPtr(Matrix4 & m) { return reinterpret_cast<float *>(&m); }

//
// Cast vectors/matrices/quaternions to const float pointers (const float *):
//
inline const float * ToFloatPtr(const Point3 & p)  { return reinterpret_cast<const float *>(&p); }
inline const float * ToFloatPtr(const Vector3 & v) { return reinterpret_cast<const float *>(&v); }
inline const float * ToFloatPtr(const Vector4 & v) { return reinterpret_cast<const float *>(&v); }
inline const float * ToFloatPtr(const Quat & q)    { return reinterpret_cast<const float *>(&q); }
inline const float * ToFloatPtr(const Matrix3 & m) { return reinterpret_cast<const float *>(&m); }
inline const float * ToFloatPtr(const Matrix4 & m) { return reinterpret_cast<const float *>(&m); }

//
// VectorMath constants:
//
extern const Point3  Point3Zero;
extern const Vector3 Vector3Zero;
extern const Vector4 Vector4Zero;
extern const Quat    QuatIdentity;
extern const Matrix3 Matrix3Identity;
extern const Matrix4 Matrix4Identity;
extern const Vector3 Vector3UnitX;
extern const Vector3 Vector3UnitY;
extern const Vector3 Vector3UnitZ;
extern const Vector3 Vector3Infinity;
extern const Vector3 Vector3NegInfinity;

} // namespace Engine {}
