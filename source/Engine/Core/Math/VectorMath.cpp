
// ================================================================================================
// -*- C++ -*-
// File: VectorMath.cpp
// Author: Guilherme R. Lampert
// Created on: 07/08/14
// Brief: Interface header for Sony's VectorMath library.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Geometry/Plane.hpp"

namespace Engine
{

// ======================================================
// Global constants:
// ======================================================

// VectorMath constants:
const Point3 Point3Zero          = Point3(0.0f, 0.0f, 0.0f);
const Vector3 Vector3Zero        = Vector3(0.0f, 0.0f, 0.0f);
const Vector4 Vector4Zero        = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
const Vector3 Vector3UnitX       = Vector3(1.0f, 0.0f, 0.0f);
const Vector3 Vector3UnitY       = Vector3(0.0f, 1.0f, 0.0f);
const Vector3 Vector3UnitZ       = Vector3(0.0f, 0.0f, 1.0f);
const Quat QuatIdentity          = Quat::identity();
const Matrix3 Matrix3Identity    = Matrix3::identity();
const Matrix4 Matrix4Identity    = Matrix4::identity();
const Vector3 Vector3Infinity    = Vector3(MathConst::Infinity, MathConst::Infinity, MathConst::Infinity);
const Vector3 Vector3NegInfinity = Vector3(-MathConst::Infinity, -MathConst::Infinity, -MathConst::Infinity);

// Commonly used planes (from Geometry/Plane.hpp):
const Plane PlaneZero = Plane(0.0f, 0.0f, 0.0f, 0.0f);
const Plane PlaneYUp  = Plane(Vector3(0.0f, 1.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f));

// ======================================================
// Project(): 3D world space to 2D screen space
// ======================================================

bool Project(const float worldX, const float worldY, const float worldZ,
             const Matrix4 & modelView, const Matrix4 & projection,
             const Vec4u viewport, Vector3 & windowCoordinates) noexcept
{
    // Temps:
    float vTemp[8];
    const float * mv = ToFloatPtr(modelView);
    const float * p = ToFloatPtr(projection);

    // Model-view transform:
    vTemp[0] = mv[0] * worldX + mv[4] * worldY + mv[8] * worldZ + mv[12];
    vTemp[1] = mv[1] * worldX + mv[5] * worldY + mv[9] * worldZ + mv[13];
    vTemp[2] = mv[2] * worldX + mv[6] * worldY + mv[10] * worldZ + mv[14];
    vTemp[3] = mv[3] * worldX + mv[7] * worldY + mv[11] * worldZ + mv[15];

    // Projection transform, the final row of projection matrix is always [0 0 -1 0] so we optimize for that:
    vTemp[4] = p[0] * vTemp[0] + p[4] * vTemp[1] + p[8] * vTemp[2] + p[12] * vTemp[3];
    vTemp[5] = p[1] * vTemp[0] + p[5] * vTemp[1] + p[9] * vTemp[2] + p[13] * vTemp[3];
    vTemp[6] = p[2] * vTemp[0] + p[6] * vTemp[1] + p[10] * vTemp[2] + p[14] * vTemp[3];
    vTemp[7] = -vTemp[2];

    // The result is normalized between -1 and 1:
    if (FloatIsZero(vTemp[7])) // Avoid a division by zero
    {
        windowCoordinates = Vector3Zero;
        return false;
    }

    vTemp[7] = 1.0f / vTemp[7];

    // Perspective division:
    vTemp[4] *= vTemp[7];
    vTemp[5] *= vTemp[7];
    vTemp[6] *= vTemp[7];

    // Window coordinates map x, y to range 0,1:
    windowCoordinates[0] = (vTemp[4] * 0.5f + 0.5f) * viewport.width + viewport.x;
    windowCoordinates[1] = (vTemp[5] * 0.5f + 0.5f) * viewport.height + viewport.y;
    windowCoordinates[2] = (1.0f + vTemp[6]) * 0.5f;
    return true;
}

// ======================================================
// UnProject(): 2D screen space to 3D world space
// ======================================================

bool UnProject(const float winX, const float winY, const float winZ,
               const Matrix4 & modelView, const Matrix4 & projection,
               const Vec4u viewport, Vector3 & worldCoordinates) noexcept
{
    /* This is only necessary if we are changing the standard
	 * OpenGL setup, which is 2D origin at the bottom-left and Y pointing up.
	 *
	 * Invert Y if screen Y-origin point down, while 3D Y-origin always points up:
	 * winY = static_cast<float>(viewport.height) - winY;
	 * assert(winY >= 0.0f);
	 */

    // Compute (projection x modelView) ^ -1:
    const Matrix4 m(inverse(projection * modelView));

    // Transformation of normalized coordinates between -1 and 1:
    Vector4 in;
    in[0] = (winX - static_cast<float>(viewport.x)) / static_cast<float>(viewport.width)  * 2.0f - 1.0f;
    in[1] = (winY - static_cast<float>(viewport.y)) / static_cast<float>(viewport.height) * 2.0f - 1.0f;
    in[2] = 2.0f * winZ - 1.0f;
    in[3] = 1.0f;

    // To world coordinates:
    Vector4 out(m * in);
    if (FloatIsZero(out[3])) // Avoid a division by zero
    {
        worldCoordinates = Vector3Zero;
        return false;
    }

    out[3] = 1.0f / out[3];
    worldCoordinates[0] = out[0] * out[3];
    worldCoordinates[1] = out[1] * out[3];
    worldCoordinates[2] = out[2] * out[3];
    return true;
}

} // namespace Engine {}
