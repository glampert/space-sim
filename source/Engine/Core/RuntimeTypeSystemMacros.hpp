
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: RuntimeTypeSystemMacros.hpp
// Author: Guilherme R. Lampert
// Created on: 16/08/14
// Brief: Ugly macros used by the Runtime Type System. Better hide them here :P
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// ======================================================
// RUNTIME_CLASS_DECL():
//   Must be added to the declaration of every concrete
//   class that uses the Runtime Type System.
// ======================================================

#define RUNTIME_CLASS_DECL(className)                                \
  public:                                                            \
    static ::Engine::TypeInfo Type;                                  \
    static ::Engine::Class * CreateInstance();                       \
    static void DestroyInstance(::Engine::Class * instance);         \
    virtual const ::Engine::TypeInfo & GetTypeInfo() const override; \
    virtual className * Clone() const override;

// ======================================================
// RUNTIME_CLASS_DEFINITION():
//   Must be added to the definition of every concrete
//   class that uses the Runtime Type System.
// ======================================================

#define RUNTIME_CLASS_DEFINITION(superclassName, className, customInstancer, customDeleter)                                 \
    ::Engine::TypeInfo className::Type(#superclassName, #className, className::CreateInstance, className::DestroyInstance); \
    ::Engine::Class * className::CreateInstance()                                                                           \
    {                                                                                                                       \
        return customInstancer();                                                                                           \
    }                                                                                                                       \
    void className::DestroyInstance(::Engine::Class * instance)                                                             \
    {                                                                                                                       \
        customDeleter(::Engine::checked_cast<className *>(instance));                                                       \
    }                                                                                                                       \
    const ::Engine::TypeInfo & className::GetTypeInfo() const                                                               \
    {                                                                                                                       \
        return className::Type;                                                                                             \
    }                                                                                                                       \
    className * className::Clone() const                                                                                    \
    {                                                                                                                       \
        return customInstancer(*this);                                                                                      \
    }

// ======================================================
// RUNTIME_INTERFACE_DECL():
//   Abstract base classes (interfaces) that use the Runtime
//   Type System must have this macro added to their declaration.
// ======================================================

#define RUNTIME_INTERFACE_DECL(ifaceName)                       \
  public:                                                       \
    static ::Engine::TypeInfo Type;                             \
    virtual const ::Engine::TypeInfo & GetTypeInfo() const = 0; \
    virtual ifaceName * Clone() const = 0;

// ======================================================
// RUNTIME_INTERFACE_DEFINITION():
//   Abstract base classes (interfaces) that use the Runtime
//   Type System must have this macro added to their definition.
//   Preferably in a .cpp file.
// ======================================================

#define RUNTIME_INTERFACE_DEFINITION(superclassName, ifaceName)                                                       \
    /* Use the line number to allow more than one interface per file. */                                              \
    /* ifaceName is not viable because it can contain :: for namespaces, which would be illegal in a function name */ \
    static ::Engine::Class * TOKEN_APPEND3(_create_iface_, __LINE__, _trap)()                                         \
    {                                                                                                                 \
        ::Engine::common->FatalErrorF("Trying to create instance of abstract class %s", #ifaceName);                  \
    }                                                                                                                 \
    ::Engine::TypeInfo ifaceName::Type(#superclassName, #ifaceName, TOKEN_APPEND3(_create_iface_, __LINE__, _trap), ::Engine::Class::DestroyInstance);

// ======================================================
// DECLARE_CLASS_INSTANCER_DELETER():
//   Some classes declare private constructors, requiring
//   custom instancers/deleter. This makes declaring them easier.
// ======================================================

#define DECLARE_CLASS_INSTANCER_DELETER(className)             \
  private:                                                     \
    static className * ClassInstancer();                       \
    static className * ClassInstancer(const className & copy); \
    static void ClassDeleter(className * instance);            \
                                                               \
  public:                                                      \
    /* This provides compatibility with C++ smart pointers. */ \
    struct PtrDeleter                                          \
    {                                                          \
        void operator()(className * instance) const            \
        {                                                      \
            className::DestroyInstance(instance);              \
        }                                                      \
    };

// ======================================================
// IMPLEMENT_CLASS_INSTANCER_DELETER():
//   Add this in the .cpp of a class that declared custom
//   instancers/deleter to provide a default implementation
//   that uses operator new()/delete().
// ======================================================

#define IMPLEMENT_CLASS_INSTANCER_DELETER(className)              \
    className * className::ClassInstancer()                       \
    {                                                             \
        return new className;                                     \
    }                                                             \
    className * className::ClassInstancer(const className & copy) \
    {                                                             \
        return new className(copy);                               \
    }                                                             \
    void className::ClassDeleter(className * instance)            \
    {                                                             \
        delete instance;                                          \
    }

