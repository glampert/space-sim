
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Hash.hpp
// Author: Guilherme R. Lampert
// Created on: 26/12/12
// Brief: Defines the integer type HashIndex and some common hash functions.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// Result of hashing a sequence of bytes.
using HashIndex = uint32_t;

// Signature of a generic hash function.
using HashFunction = HashIndex (*)(const void *, size_t);

// One-at-a-Time hash (OAT) algorithm.
HashIndex HashFuncOAT(const void * key, const size_t sizeInBytes);

// Cyclic redundancy check (CRC32) hash algorithm.
HashIndex HashFuncCRC32(const void * key, const size_t sizeInBytes);

// Converts a HashIndex to a printable string, formatted as hexadecimal. E.g.: "deadbeef"
std::string HashIndexToString(const HashIndex h);

// Hash an std::string (doesn't process the trailing null, of course).
inline HashIndex HashString(const std::string & str)
{
    return HashFuncOAT(str.c_str(), str.length());
}

// Hash a plain C string.
inline HashIndex HashString(const char * str)
{
    assert(str != nullptr);
    return HashFuncOAT(str, std::strlen(str));
}

// Hash a plain C string with known length.
inline HashIndex HashString(const char * str, const size_t len)
{
    assert(str != nullptr);
    return HashFuncOAT(str, len);
}

// Hash a pointer (which might be 64bits) into a 32bit HashIndex.
inline HashIndex HashPointer(const void * ptr)
{
    const uintptr_t addr = reinterpret_cast<uintptr_t>(ptr);
    return HashFuncCRC32(&addr, sizeof(addr));
}

} // namespace Engine {}
