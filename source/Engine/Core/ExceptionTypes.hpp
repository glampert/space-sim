
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: ExceptionTypes.hpp
// Author: Guilherme R. Lampert
// Created on: 14/08/14
// Brief: Exception types used by the Engine. Base Exception class inherits from std::exception.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Exception:
// ======================================================

//
// Base exception type used by the Engine Library.
// Derived from std::exception.
//
class Exception
    : public std::exception
{
  public:

    // Max length in chars of an error/exception message. Including a null at the end.
    static constexpr int MaxExceptionMessageLen = 1024;

    // Construct exception with empty error message.
    Exception();

    // Construct with given error message (ASCII string).
    Exception(const char * error);

    // Get the error message.
    const char * ErrorMessage() const noexcept;

    // Test if the ASCII error string is set.
    bool HasErrorMessage() const noexcept;

    // Provided for compatibility with std::exception.
    const char * what() const noexcept;

    // Returns a printable string with the exception name. Default implementation returns: "Exception".
    virtual const char * ExceptionName() const noexcept;

    // No-op
    virtual ~Exception() = default;

  protected:

    // Allow inheriting classes to clear the error message.
    void ClearString() noexcept;

  private:

    // Long error description (ASCII string):
    static char errorMessage[MaxExceptionMessageLen];
};

// ======================================================
// FatalErrorException:
// ======================================================

//
// Fatal error exception.
// Thrown by Common::FatalErrorF().
//
class FatalErrorException
    : public Exception
{
  public:

    FatalErrorException(const char * error)
        : Exception(error)
    { }

    const char * ExceptionName() const noexcept
    {
        return "Engine::FatalErrorException";
    }
};

} // namespace Engine {}
