
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: XMLWriter.hpp
// Author: Guilherme R. Lampert
// Created on: 26/04/13
// Brief: Simple XML stream writer.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

//
// Tiny XML writer helper.
//
// This class makes the task of writing XML files easier
// by relieving the programmer from the task of managing tags
// and other XML syntax details. The writer has minimal overhead
// and allocates no memory directly.
//
// The writer takes care of opening and closing tags, indenting the tag hierarchy
// and replacing the so called 'Entity Reference' characters with its corresponding string tokens.
//
// Note: The current implementation is only capable of writing ASCII and UTF-8 data.
//
// Example:
//
// ---- The following C++ code: ----
//
// XMLWriter xmlWriter(output, encoding);
//
// XMLRoot * rootNode = XMLElement::NewRoot("rootNode");
// rootNode->AddAttribute(XMLAttribute::NewAttribute("name", "John"));
// rootNode->AddAttribute(XMLAttribute::NewAttribute("surname", "Galt"));
//
// XMLElement * child0 = rootNode->AddChild(XMLElement::NewElement("child0"));
// XMLElement * child1 = rootNode->AddChild(XMLElement::NewElement("child1"));
// child1->AddCDATA("(Unparsed) Character Data - CDATA; <> \'&&\' \"test\" >/>");
//
// XMLElement * child1SubChild = child1->AddChild(XMLElement::NewElement("child1SubChild"));
// child1SubChild->AddText("\"Hello\" & <XML> & \'World\'");
//
// xmlWriter.WriteComment("Writing rootNode...");
// xmlWriter.WriteSubtree(*rootNode);
//
// ---- Generates the XML: ----
//
// <?xml version="1.0"?>
// <!-- Writing rootNode... -->
// <rootNode name="John" surname="Galt">
//		<child0 />
//		<child1>
//			<![CDATA[ (Unparsed) Character Data - CDATA; <> '&&' "test" >/> ]]>
//			<child1SubChild>
//				&quot;Hello&quot; &amp; &lt;XML&gt; &amp; &apos;World&apos;
//			</child1SubChild>
//		</child1>
// </rootNode>
//
class XMLWriter
{
  public:

    // Output callback. Receives the formatted XML text to be printed.
    using PrinterFunction = void (*)(void *, const char *);

    // Initializes the writer with an output stream and writes the XML version header, with optional encoding.
    XMLWriter(PrinterFunction outputFunc, void * userData, const char * encoding = nullptr);

    // Writes a XML subtree. This function implies a recursive call to every element in the tree.
    void WriteSubtree(const XMLElement & subtreeRoot);

    // Writes a XML comment in the current scope.
    void WriteComment(const char * comment, const bool multiline = false);

    // Writes a XML CDATA - (Unparsed) Character Data section in the current scope.
    void WriteCDATA(const char * CDATA_contents, const bool multiline = false);

  private:

    // Internal helpers:
    void WriteSubtreeRecursive(const XMLElement & subtreeRoot, std::string & indent);
    void WriteAttributes(const XMLElement & elem);
    void WriteTextBody(const char * text);
    void OutputXML(const char * text);

    // Output function, if any:
    PrinterFunction outputCallback;
    void * ud;

    DISABLE_COPY_AND_ASSIGN(XMLWriter)
};

} // namespace Engine {}
