
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Resource.hpp
// Author: Guilherme R. Lampert
// Created on: 09/08/14
// Brief: Base class for all game resources, like textures, 3D models, etc.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace Engine
{

// ======================================================
// Resource interface:
// ======================================================

class Resource
{
  public:

    // Flags used as hints to ResourceManager:
    enum class Type
    {
        Unspecified,   // Unspecified or don't care.
        Texture,       // Texture or image (E.g.: PNG, JPEG, TGA).
        Model3D,       // 3D mode / mesh (E.g.: OBJ, DAE, MD2).
        ShaderProgram, // GLSL shader program.
        Material,      // Material / MTR description file.
        Font,          // Texture glyph map or a FNT file.
        Count          // Number of entries in this enum. Internal use.
    };

    // Init the resource from a file on disk.
    virtual bool InitFromFile(std::string filename, std::string resourceId) = 0;

    // Init the resource from a block of data. If the data originated from a file, the filename is also provided.
    virtual bool InitFromData(const DataBlob blob, std::string filename) = 0;

    // Frees all underlaying resource data.
    virtual void FreeAllData() = 0;

    // Test if the resource is a default. This is usually the case if an initialization error occurs.
    virtual bool IsDefault() const = 0;

    // Estimate the amount of memory consumed by this resource.
    virtual size_t GetEstimateMemoryUsage() const = 0;

    // Resource name or id. Usually a filename.
    virtual std::string GetResourceId() const = 0;

    // Get a short description string for the resource type. Used for debugging only.
    virtual std::string GetResourceTypeString() const = 0;

    // Print a description of the resource to the developer log (Common).
    virtual void PrintSelf() const = 0;

    // No-op virtual destructor:
    virtual ~Resource() = default;
};

// Strong reference counted pointer to Resource.
using ResourcePtr = std::shared_ptr<Resource>;
using ConstResourcePtr = std::shared_ptr<const Resource>;

} // namespace Engine {}
