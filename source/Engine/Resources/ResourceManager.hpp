
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: ResourceManager.hpp
// Author: Guilherme R. Lampert
// Created on: 13/08/14
// Brief: Resource data management and caching.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <map>

namespace Engine
{

// ======================================================
// Helper types and macros:
// ======================================================

// Signature for a resource creation callback function:
using ResourceFactoryFunction = ResourcePtr (*)(bool);

//
// Registers a static or global function as a resource factory
// with ResourceManager. Should be called at global scope.
// Receives a Resource::Type constant to associate with the callback.
//
#define REGISTER_RESOURCE_FACTORY_FOR_TYPE(resourceType, function) \
    static bool TOKEN_APPEND3(_b_, __LINE__, _dummy_) = ResourceManager::RegisterResourceFactoryFunction((resourceType), (function))

//
// Registers a static or global function as a resource factory
// with ResourceManager. Should be called at global scope.
// Associates the callback with a given filename extension.
// Extension string must include the dot. E.g.: ".png"
//
#define REGISTER_RESOURCE_FACTORY_FOR_FILENAME_EXT(ext, function) \
    static bool TOKEN_APPEND3(_b_, __LINE__, _dummy_) = ResourceManager::RegisterResourceFactoryFunction((ext), (function))

// ======================================================
// ResourceManager singleton:
// ======================================================

class ResourceManager final
    : public SingleInstanceType<ResourceManager>
{
  public:

    // Standard filename of the default game resource package:
    static const std::string DefaultPackageName;

    // Names of the default textures:
    static const std::string GrayTextureName;
    static const std::string WhiteTextureName;
    static const std::string BlackTextureName;
    static const std::string FlatNormalTextureName;
    static const std::string DefaultDiffuseTextureName;

    // Standard file extensions (including a dot '.' at the beginning):
    static const std::string StandardModel3DFileExt;
    static const std::string StandardTextureFileExt;
    static const std::string StandardMaterialFileExt;
    static const std::string StandardShaderFileExt;
    static const std::string StandardFontFileExt;
    static const std::string StandardCubeMapFileExt;

    // Misc:
    static const std::string DefaultMaterialNamePrefix; // Name prefix for default materials.
    static const std::string DefaultShaderProgramName;  // Name of the universal built-in Shader Program.
    static const std::string CubeMapsDirectory;         // Directory relative to the textures dir where cube-maps are stored.
    static const std::string FontsDirectory;            // Directory relative to the textures dir where font glyph textures are stored.

    // Sort predicates for PrintResourceCache():
    enum SortPredicate
    {
        DontSort,          // Don't sort.
        SortByType,        // Sort by resource type.
        SortAlphabetically // Sort by resource id/name in alphabetical order.
    };

    // Not directly used.
    // 'resourceMgr' global is provided instead.
    ResourceManager();

    // Initialization and shutdown. Called internally by the Engine.
    void Init();
    void Shutdown();

    // Dump all currently loaded resources to the developer log.
    void PrintResourceCache(const SortPredicate sortPred = SortAlphabetically) const;

    // Frees all cached resources that are not referenced outside the ResourceManager.
    void CollectGarbage();

    // Find an existing resource. Returns null if resource not found.
    ConstResourcePtr FindLoadedResource(const std::string & resourceId) const;
    ConstResourcePtr FindLoadedResource(const HashIndex resourceId) const;

    // Find an existing resource, attempting to load it if not found.
    // If the filename (resourceId) has an extension, type parameter is ignored.
    // If no extension in the name, a type flag different than Unspecified must be provided.
    // If an error occurs and the resource cannot be properly created, an error
    // will be logged and a default resource will be returned.
    // Call Resource::IsDefault() to make sure the loading was successful.
    ConstResourcePtr FindOrLoadResource(const std::string & resourceId, const Resource::Type type = Resource::Type::Unspecified);

    // Register a resource created externally. Fails if a resource with same name/id is already registered.
    bool RegisterNewResource(ConstResourcePtr resource);

    // Pass a filename extensions (including the dot) and get a Resource::Type constant as result.
    static Resource::Type InferResourceTypeFromFilenameExt(const std::string & filenameExt);

    // Registers a resource factory callback function with the Resource Manager. All callbacks are removed on shutdown.
    static bool RegisterResourceFactoryFunction(const std::string & filenameExt, ResourceFactoryFunction func);
    static bool RegisterResourceFactoryFunction(const Resource::Type type, ResourceFactoryFunction func);

  private:

    // Map of resource instantiation callbacks, indexed by a filename extension.
    using ResourceFactoryMap = std::map<std::string, ResourceFactoryFunction>;

    // Associates a filename extension with a resource type.
    using ResourceTypeFilenameExtMap = std::map<std::string, Resource::Type>;

    // Resources are indexed by the HashIndex of their name.
    using ResourceMap = std::map<HashIndex, ConstResourcePtr>;

    // Internal helpers:
    static ResourceFactoryMap & GetResourceFactories();
    static const ResourceTypeFilenameExtMap & GetFilenameExtensionRegistry();
    static const std::string & DirNameForResourceType(const Resource::Type type);
    ConstResourcePtr TryToLoadResource(std::string resourceId, const Resource::Type type);
    ConstResourcePtr LoadResourceFromPackage(std::string && resourceId, std::string && filenameExt, const Resource::Type type);

    // Map of currently loaded resources:
    ResourceMap resourceCache;

    // If set, will directly load resources via InitFromFile(),
    // instead of loading data from the package files. Config var: "resources.bypassResourcePacks"
    bool bypassResourcePacks; // default = false

    DISABLE_COPY_AND_ASSIGN(ResourceManager)
};

// Globally unique instance:
extern ResourceManager * resourceMgr;

} // namespace Engine {}
