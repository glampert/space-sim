
// ================================================================================================
// -*- C++ -*-
// File: ResourceManager.cpp
// Author: Guilherme R. Lampert
// Created on: 13/08/14
// Brief: Resource data management and caching.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"
#include "Engine/Resources/Resource.hpp"
#include "Engine/Resources/ResourcePackage.hpp"
#include "Engine/Resources/ResourceManager.hpp"
#include "Engine/Scripting/ScriptManager.hpp"
#include <vector>

namespace Engine
{

// ======================================================
// ResourceManager constants:
// ======================================================

// Standard filename of the default game resource package:
const std::string ResourceManager::DefaultPackageName        = "resources";

// Standard resource names for built-in resources:
const std::string ResourceManager::GrayTextureName           = "_gray";
const std::string ResourceManager::WhiteTextureName          = "_white";
const std::string ResourceManager::BlackTextureName          = "_black";
const std::string ResourceManager::FlatNormalTextureName     = "_flat_normal";
const std::string ResourceManager::DefaultDiffuseTextureName = "_checkerboard";
const std::string ResourceManager::DefaultMaterialNamePrefix = "_default_mtr_";
const std::string ResourceManager::DefaultShaderProgramName  = "_default_program";

// Standard filename extensions / directories:
const std::string ResourceManager::StandardModel3DFileExt    = ".dae";
const std::string ResourceManager::StandardTextureFileExt    = ".png";
const std::string ResourceManager::StandardMaterialFileExt   = ".mtr";
const std::string ResourceManager::StandardShaderFileExt     = ".glsl";
const std::string ResourceManager::StandardFontFileExt       = ".fnt";
const std::string ResourceManager::StandardCubeMapFileExt    = ".png";

const std::string ResourceManager::CubeMapsDirectory         = "cube_maps";
const std::string ResourceManager::FontsDirectory            = "fonts";

// ======================================================
// ResourceManager singleton:
// ======================================================

ResourceManager::ResourceManager()
    : bypassResourcePacks(false)
{
}

void ResourceManager::Init()
{
    common->PrintF("------ ResourceManager::Init() -----");

    // Cache configuration vars:
    scriptMgr->GetConfigValue("resourcesConfig.bypassResourcePacks", bypassResourcePacks);

    if (bypassResourcePacks)
    {
        common->PrintF("ResourceManager will load resources directly from file! (bypassResourcePacks is true)");
    }
}

void ResourceManager::Shutdown()
{
    common->PrintF("---- ResourceManager::Shutdown() ---");

    // Clear caches:
    resourceCache.clear();
}

void ResourceManager::PrintResourceCache(const ResourceManager::SortPredicate sortPred) const
{
    unsigned int namePadding = 7; // min for "name/id" = 7
    unsigned int typePadding = 4; // min for "type"    = 4
    std::vector<ConstResourcePtr> sortedResources;

    // Get padding needed to print aligned columns and add to 'sortedResources':
    sortedResources.reserve(resourceCache.size());
    for (const auto & it : resourceCache)
    {
        namePadding = std::max<unsigned int>(it.second->GetResourceId().length(), namePadding);
        typePadding = std::max<unsigned int>(it.second->GetResourceTypeString().length(), typePadding);
        sortedResources.push_back(it.second);
    }

    // Select sorting predicate:
    if (sortPred == SortAlphabetically)
    {
        std::sort(std::begin(sortedResources), std::end(sortedResources),
                  [](const ConstResourcePtr & a, const ConstResourcePtr & b) -> bool
                  { return CaseInsensitiveStringCompare(a->GetResourceId(), b->GetResourceId()) < 0;
                  });
    }
    else if (sortPred == SortByType)
    {
        std::sort(std::begin(sortedResources), std::end(sortedResources),
                  [](const ConstResourcePtr & a, const ConstResourcePtr & b) -> bool
                  { return a->GetResourceTypeString() < b->GetResourceTypeString();
                  });
    }
    // Else, don't sort.

    // List all currently loaded resources.
    // Table format:
    //
    //   name/id | type | ref count | is default | estimated mem size
    //
    common->PrintF("------------------ Loaded resources ------------------");
    common->PrintF("%-*s | %-*s | ref cnt | default | size", namePadding, "name/id", typePadding, "type");

    for (const auto & resource : sortedResources)
    {
        // Note: use_count() is decremented by one to hide
        // the extra temp reference added by sortedResources.
        //
        common->PrintF("%-*s | %-*s | %-7li | %-7s | %s",
                       namePadding, resource->GetResourceId().c_str(),
                       typePadding, resource->GetResourceTypeString().c_str(),
                       (resource.use_count() - 1), (resource->IsDefault() ? "yes" : "no"),
                       FormatMemoryUnit(resource->GetEstimateMemoryUsage(), true).c_str());
    }

    common->PrintF("Listed %zu resources.", sortedResources.size());
    common->PrintF("------------------------------------------------------");
}

void ResourceManager::CollectGarbage()
{
    int freeCount   = 0;
    size_t memCount = 0;

    const int64_t start = system->ClockMillisec();

    auto it = std::begin(resourceCache);
    while (it != std::end(resourceCache))
    {
        // Just this local reference. We can free it:
        if (it->second.unique())
        {
            ++freeCount, memCount += it->second->GetEstimateMemoryUsage();
            resourceCache.erase(it);

            // Now we have to restart the loop from the beginning
            // because if the freed resource referenced other resources,
            // they might be eligible for garbage collection too.
            // If they are placed in a map slot that goes BEFORE this one,
            // we have no choice but to restart the iteration.
            it = std::begin(resourceCache);
            continue;
        }
        ++it;
    }

    const int64_t end = system->ClockMillisec();

    common->PrintF("ResourceManager::CollectGarbage() took %lli milliseconds.", (end - start));
    common->PrintF("ResourceManager::CollectGarbage() freed %d unreferenced resources (~%s).",
                   freeCount, FormatMemoryUnit(memCount).c_str());
}

ConstResourcePtr ResourceManager::FindLoadedResource(const std::string & resourceId) const
{
    assert(!resourceId.empty());
    const auto it = resourceCache.find(HashString(resourceId));
    if (it != std::end(resourceCache))
    {
        return it->second;
    }

    // Not found.
    return ConstResourcePtr();
}

ConstResourcePtr ResourceManager::FindLoadedResource(const HashIndex resourceId) const
{
    assert(resourceId != 0);
    const auto it = resourceCache.find(resourceId);
    if (it != std::end(resourceCache))
    {
        return it->second;
    }

    // Not found.
    return ConstResourcePtr();
}

ConstResourcePtr ResourceManager::FindOrLoadResource(const std::string & resourceId, const Resource::Type type)
{
    assert(!resourceId.empty());
    auto it = resourceCache.find(HashString(resourceId));
    if (it != std::end(resourceCache))
    {
        return it->second;
    }

    // Not found. Try to create/load new:
    return TryToLoadResource(resourceId, type);
}

bool ResourceManager::RegisterNewResource(ConstResourcePtr resource)
{
    assert(resource != nullptr);

    const HashIndex hResourceId = HashString(resource->GetResourceId());
    assert(hResourceId != 0);

    // Don't allow re-registering a resource.
    // (this could also be a name clash, but unlikely)
    auto it = resourceCache.find(hResourceId);
    if (it != std::end(resourceCache))
    {
        common->ErrorF("A resource named \"%s\" (%s) is already registered with ResourceManager!",
                       resource->GetResourceId().c_str(), HashIndexToString(hResourceId).c_str());
        return false;
    }

    resourceCache.emplace(std::make_pair(hResourceId, resource));
    common->PrintF("Registered new resource \"%s\" (%s)",
                   resource->GetResourceId().c_str(),
                   HashIndexToString(hResourceId).c_str());
    return true;
}

ConstResourcePtr ResourceManager::TryToLoadResource(std::string resourceId, const Resource::Type type)
{
    assert(!resourceId.empty());
    std::string filenameExt = ExtractFilenameExtension(resourceId, /* includeDot = */ true);

    // Load with explicit file extension (type is ignored):
    if (!filenameExt.empty())
    {
        if (type != Resource::Type::Unspecified)
        {
            common->WarningF("Resource name \"%s\" has an explicit filename extension. "
                             "\'Resource::Type\' will be ignored!",
                             resourceId.c_str());
        }

        return LoadResourceFromPackage(std::move(resourceId),
                                       std::move(filenameExt),
                                       InferResourceTypeFromFilenameExt(filenameExt));
    }
    else
    {
        // Add default extension based on Resource::Type:
        switch (type)
        {
        case Resource::Type::Texture:
            filenameExt = StandardTextureFileExt;
            break;

        case Resource::Type::Model3D:
            filenameExt = StandardModel3DFileExt;
            break;

        case Resource::Type::ShaderProgram:
            filenameExt = StandardShaderFileExt;
            break;

        case Resource::Type::Material:
            filenameExt = StandardMaterialFileExt;
            break;

        case Resource::Type::Font:
            filenameExt = StandardFontFileExt;
            break;

        default:
            // Try just the filename, without extension.
            break;
        } // switch (type)

        return LoadResourceFromPackage(std::move(resourceId + filenameExt), std::move(filenameExt), type);
    }
}

ConstResourcePtr ResourceManager::LoadResourceFromPackage(std::string && resourceId, std::string && filenameExt, const Resource::Type type)
{
    //
    // NOTE: We can add multi-package support in the future
    // by doing a search here, in this method, testing the given filename
    // against the registered packages and loading the first occurrence.
    //

    // First see if we can create the resource object:
    auto it = GetResourceFactories().find(filenameExt);
    if (it == std::end(GetResourceFactories()))
    {
        common->FatalErrorF("Resource type is unknown! Filename: \"%s\", extension: \"%s\"",
                            resourceId.c_str(), filenameExt.c_str());
    }

    // Shortcut variable:
    ResourceFactoryFunction factoryFunc = it->second;
    ResourcePtr newResource;
    bool defaulted = false;

    // Load the resource data from storage:
    if (!bypassResourcePacks)
    {
        assert(false && "Resource packages not yet implemented!");
        //TODO
        /*
		DataBlob blob;
		if (type != Resource::Unspecified)
		{
			// Append standard directory:
			success = resourcePack->LoadFile(DirNameForResourceType(type) + resourceId, blob);
		}
		else // 'resourceId' should specify the full file path with directory:
		{
			success = resourcePack->LoadFile(resourceId, blob);
		}
		*/
    }
    else
    {
        // Create the resource object:
        newResource = factoryFunc(/* forceDefault = */ false);
        if (newResource == nullptr)
        {
            common->FatalErrorF("Resource factory function returned a null pointer!");
        }

        std::string fullPathName(DefaultPackageName + System::PathSepString + DirNameForResourceType(type) + resourceId);
        common->PrintF("Trying to load resource from path: \"%s\"...", fullPathName.c_str());

        // Try to load from file.
        // A default resource will be returned on failure.
        if (!newResource->InitFromFile(std::move(fullPathName), resourceId))
        {
            common->WarningF("Failed to load resource from file! Retuning a default...");
            newResource = factoryFunc(/* forceDefault = */ true);
            defaulted = true;
        }
    }

    // Register new resource and return user pointer.
    // Pointer will always be valid, but possibly carrying a default resource.
    if (!defaulted)
    {
        // Defaults are not registered here.
        assert(resourceId == newResource->GetResourceId());
        resourceCache.emplace(std::make_pair(HashString(resourceId), newResource));
    }

    return newResource;
}

Resource::Type ResourceManager::InferResourceTypeFromFilenameExt(const std::string & filenameExt)
{
    auto it = GetFilenameExtensionRegistry().find(filenameExt);
    if (it != std::end(GetFilenameExtensionRegistry()))
    {
        return it->second;
    }
    return Resource::Type::Unspecified;
}

bool ResourceManager::RegisterResourceFactoryFunction(const std::string & filenameExt, ResourceFactoryFunction func)
{
    assert(func != nullptr);
    assert(!filenameExt.empty());
    assert(filenameExt[0] == '.');

    auto it = GetResourceFactories().find(filenameExt);
    if (it != std::end(GetResourceFactories()))
    {
        // Not a common->ErrorF() because this can be called before main() runs!
        assert(false && "A factory function is already registered for this file extension!");
        return false; // Already registered!
    }

    GetResourceFactories().emplace(std::make_pair(filenameExt, func));
    return true;
}

bool ResourceManager::RegisterResourceFactoryFunction(const Resource::Type type, ResourceFactoryFunction func)
{
    assert(func != nullptr);
    assert(type != Resource::Type::Unspecified);

    // Register for all known extension associated with this resource type:
    const ResourceTypeFilenameExtMap & reg = GetFilenameExtensionRegistry();
    for (const auto & it : reg)
    {
        if (it.second == type)
        {
            if (!RegisterResourceFactoryFunction(it.first, func))
            {
                return false;
            }
        }
    }
    return true;
}

ResourceManager::ResourceFactoryMap & ResourceManager::GetResourceFactories()
{
    static ResourceFactoryMap resourceFactories;
    return resourceFactories;
}

const ResourceManager::ResourceTypeFilenameExtMap & ResourceManager::GetFilenameExtensionRegistry()
{
    // Registry associating a filename extension with a Resource::Type constant:
    static const ResourceTypeFilenameExtMap filenameExtensionRegistry = {
        // Image files:
        { ".png",  Resource::Type::Texture },
        { ".tga",  Resource::Type::Texture },
        { ".jpg",  Resource::Type::Texture },
        { ".jpeg", Resource::Type::Texture },

        // Alias for cube-map sets:
        { ".cm", Resource::Type::Texture },

        // Mesh files:
        { ".dae", Resource::Type::Model3D },
        { ".obj", Resource::Type::Model3D },

        // Shader source files:
        { ".glsl", Resource::Type::ShaderProgram },

        // Font description files:
        { ".fnt", Resource::Type::Font },

        // Material description files:
        { ".mtr", Resource::Type::Material }
    };
    return filenameExtensionRegistry;
}

const std::string & ResourceManager::DirNameForResourceType(const Resource::Type type)
{
    // Registry associating a Resource::Type with a base directly name.
    // Must be in the same order of the Resource::Type enum!
    static const std::string resourceTypeDirNameRegistry[] = {
        "",           // Unspecified
        "textures/",  // Texture
        "models/",    // Model3D
        "shaders/",   // ShaderProgram
        "materials/", // Material
        "fonts/"      // Font
    };
    static_assert(ArrayLength(resourceTypeDirNameRegistry) == size_t(Resource::Type::Count),
                  "Update this if Resource::Type changed!");

    const size_t index = static_cast<size_t>(type);
    assert(index < ArrayLength(resourceTypeDirNameRegistry));
    return resourceTypeDirNameRegistry[index];
}

// ======================================================
// Global ResourceManager instance:
// ======================================================

static ResourceManager theResourceManager;
ResourceManager * resourceMgr = &theResourceManager;

} // namespace Engine {}
