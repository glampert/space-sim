
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: ScriptManager.hpp
// Author: Guilherme R. Lampert
// Created on: 06/08/14
// Brief: Scripting and configurations subsystem. Uses the Lua scripting language.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <initializer_list>
#include <map>

namespace Engine
{

// ======================================================
// ScriptVar:
// ======================================================

// Script variant:
class ScriptVar final
{
  public:

    enum class Type
    {
        Undefined = '?',
        Boolean   = 'b',
        Integer   = 'i',
        Float     = 'f',
        CString   = 's',
        Vector3   = '3',
        Vector4   = '4'
    };

    union Data
    {
        void *  asVoidPtr;
        bool    asBoolean;
        int     asInteger;
        float   asFloat;
        char *  asCString;
        Vector3 asVector3;
        Vector4 asVector4;

        // No-op.
        Data() noexcept { }
    };

    // Default constructor will set type to Undefined and data to null/zero.
    ScriptVar() noexcept;

    // Construct with type tag and data. Takes ownership of the data.
    ScriptVar(const Type t, Data && d) noexcept;

    // Copy and assignment:
    ScriptVar(const ScriptVar & other);
    ScriptVar & operator=(const ScriptVar & other);

    // Move/assign:
    ScriptVar(ScriptVar && other) noexcept;
    ScriptVar & operator=(ScriptVar && other) noexcept;

    // Destructor will free memory if type tag requires so.
    ~ScriptVar();

    // Sets the var type to Undefined and nullifies its value, deallocating memory if necessary.
    void Reset();

    // Get a printable string describing the variable's type. E.g.: "int" for Type::Integer.
    std::string GetTypeString() const;

    // Get variable type tag.
    Type GetType() const { return type; }

    // Inline data accessors. Will get a null/zero if data is Undefined (except for AsString).
    bool AsBoolean() const { return data.asBoolean; }
    int AsInteger()  const { return data.asInteger; }
    float AsFloat()  const { return data.asFloat;   }

    const char * AsCString() const { return data.asCString; }
    const void * AsVoidPtr() const { return data.asVoidPtr; }

    Vector3 AsVector3() const { return data.asVector3; }
    Vector4 AsVector4() const { return data.asVector4; }

    std::string AsString() const
    {
        assert(data.asCString != nullptr);
        return data.asCString;
    }

  private:

    // ScriptManager can edit the var directly.
    friend class ScriptManager;

    // Does copy marshaling.
    void InitFromCopy(const ScriptVar & other);

    // Data and type identifier tag:
    Data data;
    Type type;
};

// Make a printable string from a ScriptVar's value.
std::string ToString(const ScriptVar & var);

// ======================================================
// ScriptManager singleton:
// ======================================================

class ScriptManager final
    : public SingleInstanceType<ScriptManager>
{
  public:

    // Not directly used.
    // 'scriptMgr' global is provided instead.
    ScriptManager();

    // Initialization and shutdown. Called internally by the Engine.
    void Init();
    void Shutdown();

    // ---- Scripts: ----

    // Get/set debug/developer flags:
    bool IsScriptLogSilent() const;
    bool IsScriptDebugEnabled() const;
    void SetScriptLogSilent(const bool silent);
    void SetScriptDebugEnabled(const bool enable);

    // Run Lua scripts:
    bool RunScriptFile(const std::string & filename);
    bool RunScriptString(const char * sourceCode, const size_t * sourceLength = nullptr);

    // ---- Engine configuration: ----

    // Runs the configuration Lua script loading all vars found in the given table.
    bool RunConfigFile(const std::string & filename, const std::string & configTableName);
    bool RunConfigFile(const std::string & filename, const std::initializer_list<std::string> & configTables);

    // Access script/config globals. Used mainly for Engine configuration.
    // If 'optionalVar' is true no warning is generated if it doesn't exist.
    // Otherwise, it will print a warning of the variable cannot be found.
    const ScriptVar * FindConfigVar(const std::string & varName) const;
    bool GetConfigValue(const std::string & varName, bool & dest, const bool optionalVar = false) const;
    bool GetConfigValue(const std::string & varName, int & dest, const bool optionalVar = false) const;
    bool GetConfigValue(const std::string & varName, unsigned int & dest, const bool optionalVar = false) const;
    bool GetConfigValue(const std::string & varName, float & dest, const bool optionalVar = false) const;
    bool GetConfigValue(const std::string & varName, std::string & dest, const bool optionalVar = false) const;
    bool GetConfigValue(const std::string & varName, Vector3 & dest, const bool optionalVar = false) const;
    bool GetConfigValue(const std::string & varName, Vector4 & dest, const bool optionalVar = false) const;

    // Dumps all configuration variables to the developer log.
    void PrintConfigVars() const;

  private:

    // Global context for all scripts ran by ScriptManager.
    // As a void* so we don't have to make the Lua API public.
    void * luaContext; // lua_State*

    // Configuration variables loaded via RunConfigFile().
    std::map<std::string, ScriptVar> configVars;

    // Script debug flags:
    bool scritLogSilent;
    bool scriptDebugEnabled;

    DISABLE_COPY_AND_ASSIGN(ScriptManager)
};

// Globally unique instance:
extern ScriptManager * scriptMgr;

} // namespace Engine {}
