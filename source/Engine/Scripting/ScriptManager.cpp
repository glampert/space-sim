
// ================================================================================================
// -*- C++ -*-
// File: ScriptManager.cpp
// Author: Guilherme R. Lampert
// Created on: 07/08/14
// Brief: Basic Lua scripting interface.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

#include <cmath> // for std::floor()

// Lua API:
#include "ThirdParty/lualib/include/lua.hpp"

namespace Engine
{

// ======================================================
// External helpers:
// ======================================================

// LuaCommon.cpp:
extern void InitLuaCommon();
extern bool RunLuaScriptFile(lua_State * luaState, const char * scriptName);

// ======================================================
// ScriptVar:
// ======================================================

ScriptVar::ScriptVar() noexcept
{
    Reset();
}

ScriptVar::ScriptVar(const Type t, Data && d) noexcept
{
    // Shallow copy of 'data':
    type = t;
    std::memcpy(&data, &d, sizeof(Data));

    // Reset input:
    ZeroObjectUnchecked(d);
}

ScriptVar::ScriptVar(const ScriptVar & other)
{
    type = Type::Undefined;
    InitFromCopy(other);
}

ScriptVar & ScriptVar::operator=(const ScriptVar & other)
{
    InitFromCopy(other);
    return *this;
}

ScriptVar::ScriptVar(ScriptVar && other) noexcept
{
    // Shallow copy:
    type = other.type;
    std::memcpy(&data, &other.data, sizeof(Data));

    // Reset other without deallocating memory.
    other.type = Type::Undefined;
    ZeroObjectUnchecked(other.data);
}

ScriptVar & ScriptVar::operator=(ScriptVar && other) noexcept
{
    Reset();

    // Shallow copy:
    type = other.type;
    std::memcpy(&data, &other.data, sizeof(Data));

    // Reset other without deallocating memory.
    other.type = Type::Undefined;
    ZeroObjectUnchecked(other.data);

    return *this;
}

ScriptVar::~ScriptVar()
{
    Reset();
}

void ScriptVar::InitFromCopy(const ScriptVar & other)
{
    Reset();

    // Deep copy the string:
    if (other.type == Type::CString)
    {
        data.asCString = DupCString(other.data.asCString);
    }
    else // Simple data type, binary copy:
    {
        std::memcpy(&data, &other.data, sizeof(Data));
    }

    type = other.type;
}

void ScriptVar::Reset()
{
    // C-strings are allocated dynamically.
    if (type == Type::CString)
    {
        delete[] data.asCString;
    }

    type = Type::Undefined;
    ZeroObjectUnchecked(data);
}

std::string ScriptVar::GetTypeString() const
{
    switch (type)
    {
    case ScriptVar::Type::Undefined:
        return "???";
    case ScriptVar::Type::Boolean:
        return "bool";
    case ScriptVar::Type::Integer:
        return "int";
    case ScriptVar::Type::CString:
        return "str";
    case ScriptVar::Type::Float:
        return "float";
    case ScriptVar::Type::Vector3:
        return "vec3";
    case ScriptVar::Type::Vector4:
        return "vec4";
    default:
        common->FatalErrorF("Invalid ScriptVar::Type!");
    } // switch (type)
}

// ======================================================
// ToString() for ScriptVar:
// ======================================================

std::string ToString(const ScriptVar & var)
{
    switch (var.GetType())
    {
    case ScriptVar::Type::Undefined:
        return Format("%p", var.AsVoidPtr());
    case ScriptVar::Type::Boolean:
        return Format("%s", (var.AsBoolean() ? "true" : "false"));
    case ScriptVar::Type::Integer:
        return Format("%d", var.AsInteger());
    case ScriptVar::Type::CString:
        return Format("\"%s\"", ((var.AsCString() != nullptr) ? var.AsCString() : "<NULL>"));
    case ScriptVar::Type::Float:
        return ToString(var.AsFloat());
    case ScriptVar::Type::Vector3:
        return ToString(var.AsVector3());
    case ScriptVar::Type::Vector4:
        return ToString(var.AsVector4());
    default:
        common->FatalErrorF("Invalid ScriptVar::Type!");
    } // switch (var.GetType())
}

// ======================================================
// ScriptManager singleton:
// ======================================================

ScriptManager::ScriptManager()
    : luaContext(nullptr)
    , scritLogSilent(false)
    , scriptDebugEnabled(true)
{
}

void ScriptManager::Init()
{
    common->PrintF("------ ScriptManager::Init() -------");

    // Init our custom extensions, etc:
    InitLuaCommon();

    // Create the global Lua state/context:
    if ((luaContext = luaL_newstate()) == nullptr)
    {
        common->FatalErrorF("Unable to create global Lua context!");
    }

    // Open default libraries:
    common->PrintF("Opening Lua libraries...");
    luaL_openlibs(reinterpret_cast<lua_State *>(luaContext));
}

void ScriptManager::Shutdown()
{
    common->PrintF("----- ScriptManager::Shutdown() ----");

    configVars.clear();

    if (luaContext != nullptr)
    {
        lua_close(reinterpret_cast<lua_State *>(luaContext));
        luaContext = nullptr;
    }
}

bool ScriptManager::IsScriptLogSilent() const
{
    return scritLogSilent;
}

bool ScriptManager::IsScriptDebugEnabled() const
{
    return scriptDebugEnabled;
}

void ScriptManager::SetScriptLogSilent(const bool silent)
{
    scritLogSilent = silent;
}

void ScriptManager::SetScriptDebugEnabled(const bool enable)
{
    scriptDebugEnabled = enable;
}

bool ScriptManager::RunScriptFile(const std::string & filename)
{
    assert(luaContext != nullptr);
    assert(!filename.empty());

    return RunLuaScriptFile(reinterpret_cast<lua_State *>(luaContext), filename.c_str());
}

bool ScriptManager::RunScriptString(const char * sourceCode, const size_t * sourceLength)
{
    assert(luaContext != nullptr);
    assert(sourceCode != nullptr);
    // TODO
    UNUSED_VAR(sourceCode);
    UNUSED_VAR(sourceLength);
    common->ErrorF("RunScriptString() not implemented!");
    return false;
}

bool ScriptManager::RunConfigFile(const std::string & filename, const std::string & configTableName)
{
    return RunConfigFile(filename, { configTableName });
}

bool ScriptManager::RunConfigFile(const std::string & filename, const std::initializer_list<std::string> & configTables)
{
    assert(luaContext != nullptr);
    assert(!filename.empty());
    assert(configTables.size() != 0);

    lua_State * luaCtx = reinterpret_cast<lua_State *>(luaContext);

    if (!RunLuaScriptFile(luaCtx, filename.c_str()))
    {
        common->WarningF("Config file \"%s\" failed to execute...", filename.c_str());
        return false;
    }

    int varsFound = 0;
    int tablesProcessed = 0;

    for (const auto & tableName : configTables)
    {
        assert(!tableName.empty());

        // Reset the stack index:
        lua_settop(luaCtx, 0);

        // Put the table on the stack:
        lua_getglobal(luaCtx, tableName.c_str());

        // Make sure it is a table:
        if (!lua_istable(luaCtx, -1))
        {
            common->ErrorF("'%s' is not a valid Lua table!", tableName.c_str());
            continue; // Try another
        }

        ScriptVar::Type luaVarType = ScriptVar::Type::Undefined;
        ScriptVar::Data luaVarData;
        std::string luaVarName;

        lua_pushnil(luaCtx);
        while (lua_next(luaCtx, -2))
        {
            if (lua_isnumber(luaCtx, -1)) // int / unsigned int / float:
            {
                luaVarName = lua_tostring(luaCtx, -2);
                double n = static_cast<double>(lua_tonumber(luaCtx, -1));

                // Figure out if int or float:
                if (std::floor(n) != n)
                {
                    luaVarType = ScriptVar::Type::Float;
                    luaVarData.asFloat = static_cast<float>(n);
                }
                else
                {
                    luaVarType = ScriptVar::Type::Integer;
                    luaVarData.asInteger = static_cast<int>(n);
                }
            }
            else if (lua_isstring(luaCtx, -1)) // string (char *)
            {
                luaVarName = lua_tostring(luaCtx, -2);
                luaVarType = ScriptVar::Type::CString;
                luaVarData.asCString = DupCString(lua_tostring(luaCtx, -1));
            }
            else if (lua_isboolean(luaCtx, -1)) // bool
            {
                luaVarName = lua_tostring(luaCtx, -2);
                luaVarType = ScriptVar::Type::Boolean;
                luaVarData.asBoolean = static_cast<bool>(lua_toboolean(luaCtx, -1));
            }
            else if (lua_istable(luaCtx, -1)) // Tables can be a Vector3 or Vector4:
            {
                int numbersFound = 0;
                float v[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

                luaVarName = lua_tostring(luaCtx, -2);

                // Iterate table to see if it is a vector:
                lua_pushnil(luaCtx);
                while (lua_next(luaCtx, -2) && (numbersFound < 4))
                {
                    // index -1 is the value (a number)
                    // index -2 is the key (also a number)
                    if (!lua_isnumber(luaCtx, -1) || !lua_isnumber(luaCtx, -2))
                    {
                        break;
                    }
                    v[numbersFound++] = lua_tonumber(luaCtx, -1);
                    lua_pop(luaCtx, 1);
                }

                // We assume a vector if at least one number was read.
                if (numbersFound > 0)
                {
                    if (numbersFound <= 3)
                    {
                        luaVarType = ScriptVar::Type::Vector3;
                        luaVarData.asVector3 = Vector3(v[0], v[1], v[2]);
                    }
                    else if (numbersFound == 4)
                    {
                        luaVarType = ScriptVar::Type::Vector4;
                        luaVarData.asVector4 = Vector4(v[0], v[1], v[2], v[3]);
                    }
                }
            }
            else
            {
                // Some other ignored entity, such as a function.
            }

            lua_pop(luaCtx, 1);

            // Found a usable config var?
            if (luaVarType != ScriptVar::Type::Undefined)
            {
                // Names are fully decorated: "table_name.variable_name"
                configVars[tableName + "." + luaVarName] = ScriptVar(luaVarType, std::move(luaVarData));

                // Reset:
                luaVarName.clear();
                luaVarType = ScriptVar::Type::Undefined;

                ++varsFound;
            }
        }

        lua_pop(luaCtx, 1);
        ++tablesProcessed;
    }

    common->PrintF("Config file \"%s\" executed successfully. "
                   "%d tables read with %d total vars.",
                   filename.c_str(), tablesProcessed, varsFound);

    return true;
}

const ScriptVar * ScriptManager::FindConfigVar(const std::string & varName) const
{
    assert(!varName.empty());

    const size_t found = configVars.count(varName);
    if (found == 0)
    {
        return nullptr;
    }
    if (found > 1)
    {
        common->WarningF("More than one config var named \"%s\" found! Returning first occurrence...", varName.c_str());
    }

    const ScriptVar & var = configVars.at(varName);
    return &var;
}

bool ScriptManager::GetConfigValue(const std::string & varName, bool & dest, const bool optionalVar) const
{
    const ScriptVar * var = FindConfigVar(varName);
    if (var == nullptr)
    {
        if (!optionalVar)
        {
            common->WarningF("No such config var \"%s\" found...", varName.c_str());
        }
        return false;
    }

    if (var->type == ScriptVar::Type::Boolean)
    {
        dest = var->data.asBoolean;
        return true;
    }

    if (var->type == ScriptVar::Type::Integer) // Allow int->bool conversion
    {
        dest = static_cast<bool>(var->data.asInteger);
        return true;
    }

    common->ErrorF("Config var \"%s\" is not convertible to a boolean value!", varName.c_str());
    return false;
}

bool ScriptManager::GetConfigValue(const std::string & varName, int & dest, const bool optionalVar) const
{
    const ScriptVar * var = FindConfigVar(varName);
    if (var == nullptr)
    {
        if (!optionalVar)
        {
            common->WarningF("No such config var \"%s\" found...", varName.c_str());
        }
        return false;
    }

    if (var->type != ScriptVar::Type::Integer)
    {
        common->ErrorF("Config var \"%s\" is not convertible to an integer value!", varName.c_str());
        return false;
    }

    dest = var->data.asInteger;
    return true;
}

bool ScriptManager::GetConfigValue(const std::string & varName, unsigned int & dest, const bool optionalVar) const
{
    const ScriptVar * var = FindConfigVar(varName);
    if (var == nullptr)
    {
        if (!optionalVar)
        {
            common->WarningF("No such config var \"%s\" found...", varName.c_str());
        }
        return false;
    }

    if (var->type != ScriptVar::Type::Integer)
    {
        common->ErrorF("Config var \"%s\" is not convertible to an integer value!", varName.c_str());
        return false;
    }

    dest = static_cast<unsigned int>(var->data.asInteger);
    return true;
}

bool ScriptManager::GetConfigValue(const std::string & varName, float & dest, const bool optionalVar) const
{
    const ScriptVar * var = FindConfigVar(varName);
    if (var == nullptr)
    {
        if (!optionalVar)
        {
            common->WarningF("No such config var \"%s\" found...", varName.c_str());
        }
        return false;
    }

    if (var->type == ScriptVar::Type::Float)
    {
        dest = var->data.asFloat;
        return true;
    }

    if (var->type == ScriptVar::Type::Integer) // Allow int->float conversion
    {
        dest = static_cast<float>(var->data.asInteger);
        return true;
    }

    common->ErrorF("Config var \"%s\" is not convertible to a float value!", varName.c_str());
    return false;
}

bool ScriptManager::GetConfigValue(const std::string & varName, std::string & dest, const bool optionalVar) const
{
    const ScriptVar * var = FindConfigVar(varName);
    if (var == nullptr)
    {
        if (!optionalVar)
        {
            common->WarningF("No such config var \"%s\" found...", varName.c_str());
        }
        return false;
    }

    if (var->type != ScriptVar::Type::CString)
    {
        common->ErrorF("Config var \"%s\" is not convertible to a string value!", varName.c_str());
        return false;
    }

    assert(var->data.asCString != nullptr);
    dest = var->data.asCString;
    return true;
}

bool ScriptManager::GetConfigValue(const std::string & varName, Vector3 & dest, const bool optionalVar) const
{
    const ScriptVar * var = FindConfigVar(varName);
    if (var == nullptr)
    {
        if (!optionalVar)
        {
            common->WarningF("No such config var \"%s\" found...", varName.c_str());
        }
        return false;
    }

    if (var->type != ScriptVar::Type::Vector3)
    {
        common->ErrorF("Config var \"%s\" is not convertible to a Vector3!", varName.c_str());
        return false;
    }

    dest = var->data.asVector3;
    return true;
}

bool ScriptManager::GetConfigValue(const std::string & varName, Vector4 & dest, const bool optionalVar) const
{
    const ScriptVar * var = FindConfigVar(varName);
    if (var == nullptr)
    {
        if (!optionalVar)
        {
            common->WarningF("No such config var \"%s\" found...", varName.c_str());
        }
        return false;
    }

    if (var->type != ScriptVar::Type::Vector4)
    {
        common->ErrorF("Config var \"%s\" is not convertible to a Vector4!", varName.c_str());
        return false;
    }

    dest = var->data.asVector4;
    return true;
}

void ScriptManager::PrintConfigVars() const
{
    unsigned int namePadding = 8; // min for "var name" = 8

    // Get padding needed to print name column:
    for (const auto & it : configVars)
    {
        namePadding = std::max<unsigned int>(it.first.length(), namePadding);
    }

    // Print table header:
    common->PrintF("----------------- Config var listing -----------------");
    common->PrintF("%-*s | type  | value", namePadding, "var name");

    // Now print the vars in table format:
    for (const auto & it : configVars)
    {
        common->PrintF("%-*s | %-5s | %s", namePadding, it.first.c_str(),
                       it.second.GetTypeString().c_str(), ToString(it.second).c_str());
    }

    common->PrintF("Listed %zu variables.", configVars.size());
    common->PrintF("------------------------------------------------------");
}

// ======================================================
// Global ScriptManager instance:
// ======================================================

static ScriptManager theScriptManager;
ScriptManager * scriptMgr = &theScriptManager;

} // namespace Engine {}
