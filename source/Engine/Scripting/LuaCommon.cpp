
// ================================================================================================
// -*- C++ -*-
// File: LuaCommon.cpp
// Author: Guilherme R. Lampert
// Created on: 07/08/14
// Brief: Common helpers used by ScriptManager related to the Lua language/API.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Utilities.hpp"
#include "Engine/Core/Common.hpp"
#include "Engine/Core/System.hpp"
#include "Engine/Scripting/ScriptManager.hpp"

// Lua API:
#include "ThirdParty/lualib/include/lua.hpp"

namespace Engine
{

// ======================================================
// Handy #defines:
// ======================================================

// Declare a native Lua function:
#define SCRIPT_FUNC_NAME(nameSpace, funcName) Native_##nameSpace##_##funcName
#define SCRIPT_FUNC_DEF(nameSpace, funcName) int SCRIPT_FUNC_NAME(nameSpace, funcName)(lua_State * luaContext)
#define LUA_FUNC(nameSpace, funcName) (lua_CFunction) SCRIPT_FUNC_NAME(nameSpace, funcName)

// ======================================================
// Lua "Debug" library extensions:
// ======================================================

struct LuaLogLevel
{
    static constexpr int Silent  = 0;
    static constexpr int Error   = 1;
    static constexpr int Warning = 2;
    static constexpr int Comment = 3;
};

// Standard assert() macro could cause problems
// with Native_Debug_assert() below. Disable it here just in case...
#undef assert

// Actually print to Lua log/console:
static void LuaDebugLogDoPrint(const char * str, const int logLevel)
{
    static const char * lvlStr[] = { "Error", "Warning", "Comment" };
    common->PrintF("[Lua]: Debug.log%s( '%s' )", lvlStr[logLevel - 1], str);
}

// Resolve Lua stack bureaucracy:
static int LuaDebugLogHelper(lua_State * luaContext, const int logLevel)
{
    if (scriptMgr->IsScriptLogSilent())
    {
        return 0;
    }

    // Number of arguments:
    const int numArgs = lua_gettop(luaContext);

    // Object or whatever we are printing
    // must be convertible to string:
    lua_getglobal(luaContext, "toString");

    for (int i = 1; i <= numArgs; ++i)
    {
        // Function to be called:
        lua_pushvalue(luaContext, -1);

        // Value to print:
        lua_pushvalue(luaContext, i);

        // Perform call:
        lua_call(luaContext, 1, 1);

        // Get result:
        const char * s = lua_tostring(luaContext, -1);
        if (!s)
        {
            return luaL_error(luaContext, LUA_QL("toString()") " must return a string to 'Debug.log*()' / 'print()'");
        }

        // Actually print:
        LuaDebugLogDoPrint(s, logLevel);

        // Pop result:
        lua_pop(luaContext, 1);
    }

    return 0;
}

extern "C"
{

SCRIPT_FUNC_DEF(Debug, isEnabled)
{
    lua_pushboolean(luaContext, scriptMgr->IsScriptDebugEnabled());
    return 1;
}

SCRIPT_FUNC_DEF(Debug, isSilent)
{
    lua_pushboolean(luaContext, scriptMgr->IsScriptLogSilent());
    return 1;
}

SCRIPT_FUNC_DEF(Debug, logComment)
{
    return LuaDebugLogHelper(luaContext, LuaLogLevel::Comment);
}

SCRIPT_FUNC_DEF(Debug, logWarning)
{
    return LuaDebugLogHelper(luaContext, LuaLogLevel::Warning);
}

SCRIPT_FUNC_DEF(Debug, logError)
{
    return LuaDebugLogHelper(luaContext, LuaLogLevel::Error);
}

SCRIPT_FUNC_DEF(Debug, assert)
{
    if (!lua_toboolean(luaContext, 1))
    {
        const char * msg = luaL_optstring(luaContext, 2, "");

        if (msg && (*msg != '\0'))
        {
            return luaL_error(luaContext, "assertion failed! \"%s\"", msg);
        }
        else
        {
            return luaL_error(luaContext, "assertion failed!");
        }
    }
    return lua_gettop(luaContext);
}

} // extern "C"

// ======================================================
// debugLibExtensions[] function table:
// ======================================================

//
// "Debug" library extension functions:
// + Debug.isEnabled()  -> Test if script debug is enabled. Returns true or false.
// + Debug.isSilent()   -> Test if script text output is enabled. Returns true or false.
// + Debug.logComment() -> Log a comment if script debug is enabled. Output is usually printed in the Engine log.
// + Debug.logWarning() -> Log a warning if script debug is enabled. Output is usually printed in the Engine log.
// + Debug.logError()   -> Log an error and continue executing the script normally. Output is usually printed in the Engine log.
// + Debug.assert()     -> Same as assert(). Log error if assertion fails and stop script execution.
//
static const luaL_Reg debugLibExtensions[] =
{
    { "isEnabled",  LUA_FUNC(Debug, isEnabled)  },
    { "isSilent",   LUA_FUNC(Debug, isSilent)   },
    { "logComment", LUA_FUNC(Debug, logComment) },
    { "logWarning", LUA_FUNC(Debug, logWarning) },
    { "logError",   LUA_FUNC(Debug, logError)   },
    { "assert",     LUA_FUNC(Debug, assert)     },
    { nullptr,      nullptr                     } // End of list
};

// ======================================================
// Lua "System" library extensions:
// ======================================================

extern "C"
{

SCRIPT_FUNC_DEF(System, clockMillisec)
{
    const int64_t ms = system->ClockMillisec();
    lua_pushunsigned(luaContext, static_cast<lua_Unsigned>(ms));
    return 1;
}

} // extern "C"

// ======================================================
// sysLibExtensions[] function table:
// ======================================================

//
// "System" library extension functions:
// + System.print()         -> Prints a string to the default script output. Same as Debug.logComment().
// + System.clockMillisec() -> Get the number of milliseconds elapsed since the application started.
//
static const luaL_Reg sysLibExtensions[] =
{
    { "print",         LUA_FUNC(Debug, logComment)     }, // 'print' is an alias for 'Debug.logComment'
    { "clockMillisec", LUA_FUNC(System, clockMillisec) }, // Engine absolute time since startup
    { nullptr,         nullptr                         }  // End of list
};

// ======================================================
// InitLuaCommon():
// ======================================================

void InitLuaCommon()
{
    // After the Lua library extensions are registered, they will be visible to all Lua contexts.
    lua_debug_set_extensions(debugLibExtensions, ArrayLength(debugLibExtensions) - 1);
    lua_system_set_extensions(sysLibExtensions, ArrayLength(sysLibExtensions) - 1);

    // Done.
    common->PrintF("Lua common initialized...");
}

// ======================================================
// RunLuaScriptFile():
// ======================================================

bool RunLuaScriptFile(lua_State * luaContext, const char * scriptName)
{
    if (luaL_dofile(luaContext, scriptName) != 0)
    {
        common->ErrorF("Error(s) in Lua script \"%s\": '%s'", scriptName, lua_tostring(luaContext, -1));
        return false;
    }
    return true;
}

} // namespace Engine {}
