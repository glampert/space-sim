
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: SpaceCraft.hpp
// Author: Guilherme R. Lampert
// Created on: 15/10/14
// Brief: Space Craft/Ship specialization of GameEntity.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Game/Weapon.hpp"

namespace SpaceSim
{

class TargetIndicator;
class StatsLabel;

// ======================================================
// SpaceCraft:
// ======================================================

class SpaceCraft
    : public GameEntity
{
    RUNTIME_CLASS_DECL(SpaceCraft)

  public:

    SpaceCraft();
    SpaceCraft(const SpaceCraft & other);
    virtual ~SpaceCraft() = default;

    // No direct copy. Use Clone().
    SpaceCraft & operator=(const SpaceCraft &) = delete;

    virtual void OnUpdate(const Engine::GameTime & time) override;
    virtual void OnRender(const Engine::GameTime & time) override;

    void SetStatsLabel(std::unique_ptr<StatsLabel> label);
    void SetTargetIndicator(std::unique_ptr<TargetIndicator> indicator);

    void SetShieldRestoreRate(double amount) { shieldRestoreRate = amount; }
    double GetShieldRestoreRate() const { return shieldRestoreRate; }

    void SetUniformScale(const float s) { uniformScale = s; }
    float GetUniformScale() const { return uniformScale; }

    LocalPlayerView * GetView() const { return myView; }
    void SetView(LocalPlayerView * view) { myView = view; }

    void AddEngineParticles(const Engine::Vector3 & pos, const Engine::Color4f & color);
    void SetupRigidBody(float mass, const Engine::Vector3 & initialPosition);

  protected:

    void SetupEngineEmitter(Engine::ParticleRenderer & prt,
                            const Engine::ConstTexturePtr & spriteTex,
                            const Engine::Color4f & baseColor) const;

    // Particle emitters of the ship's engines/turbines or other effects.
    // The emitter's origin is set to the offset position.
    // Position offset is relative to the entity's world position.
    using ParticleEmitterList = Engine::IntrusiveSList<Engine::ParticleRenderer>;
    ParticleEmitterList particleEmitters;

    // List of weapons attached, if any.
    using WeaponList = Engine::IntrusiveSList<Weapon>;
    WeaponList weapons;

    // Shield restore rate in points per second.
    double shieldRestoreRate;
    double shieldRestoreAmt;

    // Optional 3D billboards for target indicator and ship stats/name.
    std::unique_ptr<StatsLabel> statsLabel;
    std::unique_ptr<TargetIndicator> targetIndicator;

    // Scaling of this ship's model.
    float uniformScale;

    // Local view that owns this player. Must not be null!
    LocalPlayerView * myView;

    // An opaque pointer to a btDefaultMotionState or a custom MotionState.
    void * motionState;
};

// ======================================================
// TargetIndicator:
// ======================================================

class TargetIndicator
{
  public:

    Engine::ConstTexturePtr sprite; // Background/frame sprite.
    Engine::Vec2f dimensions;       // Width/height of the billboard.
    Engine::Color4f baseColor;      // Base color of the billboard.
    Engine::Vector3 positionOffset; // Position offset relative to owner.

    TargetIndicator();
    void Render();
};

// ======================================================
// StatsLabel:
// ======================================================

class StatsLabel
{
  public:

    GameEntity * owner;                // Weak reference to owning entity.
    Engine::ConstTexturePtr barSprite; // Health/shield bar sprite.
    Engine::Vec2f healthBarOffset;     // Health bar offset from entity position.
    Engine::Vec2f shieldBarOffset;     // Shield bar offset from entity position.
    Engine::Vec2f textOffset;          // Text offset from entity position.

    StatsLabel();
    void Render();
};

} // namespace SpaceSim {}
