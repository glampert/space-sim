
// ================================================================================================
// -*- C++ -*-
// File: LocalPlayerView.cpp
// Author: Guilherme R. Lampert
// Created on: 14/10/14
// Brief: Game View for the local player.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/LocalPlayerView.hpp"
using namespace Engine;

namespace SpaceSim
{

// ======================================================
// LocalPlayerView:
// ======================================================

LocalPlayerView::LocalPlayerView(const GameViewId vId)
    : playerEntityHandle(InvalidGameEntityHandle)
    , textRenderer(*checked_pointer_cast<const Font>(resourceMgr->FindOrLoadResource("coalition.fnt")), spriteBatch, SpriteLayer::Layer0)
    , debugRenderer(&textRenderer)
    , camera(this)
{
    common->PrintF("New LocalPlayerView instance created...");
    SetViewId(vId);

    // Camera setup:
    camera.RunConfigScripts();

    // Necessary for good functioning of the 3D camera:
    mouseInput.SetCursorPosition({ (renderMgr->GetFramebufferDimensions().width / 2.0f),
                                   (renderMgr->GetFramebufferDimensions().height / 2.0f) });

    // Create the player entity:
    playerEntityHandle = game->world->CreateEntity(PlayerEntity::Type);

    // Set the PlayerView pointer:
    PlayerEntity * playerEntity = checked_cast<PlayerEntity *>(game->world->GetEntityPointer(playerEntityHandle));
    playerEntity->SetView(this);

    if (GetViewId() == 1)
    {
        camera.SetWorldPosition(Vector3(0, -5, 100));
        camera.SetLookAt(Vector3(0, 0, -1));
        playerEntity->InitPlayer1();
    }
    else
    {
        camera.SetWorldPosition(Vector3(0, -5, -100));
        camera.SetLookAt(Vector3(0, 0, +1));
        playerEntity->InitPlayer2();
    }
    camera.SetFOVRadians(DegToRad(60.f));

    // HUD sprites:
    barFrameSprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/bar_frame.png"));
    barSprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/bar.png"));
}

void LocalPlayerView::OnUpdate(const GameTime time)
{
    if (game->splitScreenGame)
    {
        camera.SetViewport(renderMgr->GetFramebufferRect());
    }

    // Player 1 always has the controller, while 2 uses the keyboard/mouse.
    if (GetViewId() == 1)
    {
        xbController.PollInput();
        camera.Look(xbController, time.deltaTime.seconds);
        camera.Move(xbController, time.deltaTime.seconds);
    }
    else
    {
        camera.Look(mouseInput, time.deltaTime.seconds);
        camera.Move(keyboardInput, time.deltaTime.seconds);
    }

    // Update the local camera:
    camera.Update(time.deltaTime.seconds);
}

void LocalPlayerView::OnRender(const GameTime time)
{
    // Set the camera for this view:
    renderMgr->SetCameraRef(camera);
    renderMgr->SetViewProjectionMatrix(camera.GetViewMatrix(), camera.GetProjectionMatrix());

    // Render all the entities and skybox background for this view:
    game->world->OnRender(time);

    // Render debug physics, such as AABBs, and contacts:
    if (game->debugRenderPhysics)
    {
        game->physics->OnRender(time);
    }

    // More debug lines/text:
    if (debugRenderer.HasDebugDataToDraw())
    {
        debugRenderer.FlushDebugRenderData(time, camera.GetViewMatrix(),
                                           camera.GetProjectionMatrix(),
                                           nullptr);
    }

    if (game->drawHUD)
    {
        DrawHUD();
    }

    if (game->drawFPSCounter)
    {
        DrawFPSCounter();
    }

    // 2D text, billboards and fullscreen UI elements:
    if (!spriteBatch.IsEmpty() || !billboardBatch.IsEmpty())
    {
        renderMgr->EnableSpriteBlendMode();
        renderMgr->DisableDepthWrite();

        billboardBatch.FlushBatch(renderMgr->GetViewProjectionMatrix(), camera.GetWorldPosition());

        renderMgr->DisablePolygonCull();
        renderMgr->DisableDepthTest();

        spriteBatch.FlushBatch(Basic2DTextRenderer::GetFullscreenOrthoMatrix());

        renderMgr->EnableDepthTest();
        renderMgr->EnablePolygonCull();
        renderMgr->EnableDepthWrite();
        renderMgr->DisableColorBlend();
    }
}

void LocalPlayerView::DrawHUD()
{
    const PlayerEntity * player = static_cast<PlayerEntity *>(game->world->GetEntityPointer(playerEntityHandle));
    const Weapon & weap = player->GetMainWeapon();

    // Aim reticle:
    spriteBatch.DrawQuad(
    weap.aimReticlePos.x, weap.aimReticlePos.y,
    weap.aimReticleSize.width, weap.aimReticleSize.height,
    weap.aimReticleColor, weap.aimReticleSprite);

    // Health bars and such:
    // At the bottom-left corner of the window:
    // - health
    // - shield
    // - afterburner/booster
    //
    constexpr float spacing = 80.0f;
    constexpr float maxWidth = 380.0f;
    constexpr float maxHeight = 34.0f;
    constexpr float frameOffsetX = 22.0f;
    constexpr float frameOffsetY = 62.0f;

    float x, y, w;
    const int playerHealth = player->GetHealth();
    const int playerShield = player->GetShield();
    const int playerBoostr = player->GetBooster();

    // Afterburner/booster (yellow):
    x = 50.0f;
    y = 50.0f;
    w = MapRange<float>(playerBoostr, 0.0f, player->GetMaxBooster(), 0.0f, maxWidth);
    spriteBatch.DrawQuad(x, y, w, maxHeight, Color4f(1.0f, 1.0f, 0.0f, 1.0f), barSprite, false, nullptr, SpriteLayer::Layer0);
    spriteBatch.DrawQuad(x - (frameOffsetX / 2), y - (frameOffsetY / 2), maxWidth + frameOffsetX, maxHeight + frameOffsetY,
                         Color4f(1.0f), barFrameSprite, false, nullptr, SpriteLayer::Layer1);

    // Shield (blue):
    y += maxHeight + spacing;
    w = MapRange<float>(playerShield, 0.0f, player->GetMaxShield(), 0.0f, maxWidth);
    spriteBatch.DrawQuad(x, y, w, maxHeight, Color4f(0.1f, 0.4f, 1.0f, 1.0f), barSprite, false, nullptr, SpriteLayer::Layer0);
    spriteBatch.DrawQuad(x - (frameOffsetX / 2), y - (frameOffsetY / 2), maxWidth + frameOffsetX, maxHeight + frameOffsetY,
                         Color4f(1.0f), barFrameSprite, false, nullptr, SpriteLayer::Layer1);

    // Health (red):
    y += maxHeight + spacing;
    w = MapRange<float>(playerHealth, 0.0f, player->GetMaxHealth(), 0.0f, maxWidth);
    spriteBatch.DrawQuad(x, y, w, maxHeight, Color4f(1.0f, 0.0f, 0.0f, 1.0f), barSprite, false, nullptr, SpriteLayer::Layer0);
    spriteBatch.DrawQuad(x - (frameOffsetX / 2), y - (frameOffsetY / 2), maxWidth + frameOffsetX, maxHeight + frameOffsetY,
                         Color4f(1.0f), barFrameSprite, false, nullptr, SpriteLayer::Layer1);

    // Movement speed counter at the base of the screen:
    char str[128];
    SNPrintF(str, "Speed: %.1fms", camera.GetForwardSpeed() * 10.0f);
    const Vec2u fb = renderMgr->GetFramebufferDimensions();
    textRenderer.DrawText(Vec2f(fb.width / 2.0f, 50.0f), Color4f(1.0f), TextAlign::Center, str);
}

void LocalPlayerView::DrawFPSCounter()
{
    const int64_t timeMillisec = Engine::system->ClockMillisec(); // Real time clock
    const int64_t frameTime = (timeMillisec - fps.previousTime);

    fps.previousTimes[fps.index++] = frameTime;
    fps.previousTime = timeMillisec;

    if (fps.index == FPSCounter::MaxFrames)
    {
        int64_t total = 0;
        for (int i = 0; i < FPSCounter::MaxFrames; ++i)
        {
            total += fps.previousTimes[i];
        }

        if (total == 0)
        {
            total = 1;
        }

        fps.fpsCount = (10000 * FPSCounter::MaxFrames / total);
        fps.fpsCount = (fps.fpsCount + 5) / 10;
        fps.index = 0;
    }

    char str[128];
    SNPrintF(str, "FPS:%u", static_cast<unsigned int>(fps.fpsCount));

    // Draw in the top-right corner of the screen:
    const Vec2u fb = renderMgr->GetFramebufferDimensions();
    textRenderer.DrawText(Vec2f(fb.width - 200.0f, fb.height - 50.0f), Color4f(1.0f), TextAlign::Left, str);
}

bool LocalPlayerView::OnAppEvent(const AppEvent event)
{
    if (GetViewId() == 1)
    {
        return false; // Using the controller
    }

    // Player input:
    if (mouseInput.OnAppEvent(event))
    {
        return true;
    }

    if (keyboardInput.OnAppEvent(event))
    {
        if (keyboardInput.IsKeyDown(Key::Escape)) // End current game.
        {
            game->EndGame();
        }
        else if (keyboardInput.IsKeyDown(Key::Num9)) // NOTE: These keys are used for development only!
        {
            camera.RunConfigScripts(); // Resets the camera
        }
        else if (keyboardInput.IsKeyDown(Key::Num0))
        {
            game->recordingScreen = !game->recordingScreen;
            renderMgr->EnableAviScreenCapture(game->recordingScreen); // Toggle screen capture
        }

        return true;
    }

    return false; // Event not handled here.
}

PlayerEntity & LocalPlayerView::GetPlayerEntity()
{
    PlayerEntity * player = static_cast<PlayerEntity *>(game->world->GetEntityPointer(playerEntityHandle));
    assert(player != nullptr);
    return *player;
}

} // namespace SpaceSim {}
