
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: FlightCamera.hpp
// Author: Guilherme R. Lampert
// Created on: 31/08/14
// Brief: In-game flight camera.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace SpaceSim
{

// ======================================================
// FlightCamera:
// ======================================================

//
//  (up/yaw)
//    +Y   +Z (forward/roll)
//    |   /
//    |  /
//    | /
//    + ------ +X (right/pitch)
//
// [W], [S] => Move back/forward
// [A], [D] => Move left/right (somewhat like strafing)
// [Q], [E] => Roll on ship's Z-axis
//
// Free mouse look with uncapped rotation on X and Y.
//
class FlightCamera final
    : public Engine::Camera
{
  public:

    // Default constructor. Init stuff to save values.
    explicit FlightCamera(class LocalPlayerView * playerView);

    // Updates the camera matrices. This is meant to be run on every frame.
    // The position and location of the camera are also updated.
    void Update(const double deltaSeconds);

    // Test the input keys and moves the camera accordingly.
    void Move(Engine::KeyboardInputHandler & keyboardInput, const double deltaSeconds);
    void Move(Engine::XB360Controller & xbController, const double deltaSeconds);

    // Check for mouse movement to move/rotate the view.
    void Look(Engine::MouseInputHandler & mouseInput, const double deltaSeconds);
    void Look(Engine::XB360Controller & xbController, const double deltaSeconds);

    // Change the pitch (up, down) for the free-flight camera.
    void ChangePitch(float radians);

    // Change yaw (left, right) for the free-flight camera.
    void ChangeYaw(float radians);

    // Runs the camera configuration script(s) updating the camera parameters.
    // Note: This will also reset the camera's current state.
    void RunConfigScripts();

    // Prints the current camera state to the developer log.
    void PrintSelf() const;

    // Set projection parameters:
    void SetViewport(const Engine::Vec4u vp);
    void SetWorldPosition(const Engine::Vector3 pos);
    void SetClipping(const float nearClipDistance, const float farClipDistance);
    void SetLookAt(const Engine::Vector3 pos) { lookAtPos = pos; }
    void SetFOVRadians(const float radians) { fovyRadians = radians; }

    // Set speeds/bounds/damping:
    void SetBoosterSpeedScale(const float speed) { boosterSpeedScale = speed; }
    void SetForwardSpeed(const float speed) { forwardSpeed = speed; }
    void SetMaxForwardSpeed(const float speed) { maxForwardSpeed = speed; }
    void SetMovementSpeed(const float speed) { movementSpeed = speed; }
    void SetRollSpeed(const float speed) { rollSpeed = speed; }
    void SetTurnSpeed(const float speed) { turnSpeed = speed; }
    void SetMaxPitchRate(const float max) { maxPitchRate = max; }
    void SetMaxYawRate(const float max) { maxYawRate = max; }
    void SetMovementDamping(const float amount) { movementDamping = amount; }
    void SetLookDamping(const float amount) { lookDamping = amount; }
    void SetRollResetRate(const float amount) { rollResetRate = amount; }

    // Get speeds/bounds:
    float GetBoosterSpeedScale() const { return boosterSpeedScale; }
    float GetForwardSpeed() const { return forwardSpeed; }
    float GetMaxForwardSpeed() const { return maxForwardSpeed; }
    float GetMovementSpeed() const { return movementSpeed; }
    float GetRollSpeed() const { return rollSpeed; }
    float GetTurnSpeed() const { return turnSpeed; }
    float GetMaxPitchRate() const { return maxPitchRate; }
    float GetMaxYawRate() const { return maxYawRate; }
    float GetMovementDamping() const { return movementDamping; }
    float GetLookDamping() const { return lookDamping; }
    float GetRollResetRate() const { return rollResetRate; }

    // World position and lookAt:
    Engine::Vector3 GetWorldPosition() const { return worldPosition; }
    Engine::Vector3 GetLookAt() const { return lookAtPos; }
    Engine::Vector3 GetUpVector() const { return upVector; }
    Engine::Vector3 GetDirectionVector() const { return directionVector; }

    // FoV, viewport and clipping palnes:
    float GetAspect() const { return aspect; }
    float GetFOVRadians() const { return fovyRadians; }
    Engine::Vec4u GetViewport() const { return viewport; }
    Engine::Vec2f GetClipping() const { return Engine::Vec2f(nearClip, farClip); }

    // Get the camera matrices and frustum:
    Engine::Matrix4 GetViewMatrix() const { return viewMatrix; }
    Engine::Matrix4 GetProjectionMatrix() const { return projectionMatrix; }
    const Engine::Frustum & GetFrustum() const { return frustum; }

    // Get a quaternion that represents the current camera orientation.
    Engine::Quat GetOrientation() const;

  private:

    // Local view that owns this camera. Must not be null!
    LocalPlayerView * myView;

    // Projection/viewport parameters:
    Engine::Vec4u viewport;
    float nearClip;
    float farClip;
    float aspect;
    float fovyRadians;

    // Misc camera parameters (angles are in radians):
    float boosterSpeedScale;
    float forwardSpeed;
    float maxForwardSpeed;
    float acceleration;
    float movementSpeed;
    float rollSpeed;
    float turnSpeed;
    float yawAngleRadians;
    float pitchAngleRadians;
    float rollAngleRadians;
    float maxPitchRate;
    float maxYawRate;
    float movementDamping;
    float lookDamping;

    // Vars used to keep track of the current Z-rotation (roll)
    // that allow us to reposition the ship when the user hits the space bar.
    float accumulatedRollRadians;
    float maxAccumulatedRoll;
    float rollResetRate;
    bool  resetingCameraRoll;
    Engine::Vector3 savedUpVector;

    // Points:
    Engine::Vector3 lookAtPos;
    Engine::Vector3 worldPosition;
    Engine::Vector3 worldPositionDelta;

    // Actual vectors:
    Engine::Vector3 directionVector;
    Engine::Vector3 upVector;
    Engine::Vector3 pitchAxis;

    // Matrices/frustum:
    Engine::Matrix4 viewMatrix;
    Engine::Matrix4 projectionMatrix;
    Engine::Frustum frustum;

    DISABLE_COPY_AND_ASSIGN(FlightCamera)
};

} // namespace SpaceSim {}
