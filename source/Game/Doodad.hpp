
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Doodad.hpp
// Author: Guilherme R. Lampert
// Created on: 19/11/14
// Brief: Base type for scene props like asteroids and planets.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace SpaceSim
{

// ======================================================
// Doodad:
// ======================================================

class Doodad
	: public GameEntity
{
    RUNTIME_CLASS_DECL(Doodad)

  public:

    Doodad();
    Doodad(const Doodad & other);
    virtual ~Doodad() = default;
    Doodad & operator=(const Doodad &) = delete;

  protected:

    // An opaque pointer to a btDefaultMotionState or a custom MotionState.
    void * motionState;
};

// ======================================================
// Asteroid:
// ======================================================

class Asteroid
	: public Doodad
{
    RUNTIME_CLASS_DECL(Asteroid)

  public:

    static constexpr int firstAsteroidShapeNum = 1;
    static constexpr int lastAsteroidShapeNum  = 30;

    static constexpr int firstAsteroidSkinNum = 1;
    static constexpr int lastAsteroidSkinNum  = 15;

    static Engine::ConstModel3DPtr GetRandomAsteroidShape(int & shapeNum);
    static Engine::ConstTexturePtr GetRandomAsteroidSkin(int & skinNum);

    Asteroid();
    Asteroid(const Asteroid & other);
    Asteroid & operator=(const Asteroid &) = delete;

    void InitRandomAsteroid(const Engine::Vector3 & pos, const Engine::Vec2f scaleMinMax);
    void InitRandomAsteroidWithSkin(const Engine::Vector3 & pos, const Engine::Vec2f scaleMinMax, const int skinNum);
    void InitRandomAsteroidWithSkin(const Engine::Vector3 & pos, const Engine::Vec2f scaleMinMax, Engine::ConstTexturePtr skinTex);

    void OnUpdate(const Engine::GameTime & time) override;
    void OnRender(const Engine::GameTime & time) override;

  private:

    Engine::MaterialPtr asteroidMtr;
    Engine::Vector3 positionOffset;
    float uniformScale;
};

// ======================================================
// Planet:
// ======================================================

class Planet
	: public Doodad
{
    RUNTIME_CLASS_DECL(Planet)

  public:

    Planet();
    Planet(const Planet & other);
    Planet & operator=(const Planet &) = delete;

    void InitWithParams(const Engine::Vector3 & worldPos, float rotationDegs, float planetScale,
                        float boundsScale, const char * planetMesh, const char * planetSkin, bool hasRings);

    void OnUpdate(const Engine::GameTime & time) override;
    void OnRender(const Engine::GameTime & time) override;

  private:

    Engine::MaterialPtr customMtr;
    Engine::Vector3 worldPosition;
    Engine::Matrix4 rotationMatrix;
    float uniformScale;
    bool  withRings;
};

} // namespace SpaceSim {}
