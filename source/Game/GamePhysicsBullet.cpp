
// ================================================================================================
// -*- C++ -*-
// File: GamePhysicsBullet.cpp
// Author: Guilherme R. Lampert
// Created on: 16/10/14
// Brief: GamePhysics implemented with the Bullet Physics Library as back-end.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GamePhysicsBullet.hpp"
using namespace Engine;

namespace SpaceSim
{

// Bullet DebugRenderer. Implemented in GamePhysicsBulletDebug.cpp
extern btIDebugDraw * bulletDebugDraw;

// ======================================================
// GamePhysicsBullet:
// ======================================================

GamePhysicsBullet::GamePhysicsBullet()
{
    // Init Bullet Physics:
    collisionConfiguration = new btDefaultCollisionConfiguration();
    collisionDispatcher    = new btCollisionDispatcher(collisionConfiguration);
    broadPhaseInterface    = new btDbvtBroadphase();
    constraintSolver       = new btSequentialImpulseConstraintSolver();

    dynamicsWorld = new btDiscreteDynamicsWorld(collisionDispatcher,
                                                broadPhaseInterface,
                                                constraintSolver,
                                                collisionConfiguration);

    // Debug drawing adapter that uses our global DebugRenderer:
    dynamicsWorld->setDebugDrawer(bulletDebugDraw);

    // No gravity in space, of course.
    dynamicsWorld->setGravity(btVector3(0.0f, 0.0f, 0.0f));

    common->PrintF("Bullet GamePhysics initialized!");
}

GamePhysicsBullet::~GamePhysicsBullet()
{
    // Cleanup:
    delete dynamicsWorld;
    delete constraintSolver;
    delete broadPhaseInterface;
    delete collisionDispatcher;
    delete collisionConfiguration;
}

void GamePhysicsBullet::FullReset()
{
    common->PrintF("Bullet GamePhysics full reset.");

    // Bullet could use some smart pointers! ...
    for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; --i)
    {
        btCollisionObject * obj = dynamicsWorld->getCollisionObjectArray()[i];

        btRigidBody * body = btRigidBody::upcast(obj);
        if (body != nullptr)
        {
            delete body->getMotionState();
            delete body->getCollisionShape();
        }

        dynamicsWorld->removeCollisionObject(obj);
        delete obj;
    }
}

void GamePhysicsBullet::OnUpdate(const GameTime & time)
{
    dynamicsWorld->stepSimulation(time.deltaTime.seconds, 4);
}

void GamePhysicsBullet::OnRender(const GameTime &)
{
    dynamicsWorld->debugDrawWorld();
}

RigidBodyHandle GamePhysicsBullet::CreateRigidBody(const RigidBodyConstructionParams & params)
{
    assert(params.collisionShape != nullptr);
    assert(params.motionState != nullptr);

    // Create the body:
    btRigidBody::btRigidBodyConstructionInfo rbInfo(params.mass, params.motionState, params.collisionShape, ToBtVector3(params.localInertia));
    btRigidBody * rigidBody = new btRigidBody(rbInfo);

    // Set additional flags:
    if (params.kinematic)
    {
        rigidBody->setCollisionFlags(btCollisionObject::CF_KINEMATIC_OBJECT);
    }
    if (params.alwaysActive)
    {
        rigidBody->setActivationState(DISABLE_DEACTIVATION);
    }

    // The physics system now owns the rigid body.
    dynamicsWorld->addRigidBody(rigidBody);

    // Return the pointer as an opaque handle:
    return reinterpret_cast<RigidBodyHandle>(rigidBody);
}

void GamePhysicsBullet::DestroyRigidBody(RigidBodyHandle & rigidBodyHandle)
{
    assert(rigidBodyHandle != InvalidRigidBodyHandle);
    btRigidBody * body = reinterpret_cast<btRigidBody *>(rigidBodyHandle);

    // Delete the motion state, if any:
    delete body->getMotionState();

    // Delete the collision shape, RigidBody also owns it:
    delete body->getCollisionShape();

    // Remove from world and delete the instance:
    dynamicsWorld->removeRigidBody(body);
    delete body;

    rigidBodyHandle = InvalidRigidBodyHandle;
}

void GamePhysicsBullet::SetRigidBodyTransform(RigidBodyHandle rigidBodyHandle, const Matrix4 & xform)
{
    assert(rigidBodyHandle != InvalidRigidBodyHandle);
    btRigidBody * body = reinterpret_cast<btRigidBody *>(rigidBodyHandle);

    btTransform btXform;
    btXform.setFromOpenGLMatrix(ToFloatPtr(xform));
    body->proceedToTransform(btXform);
    body->activate(true); // Refresh the object's internal state
}

AABB GamePhysicsBullet::GetCollisionAABB(RigidBodyHandle rigidBodyHandle)
{
    assert(rigidBodyHandle != InvalidRigidBodyHandle);
    const btRigidBody * body = reinterpret_cast<const btRigidBody *>(rigidBodyHandle);

    btVector3 mins, maxs;
    body->getAabb(mins, maxs);
    return AABB(BtVector3ToVector3(mins), BtVector3ToVector3(maxs));
}

void GamePhysicsBullet::GetCollisionSphere(RigidBodyHandle rigidBodyHandle, Vector3 & center, float & radius)
{
    assert(rigidBodyHandle != InvalidRigidBodyHandle);
    const btRigidBody * body = reinterpret_cast<const btRigidBody *>(rigidBodyHandle);

    const btCollisionShape * colShape = body->getCollisionShape();
    if (colShape == nullptr)
    {
        center = Vector3Zero;
        radius = 0.0f;
        return;
    }

    const int type = colShape->getShapeType();
    if (type != SPHERE_SHAPE_PROXYTYPE)
    {
        center = Vector3Zero;
        radius = 0.0f;
        return;
    }

    const btSphereShape * sphere = checked_cast<const btSphereShape *>(colShape);
    btTransform xform;
    body->getMotionState()->getWorldTransform(xform);
    center = BtVector3ToVector3(xform.getOrigin());
    radius = sphere->getRadius();
}

// ======================================================
// GamePhysics::Create():
// ======================================================

GamePhysics * GamePhysics::Create()
{
    return new GamePhysicsBullet();
}

} // namespace SpaceSim {}
