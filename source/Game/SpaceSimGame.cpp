
// ================================================================================================
// -*- C++ -*-
// File: SpaceSimGame.cpp
// Author: Guilherme R. Lampert
// Created on: 14/10/14
// Brief: Miscellaneous Game and Player logic.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
#include "Game/GameMenu.hpp"
#include "Game/LocalPlayerView.hpp"
#include "Game/SpaceSimGame.hpp"
using namespace Engine;

namespace SpaceSim
{

// ======================================================
// SpaceSimGame:
// ======================================================

void SpaceSimGame::Init()
{
    common->PrintF("------- SpaceSimGame::Init() -------");

    // Fetch global script configurations:
    scriptMgr->GetConfigValue("rendererConfig.splitScreenGame", splitScreenGame);
    scriptMgr->GetConfigValue("rendererConfig.debugRenderPhysics", debugRenderPhysics);
    scriptMgr->GetConfigValue("rendererConfig.drawFPSCounter", drawFPSCounter);
    scriptMgr->GetConfigValue("rendererConfig.drawHUD", drawHUD);

    // Set up the home menus:
    InitMenus();
}

void SpaceSimGame::Shutdown()
{
    common->PrintF("----- SpaceSimGame::Shutdown() -----");

    DestroyAllGameViews();

    recordingScreen   = false;
    currentPlayerView = nullptr;

    // Managed pointers. Setting to null will delete the objects.
    currentMenu = nullptr;
    menus[1]    = nullptr;
    menus[0]    = nullptr;
    world       = nullptr;
    physics     = nullptr;
}

void SpaceSimGame::InitMenus()
{
    menus[0].reset(new GameMenu());
    menus[1].reset(new GameMenu());
    currentMenu = menus[0].get();

    const Vec2u fb = renderMgr->GetFramebufferDimensions();

    // Menu 0 - Buttons:
    // - [1 player]
    // - [2 players]
    {
        Button btn;

        btn.btnRect = Rect(250, fb.height - 900 + 600, 640, 200);
        btn.textLabel = "1 PLAYER";
        btn.textOffset = Point(120, 90);
        btn.onClick = [this]()
        {
			splitScreenGame = false;
			currentMenu = menus[1].get(); // Next menu in the sequence.
			return true;
        };
        menus[0]->AddButton(btn);

        btn.btnRect = Rect(250, fb.height - 900 + 300, 640, 200);
        btn.textLabel = "2 PLAYERS";
        btn.textOffset = Point(120, 90);
        btn.onClick = [this]()
        {
			splitScreenGame = true;
			currentMenu = menus[1].get(); // Next menu in the sequence.
			return true;
        };
        menus[0]->AddButton(btn);
    }

    // Menu 1 - Buttons:
    // - [Phobos Field]
    // - [Rogue Station]
    // - [Jupiter System]
    {
        Button btn;

        btn.btnRect = Rect(250, fb.height - 900 + 600, 640, 200);
        btn.textLabel = "Phobos Field";
        btn.textOffset = Point(120, 90);
        btn.onClick = [this]()
        { InitLocalGame("mars"); currentMenu = nullptr; return true;
        };
        menus[1]->AddButton(btn);

        btn.btnRect = Rect(250, fb.height - 900 + 300, 640, 200);
        btn.textLabel = "Rogue Station";
        btn.textOffset = Point(120, 90);
        btn.onClick = [this]()
        { InitLocalGame("green_nebula"); currentMenu = nullptr; return true;
        };
        menus[1]->AddButton(btn);

        btn.btnRect = Rect(250, fb.height - 900, 640, 200);
        btn.textLabel = "Jupiter System";
        btn.textOffset = Point(120, 90);
        btn.onClick = [this]()
        { InitLocalGame("jupiter"); currentMenu = nullptr; return true;
        };
        menus[1]->AddButton(btn);
    }
}

void SpaceSimGame::InitLocalGame(const std::string & levelName)
{
    common->PrintF("Starting a new game. Level: %s", levelName.c_str());

    // Hide an grab the system mouse-pointer/cursor:
    common->GrabSystemCursor();

    // Paint the whole screen with the loading-screen splash.
    currentMenu->DrawEarlySplashScreen();

    // Init common singletons:
    physics.reset(GamePhysics::Create());
    world.reset(GameWorld::Create());

    // Create the local player(s) (AddGameView() transfers ownership to the gameViews array):

    LocalPlayerView * player1 = new LocalPlayerView(splitScreenGame ? 1 : 0);
    AddGameView(std::unique_ptr<GameView>(player1));

    if (splitScreenGame)
    {
        LocalPlayerView * player2 = new LocalPlayerView(2);
        AddGameView(std::unique_ptr<GameView>(player2));
    }

    world->LoadLevel(levelName);
    resourceMgr->CollectGarbage();
}

void SpaceSimGame::EndGame()
{
    common->PrintF("Ending a game...");
    common->RestoreSystemCursor();

    DestroyAllGameViews();

    // Setting to null will make the unique_ptrs destroy the objects.
    // We then create new ones if another game is loaded.
    world = nullptr;
    physics = nullptr;
    currentPlayerView = nullptr;

    splitScreenGame = false;
    currentMenu = menus[0].get();
    renderMgr->SetSplitScreen(SplitScreen::FullView);
}

void SpaceSimGame::OnUpdate(const GameTime time)
{
    if (currentMenu != nullptr)
    {
        // Don't do any regular game logic while in a menu.
        return;
    }

    // Update all views:
    if (splitScreenGame)
    {
        for (auto & it : gameViews)
        {
            currentPlayerView = checked_cast<LocalPlayerView *>(it.get());

            if (currentPlayerView->GetViewId() == 1) // Player 1
            {
                renderMgr->SetSplitScreen(SplitScreen::TopView);
            }
            else // Player 2
            {
                renderMgr->SetSplitScreen(SplitScreen::BottonView);
            }

            currentPlayerView->OnUpdate(time);
        }
    }
    else
    {
        for (auto & it : gameViews)
        {
            currentPlayerView = checked_cast<LocalPlayerView *>(it.get());
            currentPlayerView->OnUpdate(time);
        }
    }

    currentPlayerView = nullptr;

    // Run entity behavior:
    game->world->OnUpdate(time);

    // Step the physics simulation:
    game->physics->OnUpdate(time);
}

void SpaceSimGame::OnRender(const GameTime time)
{
    if (currentMenu != nullptr)
    {
        currentMenu->DrawMenuFullscreen();
        return;
    }

    // Render all views (split-screen game):
    if (splitScreenGame)
    {
        for (auto & it : gameViews)
        {
            currentPlayerView = checked_cast<LocalPlayerView *>(it.get());

            if (currentPlayerView->GetViewId() == 1) // Player 1
            {
                renderMgr->SetSplitScreen(SplitScreen::TopView);
            }
            else // Player 2
            {
                renderMgr->SetSplitScreen(SplitScreen::BottonView);
            }

            currentPlayerView->OnRender(time);
        }
    }
    else
    {
        for (auto & it : gameViews)
        {
            currentPlayerView = checked_cast<LocalPlayerView *>(it.get());
            currentPlayerView->OnRender(time);
        }
    }

    currentPlayerView = nullptr;

    // Clear batched particle effects, now that all views are done:
    world->ClearFrameParticleEmitters();
}

bool SpaceSimGame::OnAppEvent(const AppEvent event)
{
    if (currentMenu != nullptr)
    {
        if (currentMenu->OnAppEvent(event))
        {
            return true; // Event handled.
        }
    }

    return BaseGameLogic::OnAppEvent(event);
}

// ======================================================
// Global instance of the game:
// ======================================================

static SpaceSimGame theGameInstance;
SpaceSimGame * game = &theGameInstance;

} // namespace SpaceSim {}
