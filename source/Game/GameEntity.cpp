
// ================================================================================================
// -*- C++ -*-
// File: GameEntity.cpp
// Author: Guilherme R. Lampert
// Created on: 14/10/14
// Brief: Generic game entity/actor/object, whatever you wanna call it.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GamePhysicsBullet.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
using namespace Engine;

namespace SpaceSim
{
namespace /* unnamed */
{

inline bool SphereContainsPoint(const Vector3 & center, const float radius, const Vector3 & p)
{
    if (lengthSqr(p - center) > (radius * radius))
    {
        return false;
    }
    return true;
}

inline bool SphereLineIntersection(const Vector3 & center, const float radius, const Vector3 & start, const Vector3 & end)
{
    Vector3 r, s, e;
    float a;

    s = start - center;
    e = end - center;
    r = e - s;
    a = dot(-s, r);

    if (a <= 0)
    {
        return (dot(s, s) < radius * radius);
    }
    else if (a >= dot(r, r))
    {
        return (dot(e, e) < radius * radius);
    }
    else
    {
        r = s + (a / dot(r, r)) * r;
        return (dot(r, r) < radius * radius);
    }
}

} // namespace unnamed {}

// ======================================================
// Register with the Runtime Type System:
// ======================================================

RUNTIME_CLASS_DEFINITION(
Engine::Class,                     /* superclass */
SpaceSim::GameEntity,              /* class name */
DefaultClassInstancer<GameEntity>, /* alloc with new() */
DefaultClassDeleter<GameEntity>)   /* free with delete() */

// ======================================================
// GameEntity:
// ======================================================

GameEntity::GameEntity()
    : flags(0)
    , health(0)
    , shield(0)
    , booster(0)
    , maxHealth(0)
    , maxShield(0)
    , maxBooster(0)
    , model3d(nullptr)
    , rigidBody(InvalidRigidBodyHandle)
    , modelMatrix(Matrix4::identity())
    , boundsType(BBox)
{
}

GameEntity::GameEntity(const GameEntity & other)
{
    // TODO
    (void)other;
    assert(false && "GameEntity cloning not yet implemented!");
}

GameEntity::~GameEntity()
{
    if (rigidBody != InvalidRigidBodyHandle)
    {
        game->physics->DestroyRigidBody(rigidBody);
    }
}

void GameEntity::SetModel3D(std::string modelName)
{
    SetModel3D(checked_pointer_cast<const Model3D>(resourceMgr->FindOrLoadResource(modelName)));
}

void GameEntity::OnUpdate(const GameTime &)
{
    // Shouldn't even get here otherwise.
    assert(flags & EnableUpdate);
}

void GameEntity::OnRender(const GameTime &)
{
    // Shouldn't even get here otherwise.
    assert(flags & EnableRender);
    assert(model3d != nullptr);

    model3d->DrawWithMVP(renderMgr->GetCurrentMVPMatrix(),
            renderMgr->GetViewMatrix() * renderMgr->GetTopMatrix());
}

void GameEntity::OnPreRender()
{
    renderMgr->PushMatrix(modelMatrix);
}

void GameEntity::OnPostRender()
{
    renderMgr->PopMatrix();
}

bool GameEntity::InFrustum(const Frustum & frustum) const
{
    if (rigidBody == InvalidRigidBodyHandle)
    {
        return true; // Always PASSES for non-physical objects.
    }

    if (boundsType == BBox)
    {
        const AABB aabb = game->physics->GetCollisionAABB(rigidBody);
        return frustum.TestAABB(aabb);
    }

    if (boundsType == BSphere)
    {
        Vector3 center;
        float radius;
        game->physics->GetCollisionSphere(rigidBody, center, radius);
        return frustum.TestSphere(center, radius);
    }

    assert(false && "Invalid BoundsType!");
}

bool GameEntity::IntersectsPoint(const Vector3 & point) const
{
    if (rigidBody == InvalidRigidBodyHandle)
    {
        return false; // Always FAILS for non-physical objects.
    }

    if (boundsType == BBox)
    {
        const AABB aabb = game->physics->GetCollisionAABB(rigidBody);
        return aabb.ContainsPoint(point);
    }

    if (boundsType == BSphere)
    {
        Vector3 center;
        float radius;
        game->physics->GetCollisionSphere(rigidBody, center, radius);
        return SphereContainsPoint(center, radius, point);
    }

    assert(false && "Invalid BoundsType!");
}

bool GameEntity::IntersectsLine(const LineSegment & lineSeg) const
{
    if (rigidBody == InvalidRigidBodyHandle)
    {
        return false; // Always FAILS for non-physical objects.
    }

    if (boundsType == BBox)
    {
        const AABB aabb = game->physics->GetCollisionAABB(rigidBody);
        return aabb.LineIntersection(lineSeg.start, lineSeg.end);
    }

    if (boundsType == BSphere)
    {
        Vector3 center;
        float radius;
        game->physics->GetCollisionSphere(rigidBody, center, radius);
        return SphereLineIntersection(center, radius, lineSeg.start, lineSeg.end);
    }

    assert(false && "Invalid BoundsType!");
}

float GameEntity::GetDistanceFromCamera() const
{
    return length(modelMatrix.getTranslation() - renderMgr->GetCameraRef().GetWorldPosition());
}

void GameEntity::TakeDamage(const double damage, const Vector3 & point, const Color4f & fxColor)
{
    // If shields not depleted, take damage from shield.
    if (shield > 0)
    {
        double newShield = static_cast<double>(shield) - damage;
        if (newShield < 0.0)
        {
            newShield = 0.0;
        }
        shield = static_cast<int32_t>(std::floor(newShield));
    }
    else if (health > 0)
    {
        double newHealth = static_cast<double>(health) - damage;
        if (newHealth < 0.0)
        {
            newHealth = 0.0;
        }
        health = static_cast<int32_t>(std::floor(newHealth));
    }

    SpawnDamageEffect(point, fxColor);
}

void GameEntity::SpawnDamageEffect(const Vector3 & point, const Color4f & fxColor) const
{
    ParticleRenderer * prt = game->world->GetDamageEffectParticleEmitter();
    if (prt != nullptr)
    {
        prt->SetParticleBaseColor(fxColor);
        prt->SetEmitterOrigin(point);
        prt->ResetEmitter();
    }
    // We don't have any emitter available at the moment.
    // This damage impact will not render an effect this time.
}

void GameEntity::SetupRigidBody(const BoundsType bType, const float mass, void * motionState, const Matrix4 * xform,
                                const bool kinematic, const bool alwaysActive, const Engine::AABB * bbox)
{
    if (model3d == nullptr)
    {
        common->FatalErrorF("SetupRigidBody() needs a 3D model to derive the collision shape from!");
    }

    const AABB aabb = (bbox != nullptr) ? *bbox : model3d->GetAABB();
    btCollisionShape * colShape;

    if (bType == BSphere)
    {
        boundsType = BSphere;
        const float radius = length(aabb.Center() - aabb.maxs);
        colShape = new btSphereShape(radius);
    }
    else
    {
        boundsType = BBox;
        colShape = new btBoxShape(btVector3(
        (aabb.maxs[0] - aabb.mins[0]) * 0.5f,
        (aabb.maxs[1] - aabb.mins[1]) * 0.5f,
        (aabb.maxs[2] - aabb.mins[2]) * 0.5f));
    }

    btTransform startTransform;
    startTransform.setIdentity();
    if (xform != nullptr)
    {
        startTransform.setFromOpenGLMatrix(ToFloatPtr(*xform));
    }

    btVector3 localInertia(0.0f, 0.0f, 0.0f);
    btMotionState * myMotionState = (motionState == nullptr) ?
                    new btDefaultMotionState(startTransform) :
                    reinterpret_cast<btMotionState *>(motionState);

    if (mass > 0.0f)
    {
        colShape->calculateLocalInertia(mass, localInertia);
    }

    const RigidBodyConstructionParams rbParams = {
        /* motionState    = */ myMotionState,
        /* collisionShape = */ colShape,
        /* localInertia   = */ BtVector3ToVector3(localInertia),
        /* mass           = */ mass,
        /* kinematic      = */ kinematic,
        /* alwaysActive   = */ alwaysActive
    };
    rigidBody = game->physics->CreateRigidBody(rbParams);
    assert(rigidBody != InvalidRigidBodyHandle);
}

} // namespace SpaceSim {}
