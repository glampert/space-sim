
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: LocalPlayerView.hpp
// Author: Guilherme R. Lampert
// Created on: 14/10/14
// Brief: Game View for the local player.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Dependencies:
#include "Game/FlightCamera.hpp"
#include "Game/PlayerEntity.hpp"

namespace SpaceSim
{

// ======================================================
// LocalPlayerView:
// ======================================================

class LocalPlayerView final
    : public Engine::HumanGameView
{
  public:

    LocalPlayerView(const Engine::GameViewId vId = Engine::InvalidGameViewId);

    // Frame updates/events:
    void OnUpdate(const Engine::GameTime time) override;
    void OnRender(const Engine::GameTime time) override;
    bool OnAppEvent(const Engine::AppEvent event) override;

    // Accessors:
    Engine::MouseInputHandler & GetMouseInput() { return mouseInput; }
    Engine::KeyboardInputHandler & GetKeyboardInput() { return keyboardInput; }
    Engine::XB360Controller & GetXbController() { return xbController; }
    Engine::BillboardBatchRenderer & GetBillboardRenderer() { return billboardBatch; }
    Engine::SpriteBatchRenderer & GetSpriteBatch() { return spriteBatch; }
    Engine::Basic2DTextRenderer & GetTextRenderer() { return textRenderer; }
    Engine::DebugRenderer & GetDebugRenderer() { return debugRenderer; }
    FlightCamera & GetCameraRef() { return camera; }
    PlayerEntity & GetPlayerEntity();

  private:

    // Internal helpers:
    void DrawHUD();
    void DrawFPSCounter();

    // Special player entity. Also its space craft.
    GameEntityHandle playerEntityHandle;

    // Input devices:
    Engine::XB360Controller xbController;
    Engine::MouseInputHandler mouseInput;
    Engine::KeyboardInputHandler keyboardInput;

    // Rendering helpers:
    Engine::BillboardBatchRenderer billboardBatch;
    Engine::SpriteBatchRenderer spriteBatch;
    Engine::Basic2DTextRenderer textRenderer;
    Engine::DebugRenderer debugRenderer;

    // HUD elements:
    Engine::ConstTexturePtr barSprite;
    Engine::ConstTexturePtr barFrameSprite;

    // Player's perspective camera:
    FlightCamera camera;

    // We average multiple frames together to smooth changes out a bit.
    struct FPSCounter
    {
        static constexpr int MaxFrames = 4;
        int64_t previousTimes[MaxFrames];
        int64_t previousTime;
        int64_t fpsCount;
        int index;
    } fps = {};

    DISABLE_COPY_AND_ASSIGN(LocalPlayerView)
};

} // namespace SpaceSim {}
