
// ================================================================================================
// -*- C++ -*-
// File: NetGamePacket.cpp
// Author: Guilherme R. Lampert
// Created on: 16/11/14
// Brief: Network packet type used by the MP game.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/NetGamePacket.hpp"
using namespace Engine;

namespace SpaceSim
{

void SendWithRetry(Socket & socket, const void * bytes, const size_t numBytes)
{
    size_t r = socket.SendBytes(bytes, numBytes);
    assert(r == numBytes); // TODO better error handling / retry!
}

void ReceiveWithRetry(Socket & socket, void * bytes, const size_t numBytes)
{
    size_t r = socket.ReceiveBytes(bytes, numBytes);
    assert(r == numBytes); // TODO better error handling / retry!
}

// ======================================================
// NetGamePacket:
// ======================================================

void NetGamePacket::Send(Socket & socket) const
{
    // Serialize the data map:
    std::vector<uint32_t> mapBytes(dataMap.size() * 2);
    for (const auto & pair : dataMap)
    {
        mapBytes.push_back(pair.first);
        mapBytes.push_back(pair.second);
    }

    // Send the map size:
    uint32_t len = static_cast<uint32_t>(mapBytes.size());
    SendWithRetry(socket, &len, sizeof(len));

    // And the map bytes:
    if (len != 0)
    {
        SendWithRetry(socket, mapBytes.data(), len * sizeof(uint32_t));
    }

    // Now send the data size and raw bytes:
    len = byteBuffer.size();
    SendWithRetry(socket, &len, sizeof(len)); // len in bytes
    if (len != 0)
    {
        SendWithRetry(socket, byteBuffer.data(), byteBuffer.size());
    }
}

void NetGamePacket::Receive(Socket & socket)
{
    std::vector<uint32_t> mapBytes;
    uint32_t len = 0;

    // Receive size in elements of mapBytes:
    ReceiveWithRetry(socket, &len, sizeof(len));
    if (len == 0)
    {
        return;
    }

    mapBytes.resize(len);
    ReceiveWithRetry(socket, mapBytes.data(), len * sizeof(uint32_t));

    // Now read the byteBuffer:
    len = 0;
    ReceiveWithRetry(socket, &len, sizeof(len)); // len in bytes
    if (len == 0)
    {
        return;
    }

    byteBuffer.resize(len);
    ReceiveWithRetry(socket, byteBuffer.data(), len);

    // Rebuild the indexing map:
    for (size_t i = 0; i < mapBytes.size(); i += 2)
    {
        dataMap[mapBytes[i]] = mapBytes[i + 1];
        assert(mapBytes[i + 1] < byteBuffer.size());
    }
}

bool NetGamePacket::Read(const uint32_t key, int32_t & n) const
{
    const auto it = dataMap.find(key);
    if (it == dataMap.end())
    {
        return false;
    }

    n = *reinterpret_cast<const int32_t *>(&byteBuffer[it->second]);
    return true;
}

void NetGamePacket::Write(const uint32_t key, const int32_t n)
{
    const uint8_t * ptr = reinterpret_cast<const uint8_t *>(&n);
    const size_t len = sizeof(n);

    byteBuffer.insert(byteBuffer.end(), ptr, ptr + len);
    dataMap[key] = writePos;
    writePos += len;
}

bool NetGamePacket::Read(const uint32_t key, std::string & s) const
{
    const auto it = dataMap.find(key);
    if (it == dataMap.end())
    {
        return false;
    }

    const char * ptr = reinterpret_cast<const char *>(&byteBuffer[it->second]);
    s = ptr; // Should be null terminated if transmission when OK...
    return true;
}

void NetGamePacket::Write(const uint32_t key, const std::string & s)
{
    const uint8_t * ptr = reinterpret_cast<const uint8_t *>(s.c_str());
    const size_t len = s.length() + 1; // Append the null terminator for simplicity.

    byteBuffer.insert(byteBuffer.end(), ptr, ptr + len);
    dataMap[key] = writePos;
    writePos += len;
}

bool NetGamePacket::Read(const uint32_t key, Matrix4 & m) const
{
    const auto it = dataMap.find(key);
    if (it == dataMap.end())
    {
        return false;
    }

    const Matrix4 * ptr = reinterpret_cast<const Matrix4 *>(&byteBuffer[it->second]);
    std::memcpy(&m, ptr, sizeof(m));
    return true;
}

void NetGamePacket::Write(const uint32_t key, const Matrix4 & m)
{
    const uint8_t * ptr = reinterpret_cast<const uint8_t *>(&m);
    const size_t len = sizeof(m);

    byteBuffer.insert(byteBuffer.end(), ptr, ptr + len);
    dataMap[key] = writePos;
    writePos += len;
}

void NetGamePacket::Clear()
{
    dataMap.clear();
    byteBuffer.clear();
    writePos = 0;
}

} // namespace SpaceSim {}
