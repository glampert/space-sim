
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: SpaceSimGame.hpp
// Author: Guilherme R. Lampert
// Created on: 14/10/14
// Brief: Miscellaneous Game and Player logic.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace SpaceSim
{

// ======================================================
// Forward declarations:
// ======================================================

class GameMenu;
class GameEntity;
class GameWorld;
class GamePhysics;
class LocalPlayerView;

// ======================================================
// SpaceSimGame:
// ======================================================

class SpaceSimGame final
    : public Engine::BaseGameLogic
{
  public:

    // Common game data / singletons:
    std::unique_ptr<GamePhysics> physics;
    std::unique_ptr<GameWorld> world;

    // Global random number generator used by this instance of the game.
    Engine::MT19937Random randGen;

    // Misc global flags:
    bool recordingScreen = false;
    bool splitScreenGame = false;
    bool debugRenderPhysics = false;
    bool drawFPSCounter = true;
    bool drawHUD = true;

    SpaceSimGame() = default;

    void Init();
    void Shutdown();
    void EndGame(); // Back to the main menu.

    void OnUpdate(const Engine::GameTime time) override;
    void OnRender(const Engine::GameTime time) override;
    bool OnAppEvent(const Engine::AppEvent event) override;

    LocalPlayerView * GetCurrentView() { return currentPlayerView; }

  private:

    LocalPlayerView * currentPlayerView = nullptr;

    std::unique_ptr<GameMenu> menus[2];
    GameMenu * currentMenu = nullptr; // Ref to menus[]. Null when in-game.

    void InitMenus();
    void InitLocalGame(const std::string & levelName);

    DISABLE_COPY_AND_ASSIGN(SpaceSimGame)
};

extern SpaceSimGame * game;

} // namespace SpaceSim {}
