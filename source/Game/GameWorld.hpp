
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: GameWorld.hpp
// Author: Guilherme R. Lampert
// Created on: 14/10/14
// Brief: Instance of the game world/level/scene.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace SpaceSim
{

// ======================================================
// Forward declarations / helper types:
// ======================================================

class GameEntity;

// We are forced to batch particle renderers
// to ensure correct draw order. This has the benefit
// of reducing render states changes, which is nice.
struct FrameParticleEmitter
{
    Engine::ParticleRenderer * particleRenderer;
    Engine::Matrix4 modelMatrix;
};

// ======================================================
// GameWorld:
// ======================================================

class GameWorld
{
  public:

    static GameWorld * Create();

    GameWorld();
    ~GameWorld();

    // Game level management:
    void LoadLevel(const std::string & levelName);

    // Frame updates:
    void OnUpdate(const Engine::GameTime & time);
    void OnRender(const Engine::GameTime & time);

    // Rendering helpers:
    void AddFrameParticleEmitter(const FrameParticleEmitter & frameEmitter);
    void RenderFrameParticleEmitters(const Engine::GameTime & time);
    void ClearFrameParticleEmitters();

    // Allocate and link a new entity (throws an exception if entity pool is depleted).
    // The world will keep ownership of the entity and will free it when it
    // is destroyed with DestroyEntity() or when the world instance itself gets destroyed.
    GameEntityHandle CreateEntity(const char * className);
    GameEntityHandle CreateEntity(const Engine::TypeInfo & type);
    void DestroyEntity(GameEntityHandle & entityHandle);

    // Dereference an entity handle to a pointer.
    // The pointer is a temporary object and should not be kept around.
    // Only the reference is guaranteed to remain valid. The pointer might get reallocated.
    GameEntity * GetEntityPointer(const GameEntityHandle entityHandle) const;

    // Query the world for entity intersections (always returns the first intersection):
    GameEntity * FindIntersection(const Engine::Vector3 & point, const uint32_t entityFlags, const GameEntity * exclude) const;

    // Get a pointer to a free particle renderer for damage effects.
    // Will return a null pointer if not emitters are available this frame.
    Engine::ParticleRenderer * GetDamageEffectParticleEmitter();

    // Misc accessors / helpers:
    void DumpEntityTables() const;
    int GetNumAliveEntities() const;
    int GetNumVisibleEntities() const;

    // Internal helper. Places an allocate entity into the gameEntities table.
    // Your should not normally have to call this method.
    GameEntityHandle LinkEntity(GameEntity * entity);

    // Max game entities per world/level. This is a hard constraint.
    static constexpr int MaxEntities = 4096;

  private:

    // Internal helpers:
    static void Picking(GameEntity ** visibleEntities, const size_t numVisibleEntities);
    void SetupDamageFXEmitters();

    // Aux counters:
    int freeSlotsAvailable; // Entries available in freeSlots[].
    int linkedEntities;     // Currently linked count.

    // Each index in this array that is != InvalidGameEntityHandle
    // has an index to an entity in gameEntities[]. Free indexes
    // are always kept in sequential order in this array, so when
    // we need to allocate a new entity, just pop the last entry from
    // here and set it in gameEntities[].
    Engine::FixedArray<GameEntityHandle, MaxEntities> freeSlots;

    // Pointers to entities linked to the world.
    // A null pointer means a free slot.
    Engine::FixedArray<GameEntity *, MaxEntities> gameEntities;

    // Visible entities for the current frame.
    // Only those flagged as IsInteractive make this list!
    size_t numVisibleEntities;
    Engine::FixedArray<GameEntity *, 1024> visibleEntities;

    // Pool of particle emitters used for damage effects.
    // The actives once range from 0 to numDamageFXEmittersInUse-1.
    size_t numDamageFXEmittersInUse;
    Engine::FixedArray<Engine::ParticleRenderer *, 256> damageFXEmitters;
    std::unique_ptr<Engine::ParticleRenderer[]> damageFXEmitterPool;

    // Current skybox background:
    Engine::SkyboxRenderer skybox;

    // Batch of particle emitter for the current frame:
    std::vector<FrameParticleEmitter> frameEmitters;

    // This class is too big for direct value copy.
    DISABLE_COPY_AND_ASSIGN(GameWorld);
};

} // namespace SpaceSim {}
