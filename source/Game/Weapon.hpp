
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: Weapon.hpp
// Author: Guilherme R. Lampert
// Created on: 15/11/14
// Brief: Weapons for space crafts / stations / whatnot.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace SpaceSim
{

// ======================================================
// Projectile:
// ======================================================

struct Projectile
{
    Engine::ParticleRenderer * particleFX;
    Engine::Vector3 position;
    Engine::Vector3 direction;
    Engine::Vector3 origin;
};

// ======================================================
// Weapon:
// ======================================================

class Weapon
    : public Engine::SListNode<Weapon>
{
  public:

    // Weak reference to the entity that owns this weapon.
    GameEntity * owner;

    // Projectile/weapon properties:
    uint32_t fireCoolDownTimeMs;
    uint32_t lastTimeFiredMs;
    float projectileRangeMeters;
    float projectileDamage;
    float projectileVelocityMetersPerSec;

    // Offset of the weapon relative to the space craft that owns it.
    Engine::Vector3 positionOffset;

    // Offset added to the aim reticle for this weapon and its size/pos.
    float aimReticleYOffset;
    Engine::Vec2f aimReticleSize;
    Engine::Vec2f aimReticlePos;
    Engine::Color4f aimReticleColor;

    // Can provide a custom aim reticle sprite.
    Engine::ConstTexturePtr aimReticleSprite;

    // Weapon name/description.
    std::string name;

    Weapon();
    bool Fire(const Engine::GameTime & time, const Engine::Vector3 origin, const Engine::Vector3 fireDirection);
    void OnUpdate(const Engine::GameTime & time);
    void SetupParticleEmitters(Engine::ConstTexturePtr particleSprite,
                               const Engine::Color4f baseColor,
                               const int prtMinParticles,
                               const int prtMaxParticles,
                               const int prtReleaseAmount,
                               const int prtReleaseInterval,
                               const int prtMinParticleSize,
                               const int prtMaxParticleSize,
                               const int prtLifeCicle,
                               const float prtVelocity,
                               const float prtVelocityVar);

  private:

    // Particle effect properties:
    float particleVelocity;
    Engine::Color4f projectileColor;

    // Max unique projectiles. Once all are used,
    // the weapon stops firing until one is recycled.
    static constexpr uint32_t MaxProjectiles = 1024;

    // Fixed-size pool of projectiles for this weapon.
    // Alive ones range from 0 to numProjectilesAlive-1.
    uint32_t numProjectilesAlive;
    std::unique_ptr<Projectile[]> projectilePool;

    // Each projectile in the projectilePool references the equivalent
    // ParticleRenderer index in this array.
    std::unique_ptr<Engine::ParticleRenderer[]> projectilePrts;

    DISABLE_COPY_AND_ASSIGN(Weapon)
};

} // namespace SpaceSim {}
