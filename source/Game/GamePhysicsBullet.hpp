
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: GamePhysicsBullet.hpp
// Author: Guilherme R. Lampert
// Created on: 16/10/14
// Brief: GamePhysics implemented with the Bullet Physics Library as back-end.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Bullet headers produce quite a few warning on Clang (probably also with GCC).
// Some are quite worrying, but I cannot fix the library myself at the moment...
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wignored-qualifiers"
#pragma GCC diagnostic ignored "-Woverloaded-virtual"
#endif // __GNUC__

// Bullet Physics Library:
#include "btBulletDynamicsCommon.h"

// Restore warnings:
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif // __GNUC__

namespace SpaceSim
{

// ======================================================
// RigidBodyConstructionParams:
// ======================================================

struct RigidBodyConstructionParams
{
    btMotionState * motionState;
    btCollisionShape * collisionShape;
    Engine::Vector3 localInertia;
    float mass;
    bool kinematic;
    bool alwaysActive;
};

// ======================================================
// GamePhysicsBullet:
// ======================================================

class GamePhysicsBullet final
    : public GamePhysics
{
  public:

    GamePhysicsBullet();
    ~GamePhysicsBullet();

    // Frame updates:
    void OnUpdate(const Engine::GameTime & time) override;
    void OnRender(const Engine::GameTime & time) override;

    // Rigid Body management:
    RigidBodyHandle CreateRigidBody(const RigidBodyConstructionParams & params) override;
    void DestroyRigidBody(RigidBodyHandle & rigidBodyHandle) override;

    void SetRigidBodyTransform(RigidBodyHandle rigidBodyHandle, const Engine::Matrix4 & xform) override;
    Engine::AABB GetCollisionAABB(RigidBodyHandle rigidBodyHandle) override;
    void GetCollisionSphere(RigidBodyHandle rigidBodyHandle, Engine::Vector3 & center, float & radius) override;

    // Miscellaneous:
    void FullReset() override;

  private:

    // Bullet interfaces:
    btDefaultCollisionConfiguration * collisionConfiguration;
    btCollisionDispatcher * collisionDispatcher;
    btBroadphaseInterface * broadPhaseInterface;
    btConstraintSolver * constraintSolver;
    btDynamicsWorld * dynamicsWorld;
};

// ======================================================
// Data conversion helpers:
// ======================================================

// Engine types to btVector3:
inline btVector3 ToBtVector3(const Engine::Vec3f & v) { return btVector3(v.x, v.y, v.z); }
inline btVector3 ToBtVector3(const Engine::Color4f & c) { return btVector3(c.r, c.g, c.b); }
inline btVector3 ToBtVector3(const Engine::Vector3 & v) { return btVector3(v[0], v[1], v[2]); }

// From btVector3 to Engine types:
inline Engine::Vec3f BtVector3ToVec3f(const btVector3 & v) { return Engine::Vec3f(v.x(), v.y(), v.z()); }
inline Engine::Color4f BtVector3ToColor4f(const btVector3 & v) { return Engine::Color4f(v.x(), v.y(), v.z(), 1.0f); }
inline Engine::Vector3 BtVector3ToVector3(const btVector3 & v) { return Engine::Vector3(v.x(), v.y(), v.z()); }

} // namespace SpaceSim {}
