
// ================================================================================================
// -*- C++ -*-
// File: Weapon.cpp
// Author: Guilherme R. Lampert
// Created on: 15/11/14
// Brief: Weapons for space crafts / stations / whatnot.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
#include "Game/Weapon.hpp"
using namespace Engine;

namespace SpaceSim
{

// ======================================================
// Weapon:
// ======================================================

Weapon::Weapon()
    : owner(nullptr)
    , fireCoolDownTimeMs(0)
    , lastTimeFiredMs(0)
    , projectileRangeMeters(0.0f)
    , projectileDamage(0.0f)
    , projectileVelocityMetersPerSec(0.0f)
    , positionOffset(Vector3Zero)
    , aimReticleYOffset(0.0f)
    , aimReticleSize(0.0f)
    , aimReticlePos(0.0f)
    , aimReticleColor(1.0f)
    , particleVelocity(0.0f)
    , projectileColor(1.0f)
    , numProjectilesAlive(0)
    , projectilePool(new Projectile[MaxProjectiles])
    , projectilePrts(new ParticleRenderer[MaxProjectiles])
{
    // Assign a particle renderer for each projectile:
    for (uint32_t p = 0; p < MaxProjectiles; ++p)
    {
        ZeroObjectUnchecked(projectilePool[p]);
        projectilePool[p].particleFX = &projectilePrts[p];
    }
}

void Weapon::SetupParticleEmitters(Engine::ConstTexturePtr particleSprite,
                                   const Engine::Color4f baseColor,
                                   const int prtMinParticles,
                                   const int prtMaxParticles,
                                   const int prtReleaseAmount,
                                   const int prtReleaseInterval,
                                   const int prtMinParticleSize,
                                   const int prtMaxParticleSize,
                                   const int prtLifeCicle,
                                   const float prtVelocity,
                                   const float prtVelocityVar)
{
    assert(particleSprite != nullptr);
    particleVelocity = prtVelocity;
    projectileColor = baseColor;

    for (uint32_t p = 0; p < MaxProjectiles; ++p)
    {
        Projectile & projectile = projectilePool[p];
        projectile.particleFX->SetMinMaxParticles(prtMinParticles, prtMaxParticles);
        projectile.particleFX->SetParticleReleaseAmount(prtReleaseAmount);
        projectile.particleFX->SetParticleReleaseInterval(prtReleaseInterval);
        projectile.particleFX->SetParticleMinSize(Vec2f(prtMinParticleSize, prtMinParticleSize));
        projectile.particleFX->SetParticleMaxSize(Vec2f(prtMaxParticleSize, prtMaxParticleSize));
        projectile.particleFX->SetParticleLifeCycle(prtLifeCicle);
        projectile.particleFX->SetParticleBaseColor(baseColor);
        projectile.particleFX->SetVelocityVar(prtVelocityVar);
        projectile.particleFX->SetParticleSpriteTexture(particleSprite);
        projectile.particleFX->SetParticleFadeEffect(ParticleFadeEffect::SmoothFade);
        projectile.particleFX->AllocateParticles();
    }
}

bool Weapon::Fire(const GameTime & time, const Vector3 origin, const Vector3 fireDirection)
{
    if (numProjectilesAlive == MaxProjectiles)
    {
        return false; // Pool depleted
    }

    if ((time.currentTime.milliseconds - lastTimeFiredMs) >= fireCoolDownTimeMs)
    {
        lastTimeFiredMs = time.currentTime.milliseconds;

        Projectile & projectile = projectilePool[numProjectilesAlive++];
        projectile.direction = fireDirection; // Assume normalized!
        projectile.position = origin;
        projectile.origin = origin;

        projectile.particleFX->SetEmitterOrigin(origin);
        projectile.particleFX->ResetEmitter();
        return true; // Did fire a new projectile
    }

    return false; // Still cooling-down
}

void Weapon::OnUpdate(const GameTime & time)
{
    assert(owner != nullptr);

    // Always update this because it is used outside.
    const Vec2u fb = renderMgr->GetFramebufferDimensions();
    aimReticlePos.x = (fb.width / 2.0f) - (aimReticleSize.width / 2.0f);
    aimReticlePos.y = (fb.height / 2.0f) - (aimReticleSize.height / 2.0f);
    aimReticlePos.y -= aimReticleYOffset;

    if (numProjectilesAlive == 0)
    {
        return;
    }

    // Save a square-root by using a squared value.
    const float rangeSqr = (projectileRangeMeters * projectileRangeMeters);
    for (uint32_t p = 0; p < numProjectilesAlive; ++p)
    {
        Projectile & projectile = projectilePool[p];
        projectile.position += (projectile.direction * (projectileVelocityMetersPerSec * time.deltaTime.seconds));

        // Out-of-range. Kill the projectile.
        if (lengthSqr(projectile.origin - projectile.position) > rangeSqr)
        {
            // Move the last alive projectile to slot p and put p at the end:
            if (p != (numProjectilesAlive - 1))
            {
                std::swap(projectilePool[p], projectilePool[numProjectilesAlive - 1]);
            }
            numProjectilesAlive--;
            continue;
        }

        // Collision test with interactive entities:
        GameEntity * entity = game->world->FindIntersection(projectile.position, GameEntity::IsInteractive, /* exclude = */ owner);
        if (entity != nullptr)
        {
            assert(entity->GetFlags() & GameEntity::IsInteractive);
            entity->TakeDamage(projectileDamage, projectile.position, projectileColor);

            // Dispose this projectile:
            if (p != (numProjectilesAlive - 1))
            {
                std::swap(projectilePool[p], projectilePool[numProjectilesAlive - 1]);
            }
            numProjectilesAlive--;
            continue;
        }
    }

    // Update the particle effects for alive projectiles:
    if (game->splitScreenGame)
    {
        for (uint32_t p = 0; p < numProjectilesAlive; ++p)
        {
            Projectile & projectile = projectilePool[p];
            projectile.particleFX->SetEmitterOrigin(projectile.position);
            projectile.particleFX->SetParticleVelocity(projectile.direction * -particleVelocity);
            projectile.particleFX->UpdateAllParticles(time);

            // Draw everything regardless of frustum when in a split screen game.
            // NOTE: This should be optimized!
            game->world->AddFrameParticleEmitter({ projectile.particleFX, Matrix4::identity() });
        }
    }
    else
    {
        const Frustum & frustum = renderMgr->GetCameraRef().GetFrustum();
        for (uint32_t p = 0; p < numProjectilesAlive; ++p)
        {
            Projectile & projectile = projectilePool[p];
            projectile.particleFX->SetEmitterOrigin(projectile.position);
            projectile.particleFX->SetParticleVelocity(projectile.direction * -particleVelocity);
            projectile.particleFX->UpdateAllParticles(time);

            // Add visible projectiles to the draw queue:
            if (frustum.TestPoint(projectile.position))
            {
                game->world->AddFrameParticleEmitter({ projectile.particleFX, Matrix4::identity() });
            }
        }
    }
}

} // namespace SpaceSim {}
