
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: GameEntity.hpp
// Author: Guilherme R. Lampert
// Created on: 14/10/14
// Brief: Generic game entity/actor/object, whatever you wanna call it.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Geometry/AABB.hpp"
#include "Engine/Core/Geometry/LineSegment.hpp"

namespace SpaceSim
{

// ======================================================
// Opaque Handles / forward declarations:
// ======================================================

using GameEntityHandle = uint16_t;
constexpr GameEntityHandle InvalidGameEntityHandle = UINT16_MAX;

// ======================================================
// GameEntity:
// ======================================================

class GameEntity
    : public Engine::Class
{
    RUNTIME_CLASS_DECL(GameEntity)

  public:

    enum Flags
    {
        EnableUpdate  = BITFIELD_ENUM(0), // Updated every frame
        EnableRender  = BITFIELD_ENUM(1), // Rendered if in view
        IsLocalPlayer = BITFIELD_ENUM(2), // Is the local player
        IsInteractive = BITFIELD_ENUM(3), // Takes damage (ships / players / some kinds of props)
        IsFriendly    = BITFIELD_ENUM(4), // Friendly to the local player
        IsHostile     = BITFIELD_ENUM(5)  // Hostile to the local player
    };

    enum BoundsType
    {
        BBox,
        BSphere
    };

    // Construction:
    GameEntity();
    GameEntity(const GameEntity & other);
    virtual ~GameEntity();

    // No direct copy. Use Clone().
    GameEntity & operator=(const GameEntity &) = delete;

    // Frame updates:
    virtual void OnUpdate(const Engine::GameTime & time);
    virtual void OnRender(const Engine::GameTime & time);

    // Called before and after OnRender(), if EnableRender set.
    virtual void OnPreRender();
    virtual void OnPostRender();

    // Culling / intersection tests:
    bool InFrustum(const Engine::Frustum & frustum) const;
    bool IntersectsPoint(const Engine::Vector3 & point) const;
    bool IntersectsLine(const Engine::LineSegment & lineSeg) const;
    float GetDistanceFromCamera() const;

    // Take damage at a given point in the object's surface.
    void TakeDamage(const double damage, const Engine::Vector3 & point, const Engine::Color4f & fxColor);

    // Getters/setters:

    uint32_t & GetFlags() { return flags; }
    const uint32_t & GetFlags() const { return flags; }
    void SetFlags(const uint32_t newFlags) { flags = newFlags; }

    const std::string & GetName() const { return name; }
    void SetName(std::string entityName) { name = std::move(entityName); }

    const Engine::Matrix4 & GetModelMatrix() const { return modelMatrix; }
    void SetModelMatrix(const Engine::Matrix4 & m) { modelMatrix = m; }

    RigidBodyHandle GetRigidBody() const { return rigidBody; }
    void SetRigidBody(const RigidBodyHandle rb) { rigidBody = rb; }

    Engine::ConstModel3DPtr GetModel3D() const { return model3d; }
    void SetModel3D(Engine::ConstModel3DPtr mdl) { model3d = mdl; }
    void SetModel3D(std::string modelName);

    int GetHealth()  const { return health;  }
    int GetShield()  const { return shield;  }
    int GetBooster() const { return booster; }

    void SetHealth(const int h)  { health  = h; }
    void SetShield(const int s)  { shield  = s; }
    void SetBooster(const int b) { booster = b; }

    int GetMaxHealth()  const { return maxHealth;  }
    int GetMaxShield()  const { return maxShield;  }
    int GetMaxBooster() const { return maxBooster; }

    void SetMaxHealth(const int h)  { maxHealth  = h; }
    void SetMaxShield(const int s)  { maxShield  = s; }
    void SetMaxBooster(const int b) { maxBooster = b; }

  protected:

    void SetupRigidBody(const BoundsType bType, const float mass, void * motionState, const Engine::Matrix4 * xform,
                        const bool kinematic, const bool alwaysActive, const Engine::AABB * bbox = nullptr);

    void SpawnDamageEffect(const Engine::Vector3 & point, const Engine::Color4f & fxColor) const;

    // Miscellaneous entity flags (Flags enum).
    uint32_t flags;

    // Not ideal, as many entities don't need a health/shield/booster amounts.
    // Placed here for simplicity. Could be re-factored in the future.
    int32_t health;
    int32_t shield;
    int32_t booster;
    int32_t maxHealth;
    int32_t maxShield;
    int32_t maxBooster;

    // Render model.
    Engine::ConstModel3DPtr model3d;

    // Optional rigid body if this entity has physics enabled.
    RigidBodyHandle rigidBody;

    // Model transform, ready for the renderer.
    Engine::Matrix4 modelMatrix;

    // Type of bounds/collision shape (defaults to BBox):
    BoundsType boundsType;

    // Entity name/tag/id. User can set to anything he/she pleases.
    std::string name;
};

} // namespace SpaceSim {}
