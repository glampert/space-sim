
// ================================================================================================
// -*- C++ -*-
// File: Doodad.cpp
// Author: Guilherme R. Lampert
// Created on: 19/11/14
// Brief: Base type for scene props like asteroids and planets.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GamePhysicsBullet.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
#include "Game/Doodad.hpp"
using namespace Engine;

namespace SpaceSim
{
namespace /* unnamed */
{

// The batch of asteroid models we have are somewhat misaligned
// with the origin of the 3D model, so we need to add these manual
// adjustment offsets to each model's transform. These were set by
// hand and guessing, so they are not perfect...
static const FixedArray<Vector3, 30> asteroidShapeOffsets = {
	{  0.0f,  -30.0f,  0.0f  },
	{  5.0f,  -25.0f, -5.0f  },
	{  5.0f,  -26.0f,  0.0f  },
	{  5.0f,  -26.0f,  0.0f  },
	{ -15.0f, -50.0f,  0.0f  },
	{  5.0f,  -23.0f, -5.0f  },
	{  0.0f,  -27.0f,  0.0f  },
	{  0.0f,  -25.0f,  0.0f  },
	{ -5.0f,  -30.0f,  0.0f  },
	{  0.0f,  -27.0f, -3.0f  },
	{  0.0f,  -27.0f,  0.0f  },
	{  0.0f,  -22.0f, -10.0f },
	{  0.0f,  -30.0f,  0.0f  },
	{  2.0f,  -20.0f, -10.0f },
	{  0.0f,  -32.0f,  0.0f  },
	{ -3.0f,  -27.0f,  0.0f  },
	{  0.0f,  -30.0f,  0.0f  },
	{  0.0f,  -25.0f,  0.0f  },
	{  0.0f,  -30.0f,  0.0f  },
	{  5.0f,  -15.0f,  0.0f  },
	{ -2.0f,  -32.0f,  0.0f  },
	{  0.0f,  -30.0f,  0.0f  },
	{  0.0f,  -30.0f,  0.0f  },
	{ -10.0f, -36.0f,  10.0f },
	{  0.0f,  -30.0f,  0.0f  },
	{  0.0f,  -30.0f,  0.0f  },
	{ -1.0f,  -25.0f,  0.0f  },
	{  0.0f,  -30.0f,  0.0f  },
	{  0.0f,  -30.0f,  0.0f  },
	{  0.0f,  -32.0f,  5.0f  }
};

class MyMotionState final
    : public btMotionState
{
  public:
    btTransform currentTranform;

    MyMotionState(const btTransform & initialPosition)
        : currentTranform(initialPosition) {}

    void getWorldTransform(btTransform & worldTrans) const override { worldTrans = currentTranform; }
    void setWorldTransform(const btTransform & worldTrans) override { currentTranform = worldTrans; }

    Matrix4 getMat4() const
    {
        const btVector3 pos = currentTranform.getOrigin();
        const Matrix4 t = Matrix4::translation(BtVector3ToVector3(pos));

        const btQuaternion rot = currentTranform.getRotation();
        Quat q(rot.x(), rot.y(), rot.z(), rot.w());
        const Matrix4 r = Matrix4::rotation(q);

        return (t * r);
    }
};

} // namespace unnamed {}

// ======================================================
// Doodad:
// ======================================================

// Register Doodad with the Runtime Type System:
RUNTIME_CLASS_DEFINITION(
SpaceSim::GameEntity,          /* superclass */
SpaceSim::Doodad,              /* class name */
DefaultClassInstancer<Doodad>, /* alloc with new() */
DefaultClassDeleter<Doodad>)   /* free with delete() */

// ======================================================

Doodad::Doodad()
    : GameEntity()
    , motionState(nullptr)
{
    SetFlags(GameEntity::EnableUpdate | GameEntity::EnableRender | GameEntity::IsInteractive);
}

Doodad::Doodad(const Doodad & other)
    : GameEntity(other)
{
    // TODO
    assert(false && "Doodad cloning not yet implemented!");
}

// ======================================================
// Asteroid:
// ======================================================

// Register Asteroid with the Runtime Type System:
RUNTIME_CLASS_DEFINITION(
SpaceSim::Doodad,                /* superclass */
SpaceSim::Asteroid,              /* class name */
DefaultClassInstancer<Asteroid>, /* alloc with new() */
DefaultClassDeleter<Asteroid>)   /* free with delete() */

// ======================================================

Asteroid::Asteroid()
    : Doodad()
    , positionOffset(Vector3Zero)
    , uniformScale(1.0f)
{
}

Asteroid::Asteroid(const Asteroid & other)
    : Doodad(other)
{
    // TODO
    assert(false && "Asteroid cloning not yet implemented!");
}

void Asteroid::OnUpdate(const GameTime & time)
{
    Doodad::OnUpdate(time);

    assert(motionState != nullptr);
    const Matrix4 physicsXform = reinterpret_cast<MyMotionState *>(motionState)->getMat4();
    const Matrix4 offsetXForm = Matrix4::translation(positionOffset) * Matrix4::scale(Vector3(uniformScale, uniformScale, uniformScale));
    SetModelMatrix(physicsXform * offsetXForm);
}

void Asteroid::OnRender(const GameTime &)
{
    // Shouldn't even get here otherwise.
    assert(flags & EnableRender);
    assert(model3d != nullptr);

    // This is a full override of the parent's OnRender().
    // This should NOT call Doodad::OnRender()!
    const Matrix4 mvp = renderMgr->GetCurrentMVPMatrix();
    const Matrix4 mv = renderMgr->GetViewMatrix() * renderMgr->GetTopMatrix();
    model3d->DrawWithMaterial(asteroidMtr, &mvp, &mv);
}

void Asteroid::InitRandomAsteroid(const Vector3 & pos, const Vec2f scaleMinMax)
{
    int skinNum;
    InitRandomAsteroidWithSkin(pos, scaleMinMax, GetRandomAsteroidSkin(skinNum));
}

void Asteroid::InitRandomAsteroidWithSkin(const Engine::Vector3 & pos, const Vec2f scaleMinMax, const int skinNum)
{
    assert(skinNum >= firstAsteroidSkinNum && skinNum <= lastAsteroidSkinNum);

    char filename[512];
    SNPrintF(filename, "doodads/asteroids/a%d.jpg", skinNum);
    ConstTexturePtr skinTex = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource(filename));
    InitRandomAsteroidWithSkin(pos, scaleMinMax, skinTex);
}

void Asteroid::InitRandomAsteroidWithSkin(const Engine::Vector3 & pos, const Vec2f scaleMinMax, ConstTexturePtr skinTex)
{
    assert(skinTex != nullptr);

    asteroidMtr = renderMgr->NewMaterial();
    asteroidMtr->SetDiffuseTexture(skinTex);
    asteroidMtr->SetDiffuseColor(Color4f(0.6f, 0.6f, 0.6f, 1.0f));
    asteroidMtr->SetName(Material::GetEnumeratedMaterialName());
    asteroidMtr->SetupShaderProgram();

    int shapeNum = 0;
    model3d = GetRandomAsteroidShape(shapeNum);

    uniformScale = game->randGen.NextFloat(scaleMinMax.x, scaleMinMax.y);
    positionOffset = asteroidShapeOffsets[shapeNum - 1] * uniformScale;
    SetModelMatrix(Matrix4::translation(pos));

    // Model bounds for our asteroid models are not
    // very tight for some reason... I'll make 'em smaller by hand...
    AABB asteroidBox = model3d->GetAABB();
    asteroidBox.mins *= 0.8f;
    asteroidBox.maxs *= 0.8f;
    asteroidBox.mins *= uniformScale;
    asteroidBox.maxs *= uniformScale;

    // Set up the physics body:
    constexpr float mass = 10000.0f;
    btTransform startTransform;
    startTransform.setFromOpenGLMatrix(ToFloatPtr(GetModelMatrix()));
    motionState = new MyMotionState(startTransform);
    SetupRigidBody(BBox, mass, motionState, nullptr, false, false, &asteroidBox);
}

ConstModel3DPtr Asteroid::GetRandomAsteroidShape(int & shapeNum)
{
    shapeNum = game->randGen.NextInteger(firstAsteroidShapeNum, lastAsteroidShapeNum);
    assert(shapeNum >= firstAsteroidShapeNum && shapeNum <= lastAsteroidShapeNum);

    char filename[512];
    SNPrintF(filename, "doodads/asteroids/asteroid%d.obj", shapeNum);
    return checked_pointer_cast<const Model3D>(resourceMgr->FindOrLoadResource(filename));
}

ConstTexturePtr Asteroid::GetRandomAsteroidSkin(int & skinNum)
{
    skinNum = game->randGen.NextInteger(firstAsteroidSkinNum, lastAsteroidSkinNum);
    assert(skinNum >= firstAsteroidSkinNum && skinNum <= lastAsteroidSkinNum);

    char filename[512];
    SNPrintF(filename, "doodads/asteroids/a%d.jpg", skinNum);
    return checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource(filename));
}

// ======================================================
// Planet:
// ======================================================

// Register Planet with the Runtime Type System:
RUNTIME_CLASS_DEFINITION(
SpaceSim::Doodad,              /* superclass */
SpaceSim::Planet,              /* class name */
DefaultClassInstancer<Planet>, /* alloc with new() */
DefaultClassDeleter<Planet>)   /* free with delete() */

// ======================================================

Planet::Planet()
    : Doodad()
    , worldPosition(Vector3Zero)
    , rotationMatrix(Matrix4::identity())
    , uniformScale(1.0f)
    , withRings(false)
{
}

Planet::Planet(const Planet & other)
    : Doodad(other)
{
    // TODO
    assert(false && "Planet cloning not yet implemented!");
}

void Planet::InitWithParams(const Vector3 & worldPos, float rotationDegs, float planetScale,
                            float boundsScale, const char * planetMesh, const char * planetSkin, bool hasRings)
{
    ConstTexturePtr skinTex = checked_pointer_cast<const Texture>(
    resourceMgr->FindOrLoadResource(std::string("doodads/planets/") + planetSkin, Resource::Type::Texture));

    model3d = checked_pointer_cast<const Model3D>(
    resourceMgr->FindOrLoadResource(std::string("doodads/planets/") + planetMesh, Resource::Type::Model3D));

    customMtr = renderMgr->NewMaterial();
    customMtr->SetDiffuseTexture(skinTex);
    customMtr->SetAmbientColor(Color4f(0.7f, 0.7f, 0.7f, 1.0f));
    customMtr->SetDiffuseColor(Color4f(0.9f, 0.9f, 0.9f, 0.5f));
    customMtr->SetSpecularColor(Color4f(0.5f, 0.5f, 0.5f, 1.0f));
    customMtr->SetShininess(10.0f);
    customMtr->SetName(Material::GetEnumeratedMaterialName());
    customMtr->SetupShaderProgram();

    uniformScale = planetScale;
    worldPosition = worldPos;
    rotationMatrix = Matrix4::rotationX(DegToRad(rotationDegs));
    withRings = hasRings;

    AABB planetBox = model3d->GetAABB();
    planetBox.mins *= boundsScale;
    planetBox.maxs *= boundsScale;
    planetBox.mins *= uniformScale;
    planetBox.maxs *= uniformScale;

    // Set up the physics body:
    constexpr float mass = 100000.0f;
    btTransform startTransform;
    startTransform.setFromOpenGLMatrix(ToFloatPtr(GetModelMatrix()));
    motionState = new MyMotionState(startTransform);
    SetupRigidBody(BSphere, mass, motionState, nullptr, false, false, &planetBox);
}

void Planet::OnUpdate(const Engine::GameTime & time)
{
    Doodad::OnUpdate(time);

    const Matrix4 modelXForm = Matrix4::translation(worldPosition) * rotationMatrix *
                               Matrix4::scale(Vector3(uniformScale, uniformScale, uniformScale));

    SetModelMatrix(modelXForm);
    game->physics->SetRigidBodyTransform(rigidBody, modelXForm);
}

void Planet::OnRender(const Engine::GameTime &)
{
    // Shouldn't even get here otherwise.
    assert(flags & EnableRender);
    assert(model3d != nullptr);

    const Matrix4 mvp = renderMgr->GetCurrentMVPMatrix();
    const Matrix4 mv = renderMgr->GetViewMatrix() * renderMgr->GetTopMatrix();

    if (customMtr != nullptr)
    {
        // Ringed planets need transparency.
        // This should be resolved by the material,
        // but that is not ready yet for the demo.
        if (withRings)
        {
            renderMgr->EnableSpriteBlendMode();
            model3d->DrawWithMaterial(customMtr, &mvp, &mv);
            renderMgr->DisableColorBlend();
        }
        else
        {
            model3d->DrawWithMaterial(customMtr, &mvp, &mv);
        }
    }
    else
    {
        model3d->DrawWithMVP(mvp, mv);
    }
}

} // namespace SpaceSim {}
