
// ================================================================================================
// -*- C++ -*-
// File: GamePhysicsBulletDebug.cpp
// Author: Guilherme R. Lampert
// Created on: 16/10/14
// Brief: Debugging facilities for the physics module.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GamePhysicsBullet.hpp"
#include "Game/LocalPlayerView.hpp"
using namespace Engine;

namespace SpaceSim
{

// ======================================================
// BulletDebugDrawAdapter:
// ======================================================

class BulletDebugDrawAdapter final
    : public btIDebugDraw
{
  public:

    void drawLine(const btVector3 & from, const btVector3 & to, const btVector3 & fromColor, const btVector3 & /* toColor */) override
    {
        // We don't have lines with two colors :(
        game->GetCurrentView()->GetDebugRenderer().AddLine(
        BtVector3ToVec3f(from), BtVector3ToVec3f(to), BtVector3ToColor4f(fromColor));
    }

    void drawLine(const btVector3 & from, const btVector3 & to, const btVector3 & color) override
    {
        game->GetCurrentView()->GetDebugRenderer().AddLine(
        BtVector3ToVec3f(from), BtVector3ToVec3f(to), BtVector3ToColor4f(color));
    }

    void drawSphere(const btVector3 & p, btScalar radius, const btVector3 & color) override
    {
        game->GetCurrentView()->GetDebugRenderer().AddSphere(
        BtVector3ToVec3f(p), BtVector3ToColor4f(color), radius);
    }

    void drawContactPoint(const btVector3 & pointOnB, const btVector3 & normalOnB, btScalar distance, int lifeTime, const btVector3 & color) override
    {
        game->GetCurrentView()->GetDebugRenderer().AddPoint(
        BtVector3ToVec3f(pointOnB), BtVector3ToColor4f(color), 20.0f, lifeTime);

        game->GetCurrentView()->GetDebugRenderer().AddVertexNormal(
        BtVector3ToVec3f(pointOnB), BtVector3ToVec3f(normalOnB), distance, lifeTime);
    }

    void draw3dText(const btVector3 & location, const char * textString) override
    {
        if (textString == nullptr)
        {
            return;
        }
        game->GetCurrentView()->GetDebugRenderer().Add3DLabel(textString, BtVector3ToVec3f(location), Color4f(1.0f));
    }

    void reportErrorWarning(const char * warningString) override
    {
        if (warningString == nullptr)
        {
            return;
        }

        // Remove line breaks, as the common log will break the line by default.
        std::string warn(warningString);
        std::replace(warn.begin(), warn.end(), '\n', ' ');

        common->WarningF("[Bullet Phys]: %s", warn.c_str());
    }

    void setDebugMode(int mode) override { debugMode = mode; }
    int getDebugMode() const override { return (debugMode); }

  private:

    // Initial debug draw mode:
    int debugMode = DBG_DrawWireframe;
};

// Singleton instance:
static BulletDebugDrawAdapter myBTDebugDrawAdapter;
btIDebugDraw * bulletDebugDraw = &myBTDebugDrawAdapter;

} // namespace SpaceSim {}
