
// ================================================================================================
// -*- C++ -*-
// File: SpaceCraft.cpp
// Author: Guilherme R. Lampert
// Created on: 15/10/14
// Brief: Space Craft/Ship specialization of GameEntity.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GamePhysicsBullet.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
#include "Game/SpaceCraft.hpp"
#include "Game/LocalPlayerView.hpp"
using namespace Engine;

namespace SpaceSim
{
namespace /* unnamed */
{

class MyMotionState final
    : public btMotionState
{
  public:
    btTransform currentTranform;

    MyMotionState(const btTransform & initialPosition)
        : currentTranform(initialPosition) {}

    void getWorldTransform(btTransform & worldTrans) const override { worldTrans = currentTranform; }
    void setWorldTransform(const btTransform & worldTrans) override { currentTranform = worldTrans; }

    Matrix4 getMat4() const
    {
        const btVector3 pos = currentTranform.getOrigin();
        const Matrix4 t = Matrix4::translation(BtVector3ToVector3(pos));

        const btQuaternion rot = currentTranform.getRotation();
        Quat q(rot.x(), rot.y(), rot.z(), rot.w());
        const Matrix4 r = Matrix4::rotation(q);

        return (t * r);
    }
};

} // namespace unnamed {}

// ======================================================
// Register with the Runtime Type System:
// ======================================================

RUNTIME_CLASS_DEFINITION(
SpaceSim::GameEntity,              /* superclass */
SpaceSim::SpaceCraft,              /* class name */
DefaultClassInstancer<SpaceCraft>, /* alloc with new() */
DefaultClassDeleter<SpaceCraft>)   /* free with delete() */

// ======================================================
// SpaceCraft:
// ======================================================

SpaceCraft::SpaceCraft()
    : GameEntity()
    , shieldRestoreRate(0.0)
    , shieldRestoreAmt(0.0)
    , uniformScale(1.0f)
    , myView(nullptr)
    , motionState(nullptr)
{
    SetFlags(GameEntity::EnableUpdate | GameEntity::EnableRender | GameEntity::IsInteractive);
}

SpaceCraft::SpaceCraft(const SpaceCraft & other)
    : GameEntity(other)
{
    // TODO
    assert(false && "SpaceCraft cloning not yet implemented!");
}

void SpaceCraft::OnUpdate(const GameTime & time)
{
    GameEntity::OnUpdate(time);

    // Shield restores over time.
    if ((shield < maxShield) && (shieldRestoreRate > 0.0))
    {
        shieldRestoreAmt += (time.deltaTime.seconds * shieldRestoreRate);
        if (shieldRestoreAmt >= 1.0)
        {
            shield += static_cast<int32_t>(shieldRestoreAmt);
            if (shield > maxShield)
            {
                shield = maxShield;
            }
            shieldRestoreAmt = 0.0;
        }
    }

    if (motionState != nullptr)
    {
        if (uniformScale != 1.0f)
        {
            modelMatrix = reinterpret_cast<MyMotionState *>(motionState)->getMat4() *
                          Matrix4::scale(Vector3(uniformScale, uniformScale, uniformScale));
        }
        else
        {
            modelMatrix = reinterpret_cast<MyMotionState *>(motionState)->getMat4();
        }
    }

    // Update the attached particle emitters:
    for (auto emitter : particleEmitters)
    {
        emitter->UpdateAllParticles(time);
    }
}

void SpaceCraft::OnRender(const GameTime & time)
{
    GameEntity::OnRender(time);

    // Draw for the opposing player(s).
    if (game->GetCurrentView() != myView)
    {
        if (statsLabel != nullptr)
        {
            statsLabel->Render();
        }
        if (targetIndicator != nullptr)
        {
            targetIndicator->Render();
        }
    }

    // Draw any particle effects attached to the model:
    for (auto emitter : particleEmitters)
    {
        // TODO Should stop rendering particles if ship is far enough from viewer!
        game->world->AddFrameParticleEmitter({ emitter, renderMgr->GetTopMatrix() });
    }
}

void SpaceCraft::AddEngineParticles(const Vector3 & pos, const Color4f & color)
{
    ConstTexturePtr particlePoint = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/particle_point.png"));
    assert(particlePoint != nullptr);

    ParticleRenderer * particleEmitter = new ParticleRenderer();
    SetupEngineEmitter(*particleEmitter, particlePoint, color);

    particleEmitter->SetEmitterOrigin(pos);
    particleEmitters.push_back(particleEmitter);
}

void SpaceCraft::SetupRigidBody(const float mass, const Vector3 & initialPosition)
{
    btTransform startTransform;
    startTransform.setIdentity();
    startTransform.setOrigin(ToBtVector3(initialPosition));
    motionState = new MyMotionState(startTransform);

    assert(model3d != nullptr);
    AABB aabb = model3d->GetAABB();
    aabb.mins *= uniformScale;
    aabb.maxs *= uniformScale;

    GameEntity::SetupRigidBody(BBox, mass, motionState, nullptr, false, false, &aabb);
}

void SpaceCraft::SetStatsLabel(std::unique_ptr<StatsLabel> label)
{
    statsLabel = std::move(label);
    statsLabel->owner = this;
}

void SpaceCraft::SetTargetIndicator(std::unique_ptr<TargetIndicator> indicator)
{
    targetIndicator = std::move(indicator);
}

void SpaceCraft::SetupEngineEmitter(ParticleRenderer & prt, const ConstTexturePtr & spriteTex, const Color4f & baseColor) const
{
    prt.SetMinMaxParticles(100, 500);
    prt.SetParticleReleaseAmount(100);
    prt.SetParticleReleaseInterval(10);
    prt.SetParticleMinSize(Vec2f(30, 30));
    prt.SetParticleMaxSize(Vec2f(60, 60));
    prt.SetParticleLifeCycle(10);
    prt.SetParticleBaseColor(baseColor);
    prt.SetVelocityVar(0.7f);
    prt.SetParticleSpriteTexture(spriteTex);
    prt.SetParticleFadeEffect(ParticleFadeEffect::SmoothFade);
    prt.AllocateParticles();
}

// ======================================================
// TargetIndicator:
// ======================================================

TargetIndicator::TargetIndicator()
    : sprite(nullptr)
    , dimensions(0.0f)
    , baseColor(1.0f)
    , positionOffset(Vector3Zero)
{
    // Set a common default.
    sprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/ship_selection.png"));
}

void TargetIndicator::Render()
{
    // Draw distance before culling (600m).
    constexpr float maxDrawDistSqr = (600.0f * 600.0f);

    assert(sprite != nullptr);
    const Camera & viewer = renderMgr->GetCameraRef();
    const Vector3 ownerPos = renderMgr->GetTopMatrix().getTranslation();
    const Vector3 bbPos = ownerPos + positionOffset;

    // Draw only if within distance.
    if (lengthSqr(bbPos - viewer.GetWorldPosition()) > maxDrawDistSqr)
    {
        return;
    }

    const Billboard bb = MakeBillboardFacingCameraPosition(viewer.GetUpVector(),
                                                           viewer.GetWorldPosition(),
                                                           bbPos, dimensions, baseColor);

    game->GetCurrentView()->GetBillboardRenderer().DrawBillboards(&bb, 1, sprite);
}

// ======================================================
// StatsLabel:
// ======================================================

StatsLabel::StatsLabel()
    : owner(nullptr)
    , barSprite(nullptr)
    , healthBarOffset(0.0f)
    , shieldBarOffset(0.0f)
    , textOffset(0.0f)
{
    // Set a common default:
    barSprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/bar.png"));
}

void StatsLabel::Render()
{
    // Draw distance before culling (900m).
    constexpr float maxDrawDistSqr = (900.0f * 900.0f);

    // Height is fixed. Width will be the entity's health/shield amount.
    // Max width of the bar is clamped.
    constexpr int barHeight = 15;
    constexpr int maxBarWidth = 125;

    assert(owner != nullptr);
    assert(barSprite != nullptr);

    const Camera & viewer = renderMgr->GetCameraRef();
    const Vector3 ownerPos = renderMgr->GetTopMatrix().getTranslation();

    // Draw only if within distance.
    const float distSqr = lengthSqr(ownerPos - viewer.GetWorldPosition());
    if (distSqr > maxDrawDistSqr)
    {
        return;
    }

    Vec2f pos2d;
    Vector3 windowCoordinates;

    Project(ownerPos[0], ownerPos[1], ownerPos[2],
            viewer.GetViewMatrix(), viewer.GetProjectionMatrix(),
            renderMgr->GetFramebufferRect(), windowCoordinates);

    pos2d.x = windowCoordinates[0];
    pos2d.y = windowCoordinates[1];

    // Distance from viewer/player:
    char text[128];
    SNPrintF(text, "%dm", static_cast<int>(std::sqrt(distSqr)));
    game->GetCurrentView()->GetTextRenderer().DrawText(pos2d + textOffset, Color4f(1.0f), TextAlign::Left, text);

    // Object name:
    game->GetCurrentView()->GetTextRenderer().DrawText(Vec2f(pos2d.x - textOffset.x, pos2d.y - (textOffset.y * 1.5f)),
                                                       Color4f(1.0f), TextAlign::Center, owner->GetName().c_str());

    // Draw the health bar:
    const int hBarWidth = MapRange(owner->GetHealth(), 0, owner->GetMaxHealth(), 0, maxBarWidth);
    if (hBarWidth > 0)
    {
        game->GetCurrentView()->GetSpriteBatch().DrawQuad(pos2d.x + healthBarOffset.x, pos2d.y + healthBarOffset.y,
                                                          hBarWidth, barHeight, Color4f(1.0f, 0.0f, 0.0f, 1.0f),
                                                          barSprite, false, nullptr, SpriteLayer::Layer0);
    }

    // Draw the shield bar:
    const int sBarWidth = MapRange(owner->GetShield(), 0, owner->GetMaxShield(), 0, maxBarWidth);
    if (sBarWidth > 0)
    {
        game->GetCurrentView()->GetSpriteBatch().DrawQuad(pos2d.x + shieldBarOffset.x, pos2d.y + shieldBarOffset.y,
                                                          sBarWidth, barHeight, Color4f(0.1f, 0.4f, 1.0f, 1.0f),
                                                          barSprite, false, nullptr, SpriteLayer::Layer0);
    }
}

} // namespace SpaceSim {}
