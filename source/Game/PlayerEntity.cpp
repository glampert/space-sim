
// ================================================================================================
// -*- C++ -*-
// File: PlayerEntity.cpp
// Author: Guilherme R. Lampert
// Created on: 16/10/14
// Brief: Specialized game entity representing the local player.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GamePhysicsBullet.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
#include "Game/SpaceCraft.hpp"
#include "Game/LocalPlayerView.hpp"
#include "Game/PlayerEntity.hpp"
using namespace Engine;

namespace SpaceSim
{
namespace /* unnamed */
{

const FixedArray<Color4f, 2> playerColors = {
    Color4f(1.0f, 0.0f, 0.0f, 1.0f),
    Color4f(0.0f, 0.0f, 1.0f, 1.0f)
};

const FixedArray<Vector3, 2> exhaustDirection = {
    Vector3(0.0f, 0.0f, -5.0f),
    Vector3(-5.0f, 0.0f, 0.0f)
};

float Lerpf(const float t, const float a, const float b)
{
    return (b - a) * t + a;
}

Weapon * CreateWeapon(const Vector3 & positionOffset, const int playerId)
{
    Weapon * weap = new Weapon();
    weap->fireCoolDownTimeMs = 200;
    weap->lastTimeFiredMs = 0;
    weap->projectileRangeMeters = 300;
    weap->projectileDamage = 10;
    weap->projectileVelocityMetersPerSec = 50;
    weap->aimReticleYOffset = 60.0f;
    weap->aimReticleSize = Vec2f(70.0f, 50.0f);
    weap->positionOffset = positionOffset + Vector3(5.0f, 0.0f, 0.0f);
    weap->aimReticleSprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/aim_reticle_2.png"));
    ConstTexturePtr sprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/particle_point.png"));
    weap->SetupParticleEmitters(
    /* particleSprite     = */ sprite,
    /* baseColor          = */ playerColors[playerId & 1],
    /* prtMinParticles    = */ 100,
    /* prtMaxParticles    = */ 500,
    /* prtReleaseAmount   = */ 100,
    /* prtReleaseInterval = */ 10,
    /* prtMinParticleSize = */ 30,
    /* prtMaxParticleSize = */ 60,
    /* prtLifeCicle       = */ 10,
    /* prtVelocity        = */ 10.0f,
    /* prtVelocityVar     = */ 0.7f);
    return weap;
}

} // namespace unnamed {}

// ======================================================
// Register with the Runtime Type System:
// ======================================================

RUNTIME_CLASS_DEFINITION(
SpaceSim::SpaceCraft,                /* superclass */
SpaceSim::PlayerEntity,              /* class name */
DefaultClassInstancer<PlayerEntity>, /* alloc with new() */
DefaultClassDeleter<PlayerEntity>)   /* free with delete() */

// ======================================================
// PlayerEntity:
// ======================================================

PlayerEntity::PlayerEntity()
    : SpaceCraft()
    , currCameraOrient(Quat::identity())
    , offsetXForm(Matrix4::identity())
{
}

PlayerEntity::PlayerEntity(const PlayerEntity & other)
    : SpaceCraft(other)
{
    // TODO
    assert(false && "PlayerEntity cloning not yet implemented!");
}

void PlayerEntity::InitPlayer1()
{
    const int playerId = myView->GetViewId();

    SetModel3D("ships/orca_tactical_mk1.dae");

    AddEngineParticles(Vector3(-5.0f, 5.3f, 6.9f), playerColors[playerId & 1]);
    AddEngineParticles(Vector3(-5.0f, 5.3f, -6.8f), playerColors[playerId & 1]);

    // We can't use the cheap player view shader if drawing split-screen.
    // The particles would look wrong for the opposing player.
    if (game->splitScreenGame)
    {
        for (auto emitter : particleEmitters)
        {
            emitter->SetParticleMinSize(Vec2f(30, 30));
            emitter->SetParticleMaxSize(Vec2f(60, 60));
        }
    }
    else
    {
        // Custom particle shader:
        playerParticleShader = checked_pointer_cast<const ShaderProgram>(
                resourceMgr->FindOrLoadResource("particle_player_view",
                Resource::Type::ShaderProgram));

        for (auto emitter : particleEmitters)
        {
            emitter->SetCustomShader(playerParticleShader);
        }
    }

    // Flag as the local player:
    name = "Player 1";
    GetFlags() |= GameEntity::IsLocalPlayer;
    GetFlags() |= GameEntity::IsHostile; // To other players

    // Position the player's ship:
    const Matrix4 tt = Matrix4::translation(Vector3(0.0f, -14.0f, -40.0f));
    const Matrix4 rx = Matrix4::rotationX(DegToRad(1.0f));
    const Matrix4 ry = Matrix4::rotationY(DegToRad(90.0f));
    offsetXForm = tt * rx * ry;
    modelMatrix = offsetXForm;

    // Initial stats:
    maxHealth = 100;
    maxShield = 100;
    maxBooster = 1000;
    health = maxHealth;
    shield = maxShield;
    booster = maxBooster;
    SetShieldRestoreRate(10);

    // Weapons:
    weapons.push_back(CreateWeapon(Vector3(-5.0f, 5.3f, 6.9f), playerId)); // R
    weapons.back()->owner = this;
    weapons.push_back(CreateWeapon(Vector3(-5.0f, 5.3f, -6.8f), playerId)); // L
    weapons.back()->owner = this;

    // Physics:
    float mass = 0;
    btTransform startTransform;
    startTransform.setIdentity();
    motionState = new btDefaultMotionState(startTransform);
    GameEntity::SetupRigidBody(BBox, mass, motionState, nullptr, true, true);

    // A stats label (visible to other players):
    statsLabel.reset(new StatsLabel());
    statsLabel->owner = this;
    statsLabel->barSprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/bar.png"));
    statsLabel->healthBarOffset = Vec2f(-35.0f, -95.0f);
    statsLabel->shieldBarOffset = Vec2f(-35.0f, -110.0f);
    statsLabel->textOffset = Vec2f(-25.0f, -55.0f);

    // A target indicator (visible to other players):
    targetIndicator.reset(new TargetIndicator());
    targetIndicator->sprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/ship_selection.png"));
    targetIndicator->dimensions = Vec2f(25.0f);
    targetIndicator->baseColor = Color4f(0.8f, 0.2f, 0.0f, 1.0f);
    targetIndicator->positionOffset = Vector3(0.0f, 3.5f, 0.0f);
}

void PlayerEntity::InitPlayer2()
{
    const int playerId = myView->GetViewId();

    SetModel3D("ships/cobra.dae");

    // Left-side engines:
    AddEngineParticles(Vector3(7.1f, 3.5f, -13.0f), playerColors[playerId & 1]);
    AddEngineParticles(Vector3(3.5f, 5.2f, -14.0f), playerColors[playerId & 1]);

    // Right-side engines:
    AddEngineParticles(Vector3(-7.6f, 3.5f, -13.0f), playerColors[playerId & 1]);
    AddEngineParticles(Vector3(-4.0f, 5.2f, -14.0f), playerColors[playerId & 1]);

    // We can't use the cheap player view shader if drawing split-screen.
    // The particles would look wrong for the opposing player.
    if (game->splitScreenGame)
    {
        for (auto emitter : particleEmitters)
        {
            emitter->SetParticleMinSize(Vec2f(30, 30));
            emitter->SetParticleMaxSize(Vec2f(60, 60));
        }
    }
    else
    {
        // Custom particle shader:
        playerParticleShader = checked_pointer_cast<const ShaderProgram>(
                resourceMgr->FindOrLoadResource("particle_player_view",
                Resource::Type::ShaderProgram));

        for (auto emitter : particleEmitters)
        {
            emitter->SetCustomShader(playerParticleShader);
        }
    }

    // Flag as the local player:
    name = "Player 2";
    GetFlags() |= GameEntity::IsLocalPlayer;
    GetFlags() |= GameEntity::IsHostile; // To other players

    // Position the player's ship:
    const Matrix4 tt = Matrix4::translation(Vector3(0.0f, -14.0f, -40.0f));
    const Matrix4 ry = Matrix4::rotationY(DegToRad(180.0f));
    offsetXForm = tt * ry;
    modelMatrix = offsetXForm;

    // Initial stats:
    maxHealth = 100;
    maxShield = 100;
    maxBooster = 1000;
    health = maxHealth;
    shield = maxShield;
    booster = maxBooster;
    SetShieldRestoreRate(10);

    // Weapons:
    weapons.push_back(CreateWeapon(Vector3(-1.5f, 3.5f, 7.0f), playerId)); // R
    weapons.back()->owner = this;
    weapons.push_back(CreateWeapon(Vector3(-8.7f, 3.5f, 7.0f), playerId)); // L
    weapons.back()->owner = this;

    // Physics:
    float mass = 0;
    btTransform startTransform;
    startTransform.setIdentity();
    motionState = new btDefaultMotionState(startTransform);
    GameEntity::SetupRigidBody(BBox, mass, motionState, nullptr, true, true);

    // A stats label (visible to other players):
    statsLabel.reset(new StatsLabel());
    statsLabel->owner = this;
    statsLabel->barSprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/bar.png"));
    statsLabel->healthBarOffset = Vec2f(-35.0f, -95.0f);
    statsLabel->shieldBarOffset = Vec2f(-35.0f, -110.0f);
    statsLabel->textOffset = Vec2f(-25.0f, -55.0f);

    // A target indicator (visible to other players):
    targetIndicator.reset(new TargetIndicator());
    targetIndicator->sprite = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/ship_selection.png"));
    targetIndicator->dimensions = Vec2f(25.0f);
    targetIndicator->baseColor = Color4f(0.8f, 0.2f, 0.0f, 1.0f);
    targetIndicator->positionOffset = Vector3(0.0f, -2.0f, 0.0f);
}

void PlayerEntity::OnUpdate(const GameTime & time)
{
    SpaceCraft::OnUpdate(time);

    assert(myView != nullptr);
    const FlightCamera & camera = myView->GetCameraRef();

    // Adjust according to booster scale/ship speed:
    const float amount = ValueNormalize(camera.GetForwardSpeed(), -camera.GetMaxForwardSpeed(), +camera.GetMaxForwardSpeed());
    for (const auto emitter : particleEmitters)
    {
        emitter->SetParticleReleaseAmount(Lerpf(amount, 1.0f, 1000.0f));
        emitter->SetParticleVelocity(exhaustDirection[myView->GetViewId() & 1] * Lerpf(amount, 1.0f, 2.0f));
    }

    const Vector3 cameraPosition = camera.GetWorldPosition();
    const Quat newCameraOrient = camera.GetOrientation();

    // Delayed rotation in relation to the camera:
    const float t = time.deltaTime.seconds * 1.1f;
    currCameraOrient = slerp(t, currCameraOrient, newCameraOrient);
    const Matrix4 cameraXForm = Matrix4::translation(cameraPosition) * Matrix4::rotation(currCameraOrient);

    modelMatrix = cameraXForm * offsetXForm;

    btTransform xform;
    xform.setFromOpenGLMatrix(ToFloatPtr(modelMatrix));
    reinterpret_cast<btDefaultMotionState *>(motionState)->setWorldTransform(xform);

    WeaponsUpdate(time);
}

void PlayerEntity::WeaponsUpdate(const GameTime & time)
{
    bool shouldFire;

    if (myView->GetViewId() == 1)
    {
        shouldFire = myView->GetXbController().IsButtonDown(XB360Controller::Button::RTrigger);
    }
    else
    {
        shouldFire = myView->GetMouseInput().IsButtonDown(MouseButton::Left);
    }

    if (shouldFire)
    {
        for (auto weap : weapons)
        {
            const Matrix4 mOffset = modelMatrix * Matrix4::translation(weap->positionOffset);
            const Vector3 pos = mOffset.getTranslation();
            const Vector3 dir = myView->GetCameraRef().GetDirectionVector();
            weap->Fire(time, pos, dir);
        }
    }

    for (auto weap : weapons)
    {
        weap->OnUpdate(time);
    }
}

const Weapon & PlayerEntity::GetMainWeapon() const
{
    return *weapons.front();
}

Weapon & PlayerEntity::GetMainWeapon()
{
    return *weapons.front();
}

} // namespace SpaceSim {}
