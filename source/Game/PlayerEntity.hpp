
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: PlayerEntity.hpp
// Author: Guilherme R. Lampert
// Created on: 16/10/14
// Brief: Specialized game entity representing the local player.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

// Dependencies:
#include "Game/SpaceCraft.hpp"

namespace SpaceSim
{

class LocalPlayerView;

// ======================================================
// PlayerEntity:
// ======================================================

class PlayerEntity final
    : public SpaceCraft
{
    RUNTIME_CLASS_DECL(PlayerEntity)

  public:

    PlayerEntity();
    PlayerEntity(const PlayerEntity & other);

    // No direct copy. Use Clone().
    PlayerEntity & operator=(const PlayerEntity &) = delete;

    void InitPlayer1();
    void InitPlayer2();
    void OnUpdate(const Engine::GameTime & time) override;

    const Weapon & GetMainWeapon() const;
    Weapon & GetMainWeapon();

  private:

    void WeaponsUpdate(const Engine::GameTime & time);

    // Player uses a different particle shader.
    // This was needed for our older hackish approach to the camera handling.
    // Now the hack is gone, but this shader saves a few instructions, so
    // lets leave it here, since it is working OK, and faster! ...
    Engine::ConstShaderProgramPtr playerParticleShader;

    // Misc:
    Engine::Quat currCameraOrient;
    Engine::Matrix4 offsetXForm;
};

} // namespace SpaceSim {}
