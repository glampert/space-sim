
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: NetGamePacket.hpp
// Author: Guilherme R. Lampert
// Created on: 16/11/14
// Brief: Network packet type used by the MP game.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include <vector>
#include <unordered_map>

namespace SpaceSim
{

// ======================================================
// NetGamePacket:
// ======================================================

// This class is currently unused...
class NetGamePacket
{
  public:

    NetGamePacket() = default;

    void Send(Engine::Socket & socket) const;
    void Receive(Engine::Socket & socket);

    bool Read(const uint32_t key, int32_t & n) const;
    void Write(const uint32_t key, const int32_t n);

    bool Read(const uint32_t key, std::string & s) const;
    void Write(const uint32_t key, const std::string & s);

    bool Read(const uint32_t key, Engine::Matrix4 & m) const;
    void Write(const uint32_t key, const Engine::Matrix4 & m);

    void Clear();

  private:

    using ByteBuffer = std::vector<uint8_t>;
    using DataMap    = std::unordered_map<uint32_t, uint32_t>;

    DataMap dataMap;
    ByteBuffer byteBuffer;
    uint32_t writePos = 0;

    DISABLE_COPY_AND_ASSIGN(NetGamePacket);
};

} // namespace SpaceSim {}
