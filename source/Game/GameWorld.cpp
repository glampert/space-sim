
// ================================================================================================
// -*- C++ -*-
// File: GameWorld.cpp
// Author: Guilherme R. Lampert
// Created on: 14/10/14
// Brief: Instance of the game world/level/scene.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
#include "Game/PlayerEntity.hpp"
#include "Game/LocalPlayerView.hpp"
using namespace Engine;

namespace SpaceSim
{

static_assert(sizeof(GameEntityHandle) == 2, "We don't need more than 2 bytes for a GameEntityHandle!");
static_assert(GameWorld::MaxEntities <= UINT16_MAX, "Max entity index wouldn't fit in a GameEntityHandle!");

// ======================================================
// GameWorld:
// ======================================================

GameWorld::GameWorld()
    : freeSlotsAvailable(MaxEntities - 1)
    , linkedEntities(0)
    , numVisibleEntities(0)
    , numDamageFXEmittersInUse(0)
{
    common->PrintF("New GameWorld instance created. MaxEntities: %d", MaxEntities);

    // Set each entry to an index (in reverse order since
    // the last entries in freeSlots[] are used first):
    for (int i = MaxEntities - 1, j = 0; i >= 0; --i, ++j)
    {
        freeSlots[i] = static_cast<GameEntityHandle>(j);
    }

    // Set each entry to null:
    ZeroArray(gameEntities);
    ZeroArray(visibleEntities);

    SetupDamageFXEmitters();
}

GameWorld::~GameWorld()
{
    common->PrintF("GameWorld instance destroyed...");

    // Free all currently linked entities:
    int freedCount = 0;
    for (int e = 0; e < MaxEntities; ++e)
    {
        if (gameEntities[e] != nullptr)
        {
            delete gameEntities[e];
            ++freedCount;
        }
    }

    common->PrintF("Freed %d game entities.", freedCount);
}

GameWorld * GameWorld::Create()
{
    return new GameWorld();
}

void GameWorld::SetupDamageFXEmitters()
{
    damageFXEmitterPool.reset(new ParticleRenderer[damageFXEmitters.size()]);

    ConstTexturePtr particleExplosionSprite = checked_pointer_cast<const Texture>(
    resourceMgr->FindOrLoadResource("sprites/particle_fire.png"));

    for (size_t p = 0; p < damageFXEmitters.size(); ++p)
    {
        damageFXEmitterPool[p].SetOneTimeEmission(true);
        damageFXEmitterPool[p].SetMinMaxParticles(10, 100);
        damageFXEmitterPool[p].SetParticleReleaseAmount(200);
        damageFXEmitterPool[p].SetParticleReleaseInterval(10);
        damageFXEmitterPool[p].SetParticleMinSize(Vec2f(100));
        damageFXEmitterPool[p].SetParticleMaxSize(Vec2f(150));
        damageFXEmitterPool[p].SetParticleLifeCycle(5);
        damageFXEmitterPool[p].SetParticleBaseColor(Color4f(1.0f));
        damageFXEmitterPool[p].SetVelocityVar(10.0f);
        damageFXEmitterPool[p].SetParticleSpriteTexture(particleExplosionSprite);
        damageFXEmitterPool[p].SetParticleFadeEffect(ParticleFadeEffect::SmoothFade);
        damageFXEmitterPool[p].AllocateParticles();

        damageFXEmitters[p] = &damageFXEmitterPool[p];
    }
}

void GameWorld::LoadLevel(const std::string & levelName)
{
    // Demo levels for a playable prototype.
    // We would not hardcode these values otherwise.
    //
    extern void DemoLevel_Mars(GameWorld & world);
    extern void DemoLevel_GreenNebula(GameWorld & world);
    extern void DemoLevel_Jupiter(GameWorld & world);

    if (levelName == "mars")
    {
        DemoLevel_Mars(*this);
        skybox.SetTexture(levelName);
    }
    else if (levelName == "green_nebula")
    {
        DemoLevel_GreenNebula(*this);
        skybox.SetTexture(levelName);
    }
    else if (levelName == "jupiter")
    {
        DemoLevel_Jupiter(*this);
        skybox.SetTexture(levelName);
    }
    else
    {
        common->FatalErrorF("Level '%s' doesn't exist!", levelName.c_str());
    }
}

void GameWorld::OnUpdate(const GameTime & time)
{
    // Make sure no trash was left behind by some reckless user...
    assert(frameEmitters.empty());

    // Update all linked entities that are updatable:
    for (int e = 0; e < MaxEntities; ++e)
    {
        GameEntity * entity = gameEntities[e];
        if ((entity != nullptr) && (entity->GetFlags() & GameEntity::EnableUpdate))
        {
            entity->OnUpdate(time);
        }
    }

    // Update the pool of particle renderers used by damage spawns.
    // Move the finished ones to the end of the array and decrement the used count.
    for (size_t p = 0; p < numDamageFXEmittersInUse; ++p)
    {
        ParticleRenderer * prt = damageFXEmitters[p];

        if (prt->IsOneTimeEmitterFinished())
        {
            if (p != (numDamageFXEmittersInUse - 1))
            {
                std::swap(damageFXEmitters[p], damageFXEmitters[numDamageFXEmittersInUse - 1]);
            }
            numDamageFXEmittersInUse--;
            continue;
        }

        // Draw the still active ones:
        prt->UpdateAllParticles(time);
        AddFrameParticleEmitter({ prt, Matrix4::identity() });
    }
}

void GameWorld::OnRender(const GameTime & time)
{
    const Frustum & frustum = renderMgr->GetCameraRef().GetFrustum();

    // Render all linked entities that are drawable and visible to the local player:
    numVisibleEntities = 0;
    for (int e = 0; e < MaxEntities; ++e)
    {
        GameEntity * entity = gameEntities[e];
        if ((entity != nullptr) && (entity->GetFlags() & GameEntity::EnableRender))
        {
            // InFrustum() must be called after OnPreRender()
            // because the entity transform might be composite.
            //
            entity->OnPreRender();
            if (entity->InFrustum(frustum))
            {
                if ((entity->GetFlags() & GameEntity::IsInteractive) &&
                    (numVisibleEntities < visibleEntities.size()))
                {
                    visibleEntities[numVisibleEntities++] = entity;
                }

                entity->OnRender(time);
            }
            entity->OnPostRender();
        }
    }

    // Picking for the player's aim reticle:
    if (numVisibleEntities != 0)
    {
        Picking(visibleEntities.data(), numVisibleEntities);
    }

    // Once done with the entities, we can draw the skybox background:
    const Matrix4 mvpMatrix = renderMgr->GetViewProjectionMatrix() * Matrix4::translation(renderMgr->GetCameraRef().GetWorldPosition());
    skybox.DrawWithMVP(mvpMatrix);

    // And lastly, draw the particle emitters for this frame but don't
    // clear the batch yet, in case there are other views to render.
    RenderFrameParticleEmitters(time);
}

void GameWorld::AddFrameParticleEmitter(const FrameParticleEmitter & frameEmitter)
{
    assert(frameEmitter.particleRenderer != nullptr);
    frameEmitters.push_back(frameEmitter);
}

void GameWorld::RenderFrameParticleEmitters(const GameTime & time)
{
    if (!frameEmitters.empty())
    {
        const Vector3 cameraWorldPos = renderMgr->GetCameraRef().GetWorldPosition();
        const Matrix4 viewProjMatrix = renderMgr->GetViewProjectionMatrix();

        renderMgr->EnableSpriteBlendMode();
        renderMgr->DisableDepthWrite();

        for (auto & emitter : frameEmitters)
        {
            emitter.particleRenderer->DrawAllParticles(time, emitter.modelMatrix, viewProjMatrix, cameraWorldPos);
        }

        renderMgr->EnableDepthWrite();
        renderMgr->DisableColorBlend();
    }
}

void GameWorld::ClearFrameParticleEmitters()
{
    frameEmitters.clear();
}

GameEntityHandle GameWorld::CreateEntity(const char * className)
{
    assert(className != nullptr && *className != '\0');
    return LinkEntity(checked_cast<GameEntity *>(Class::CreateInstance(className)));
}

GameEntityHandle GameWorld::CreateEntity(const TypeInfo & type)
{
    return LinkEntity(checked_cast<GameEntity *>(Class::CreateInstance(type.GetClassHashIndex())));
}

GameEntityHandle GameWorld::LinkEntity(GameEntity * entity)
{
    if (entity == nullptr)
    {
        common->FatalErrorF("Unable to link game entity! (null pointer on LinkEntity()!)");
    }

    // Currently, we throw a fatal error if the pool is depleted.
    if (freeSlotsAvailable == 0)
    {
        common->FatalErrorF("No more free entities! Set MaxEntities to a larger value!");
    }

    // Fetch and validate an index:
    assert(freeSlotsAvailable < MaxEntities);
    const int freeIdx = freeSlots[freeSlotsAvailable];
    assert(freeIdx != InvalidGameEntityHandle);
    assert(freeIdx >= 0 && freeIdx < MaxEntities);

    // Remove from free list:
    freeSlots[freeSlotsAvailable] = InvalidGameEntityHandle;

    // Update counters:
    --freeSlotsAvailable;
    ++linkedEntities;

    // Save the entity pointer:
    assert(gameEntities[freeIdx] == nullptr);
    gameEntities[freeIdx] = entity;

    // Return index as the handle:
    return static_cast<GameEntityHandle>(freeIdx);
}

void GameWorld::DestroyEntity(GameEntityHandle & entityHandle)
{
    // Validate the handle:
    assert(entityHandle != InvalidGameEntityHandle);
    assert(entityHandle >= 0 && entityHandle < MaxEntities);

    // Validate the pool:
    assert(gameEntities[entityHandle] != nullptr && "Entity already removed!");
    assert(freeSlotsAvailable >= 0 && freeSlotsAvailable < MaxEntities);
    assert(linkedEntities > 0 && linkedEntities <= MaxEntities);

    // Delete and remove the object:
    GameEntity::DestroyInstance(gameEntities[entityHandle]);
    gameEntities[entityHandle] = nullptr;

    // Update counters:
    ++freeSlotsAvailable;
    --linkedEntities;

    // Push the freed index to the end of the free list:
    assert(freeSlots[freeSlotsAvailable] == InvalidGameEntityHandle);
    freeSlots[freeSlotsAvailable] = entityHandle;

    // Invalidate the handle reference:
    entityHandle = InvalidGameEntityHandle;
}

GameEntity * GameWorld::GetEntityPointer(const GameEntityHandle entityHandle) const
{
    // Validate the handle:
    assert(entityHandle != InvalidGameEntityHandle);
    assert(entityHandle >= 0 && entityHandle < MaxEntities);

    // Constant time lookup:
    assert(gameEntities[entityHandle] != nullptr && "Invalid handle!");
    return gameEntities[entityHandle];
}

GameEntity * GameWorld::FindIntersection(const Vector3 & point, const uint32_t entityFlags, const GameEntity * exclude) const
{
    for (int e = 0; e < MaxEntities; ++e)
    {
        GameEntity * entity = gameEntities[e];
        if ((entity != nullptr) &&
            (entity != exclude) &&
            (entity->GetFlags() & entityFlags) &&
            entity->IntersectsPoint(point))
        {
            return entity;
        }
    }
    return nullptr;
}

ParticleRenderer * GameWorld::GetDamageEffectParticleEmitter()
{
    if (numDamageFXEmittersInUse == damageFXEmitters.size())
    {
        return nullptr; // We have no more emitter to recycle this frame!
    }
    return damageFXEmitters[numDamageFXEmittersInUse++];
}

void GameWorld::DumpEntityTables() const
{
    common->PrintF("------ Entity pointer table: ------");
    common->PrintF("Entity table size: %d", MaxEntities);

    for (int e = 0; e < MaxEntities; ++e)
    {
        const GameEntity * entity = gameEntities[e];
        common->PrintF("[%04d] > ptr: %p, name: %s", e, entity, ((entity != nullptr) ? entity->GetName().c_str() : "<NULL>"));
    }

    common->PrintF("---- Entity indirection table: ----");
    common->PrintF("Free slots available...: %d", freeSlotsAvailable);
    common->PrintF("Linked entities........: %d", linkedEntities);

    for (int e = 0; e < MaxEntities; ++e)
    {
        common->PrintF("[%04d] > idx: %d", e, ((freeSlots[e] != InvalidGameEntityHandle) ? freeSlots[e] : -1));
    }

    common->PrintF("-----------------------------------");
}

int GameWorld::GetNumAliveEntities() const
{
    return linkedEntities;
}

int GameWorld::GetNumVisibleEntities() const
{
    return numVisibleEntities;
}

void GameWorld::Picking(GameEntity ** visibleEntities, const size_t numVisibleEntities)
{
    PlayerEntity & playerEntity = game->GetCurrentView()->GetPlayerEntity();
    Weapon & weapon = playerEntity.GetMainWeapon();

    const float x = weapon.aimReticlePos.x;
    const float y = weapon.aimReticlePos.y;
    LineSegment lineSeg;

    // Do both un-projections for z-near (0) and z-far (1):
    UnProject(x, y, 0.0f, renderMgr->GetViewMatrix(), renderMgr->GetProjectionMatrix(), renderMgr->GetFramebufferRect(), lineSeg.start);
    UnProject(x, y, 1.0f, renderMgr->GetViewMatrix(), renderMgr->GetProjectionMatrix(), renderMgr->GetFramebufferRect(), lineSeg.end);

    // Now test the line segment with every entity in view
    // to find out if any is intersecting this line. Save
    // the closest intersection.
    const GameEntity * closestIntersection = nullptr;
    float shortestDist = std::numeric_limits<float>::max();
    for (size_t e = 0; e < numVisibleEntities; ++e)
    {
        if ((visibleEntities[e] != &playerEntity) && visibleEntities[e]->IntersectsLine(lineSeg))
        {
            const float camDist = visibleEntities[e]->GetDistanceFromCamera();
            if (camDist < shortestDist)
            {
                shortestDist = camDist;
                closestIntersection = visibleEntities[e];
            }
        }
    }

    // Color the aim reticle accordingly:
    if (closestIntersection != nullptr)
    {
        const uint32_t flags = closestIntersection->GetFlags();
        if (flags & GameEntity::IsFriendly)
        {
            weapon.aimReticleColor = Color4f(0.2f, 0.8f, 0.0f, 1.0f); // Green
        }
        else if (flags & GameEntity::IsHostile)
        {
            weapon.aimReticleColor = Color4f(0.8f, 0.2f, 0.0f, 1.0f); // Red
        }
        else // Neutral
        {
            weapon.aimReticleColor = Color4f(1.0f); // White
        }
    }
    else
    {
        weapon.aimReticleColor = Color4f(1.0f);
    }
}

} // namespace SpaceSim {}
