
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: GamePhysics.hpp
// Author: Guilherme R. Lampert
// Created on: 14/10/14
// Brief: Interface for the game physics system.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace SpaceSim
{

// ======================================================
// Opaque Handles and helper types:
// ======================================================

// Opaque handle to a implementation defined RigidBody type:
using RigidBodyHandle = uintptr_t;
constexpr RigidBodyHandle InvalidRigidBodyHandle = 0;

// Params you can set when creating a new rigid body:
struct RigidBodyConstructionParams;

// ======================================================
// GamePhysics:
// ======================================================

class GamePhysics
{
  public:

    // Factory method:
    static GamePhysics * Create();

    // Frame updates:
    virtual void OnUpdate(const Engine::GameTime & time) = 0;
    virtual void OnRender(const Engine::GameTime & time) = 0;

    // Rigid body creation and destruction.
    // The Physics System will retain ownership of the rigid body
    // until it is destroyed with DestroyRigidBody() or until the
    // GamePhysics instance is itself destroyed.
    virtual RigidBodyHandle CreateRigidBody(const RigidBodyConstructionParams & params) = 0;
    virtual void DestroyRigidBody(RigidBodyHandle & rigidBodyHandle) = 0;

    // RigidBody accessors:
    virtual void SetRigidBodyTransform(RigidBodyHandle rigidBodyHandle, const Engine::Matrix4 & xform) = 0;
    virtual Engine::AABB GetCollisionAABB(RigidBodyHandle rigidBodyHandle) = 0;
    virtual void GetCollisionSphere(RigidBodyHandle rigidBodyHandle, Engine::Vector3 & center, float & radius) = 0;

    // Reset the physics system. Destroys all rigid bodies and collision shapes.
    virtual void FullReset() = 0;

    virtual ~GamePhysics() = default;
};

} // namespace SpaceSim {}
