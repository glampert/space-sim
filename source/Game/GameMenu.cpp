
// ================================================================================================
// -*- C++ -*-
// File: GameMenu.cpp
// Author: Guilherme R. Lampert
// Created on: 06/12/14
// Brief: Minimal menus for the off-game navigation.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GameMenu.hpp"
using namespace Engine;

namespace SpaceSim
{

// ======================================================
// Button:
// ======================================================

Button::Button()
    : btnRect(0)
    , btnColor(1.0f)
    , textColor(0.9f)
    , textOffset(0)
    , onClick([]() -> bool { return false; })
    , parentMenu(nullptr)
{
}

void Button::Draw() const
{
    assert(parentMenu != nullptr);
    parentMenu->spriteBatch.DrawQuad(btnRect.x, btnRect.y, btnRect.width, btnRect.height,
                                     btnColor, parentMenu->buttonSprite, false, nullptr,
                                     SpriteLayer::Layer1);

    if (!textLabel.empty())
    {
        parentMenu->textRenderer.DrawText(Vec2f(btnRect.x + textOffset.x, btnRect.y + textOffset.y),
                                          textColor, TextAlign::Left, textLabel.c_str());
    }
}

bool Button::MouseClickEvent(const Point & where)
{
    if (RectContainsPoint(btnRect, where))
    {
        return onClick();
    }
    return false;
}

// ======================================================
// GameMenu:
// ======================================================

GameMenu::GameMenu()
    : backgroundSprite(checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/menu_bg.png")))
    , buttonSprite(checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/button.png")))
    , textFont(checked_pointer_cast<const Font>(resourceMgr->FindOrLoadResource("coalition.fnt")))
    , textRenderer(*textFont, spriteBatch, SpriteLayer::Layer2)
{
}

void GameMenu::AddButton(const Button & btn)
{
    buttons.push_back(btn);
    buttons.back().parentMenu = this;
}

bool GameMenu::OnAppEvent(const AppEvent & event)
{
    mouseInput.OnAppEvent(event);

    if (event.type != AppEvent::Type::MouseBtnClick)
    {
        return false;
    }

    for (auto & btn : buttons)
    {
        int mx = static_cast<int>(mouseInput.GetCursorPosition().x);
        int my = static_cast<int>(mouseInput.GetCursorPosition().y);

        if ((renderMgr->GetWindowDimensions().width != renderMgr->GetFramebufferDimensions().width) ||
            (renderMgr->GetWindowDimensions().height != renderMgr->GetFramebufferDimensions().height))
        {
            mx = MapRange<int>(mx, 0, renderMgr->GetWindowDimensions().width, 0, renderMgr->GetFramebufferDimensions().width);
            my = MapRange<int>(my, 0, renderMgr->GetWindowDimensions().height, 0, renderMgr->GetFramebufferDimensions().height);
        }

        if (btn.MouseClickEvent(Point(mx, my)))
        {
            return true;
        }
    }

    return false;
}

void GameMenu::DrawMenuFullscreen()
{
    for (auto & btn : buttons)
    {
        btn.Draw();
    }

    // Background:
    if (backgroundSprite != nullptr)
    {
        const Vec2u fb = renderMgr->GetFramebufferDimensions();
        spriteBatch.DrawQuad(0, 0, fb.width, fb.height, Color4f(1.0f), backgroundSprite, true, nullptr, SpriteLayer::Layer0);
    }

    if (!spriteBatch.IsEmpty())
    {
        renderMgr->EnableSpriteBlendMode();
        renderMgr->DisablePolygonCull();
        renderMgr->DisableDepthTest();

        spriteBatch.FlushBatch(Basic2DTextRenderer::GetFullscreenOrthoMatrix());

        renderMgr->EnableDepthTest();
        renderMgr->EnablePolygonCull();
        renderMgr->DisableColorBlend();
    }
}

void GameMenu::DrawEarlySplashScreen()
{
    ConstTexturePtr splash = checked_pointer_cast<const Texture>(resourceMgr->FindOrLoadResource("sprites/splash.png"));

    // Immediate render:
    renderMgr->DisablePolygonCull();
    renderMgr->DisableDepthTest();
    {
        const Vec2u fb = renderMgr->GetFramebufferDimensions();
        spriteBatch.DrawQuad(0, 0, fb.width, fb.height, Color4f(1.0f), splash, true, nullptr, SpriteLayer::Layer0);
        spriteBatch.FlushBatch(Basic2DTextRenderer::GetFullscreenOrthoMatrix());
    }
    renderMgr->EnableDepthTest();
    renderMgr->EnablePolygonCull();

    // Force a full refresh of the screen to show it immediately.
    renderMgr->SwapBuffers();
}

} // namespace SpaceSim {}
