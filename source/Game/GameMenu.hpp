
#pragma once

// ================================================================================================
// -*- C++ -*-
// File: GameMenu.hpp
// Author: Guilherme R. Lampert
// Created on: 06/12/14
// Brief: Minimal menus for the off-game navigation.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

namespace SpaceSim
{

// ======================================================
// Helper types / inline functions:
// ======================================================

class GameMenu;
class Button;

using Point = Engine::Vec2i;
using Rect  = Engine::Vec4i;

inline bool RectContainsPoint(const Rect & r, const Point & p) noexcept
{
    if (p.x < r.x)
    {
        return false;
    }
    if (p.y < r.y)
    {
        return false;
    }
    if (p.x > (r.x + r.width))
    {
        return false;
    }
    if (p.y > (r.y + r.height))
    {
        return false;
    }
    return true;
}

inline bool RectIntersection(const Rect & a, const Rect & b) noexcept
{
    return ((a.y + a.height) > b.y) && (a.y < (b.y + b.height)) &&
           ((a.x + a.width)  > b.x) && (a.x < (b.x + b.width));
}

// ======================================================
// Button:
// ======================================================

class Button
{
  public:

    using Color = Engine::Color4f;

    // Button properties:
    Rect btnRect;
    Color btnColor;

    // Text label properties:
    std::string textLabel;
    Color textColor;
    Point textOffset;

    // Button click callback:
    // Should return if the event was handled by the callback or not.
    std::function<bool()> onClick;

    // Default construction is an invalid button.
    Button();

    // Add the button to the underlaying sprite batch.
    void Draw() const;

    // Returns true if the event was handled and should
    // not be forwarded to other UI elements any further.
    bool MouseClickEvent(const Point & where);

  private:

    // Weak reference to parent.
    friend GameMenu;
    GameMenu * parentMenu;
};

// ======================================================
// GameMenu:
// ======================================================

class GameMenu
{
  public:

    GameMenu();

    void AddButton(const Button & btn);
    bool OnAppEvent(const Engine::AppEvent & event);

    void DrawMenuFullscreen();
    void DrawEarlySplashScreen();

    Engine::SpriteBatchRenderer spriteBatch;
    Engine::ConstTexturePtr backgroundSprite;
    Engine::ConstTexturePtr buttonSprite;
    Engine::ConstFontPtr textFont;
    Engine::Basic2DTextRenderer textRenderer;
    Engine::MouseInputHandler mouseInput;
    std::vector<Button> buttons;

    DISABLE_COPY_AND_ASSIGN(GameMenu)
};

} // namespace SpaceSim {}
