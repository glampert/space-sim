
// ================================================================================================
// -*- C++ -*-
// File: DemoLevels.cpp
// Author: Guilherme R. Lampert
// Created on: 05/12/14
// Brief: Throw-away code for some hardcoded demo levels.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
#include "Game/SpaceCraft.hpp"
#include "Game/Doodad.hpp"
using namespace Engine;

namespace SpaceSim
{

// ======================================================
// RandomPointInSphere():
// ======================================================

Vector3 RandomPointInSphere(float x0, float y0, float z0, float radius)
{
    /*
	 * Returns a random point of a sphere, evenly distributed over the sphere.
	 * The sphere is centered at (x0,y0,z0) with the passed in radius.
	 * The returned point is returned as a three element vector [x,y,z].
	 */
    float u = game->randGen.NextFloat();
    float v = game->randGen.NextFloat();
    float theta = 2.0f * MathConst::PI * u;
    float phi = std::acos(2.0f * v - 1.0f);
    float x = x0 + (radius * std::sin(phi) * std::cos(theta));
    float y = y0 + (radius * std::sin(phi) * std::sin(theta));
    float z = z0 + (radius * std::cos(phi));
    return Vector3(x, y, z);
}

// ======================================================
// DemoLevel_Mars():
// ======================================================

void DemoLevel_Mars(GameWorld & world)
{
    renderMgr->SetLightPosition(Vector3(0.0, 50.0, -1.0));

    // Asteroid field at the origin of world:
    {
        for (int i = 0; i < 150; ++i)
        {
            GameEntityHandle hAsteroid = world.CreateEntity(Asteroid::Type);
            Asteroid * asteroid = checked_cast<Asteroid *>(world.GetEntityPointer(hAsteroid));

            Vec2f size;
            size[0] = game->randGen.NextFloat(0.1f, 5.0f);
            size[1] = game->randGen.NextFloat(0.1f, 5.0f);

            Vector3 pos = RandomPointInSphere(0.0f, 0.0f, 0.0f, 1000.0f);

            asteroid->InitRandomAsteroidWithSkin(pos, size, 5); // Skin number 5
        }
        for (int i = 0; i < 50; ++i)
        {
            GameEntityHandle hAsteroid = world.CreateEntity(Asteroid::Type);
            Asteroid * asteroid = checked_cast<Asteroid *>(world.GetEntityPointer(hAsteroid));

            Vec2f size;
            size[0] = game->randGen.NextFloat(0.1f, 2.0f);
            size[1] = game->randGen.NextFloat(0.1f, 2.0f);

            Vector3 pos = RandomPointInSphere(0.0f, 0.0f, 0.0f, 1000.0f);

            asteroid->InitRandomAsteroidWithSkin(pos, size, 4); // Skin number 4
        }
    }

    // A battleship:
    {
        GameEntityHandle hCraft = world.CreateEntity(SpaceCraft::Type);
        SpaceCraft * battleship = checked_cast<SpaceCraft *>(world.GetEntityPointer(hCraft));

        battleship->SetModel3D("ships/icarius_perfect.dae");
        battleship->SetName("Icarus-5");

        battleship->SetUniformScale(4.0f);
        battleship->SetupRigidBody(10000, Vector3(500.0f, 0.0f, 500.f));

        std::unique_ptr<StatsLabel> statsLabel(new StatsLabel());
        statsLabel->healthBarOffset = Vec2f(-35.0f, -95.0f);
        statsLabel->shieldBarOffset = Vec2f(-35.0f, -110.0f);
        statsLabel->textOffset = Vec2f(-25.0f, -55.0f);
        battleship->SetStatsLabel(std::move(statsLabel));

        battleship->SetHealth(1000);
        battleship->SetShield(1000);
        battleship->SetBooster(0);

        battleship->SetMaxHealth(1000);
        battleship->SetMaxShield(1000);
        battleship->SetMaxBooster(0);

        battleship->SetShieldRestoreRate(50);
        battleship->GetFlags() |= GameEntity::IsHostile;
    }
}

// ======================================================
// DemoLevel_GreenNebula():
// ======================================================

void DemoLevel_GreenNebula(GameWorld & world)
{
    renderMgr->SetLightPosition(Vector3(0.0, 0.0, 1.0));

    // Asteroid field at the origin of world:
    {
        for (int i = 0; i < 150; ++i)
        {
            GameEntityHandle hAsteroid = world.CreateEntity(Asteroid::Type);
            Asteroid * asteroid = checked_cast<Asteroid *>(world.GetEntityPointer(hAsteroid));

            Vec2f size;
            size[0] = game->randGen.NextFloat(0.1f, 5.0f);
            size[1] = game->randGen.NextFloat(0.1f, 5.0f);

            Vector3 pos = RandomPointInSphere(0.0f, 0.0f, 0.0f, 1000.0f);

            asteroid->InitRandomAsteroidWithSkin(pos, size, 10); // Skin number 10
        }
        for (int i = 0; i < 50; ++i)
        {
            GameEntityHandle hAsteroid = world.CreateEntity(Asteroid::Type);
            Asteroid * asteroid = checked_cast<Asteroid *>(world.GetEntityPointer(hAsteroid));

            Vec2f size;
            size[0] = game->randGen.NextFloat(0.1f, 2.0f);
            size[1] = game->randGen.NextFloat(0.1f, 2.0f);

            Vector3 pos = RandomPointInSphere(0.0f, 0.0f, 0.0f, 1000.0f);

            asteroid->InitRandomAsteroidWithSkin(pos, size, 3); // Skin number 3
        }
        for (int i = 0; i < 4; ++i)
        {
            GameEntityHandle hAsteroid = world.CreateEntity(Asteroid::Type);
            Asteroid * asteroid = checked_cast<Asteroid *>(world.GetEntityPointer(hAsteroid));

            Vec2f size;
            size[0] = game->randGen.NextFloat(10.0f, 20.0f);
            size[1] = game->randGen.NextFloat(10.0f, 20.0f);

            Vector3 pos = RandomPointInSphere(0.0f, 0.0f, 0.0f, 1000.0f);

            asteroid->InitRandomAsteroidWithSkin(pos, size, 9); // Skin number 9
        }
    }

    // A space-station:
    {
        GameEntityHandle hCraft = world.CreateEntity(SpaceCraft::Type);
        SpaceCraft * station = checked_cast<SpaceCraft *>(world.GetEntityPointer(hCraft));

        station->SetModel3D("stations/rogue_station.dae");
        station->SetName("Unknown Rogue Station");

        station->SetUniformScale(4.0f);
        station->SetupRigidBody(100000, Vector3(1100.0f, 0.0f, 500.f));

        std::unique_ptr<StatsLabel> statsLabel(new StatsLabel());
        statsLabel->healthBarOffset = Vec2f(-35.0f, -95.0f);
        statsLabel->shieldBarOffset = Vec2f(-35.0f, -110.0f);
        statsLabel->textOffset = Vec2f(-25.0f, -55.0f);
        station->SetStatsLabel(std::move(statsLabel));

        station->SetHealth(1000);
        station->SetShield(1000);
        station->SetBooster(0);

        station->SetMaxHealth(1000);
        station->SetMaxShield(1000);
        station->SetMaxBooster(0);

        station->SetShieldRestoreRate(50);
        station->GetFlags() |= GameEntity::IsHostile;
    }
}

// ======================================================
// DemoLevel_Jupiter():
// ======================================================

void DemoLevel_Jupiter(GameWorld & world)
{
    renderMgr->SetLightPosition(Vector3(0.0, 100.0, -1.0));

    // A space-station:
    {
        GameEntityHandle hCraft = world.CreateEntity(SpaceCraft::Type);
        SpaceCraft * station = checked_cast<SpaceCraft *>(world.GetEntityPointer(hCraft));

        station->SetModel3D("stations/nawajima_orbital_shipyard.dae");
        station->SetName("Nawajima Orbital Shipyard");

        station->SetUniformScale(4.0f);
        station->SetupRigidBody(100000, Vector3(1100.0f, 0.0f, 500.f));

        std::unique_ptr<StatsLabel> statsLabel(new StatsLabel());
        statsLabel->healthBarOffset = Vec2f(-35.0f, -95.0f);
        statsLabel->shieldBarOffset = Vec2f(-35.0f, -110.0f);
        statsLabel->textOffset = Vec2f(-25.0f, -55.0f);
        station->SetStatsLabel(std::move(statsLabel));

        station->SetHealth(1000);
        station->SetShield(1000);
        station->SetBooster(0);

        station->SetMaxHealth(1000);
        station->SetMaxShield(1000);
        station->SetMaxBooster(0);

        station->SetShieldRestoreRate(50);
        station->GetFlags() |= GameEntity::IsFriendly;
    }

    // Small satellite nearby:
    {
        GameEntityHandle hCraft = world.CreateEntity(SpaceCraft::Type);
        SpaceCraft * station = checked_cast<SpaceCraft *>(world.GetEntityPointer(hCraft));

        station->SetModel3D("stations/newgensp3.dae");
        station->SetName("Jupiter comms network - unit 7");

        station->SetUniformScale(1.0f);
        station->SetupRigidBody(10000, Vector3(100.0f, -10.0f, 200.f));

        std::unique_ptr<StatsLabel> statsLabel(new StatsLabel());
        statsLabel->healthBarOffset = Vec2f(-35.0f, -95.0f);
        statsLabel->shieldBarOffset = Vec2f(-35.0f, -110.0f);
        statsLabel->textOffset = Vec2f(-25.0f, -55.0f);
        station->SetStatsLabel(std::move(statsLabel));

        station->SetHealth(1000);
        station->SetShield(1000);
        station->SetBooster(0);

        station->SetMaxHealth(1000);
        station->SetMaxShield(1000);
        station->SetMaxBooster(0);

        station->SetShieldRestoreRate(50);
        station->GetFlags() |= GameEntity::IsFriendly;
    }

    // Sprinkled with a few asteroids:
    for (int i = 0; i < 50; ++i)
    {
        GameEntityHandle hAsteroid = world.CreateEntity(Asteroid::Type);
        Asteroid * asteroid = checked_cast<Asteroid *>(world.GetEntityPointer(hAsteroid));

        Vec2f size;
        size[0] = game->randGen.NextFloat(0.1f, 2.0f);
        size[1] = game->randGen.NextFloat(0.1f, 2.0f);

        Vector3 pos = RandomPointInSphere(0.0f, 0.0f, 0.0f, 2000.0f);

        asteroid->InitRandomAsteroidWithSkin(pos, size, 8); // Skin number 8
    }
}

} // namespace SpaceSim {}
