
// ================================================================================================
// -*- C++ -*-
// File: FlightCamera.cpp
// Author: Guilherme R. Lampert
// Created on: 31/08/14
// Brief: In-game flight camera.
//
// License:
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
// ================================================================================================

#include "Engine/Core/Engine.hpp"
#include "Game/FlightCamera.hpp"
#include "Game/GamePhysics.hpp"
#include "Game/GameEntity.hpp"
#include "Game/GameWorld.hpp"
#include "Game/SpaceSimGame.hpp"
#include "Game/LocalPlayerView.hpp"
using namespace Engine;

namespace SpaceSim
{

// ======================================================
// Local constants and helpers:
// ======================================================

namespace /* unnamed */
{

// Script with the camera parameters:
static const char cameraConfigScript[] = "resources/scripts/camera_config.lua";

// Initial up-vector (unit Y):
static const Vector3 InitialUpVector = Vector3(0.0f, 1.0f, 0.0f);

// Clamp angle (in radians) to bring it in range of -360(-2pi) to +360(+2pi), if required.
static float ClampAngle2PI(float radians)
{
    if (radians > MathConst::TwoPI)
    {
        radians -= MathConst::TwoPI;
    }
    else if (radians < -MathConst::TwoPI)
    {
        radians += MathConst::TwoPI;
    }
    return radians;
}

} // namespace unnamed {}

// ======================================================
// FlightCamera:
// ======================================================

FlightCamera::FlightCamera(LocalPlayerView * playerView)
    : myView(playerView)
    , viewport(renderMgr->GetFramebufferRect())
    , nearClip(0.1f)
    , farClip(1000.0f)
    , aspect(static_cast<float>(viewport.width) / static_cast<float>(viewport.height))
    , fovyRadians(FOVyForProjection(static_cast<float>(viewport.height), farClip))
    , boosterSpeedScale(3.0f)
    , forwardSpeed(0.0f)
    , maxForwardSpeed(0.2f)
    , acceleration(0.1f)
    , movementSpeed(0.5f)
    , rollSpeed(0.25f)
    , turnSpeed(0.05f)
    , yawAngleRadians(0.0f)
    , pitchAngleRadians(0.0f)
    , rollAngleRadians(0.0f)
    , maxPitchRate(0.5f)
    , maxYawRate(0.5f)
    , movementDamping(0.5f)
    , lookDamping(0.5f)
    , accumulatedRollRadians(0.0f)
    , maxAccumulatedRoll(0.0f)
    , rollResetRate(0.1f)
    , resetingCameraRoll(false)
    , savedUpVector(InitialUpVector)
    , lookAtPos(0.0f, 0.0f, -1.0f)
    , worldPosition(0.0f, 0.0f, 0.0f)
    , worldPositionDelta(0.0f, 0.0f, 0.0f)
    , directionVector(0.0f, 0.0f, 0.0f)
    , upVector(InitialUpVector)
    , pitchAxis(1.0f, 0.0f, 0.0f)
    , viewMatrix(Matrix4::identity())
    , projectionMatrix(Matrix4::identity())
{
    // User can easily tune the camera using the provided Set*() methods.
    assert(myView != nullptr);
}

void FlightCamera::Update(const double deltaSeconds)
{
    // Determine axis for pitch rotation and look direction:
    directionVector = normalize(lookAtPos - worldPosition);
    pitchAxis = normalize(cross(directionVector, upVector));

    // Prepare the rotations:
    const Quat yawQuat = QuatFromAngleAxis(yawAngleRadians, upVector);
    const Quat pitchQuat = QuatFromAngleAxis(pitchAngleRadians, pitchAxis);
    Quat rotationQuat;

    // Code for repositioning the ship after several rotations,
    // triggered by hitting the space bar.
    if (!resetingCameraRoll)
    {
        const Quat rollQuat = QuatFromAngleAxis(rollAngleRadians, directionVector);
        rotationQuat = normalize(yawQuat * pitchQuat * rollQuat);
    }
    else
    {
        float lerpVal = ValueNormalize(accumulatedRollRadians, 0.0f, maxAccumulatedRoll);
        accumulatedRollRadians -= (rollResetRate * deltaSeconds);

        // When we reach zero or less (upVector == InitialUpVector), stop.
        if (lerpVal <= 0.0f)
        {
            resetingCameraRoll = false;
            accumulatedRollRadians = 0.0f;
            maxAccumulatedRoll = 0.0f;
            lerpVal = 0.0f;
        }

        // Interpolate upVector towards the initial up:
        upVector = normalize(lerp(lerpVal, InitialUpVector, savedUpVector));

        // Compute other rotations, without the roll:
        rotationQuat = normalize(yawQuat * pitchQuat);
    }

    // Update direction and up-vec from the quaternion.
    // It is always important to update both as a pair to avoid
    // a lock caused by the alignment of directionVector and upVector,
    // which would occur if the up-vec is not rotated along with the view basis.
    directionVector = normalize(rotate(rotationQuat, directionVector));
    upVector = normalize(rotate(rotationQuat, upVector));

    // Update delta and lookAt:
    worldPosition += worldPositionDelta;
    lookAtPos = worldPosition + directionVector;

    // Damping for smoother camera rotation:
    const float damping = (lookDamping * deltaSeconds);
    yawAngleRadians *= damping;
    pitchAngleRadians *= damping;
    rollAngleRadians *= damping;

    // And movement:
    worldPositionDelta *= (movementDamping * deltaSeconds);

    // Update camera matrices:
    viewMatrix = Matrix4::lookAt(Point3(worldPosition), Point3(lookAtPos), upVector);
    projectionMatrix = Matrix4::perspective(fovyRadians, aspect, nearClip, farClip);

    // And frustum:
    frustum.ComputeClippingPlanes(viewMatrix, projectionMatrix);
}

void FlightCamera::Move(KeyboardInputHandler & keyboardInput, const double deltaSeconds)
{
    /* Move like in a first-person game (Useful for debugging):
	if (keyboardInput.IsKeyDown(Key::W))
	{
		worldPositionDelta += directionVector * (movementSpeed * deltaSeconds);
	}
	if (keyboardInput.IsKeyDown(Key::S))
	{
		worldPositionDelta -= directionVector * (movementSpeed * deltaSeconds);
	}
	*/

    // Move like a space ship, with a constant acceleration:
    if (keyboardInput.IsKeyDown(Key::W))
    {
        forwardSpeed += (acceleration * deltaSeconds);
    }
    if (keyboardInput.IsKeyDown(Key::S))
    {
        forwardSpeed -= (acceleration * deltaSeconds);
    }

    forwardSpeed = Clamp(forwardSpeed, -maxForwardSpeed, +maxForwardSpeed);

    // Booster/afterburner, only while [SHIFT] is down:
    {
        PlayerEntity & player = myView->GetPlayerEntity();
        double booster = player.GetBooster();

        constexpr double boostUseRate = 0.1;
        constexpr double boostRestoreRate = 0.7;

        if (keyboardInput.IsKeyDown(Key::LeftShift))
        {
            if (booster > 0.0)
            {
                forwardSpeed *= boosterSpeedScale;
                booster -= (deltaSeconds * boostUseRate);
            }
        }
        else
        {
            if (booster < player.GetMaxBooster())
            {
                booster += (deltaSeconds * boostRestoreRate);
                if (booster > player.GetMaxBooster())
                {
                    booster = player.GetMaxBooster();
                }
                booster = std::ceil(booster); // For the double -> int conversion
            }
        }

        player.SetBooster(booster);
    }

    worldPositionDelta = directionVector * forwardSpeed; // W (forward)
    worldPositionDelta = directionVector * forwardSpeed; // S (back)

    // Move left/right:
    if (keyboardInput.IsKeyDown(Key::A))
    {
        worldPositionDelta -= pitchAxis * (movementSpeed * deltaSeconds);
    }
    if (keyboardInput.IsKeyDown(Key::D))
    {
        worldPositionDelta += pitchAxis * (movementSpeed * deltaSeconds);
    }

    // Barrel-roll to the left/right:
    if (keyboardInput.IsKeyDown(Key::Q))
    {
        const float s = (rollSpeed * deltaSeconds);
        rollAngleRadians -= s;
        accumulatedRollRadians -= s;
        accumulatedRollRadians = ClampAngle2PI(accumulatedRollRadians);
    }
    if (keyboardInput.IsKeyDown(Key::E))
    {
        const float s = (rollSpeed * deltaSeconds);
        rollAngleRadians += s;
        accumulatedRollRadians += s;
        accumulatedRollRadians = ClampAngle2PI(accumulatedRollRadians);
    }

    // Reset camera Z-rotation (roll):
    if (keyboardInput.IsKeyDown(Key::Space) && !resetingCameraRoll && !FloatIsZero(accumulatedRollRadians))
    {
        accumulatedRollRadians = std::fabs(accumulatedRollRadians);
        maxAccumulatedRoll = accumulatedRollRadians;
        savedUpVector = upVector;
        resetingCameraRoll = true;
    }
}

void FlightCamera::Move(XB360Controller & xbController, const double deltaSeconds)
{
    // Move like a space ship, with a constant acceleration:
    if (xbController.IsButtonDown(XB360Controller::Button::ArrowUp))
    {
        forwardSpeed += (acceleration * deltaSeconds);
    }
    if (xbController.IsButtonDown(XB360Controller::Button::ArrowDown))
    {
        forwardSpeed -= (acceleration * deltaSeconds);
    }

    forwardSpeed = Clamp(forwardSpeed, -maxForwardSpeed, +maxForwardSpeed);

    // Booster/afterburner, only while [SHIFT] is down:
    {
        PlayerEntity & player = myView->GetPlayerEntity();
        double booster = player.GetBooster();

        constexpr double boostUseRate = 0.1;
        constexpr double boostRestoreRate = 0.7;

        if (xbController.IsButtonDown(XB360Controller::Button::LButton))
        {
            if (booster > 0.0)
            {
                forwardSpeed *= boosterSpeedScale;
                booster -= (deltaSeconds * boostUseRate);
            }
        }
        else
        {
            if (booster < player.GetMaxBooster())
            {
                booster += (deltaSeconds * boostRestoreRate);
                if (booster > player.GetMaxBooster())
                {
                    booster = player.GetMaxBooster();
                }
                booster = std::ceil(booster); // For the double -> int conversion
            }
        }

        player.SetBooster(booster);
    }

    worldPositionDelta = directionVector * forwardSpeed; // W (forward)
    worldPositionDelta = directionVector * forwardSpeed; // S (back)

    // Move left/right:
    if (xbController.IsButtonDown(XB360Controller::Button::ArrowLeft))
    {
        worldPositionDelta -= pitchAxis * (movementSpeed * deltaSeconds);
    }
    if (xbController.IsButtonDown(XB360Controller::Button::ArrowRight))
    {
        worldPositionDelta += pitchAxis * (movementSpeed * deltaSeconds);
    }

    // Barrel-roll to the left/right:
    if (xbController.GetAxisState(XB360Controller::X_Left) >= 0.8f)
    {
        const float s = (rollSpeed * deltaSeconds);
        rollAngleRadians += s;
        accumulatedRollRadians += s;
        accumulatedRollRadians = ClampAngle2PI(accumulatedRollRadians);
    }
    if (xbController.GetAxisState(XB360Controller::X_Left) <= -0.8f)
    {
        const float s = (rollSpeed * deltaSeconds);
        rollAngleRadians -= s;
        accumulatedRollRadians -= s;
        accumulatedRollRadians = ClampAngle2PI(accumulatedRollRadians);
    }

    // Reset camera Z-rotation (roll):
    if (xbController.IsButtonDown(XB360Controller::Button::Y) && !resetingCameraRoll && !FloatIsZero(accumulatedRollRadians))
    {
        accumulatedRollRadians = std::fabs(accumulatedRollRadians);
        maxAccumulatedRoll = accumulatedRollRadians;
        savedUpVector = upVector;
        resetingCameraRoll = true;
    }
}

void FlightCamera::Look(MouseInputHandler & mouseInput, const double deltaSeconds)
{
    const Vec2f mousePosition = mouseInput.GetCursorPosition();
    const int middleX = (viewport.width / 2);
    const int middleY = (viewport.height / 2);

    // If our cursor is still in the middle, we never moved.
    if ((static_cast<int>(mousePosition.x) == middleX) &&
        (static_cast<int>(mousePosition.y) == middleY))
    {
        // No need to update.
        return;
    }

    // Set the mouse position to the middle of the window.
    // This is necessary to avoid reading huge deltas on the next step.
    mouseInput.SetCursorPosition(Vec2f(middleX, middleY));

    // Get the direction deltas the mouse moved in:
    const float deltaX = (middleX - mousePosition.x);
    const float deltaY = (middleY - mousePosition.y);

    // Update the rotations:
    ChangeYaw(deltaX * turnSpeed * deltaSeconds);
    ChangePitch(deltaY * turnSpeed * deltaSeconds);
}

void FlightCamera::Look(XB360Controller & xbController, const double deltaSeconds)
{
    // We scale the XBox Controller's axis deltas to move a little
    // bit faster, since they are normalized in the -1,+1 range.
    // This value was selected via manual testing with the controller.
    constexpr float XbDeltaScale = 6.0f;

    float deltaX = xbController.GetAxisState(XB360Controller::X_Right);
    float deltaY = xbController.GetAxisState(XB360Controller::Y_Right);

    // X-axis is inverted for our camera setup!
    deltaX = -deltaX;

    // Very rudimentary clamping but avoids the camera
    // from keeping moving due to noise in the system.
    if (std::fabs(deltaX) < 0.5f)
    {
        deltaX = 0.0f;
    }
    if (std::fabs(deltaY) < 0.5f)
    {
        deltaY = 0.0f;
    }

    ChangeYaw(deltaX * XbDeltaScale * turnSpeed * deltaSeconds);
    ChangePitch(deltaY * XbDeltaScale * turnSpeed * deltaSeconds);
}

void FlightCamera::ChangePitch(float radians)
{
    // Check bounds with the max pitch rate so that we aren't moving too fast:
    if (radians < -maxPitchRate)
    {
        radians = -maxPitchRate;
    }
    else if (radians > maxPitchRate)
    {
        radians = maxPitchRate;
    }

    pitchAngleRadians -= radians;

    // Check bounds for the camera pitch:
    pitchAngleRadians = ClampAngle2PI(pitchAngleRadians);
}

void FlightCamera::ChangeYaw(float radians)
{
    // HACK: For reasons not clear to me, the very first time
    // the camera is updated, after a reset/startup, we receive
    // and anomalous value for 'radians' that makes the camera
    // jerk slightly as if the mouse was abruptly pulled.
    // This rudimentary test here seems to work fine. So I'll
    // leave it like this for now...
    if (std::fabs(radians) > (maxYawRate * maxYawRate))
    {
        return;
    }

    // Check bounds with the max yaw rate so that we aren't moving too fast:
    if (radians < -maxYawRate)
    {
        radians = -maxYawRate;
    }
    else if (radians > maxYawRate)
    {
        radians = maxYawRate;
    }

    // This controls how the yaw is changed if the camera is pointed straight
    // up or down. The yaw/heading delta direction changes (+-90 / +-270 degrees).
    if (((pitchAngleRadians > MathConst::HalfPI) && (pitchAngleRadians < (3.0f * MathConst::HalfPI))) ||
        ((pitchAngleRadians < -MathConst::HalfPI) && (pitchAngleRadians > -(3.0f * MathConst::HalfPI))))
    {
        yawAngleRadians -= radians;
    }
    else
    {
        yawAngleRadians += radians;
    }

    // Check bounds for the camera yaw:
    yawAngleRadians = ClampAngle2PI(yawAngleRadians);
}

Quat FlightCamera::GetOrientation() const
{
    /*
	---- Look-At RH: ----
	 zaxis = normal(eye - at)
	 xaxis = normal(cross(up, zaxis))
	 yaxis = cross(zaxis, xaxis)

	 xaxis.x           yaxis.x           zaxis.x          0
	 xaxis.y           yaxis.y           zaxis.y          0
	 xaxis.z           yaxis.z           zaxis.z          0
	 dot(xaxis, eye)   dot(yaxis, eye)   dot(zaxis, eye)  1

	---- Look-At LH: ----
	 zaxis = normal(at - eye)
	 xaxis = normal(cross(up, zaxis))
	 yaxis = cross(zaxis, xaxis)

	 xaxis.x           yaxis.x           zaxis.x          0
	 xaxis.y           yaxis.y           zaxis.y          0
	 xaxis.z           yaxis.z           zaxis.z          0
	-dot(xaxis, eye)  -dot(yaxis, eye)  -dot(zaxis, eye)  1
	*/

    // Right handed:
    const Vector3 zAxis = normalize(worldPosition - lookAtPos);
    const Vector3 xAxis = normalize(cross(upVector, zAxis));
    const Vector3 yAxis = normalize(cross(zAxis, xAxis));
    const Matrix3 m(xAxis, yAxis, zAxis); // Build a rotation matrix.
    return Quat(m);                       // And turn it into a quaternion.
}

void FlightCamera::SetWorldPosition(const Vector3 pos)
{
    worldPosition = pos;
    worldPositionDelta = Vector3(0.0f, 0.0f, 0.0f);
}

void FlightCamera::SetViewport(const Vec4u vp)
{
    viewport = vp;
    aspect = static_cast<float>(viewport.width) / static_cast<float>(viewport.height);
}

void FlightCamera::SetClipping(const float nearClipDistance, const float farClipDistance)
{
    nearClip = nearClipDistance;
    farClip = farClipDistance;
}

void FlightCamera::RunConfigScripts()
{
    if (!scriptMgr->RunConfigFile(cameraConfigScript, "cameraConfig"))
    {
        common->ErrorF("Unable to update camera from script!");
        return;
    }

    // Variables that are not overwritten by the script will keep their current values.
    scriptMgr->GetConfigValue("cameraConfig.viewportX", viewport.x, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.viewportY", viewport.y, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.viewportWidth", viewport.width, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.viewportHeight", viewport.height, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.fovyRadians", fovyRadians, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.nearClip", nearClip, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.farClip", farClip, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.boosterSpeedScale", boosterSpeedScale, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.maxForwardSpeed", maxForwardSpeed, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.acceleration", acceleration, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.movementSpeed", movementSpeed, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.turnSpeed", turnSpeed, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.rollSpeed", rollSpeed, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.maxPitchRate", maxPitchRate, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.maxYawRate", maxYawRate, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.movementDamping", movementDamping, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.lookDamping", lookDamping, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.rollResetRate", rollResetRate, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.worldPosition", worldPosition, /* optionalVar = */ true);
    scriptMgr->GetConfigValue("cameraConfig.lookAtPos", lookAtPos, /* optionalVar = */ true);

    // Validate:
    if ((viewport.width  == 0) ||
        (viewport.height == 0) ||
        (viewport.width  > renderMgr->GetFramebufferDimensions().width) ||
        (viewport.height > renderMgr->GetFramebufferDimensions().height))
    {
        common->FatalErrorF("%s: Invalid viewport width or height!", cameraConfigScript);
    }

    if ((viewport.width > renderMgr->GetFramebufferDimensions().width) ||
        (viewport.height > renderMgr->GetFramebufferDimensions().height))
    {
        common->FatalErrorF("%s: Invalid viewport x or y!", cameraConfigScript);
    }

    if (FloatIsZero(nearClip))
    {
        common->FatalErrorF("%s: nearClip must not be zero!", cameraConfigScript);
    }

    if (FloatIsZero(farClip))
    {
        common->FatalErrorF("%s: farClip must not be zero!", cameraConfigScript);
    }

    if (FloatIsZero(fovyRadians))
    {
        common->FatalErrorF("%s: fovyRadians must not be zero!", cameraConfigScript);
    }

    // Update dependent values:
    aspect = static_cast<float>(viewport.width) / static_cast<float>(viewport.height);

    // Reset dynamic parameters:
    forwardSpeed = 0.0f;
    yawAngleRadians = 0.0f;
    pitchAngleRadians = 0.0f;
    rollAngleRadians = 0.0f;
    accumulatedRollRadians = 0.0f;
    maxAccumulatedRoll = 0.0f;
    resetingCameraRoll = false;
    worldPositionDelta = Vector3(0.0f, 0.0f, 0.0f);
    directionVector = Vector3(0.0f, 0.0f, 0.0f);
    savedUpVector = InitialUpVector;
    upVector = InitialUpVector;
    pitchAxis = Vector3(1.0f, 0.0f, 0.0f);
    viewMatrix = Matrix4::identity();
    projectionMatrix = Matrix4::identity();
    frustum.clipMatrix = Matrix4::identity();

    common->PrintF("In-game camera updated from script \"%s\"...", cameraConfigScript);
}

void FlightCamera::PrintSelf() const
{
    common->PrintF("-------- Camera -------");
    common->PrintF("Viewport..............: %s", ToString(viewport).c_str());
    common->PrintF("Near clip.............: %s", ToString(nearClip).c_str());
    common->PrintF("Far clip..............: %s", ToString(farClip).c_str());
    common->PrintF("Aspect................: %s", ToString(aspect).c_str());
    common->PrintF("FOV-Y.................: %s (%.2f degrees)", ToString(fovyRadians).c_str(), RadToDeg(fovyRadians));
    common->PrintF("Move speed............: %s", ToString(movementSpeed).c_str());
    common->PrintF("Roll speed............: %s", ToString(rollSpeed).c_str());
    common->PrintF("Turn speed............: %s", ToString(turnSpeed).c_str());
    common->PrintF("Yaw angle.............: %s", ToString(yawAngleRadians).c_str());
    common->PrintF("Pitch angle...........: %s", ToString(pitchAngleRadians).c_str());
    common->PrintF("Roll angle............: %s", ToString(rollAngleRadians).c_str());
    common->PrintF("Max pitch rate........: %s", ToString(maxPitchRate).c_str());
    common->PrintF("Max yaw rate..........: %s", ToString(maxYawRate).c_str());
    common->PrintF("Move damping..........: %s", ToString(movementDamping).c_str());
    common->PrintF("Look damping..........: %s", ToString(lookDamping).c_str());
    common->PrintF("Accumulated roll......: %s", ToString(accumulatedRollRadians).c_str());
    common->PrintF("Max accumulated roll..: %s", ToString(maxAccumulatedRoll).c_str());
    common->PrintF("Roll reset rate.......: %s", ToString(rollResetRate).c_str());
    common->PrintF("Reseting camera roll..: %s", (resetingCameraRoll ? "true" : "false"));
    common->PrintF("Saved up-vector.......: %s", ToString(savedUpVector).c_str());
    common->PrintF("Look-at pos...........: %s", ToString(lookAtPos).c_str());
    common->PrintF("World position........: %s", ToString(worldPosition).c_str());
    common->PrintF("World position delta..: %s", ToString(worldPositionDelta).c_str());
    common->PrintF("Direction vector......: %s", ToString(directionVector).c_str());
    common->PrintF("Up-vector.............: %s", ToString(upVector).c_str());
    common->PrintF("Pitch axis............: %s", ToString(pitchAxis).c_str());
}

} // namespace SpaceSim {}
